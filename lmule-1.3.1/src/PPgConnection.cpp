// PPgConnection.cpp : implementation file
//

//#include "stdafx.h"
#include "wintypes.h"
#include "emule.h"
#include <wx/msgdlg.h>
#include "muuli_wdr.h"
#include "PPgConnection.h"
#include "Wizard.h"


#include <wx/checkbox.h>
#include <wx/notebook.h>
#include <wx/slider.h>
#include "wx/xrc/xmlres.h"
#include "wx/xrc/xh_all.h"

// CPPgConnection dialog
#define GetDlgItem(x,clas) XRCCTRL(*this,#x,clas)
#define IsDlgButtonChecked(x) XRCCTRL(*this,#x,wxCheckBox)->GetValue()
#define CheckDlgButton(x,y) XRCCTRL(*this,#x,wxCheckBox)->SetValue(y)

//IMPLEMENT_DYNAMIC(CPPgConnection, CPropertyPage)
IMPLEMENT_DYNAMIC_CLASS(CPPgConnection,wxPanel)

CPPgConnection::CPPgConnection(wxWindow*parent)
  :wxPanel(parent,CPPgConnection::IDD) //: CPropertyPage(CPPgConnection::IDD)
{
  wxNotebook* book=(wxNotebook*)parent;

  wxPanel*page1=wxXmlResource::Get()->LoadPanel(this,"DLG_PPG_CONNECTION");
  book->AddPage(this,_("Connection"));

  // looks stupid? it is :)
  SetSize(page1->GetSize().GetWidth(),page1->GetSize().GetHeight()+48);
  page1->SetSize(GetSize());

  Localize();
}

CPPgConnection::~CPPgConnection()
{
}

#if 0
void CPPgConnection::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
}
#endif

BEGIN_EVENT_TABLE(CPPgConnection,wxPanel)
  EVT_BUTTON(XRCID("IDC_WIZARD"),CPPgConnection::OnBnClickedWizard)
END_EVENT_TABLE()

#if 0
BEGIN_MESSAGE_MAP(CPPgConnection, CPropertyPage)

	ON_EN_CHANGE(IDC_DOWNLOAD_CAP, OnEnChangeDownloadCap)
	ON_EN_CHANGE(IDC_UPLOAD_CAP, OnEnChangeUploadCap)
	ON_EN_CHANGE(IDC_MAXDOWN, OnEnChangeMaxdown)
	ON_EN_CHANGE(IDC_MAXUP, OnEnChangeMaxup)
	ON_EN_CHANGE(IDC_PORT, OnEnChangePort)
	ON_EN_CHANGE(IDC_MAXCON, OnEnChangeMaxcon)
	ON_EN_CHANGE(IDC_MAXSOURCEPERFILE, OnEnChangeMaxsourceperfile)
	ON_BN_CLICKED(IDC_AUTOCONNECT, OnBnClickedAutoconnect)
	ON_BN_CLICKED(IDC_RECONN, OnBnClickedReconn)
	ON_EN_CHANGE(IDC_MAXSOURCEPERFILESOFT, OnEnChangeMaxsourceperfilesoft)
	ON_EN_CHANGE(IDC_MAXSOURCEPERFILEUDP, OnEnChangeMaxsourceperfileudp)
END_MESSAGE_MAP()
#endif


// CPPgConnection message handlers


#if 0
BOOL CPPgConnection::OnInitDialog()
{
	CPropertyPage::OnInitDialog();

	LoadSettings();
	Localize();

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}
#endif

#include <wx/checkbox.h>

void CPPgConnection::LoadSettings(void)
{
  if(1)
    {
      if( app_prefs->prefs->maxupload != 0 ){
	if( app_prefs->prefs->maxupload < 4 && ( app_prefs->prefs->maxupload*3 < app_prefs->prefs->maxdownload ) )
	  app_prefs->prefs->maxdownload = app_prefs->prefs->maxupload*3;
	
	if( app_prefs->prefs->maxupload < 10 && ( app_prefs->prefs->maxupload*4 < app_prefs->prefs->maxdownload ) )
	  app_prefs->prefs->maxdownload = app_prefs->prefs->maxupload*4;
      }
      CString strBuffer;
      
      strBuffer.Format("%d", app_prefs->prefs->udpport);
      GetDlgItem(IDC_UDPPORT,wxTextCtrl)->SetValue(strBuffer);
      CheckDlgButton(IDC_UDPDISABLE,(app_prefs->prefs->udpport==0));
      
      GetDlgItem(IDC_UDPPORT,wxControl)->Enable(app_prefs->prefs->udpport>0);
      
      strBuffer.Format("%d", app_prefs->prefs->maxGraphDownloadRate);
      GetDlgItem(IDC_DOWNLOAD_CAP,wxTextCtrl)->SetValue(strBuffer);
      
      strBuffer.Format("%d", app_prefs->prefs->maxGraphUploadRate);
      GetDlgItem(IDC_UPLOAD_CAP,wxTextCtrl)->SetValue(strBuffer);
      
      if(app_prefs->prefs->maxdownload == 0xFFFF)
	GetDlgItem(IDC_MAXDOWN,wxTextCtrl)->SetValue("0");
      else
	{
	  strBuffer.Format("%d", app_prefs->prefs->maxdownload);
	  GetDlgItem(IDC_MAXDOWN,wxTextCtrl)->SetValue(strBuffer);
	}
      if(app_prefs->prefs->maxupload == 0xFFFF)
	GetDlgItem(IDC_MAXUP,wxTextCtrl)->SetValue("0");
      else
	{
	  strBuffer.Format("%d", app_prefs->prefs->maxupload);
	  GetDlgItem(IDC_MAXUP,wxTextCtrl)->SetValue(strBuffer);
	}
      
      strBuffer.Format("%d", app_prefs->prefs->port);
      GetDlgItem(IDC_PORT,wxTextCtrl)->SetValue(strBuffer);
      
      strBuffer.Format("%d", app_prefs->prefs->maxconnections);
      GetDlgItem(IDC_MAXCON,wxTextCtrl)->SetValue(strBuffer);
      
      if(app_prefs->prefs->maxsourceperfile == 0xFFFF)
	GetDlgItem(IDC_MAXSOURCEPERFILE,wxTextCtrl)->SetValue("0");
      else
	{
	  strBuffer.Format("%d", app_prefs->prefs->maxsourceperfile);
	  GetDlgItem(IDC_MAXSOURCEPERFILE,wxTextCtrl)->SetValue(strBuffer);
	}
      
      if (app_prefs->prefs->reconnect)
	CheckDlgButton(IDC_RECONN,1);
      else
	CheckDlgButton(IDC_RECONN,0);
      
      if (app_prefs->prefs->m_bshowoverhead)
	CheckDlgButton(IDC_SHOWOVERHEAD,1);
      else
	CheckDlgButton(IDC_SHOWOVERHEAD,0);
      
      if (app_prefs->prefs->autoconnect)
	CheckDlgButton(IDC_AUTOCONNECT,1);
      else
	CheckDlgButton(IDC_AUTOCONNECT,0);
    }
}

void CPPgConnection::OnApply()
{
  wxString buffer;
  int lastmaxgu = app_prefs->prefs->maxGraphUploadRate;
  int lastmaxgd = app_prefs->prefs->maxGraphDownloadRate;
  
  if(GetDlgItem(IDC_DOWNLOAD_CAP,wxTextCtrl)->GetValue().Length())
    { 
      buffer=GetDlgItem(IDC_DOWNLOAD_CAP,wxTextCtrl)->GetValue();
      app_prefs->prefs->maxGraphDownloadRate = (atoi(buffer)) ? atoi(buffer) : 100;
    }
  if(GetDlgItem(IDC_UPLOAD_CAP,wxTextCtrl)->GetValue().Length())
    {
      buffer=GetDlgItem(IDC_UPLOAD_CAP,wxTextCtrl)->GetValue();
      app_prefs->prefs->maxGraphUploadRate = (atoi(buffer)) ? atoi(buffer) : 25;
    }
  
  if(GetDlgItem(IDC_MAXUP,wxTextCtrl)->GetValue().Length())
    {
      buffer=GetDlgItem(IDC_MAXUP,wxTextCtrl)->GetValue();
      app_prefs->prefs->maxupload = (atoi(buffer)) ? atoi(buffer) : 0xFFFF;
    }
  if(GetDlgItem(IDC_MAXDOWN,wxTextCtrl)->GetValue().Length()) 
    {
      buffer=GetDlgItem(IDC_MAXDOWN,wxTextCtrl)->GetValue();
      app_prefs->prefs->maxdownload = (atoi(buffer)) ? atoi(buffer) : 0xFFFF;
    }
  
  if(GetDlgItem(IDC_PORT,wxTextCtrl)->GetValue().Length())
    {
      buffer=GetDlgItem(IDC_PORT,wxTextCtrl)->GetValue();
      app_prefs->prefs->port = (atoi(buffer)) ? atoi(buffer) : 4662;
    }
  
  if(GetDlgItem(IDC_MAXSOURCEPERFILE,wxTextCtrl)->GetValue().Length())
    {
      buffer=GetDlgItem(IDC_MAXSOURCEPERFILE,wxTextCtrl)->GetValue();
      app_prefs->prefs->maxsourceperfile = (atoi(buffer)) ? atoi(buffer) : 1;
    }
  
  if(GetDlgItem(IDC_UDPPORT,wxTextCtrl)->GetValue().Length())
    {
      buffer=GetDlgItem(IDC_UDPPORT,wxTextCtrl)->GetValue();
      app_prefs->prefs->udpport = (atoi(buffer) && !IsDlgButtonChecked(IDC_UDPDISABLE) ) ? atoi(buffer) : 0;
    }
  
  if(IsDlgButtonChecked(IDC_SHOWOVERHEAD))
    app_prefs->prefs->m_bshowoverhead = true;
  else
    app_prefs->prefs->m_bshowoverhead = false;
  
  
  //	if(IsDlgButtonChecked(IDC_UDPDISABLE)) app_prefs->prefs->udpport=0;
  GetDlgItem(IDC_UDPPORT,wxControl)->Enable(!IsDlgButtonChecked(IDC_UDPDISABLE));
  
  app_prefs->prefs->autoconnect = (int8)IsDlgButtonChecked(IDC_AUTOCONNECT);
  app_prefs->prefs->reconnect = (int8)IsDlgButtonChecked(IDC_RECONN);
  
  if(lastmaxgu != app_prefs->prefs->maxGraphUploadRate) 
    theApp.emuledlg->statisticswnd->SetARange(false,app_prefs->prefs->maxGraphUploadRate);
  if(lastmaxgd!=app_prefs->prefs->maxGraphDownloadRate)
    theApp.emuledlg->statisticswnd->SetARange(true,app_prefs->prefs->maxGraphDownloadRate);
  
  uint16 tempcon = app_prefs->prefs->maxconnections;
  if(GetDlgItem(IDC_MAXCON,wxTextCtrl)->GetValue().Length())
    {
      buffer=GetDlgItem(IDC_MAXCON,wxTextCtrl)->GetValue();
      tempcon = (atoi(buffer)) ? atoi(buffer) : CPreferences::GetRecommendedMaxConnections();
    }
  
  if(tempcon > (unsigned)::GetMaxConnections())
    {
      CString strMessage;
      strMessage.Format(GetResString(IDS_PW_WARNING), GetResString(IDS_PW_MAXC).GetData(), ::GetMaxConnections());
      int iResult = wxMessageBox(strMessage, GetResString(IDS_PW_MAXC), wxICON_WARNING | wxYES_NO);
      if(iResult != wxYES)
	{
	  //TODO: set focus to max connection?
	  strMessage.Format("%d", app_prefs->prefs->maxconnections);
	  GetDlgItem(IDC_MAXCON,wxTextCtrl)->SetValue(strMessage);
	  tempcon = ::GetMaxConnections();
	}
    }
  app_prefs->prefs->maxconnections = tempcon;
  
	//if (app_prefs->prefs->maxGraphDownloadRate<app_prefs->prefs->maxdownload) app_prefs->prefs->maxdownload=UNLIMITED;
	//if (app_prefs->prefs->maxGraphUploadRate<app_prefs->prefs->maxupload) app_prefs->prefs->maxupload=UNLIMITED;
  
	//SetModified(FALSE);
  //	app_prefs->Save();
  LoadSettings();
  //	theApp.emuledlg->preferenceswnd.m_wndTweaks.LoadSettings();
  //return CPropertyPage::OnApply();
}

void CPPgConnection::Localize(void)
{	
  if(1)
    {     
      GetDlgItem(IDC_CAPACITIES_FRM,wxControl)->SetLabel(GetResString(IDS_PW_CON_CAPFRM));
      GetDlgItem(IDC_DCAP_LBL,wxControl)->SetLabel(GetResString(IDS_PW_CON_DOWNLBL));
      GetDlgItem(IDC_UCAP_LBL,wxControl)->SetLabel(GetResString(IDS_PW_CON_UPLBL));
      
      GetDlgItem(IDC_LIMITS_FRM,wxControl)->SetLabel(GetResString(IDS_PW_CON_LIMITFRM));
      GetDlgItem(IDC_DLIMIT_LBL,wxControl)->SetLabel(GetResString(IDS_PW_CON_DOWNLBL));
      GetDlgItem(IDC_ULIMIT_LBL,wxControl)->SetLabel(GetResString(IDS_PW_CON_UPLBL));
      
      GetDlgItem(IDC_KBS1,wxControl)->SetLabel(GetResString(IDS_KBYTESEC));
      GetDlgItem(IDC_KBS2,wxControl)->SetLabel(GetResString(IDS_KBYTESEC));
      GetDlgItem(IDC_KBS3,wxControl)->SetLabel(GetResString(IDS_KBYTESEC));
      GetDlgItem(IDC_KBS4,wxControl)->SetLabel(GetResString(IDS_KBYTESEC));
      
      GetDlgItem(IDC_MAXCONN_FRM,wxControl)->SetLabel(GetResString(IDS_PW_CONLIMITS));
      GetDlgItem(IDC_MAXCONLABEL,wxControl)->SetLabel(GetResString(IDS_PW_MAXC));
      
      GetDlgItem(IDC_SHOWOVERHEAD,wxControl)->SetLabel(GetResString(IDS_SHOWOVERHEAD));
		
      GetDlgItem(IDC_CLIENTPORT_FRM,wxControl)->SetLabel(GetResString(IDS_PW_CLIENTPORT));
      GetDlgItem(IDC_MAXSRC_FRM,wxControl)->SetLabel(GetResString(IDS_PW_MAXSOURCES));
      
      GetDlgItem(IDC_AUTOCONNECT,wxControl)->SetLabel(GetResString(IDS_PW_AUTOCON));
      GetDlgItem(IDC_RECONN,wxControl)->SetLabel(GetResString(IDS_PW_RECON));
      GetDlgItem(IDC_MAXSRCHARD_LBL,wxControl)->SetLabel(GetResString(IDS_HARDLIMIT));
      GetDlgItem(IDC_WIZARD,wxControl)->SetLabel(GetResString(IDS_WIZARD));

			// okok, these seems to have vanished.. needs to be fixed at some point      
      //GetDlgItem(IDC_UDPPORTLABEL,wxControl)->SetLabel(GetResString(IDS_UDPPORT));
      GetDlgItem(IDC_UDPPORTLABEL,wxControl)->SetLabel("UDP Port");
      GetDlgItem(IDC_UDPDISABLE,wxControl)->SetLabel(GetResString(IDS_UDPDISABLED));
    }
}

void CPPgConnection::OnBnClickedWizard(wxEvent& evt)
{
  Wizard test(this);
  test.SetPrefs(app_prefs);
  test.OnInitDialog();
  test.ShowModal();
}
