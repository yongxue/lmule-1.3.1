//this file is part of eMule
//Copyright (C)2002 Merkur ( merkur-@users.sourceforge.net / http://www.emule-project.net )
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


#ifndef PACKETS_H
#define PACKETS_H

#include "types.h"
#include "opcodes.h"
#include "otherstructs.h"
//#include "zlib/zlib.h"
#include "CFile.h"

//			CLIENT TO SERVER



//			PACKET CLASS
// TODO some parts could need some work to make it more efficient
class Packet {
public:
	Packet(uint8 protocol = OP_EDONKEYPROT);
	Packet(char* header); // only used for receiving packets
	Packet(char* pPacketPart,uint32 nSize,bool bLast); // only used for splitted packets!
	Packet(CMemFile* datafile,uint8 protocol = OP_EDONKEYPROT);
	Packet(int8 in_opcode,int32 in_size,uint8 protocol = OP_EDONKEYPROT);
	~Packet();
	char*	GetHeader();
	char*	GetUDPHeader();
	char*	GetPacket();
	char*	DetachPacket();
	uint32	GetRealPacketSize()		{return size+6;}
	bool	IsSplitted()			{return m_bSplitted;}
	bool	IsLastSplitted()		{return m_bLastSplitted;}
	void	PackPacket();
	bool	UnPackPacket();
	char*	pBuffer;
	uint32	size;
	uint8	opcode;
	uint8	prot;
private:
	bool	m_bSplitted;
	bool	m_bLastSplitted;
	char	head[6];
	char*	completebuffer;
	char*	tempbuffer;
	bool	m_bPacked;
};
struct Tag_Struct{
	int8	type;
	char*	tagname;
	char*	stringvalue;
	int32	intvalue;
	int8	specialtag;
	
};

class CTag {
public:
	CTag(char* name,uint32 intvalue);
	CTag(int8 special, uint32 intvalue);
	CTag(char* name,char* strvalue);
	CTag(int8 special, char* strvalue);
	CTag(Tag_Struct* in_tag);
	CTag(CFile* in_data);
	CTag(CMemFile* in_data);
	CTag(FILE* in_data);
	~CTag();
	CTag* CloneTag()	{return new CTag(tag);};
	bool WriteTagToFile(CFile* file); //used for CMemfiles
	bool WriteTagToFile(CMemFile* file);
	bool WriteTagToFile(FILE* file);
	Tag_Struct* tag;
};

#endif
