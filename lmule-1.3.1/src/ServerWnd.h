//this file is part of eMule
//Copyright (C)2002 Merkur ( merkur-@users.sourceforge.net / http://www.emule-project.net )
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


#ifndef SERVERWND_H
#define SERVERWND_H

#include "ServerListCtrl.h"
//#include "afxwin.h"
//#include "ResizableLib/ResizableDialog.h"
//#include "HyperTextCtrl.h"
//#include "IconStatic.h"

// CServerWnd dialog

#include "wx/textctrl.h"
#include "wx/statbmp.h"
#include "wx/html/htmlwin.h"
#include "wx/dialog.h"

class CServerWnd : public wxPanel //CResizableDialog
{
  //DECLARE_DYNAMIC(CServerWnd)
  DECLARE_DYNAMIC_CLASS(CServerWnd)
    
    CServerWnd() {};

public:
	CServerWnd(wxWindow* pParent);   // standard constructor
	virtual ~CServerWnd();
	void Localize();
	void UpdateServerMetFromURL(wxString strURL);
	void UpdateMyInfo();

	//wxSizer* Create(wxWindow* parent);
// Dialog Data
	enum { IDD = IDD_SERVER };
	CServerListCtrl* serverlistctrl;
protected:
	//virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//virtual BOOL OnInitDialog();
	//DECLARE_MESSAGE_MAP()
public:
	//CHyperTextCtrl servermsgbox;
	wxHtmlWindow servermsgbox;
	wxTextCtrl logbox;
	void OnBnClickedAddserver(wxEvent& evt);
	void OnBnClickedUpdateservermetfromurl(wxEvent& evt);
	//afx_msg void OnBnClickedAddserver();
	//afx_msg void OnBnClickedUpdateservermetfromurl();
	//afx_msg void OnBnClickedResetLog();
private:
	wxStaticBitmap m_ctrlNewServerFrm;
	wxStaticBitmap m_ctrlUpdateServerFrm;

	DECLARE_EVENT_TABLE()
};
#endif
