//this file is part of eMule
//Copyright (C)2002 Merkur ( merkur-@users.sourceforge.net / http://www.emule-project.net )
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#ifndef _UPLOADLISTCTRL_H
#define _UPLOADLISTCTRL_H

#include "types.h"
//#include "TitleMenu.h"
#include "MuleListCtrl.h"


// CUploadListCtrl
class CUpDownClient;

#include <wx/menu.h>
#include <wx/imaglist.h>

#define SW_HIDE 1
#define SW_SHOW 2

class CUploadListCtrl : public CMuleListCtrl
{
  //DECLARE_DYNAMIC(CUploadListCtrl)

public:
	CUploadListCtrl();
	CUploadListCtrl(wxWindow*& parent,int id,const wxPoint& pos,wxSize siz,int flags);

	virtual ~CUploadListCtrl();
	void	Init();
	void InitSort();
	void	AddClient(CUpDownClient* client);
	void	RemoveClient(CUpDownClient* client);
	void	RefreshClient(CUpDownClient* client);
	void	Hide() {/*ShowWindow(SW_HIDE);*/}
	void	Visable() {/*ShowWindow(SW_SHOW);*/}
	void	Localize();
	//virtual BOOL OnCommand(WPARAM wParam,LPARAM lParam );
protected:
	/*
	static int CALLBACK SortProc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort);
	afx_msg	void OnColumnClick( NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnNMRclick(NMHDR *pNMHDR, LRESULT *pResult);
	DECLARE_MESSAGE_MAP()
	*/
	
	static int SortProc(long lp1,long lp2,long lpSort);

	void OnColumnClick(wxListEvent& evt);
	void OnNMRclick(wxMouseEvent& evt);

	DECLARE_EVENT_TABLE()

	bool ProcessEvent(wxEvent& evt);

 public:
	virtual void OnDrawItem(int item,wxDC* dc,const wxRect& rect,const wxRect& rectHL,bool highlighted);
	void	ShowSelectedUserDetails();

private:
	bool		 asc_sort[7];	 
	wxImageList imagelist;
	wxMenu*	   m_ClientMenu;
	wxBrush* m_hilightBrush,*m_hilightUnfocusBrush;
};


#endif
