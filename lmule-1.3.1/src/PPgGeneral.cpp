// PPgGeneral.cpp : implementation file
//

//#include "stdafx.h"
#include "wintypes.h"
#include "emule.h"
#include "PPgGeneral.h"

#include <wx/checkbox.h>
#include <wx/notebook.h>
#include <wx/slider.h>
#include "wx/xrc/xmlres.h"
#include "wx/xrc/xh_all.h"

#define ID_DESKTOPMODE 3358

// CPPgGeneral dialog
#define GetDlgItem(x,clas) XRCCTRL(*this,#x,clas)
#define IsDlgButtonChecked2(x,y) XRCCTRL(*this,#x,y)->GetValue()
#define IsDlgButtonChecked(x) XRCCTRL(*this,#x,wxCheckBox)->GetValue()
#define CheckDlgButton3(x,y,z) XRCCTRL(*this,#x,z)->SetValue(y)
#define CheckDlgButton(x,y) XRCCTRL(*this,#x,wxCheckBox)->SetValue(y)

//IMPLEMENT_DYNAMIC(CPPgGeneral, CPropertyPage)
IMPLEMENT_DYNAMIC_CLASS(CPPgGeneral,wxPanel)

CPPgGeneral::CPPgGeneral(wxWindow* parent)
  :wxPanel(parent,CPPgGeneral::IDD) //: CPropertyPage(CPPgGeneral::IDD)
{
  wxNotebook* book=(wxNotebook*)parent;

  wxPanel*page1=wxXmlResource::Get()->LoadPanel(this,"DLG_PPG_GENERAL");
  book->AddPage(this,_("General"));

  SetSize(page1->GetSize().GetWidth(),page1->GetSize().GetHeight()+40);
  page1->SetSize(GetSize());

  GetDlgItem(IDC_LANGS,wxComboBox)->Append(_("System Default"));
  GetDlgItem(IDC_ROUND,wxControl)->SetWindowStyle(wxALIGN_RIGHT);
  GetDlgItem(IDC_ROUND,wxControl)->Refresh();

  // add extra button for configuring the desktop mode
  wxButton* mybtn=new wxButton(page1,ID_DESKTOPMODE,_("Systray Integration"),
			       wxPoint(265,245),wxSize(110,24));
  
  Localize();
}

CPPgGeneral::~CPPgGeneral()
{
}

#if 0
void CPPgGeneral::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LANGS, m_language);
}
#endif

BEGIN_EVENT_TABLE(CPPgGeneral,wxPanel)
  EVT_SCROLL(CPPgGeneral::OnHScroll)
  EVT_BUTTON(ID_DESKTOPMODE,CPPgGeneral::OnDesktopmode)
END_EVENT_TABLE()

void CPPgGeneral::OnDesktopmode(wxEvent& evt)
{
  theApp.emuledlg->changeDesktopMode();
}

#if 0
BEGIN_MESSAGE_MAP(CPPgGeneral, CPropertyPage)

	ON_EN_CHANGE(IDC_NICK, OnEnChangeNick)
	ON_BN_CLICKED(IDC_MINTRAY, OnBnClickedMintray)
	ON_BN_CLICKED(IDC_BEEPER, OnBnClickedBeeper)
	ON_BN_CLICKED(IDC_EXIT, OnBnClickedExit)
	ON_BN_CLICKED(IDC_SPLASHON, OnBnClickedSplashon)
	ON_BN_CLICKED(IDC_DBLCLICK, OnBnClickedDblclick)
	ON_BN_CLICKED(IDC_BRINGTOFOREGROUND, OnBnClickedBringtoforeground)
	ON_BN_CLICKED(IDC_NOTIFY, OnBnClickedNotify)
	ON_EN_CHANGE(IDC_TOOLTIPDELAY, OnEnChangeTooltipdelay)
	ON_CBN_SELCHANGE(IDC_LANGS, OnCbnSelchangeLangs)
	ON_BN_CLICKED(IDC_FLAT, OnBnClickedFlat)
	ON_BN_CLICKED(IDC_ED2KFIX, OnBnClickedEd2kfix)
	ON_BN_CLICKED(IDC_ONLINESIG, OnBnClickedOnlinesig)
END_MESSAGE_MAP()
#endif

#include <wx/checkbox.h>

void CPPgGeneral::LoadSettings(void)
{
  // Barry - Controls depth of 3d colour shading
  wxSlider *slider3D = GetDlgItem(IDC_3DDEPTH,wxSlider);
  slider3D->SetRange(0, 5);
  slider3D->SetValue(app_prefs->Get3DDepth());
  
  wxSlider *sliderUpdate = GetDlgItem(IDC_CHECKDAYS,wxSlider);
  sliderUpdate->SetRange(2, 7);
  sliderUpdate->SetValue(app_prefs->GetUpdateDays());

  GetDlgItem(IDC_NICK,wxTextCtrl)->SetValue(app_prefs->prefs->nick);
  
  //for(int i = 0; i != m_language.GetCount(); i++)
  //  if(m_language.GetItemData(i) == app_prefs->GetLanguageID())
  //    m_language.SetCurSel(i);
  GetDlgItem(IDC_LANGS,wxComboBox)->SetSelection(0);

  if(app_prefs->prefs->startMinimized)
    CheckDlgButton(IDC_STARTMIN,1);
  else
    CheckDlgButton(IDC_STARTMIN,0);
  
  if(app_prefs->prefs->mintotray)
    CheckDlgButton(IDC_MINTRAY,1);
  else
    CheckDlgButton(IDC_MINTRAY,0);
  
  if (app_prefs->prefs->onlineSig)
    CheckDlgButton(IDC_ONLINESIG,1);
  else
    CheckDlgButton(IDC_ONLINESIG,0);
  
  if(app_prefs->prefs->beepOnError)
    CheckDlgButton(IDC_BEEPER,1);
  else
    CheckDlgButton(IDC_BEEPER,0);
  
  if(app_prefs->prefs->confirmExit)
    CheckDlgButton(IDC_EXIT,1);
  else
    CheckDlgButton(IDC_EXIT,0);
  
  if(app_prefs->prefs->splashscreen)
    CheckDlgButton(IDC_SPLASHON,1);
  else
    CheckDlgButton(IDC_SPLASHON,0);
  
  if(app_prefs->prefs->transferDoubleclick)
    CheckDlgButton(IDC_DBLCLICK,1);
  else
    CheckDlgButton(IDC_DBLCLICK,0);
  
  if(app_prefs->prefs->bringtoforeground)
    CheckDlgButton(IDC_BRINGTOFOREGROUND,1);
  else
    CheckDlgButton(IDC_BRINGTOFOREGROUND,0);
  
  if(app_prefs->prefs->updatenotify)
    CheckDlgButton(IDC_CHECK4UPDATE,1);
  else
    CheckDlgButton(IDC_CHECK4UPDATE,0);
  
  CString strBuffer;
  strBuffer.Format("%d", app_prefs->prefs->m_iToolDelayTime);
  GetDlgItem(IDC_TOOLTIPDELAY,wxTextCtrl)->SetValue(strBuffer);
  strBuffer.Format("%i %s",app_prefs->prefs->versioncheckdays ,GetResString(IDS_DAYS2).GetData());
  GetDlgItem(IDC_DAYS,wxControl)->SetLabel(strBuffer);
}

#if 0
BOOL CPPgGeneral::OnInitDialog()
{
	CPropertyPage::OnInitDialog();

	((CEdit*)GetDlgItem(IDC_NICK))->SetLimitText(49);
	
	m_language.SetItemData(m_language.AddString("Bulgarian"),MAKELANGID(LANG_BULGARIAN ,SUBLANG_DEFAULT));
	m_language.SetItemData(m_language.AddString("Catalonian"),MAKELANGID(LANG_CATALAN ,SUBLANG_DEFAULT));
	m_language.SetItemData(m_language.AddString("Chinese (simplified)"),MAKELANGID(LANG_CHINESE,SUBLANG_CHINESE_SIMPLIFIED));
	m_language.SetItemData(m_language.AddString("Chinese (traditional)"),MAKELANGID(LANG_CHINESE,SUBLANG_CHINESE_TRADITIONAL));
	m_language.SetItemData(m_language.AddString("Danish"),MAKELANGID(LANG_DANISH,SUBLANG_DEFAULT));
	m_language.SetItemData(m_language.AddString("Dutch"),MAKELANGID(LANG_DUTCH,SUBLANG_DEFAULT));
	m_language.SetItemData(m_language.AddString("English"),MAKELANGID(LANG_ENGLISH,SUBLANG_DEFAULT));
	m_language.SetItemData(m_language.AddString("Finnish"),MAKELANGID(LANG_FINNISH,SUBLANG_DEFAULT));
	m_language.SetItemData(m_language.AddString("French"),MAKELANGID(LANG_FRENCH,SUBLANG_DEFAULT));
	m_language.SetItemData(m_language.AddString("German"),MAKELANGID(LANG_GERMAN,SUBLANG_DEFAULT));
	m_language.SetItemData(m_language.AddString("Greek"),MAKELANGID(LANG_GREEK,SUBLANG_DEFAULT));
	m_language.SetItemData(m_language.AddString("Italian"),MAKELANGID(LANG_ITALIAN,SUBLANG_DEFAULT));
	m_language.SetItemData(m_language.AddString("Korean"),MAKELANGID(LANG_KOREAN,SUBLANG_DEFAULT));
	m_language.SetItemData(m_language.AddString("Latvian"),MAKELANGID(LANG_LATVIAN,SUBLANG_DEFAULT));
	m_language.SetItemData(m_language.AddString("Lithuanian"),MAKELANGID(LANG_LITHUANIAN,SUBLANG_DEFAULT));
	m_language.SetItemData(m_language.AddString("Norwegian"),MAKELANGID(LANG_NORWEGIAN,SUBLANG_NORWEGIAN_BOKMAL));
	m_language.SetItemData(m_language.AddString("Polish"),MAKELANGID(LANG_POLISH,SUBLANG_DEFAULT));
	m_language.SetItemData(m_language.AddString("Portuguese"),MAKELANGID(LANG_PORTUGUESE,SUBLANG_PORTUGUESE));
	m_language.SetItemData(m_language.AddString("Portuguese (Brasilian)"),MAKELANGID(LANG_PORTUGUESE,SUBLANG_PORTUGUESE_BRAZILIAN));
	m_language.SetItemData(m_language.AddString("Russian"),MAKELANGID(LANG_RUSSIAN,SUBLANG_DEFAULT));
	m_language.SetItemData(m_language.AddString("Slovenian"),MAKELANGID(LANG_SLOVENIAN,SUBLANG_DEFAULT));
	m_language.SetItemData(m_language.AddString("Spanish"),MAKELANGID(LANG_SPANISH,SUBLANG_SPANISH));
	m_language.SetItemData(m_language.AddString("Swedish"),MAKELANGID(LANG_SWEDISH,SUBLANG_DEFAULT));
	
	GetDlgItem(IDC_ED2KFIX)->EnableWindow(Ask4RegFix(true));

	LoadSettings();
	Localize();

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

BOOL CPPgGeneral::OnApply()
#endif

void CPPgGeneral::OnApply()
{
  wxString buffer;
  if(GetDlgItem(IDC_NICK,wxTextCtrl)->GetValue().Length())
    {
      buffer=GetDlgItem(IDC_NICK,wxTextCtrl)->GetValue();
      strcpy(app_prefs->prefs->nick,buffer);
    }
#if 0
  // we support no languages yet!
  if (m_language.GetCurSel() != CB_ERR){
    WORD byNewLang =  m_language.GetItemData(m_language.GetCurSel());
    if (app_prefs->prefs->languageID != byNewLang){
      app_prefs->prefs->languageID= byNewLang;
      
      theApp.glob_prefs->SetLanguage();
      
      theApp.emuledlg->preferenceswnd.Localize();
      theApp.emuledlg->statisticswnd.Localize();
      theApp.emuledlg->serverwnd.Localize();
      theApp.emuledlg->transferwnd.Localize();
      theApp.emuledlg->searchwnd.Localize();
      theApp.emuledlg->sharedfileswnd.Localize();
      theApp.emuledlg->chatwnd.Localize();
      theApp.emuledlg->Localize();
      theApp.emuledlg->ircwnd.Localize();
    }
  }
#endif
  
  int8 mintotray_old= app_prefs->prefs->mintotray;
  app_prefs->prefs->startMinimized= (int8)IsDlgButtonChecked(IDC_STARTMIN);
  app_prefs->prefs->mintotray = (int8)IsDlgButtonChecked(IDC_MINTRAY);
  app_prefs->prefs->beepOnError= (int8)IsDlgButtonChecked(IDC_BEEPER);
  app_prefs->prefs->confirmExit= (int8)IsDlgButtonChecked(IDC_EXIT);
  app_prefs->prefs->splashscreen = (int8)IsDlgButtonChecked(IDC_SPLASHON);
  app_prefs->prefs->transferDoubleclick= (int8)IsDlgButtonChecked(IDC_DBLCLICK);
  app_prefs->prefs->bringtoforeground = (int8)IsDlgButtonChecked(IDC_BRINGTOFOREGROUND);
  app_prefs->prefs->updatenotify = (int8)IsDlgButtonChecked(IDC_CHECK4UPDATE);
  app_prefs->prefs->onlineSig= (int8)IsDlgButtonChecked(IDC_ONLINESIG);
  app_prefs->prefs->depth3D = GetDlgItem(IDC_3DDEPTH,wxSlider)->GetValue();
  app_prefs->prefs->versioncheckdays = GetDlgItem(IDC_CHECKDAYS,wxSlider)->GetValue();
  
  buffer=GetDlgItem(IDC_TOOLTIPDELAY,wxTextCtrl)->GetValue();
  if(atoi(buffer) > 32)
    app_prefs->prefs->m_iToolDelayTime = 32;
  else
    app_prefs->prefs->m_iToolDelayTime = atoi(buffer);
  
#if 0
  ((CemuleDlg*)AfxGetMainWnd())->transferwnd.m_ttip.SetDelayTime(TTDT_INITIAL, theApp.glob_prefs->GetToolTipDelay()*1000);
  
#endif
  // no setstyle as we can't do that anyway
  //theApp.emuledlg->transferwnd->downloadlistctrl->SetStyle();

  //	app_prefs->Save();
  LoadSettings();
  
#if 0
  if (mintotray_old != app_prefs->prefs->mintotray)
    theApp.emuledlg->TrayMinimizeToTrayChange();
#endif
  
  //SetModified(FALSE);
  //return CPropertyPage::OnApply();
}

#if 0
void CPPgGeneral::OnBnClickedEd2kfix()
{
	Ask4RegFix(false);
	GetDlgItem(IDC_ED2KFIX)->EnableWindow(Ask4RegFix(true));
}
#endif

void CPPgGeneral::Localize(void)
{
  if(1)
    {
      //SetWindowText(GetResString(IDS_PW_GENERAL));
      GetDlgItem(IDC_NICK_FRM,wxControl)->SetLabel(GetResString(IDS_PW_NICK));
      GetDlgItem(IDC_LANG_FRM,wxControl)->SetLabel(GetResString(IDS_PW_LANG));
      GetDlgItem(IDC_MISC_FRM,wxControl)->SetLabel(GetResString(IDS_PW_MISC));
      GetDlgItem(IDC_MINTRAY,wxControl)->SetLabel(GetResString(IDS_PW_TRAY));
      GetDlgItem(IDC_BEEPER,wxControl)->SetLabel(GetResString(IDS_PW_BEEP));
      GetDlgItem(IDC_EXIT,wxControl)->SetLabel(GetResString(IDS_PW_PROMPT));
      GetDlgItem(IDC_SPLASHON,wxControl)->SetLabel(GetResString(IDS_PW_SPLASH));
      GetDlgItem(IDC_DBLCLICK,wxControl)->SetLabel(GetResString(IDS_PW_DBLCLICK));
      GetDlgItem(IDC_TOOLTIPDELAY_LBL,wxControl)->SetLabel(GetResString(IDS_PW_TOOL));
      GetDlgItem(IDC_BRINGTOFOREGROUND,wxControl)->SetLabel(GetResString(IDS_PW_FRONT));
      GetDlgItem(IDC_ONLINESIG,wxControl)->SetLabel(GetResString(IDS_PREF_ONLINESIG));	
      GetDlgItem(IDC_STARTMIN,wxControl)->SetLabel(GetResString(IDS_PREF_STARTMIN));	
      GetDlgItem(IDC_3DDEP,wxControl)->SetLabel(GetResString(IDS_3DDEP));
      GetDlgItem(IDC_FLAT,wxControl)->SetLabel(GetResString(IDS_FLAT));
      GetDlgItem(IDC_ROUND,wxControl)->SetLabel(GetResString(IDS_ROUND));
      GetDlgItem(IDC_WEBSVEDIT,wxControl)->SetLabel(GetResString(IDS_WEBSVEDIT));
      GetDlgItem(IDC_ED2KFIX,wxControl)->SetLabel(GetResString(IDS_ED2KLINKFIX));
      GetDlgItem(IDC_CHECK4UPDATE,wxControl)->SetLabel(GetResString(IDS_CHECK4UPDATE));
      GetDlgItem(IDC_STARTUP,wxControl)->SetLabel(GetResString(IDS_STARTUP));
    }
}

void CPPgGeneral::OnHScroll(wxScrollEvent& evt)
{
  //SetModified(TRUE);

  wxSlider* slider=(wxSlider*)evt.GetEventObject();
  if (slider==GetDlgItem(IDC_CHECKDAYS,wxSlider)) {
    //CSliderCtrl* slider =(CSliderCtrl*)pScrollBar;
    CString text;
    text.Format("%i %s",evt.GetPosition(),GetResString(IDS_DAYS2).GetData());
    GetDlgItem(IDC_DAYS,wxControl)->SetLabel(text);
  }
  
  //UpdateData(false); 
  //CPropertyPage::OnHScroll(nSBCode, nPos, pScrollBar);
}
