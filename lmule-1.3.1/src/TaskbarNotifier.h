// CTaskbarNotifier Header file
// By John O'Byrne - 15 July 2002

#ifndef 

#include <shellapi.h>

#define WM_TASKBARNOTIFIERCLICKED	WM_USER+123
#define TN_TEXT_NORMAL			0x0000
#define TN_TEXT_BOLD			0x0001
#define TN_TEXT_ITALIC			0x0002
#define TN_TEXT_UNDERLINE		0x0004

//START - enkeyDEV(kei-kun) -TaskbarNotifier-
#define TBN_NULL				0
#define TBN_CHAT				1
#define TBN_DLOAD				2
#define TBN_LOG					3
//END - enkeyDEV(kei-kun) -TaskbarNotifier-

// CTaskbarNotifier

class CTaskbarNotifier : public CWnd
{
	DECLARE_DYNAMIC(CTaskbarNotifier)

public:
	CTaskbarNotifier();
	virtual ~CTaskbarNotifier();

	int Create(CWnd *pWndParent);
	void Show(LPCTSTR szCaption, int nMsgType,DWORD dwTimeToShow=500,DWORD dwTimeToStay=4000,DWORD dwTimeToHide=200); //<<--enkeyDEV(kei-kun) -TaskbarNotifier- MODIFICATA DICHIARAZIONE E DEFINIZIONE
	void Hide();
	int GetMessageType(); //<<--enkeyDEV(kei-kun) -TaskbarNotifier-
	BOOL SetBitmap(UINT nBitmapID,short red=-1,short green=-1,short blue=-1);
	BOOL SetBitmap(LPCTSTR szFileName,short red=-1,short green=-1,short blue=-1);
	BOOL SetBitmap(CBitmap* Bitmap,short red,short green,short blue);
	void SetTextFont(LPCTSTR szFont,int nSize,int nNormalStyle,int nSelectedStyle);
	void SetTextDefaultFont();
	void SetTextColor(COLORREF crNormalTextColor,COLORREF crSelectedTextColor);
	void SetTextRect(RECT rcText);
	void SetTextFormat(UINT uTextFormat);
			
	CWnd *m_pWndParent;
	
	CFont m_myNormalFont;
	CFont m_mySelectedFont;
	COLORREF m_crNormalTextColor;
	COLORREF m_crSelectedTextColor;
	HCURSOR m_hCursor;
	
	CBitmap m_bitmapBackground;
	HRGN m_hBitmapRegion;
	int m_nBitmapWidth;
	int m_nBitmapHeight;

	CString m_strCaption;
	CRect m_rcText;
	UINT m_uTextFormat;
	BOOL m_bMouseIsOver;

	int m_nAnimStatus;
	int m_nTaskbarPlacement;
	DWORD m_dwTimerPrecision;
 	DWORD m_dwTimeToStay;
	DWORD m_dwShowEvents;
	DWORD m_dwHideEvents;
	int m_nCurrentPosX;
	int m_nCurrentPosY;
	int m_nCurrentWidth;
	int m_nCurrentHeight;
	int m_nIncrementShow;
	int m_nIncrementHide;
	int m_nActiveMessageType;  //<<--enkeyDEV(kei-kun) -TaskbarNotifier-
	int m_nMessageTypeClicked; //<<--enkeyDEV(kei-kun) -TaskbarNotifier-
	
protected:
	HRGN CreateRgnFromBitmap(HBITMAP hBmp, COLORREF color);

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg LRESULT OnMouseHover(WPARAM w, LPARAM l);
	afx_msg LRESULT OnMouseLeave(WPARAM w, LPARAM l);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnPaint();
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnTimer(UINT nIDEvent);
};


