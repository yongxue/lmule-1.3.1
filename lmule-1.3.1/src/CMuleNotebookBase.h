///////////////////////////////////////////////////////////////////////////////
// Name:        wx/notebook.h
// Purpose:     wxNotebook interface
// Author:      Vadim Zeitlin
// Modified by:
// Created:     01.02.01
// RCS-ID:      $Id: CMuleNotebookBase.h,v 1.4 2003/03/19 13:52:18 tiku Exp $
// Copyright:   (c) 1996-2000 wxWindows team
// Licence:     wxWindows licence
///////////////////////////////////////////////////////////////////////////////

#ifndef _WX_MULENOTEBOOK_H_BASE_
#define _WX_MULENOTEBOOK_H_BASE_

#if defined(__GNUG__) && !defined(__APPLE__)
    #pragma interface "CMuleNotebookBase.h"
#endif

// ----------------------------------------------------------------------------
// headers
// ----------------------------------------------------------------------------

#include "wx/defs.h"

#if wxUSE_NOTEBOOK

#include "wx/control.h"
#include "wx/dynarray.h"
#include "wx/imaglist.h"

// ----------------------------------------------------------------------------
// types
// ----------------------------------------------------------------------------

// array of notebook pages
typedef wxWindow CMuleNotebookPage;  // so far, any window can be a page

WX_DEFINE_EXPORTED_ARRAY(CMuleNotebookPage *, wxMuleArrayPages);

#define wxNOTEBOOK_NAME _T("notebook")

// ----------------------------------------------------------------------------
// CMuleNotebookBase: define CMuleNotebook interface
// ----------------------------------------------------------------------------

class WXDLLEXPORT CMuleNotebookBase : public wxControl
{
public:
    // ctor
    CMuleNotebookBase()
    {
        Init();
    }

    // quasi ctor
    bool Create(wxWindow *parent,
                wxWindowID id,
                const wxPoint& pos = wxDefaultPosition,
                const wxSize& size = wxDefaultSize,
                long style = 0,
                const wxString& name = wxNOTEBOOK_NAME);

    // dtor
    virtual ~CMuleNotebookBase();

    // accessors
    // ---------

    // get number of pages in the dialog
    int GetPageCount() const { return m_pages.GetCount(); }

    // get the panel which represents the given page
    CMuleNotebookPage *GetPage(int nPage) { return m_pages[nPage]; }

    // get the currently selected page
    virtual int GetSelection() const = 0;

    // set/get the title of a page
    virtual bool SetPageText(int nPage, const wxString& strText) = 0;
    virtual wxString GetPageText(int nPage) const = 0;

    // image list stuff: each page may have an image associated with it (all
    // images belong to the same image list)
    virtual void SetImageList(wxImageList* imageList);
    virtual void SetStateImageList(wxImageList* imageList);

    // as SetImageList() but we will delete the image list ourselves
    void AssignImageList(wxImageList* imageList);
    void AssignStateImageList(wxImageList* imageList);

    // get pointer (may be NULL) to the associated image list
    wxImageList* GetImageList() const { return m_imageList; }
    wxImageList* GetStateImageList() const { return m_stateImageList; }

    // sets/returns item's image index in the current image list
    virtual int GetPageImage(int nPage) const = 0;
    virtual bool SetPageImage(int nPage, int nImage) = 0;

    // get the number of rows for a control with wxNB_MULTILINE style (not all
    // versions support it - they will always return 1 then)
    virtual int GetRowCount() const { return 1; }

    // set the size (the same for all pages)
    virtual void SetPageSize(const wxSize& size) = 0;

    // set the padding between tabs (in pixels)
    virtual void SetPadding(const wxSize& padding) = 0;

    // set the size of the tabs for wxNB_FIXEDWIDTH controls
    virtual void SetTabSize(const wxSize& sz) = 0;

    // calculate the size of the notebook from the size of its page
    virtual wxSize CalcSizeFromPage(const wxSize& sizePage);

    // operations
    // ----------

    // remove one page from the notebook and delete it
    virtual bool DeletePage(int nPage);

    // remove one page from the notebook, without deleting it
    virtual bool RemovePage(int nPage) { return DoRemovePage(nPage) != NULL; }

    // remove all pages and delete them
    virtual bool DeleteAllPages() { WX_CLEAR_ARRAY(m_pages); return TRUE; }

    // adds a new page to the notebook (it will be deleted by the notebook,
    // don't delete it yourself) and make it the current one if bSelect
    virtual bool AddPage(CMuleNotebookPage *pPage,
                         const wxString& strText,
                         bool bSelect = FALSE,
                         int imageId = -1,
			 unsigned long itemData=0)
    {
        return InsertPage(GetPageCount(), pPage, strText, bSelect, imageId, itemData);
    }

    // the same as AddPage(), but adds the page at the specified position
    virtual bool InsertPage(int nPage,
                            CMuleNotebookPage *pPage,
                            const wxString& strText,
                            bool bSelect = FALSE,
                            int imageId = -1,unsigned long itemData=0) = 0;

    // set the currently selected page, return the index of the previously
    // selected one (or -1 on error)
    //
    // NB: this function will _not_ generate wxEVT_NOTEBOOK_PAGE_xxx events
    virtual int SetSelection(int nPage) = 0;

    // cycle thru the tabs
    void AdvanceSelection(bool forward = TRUE)
    {
        int nPage = GetNextPage(forward);
        if ( nPage != -1 )
            SetSelection(nPage);
    }

protected:
    // remove the page and return a pointer to it
    virtual CMuleNotebookPage *DoRemovePage(int page);

    // common part of all ctors
    void Init();

    // get the next page wrapping if we reached the end
    int GetNextPage(bool forward) const;

    wxMuleArrayPages  m_pages;      // array of pages
    wxImageList  *m_imageList;  // we can have an associated image list
    wxImageList*m_stateImageList;
    bool m_ownsImageList;       // true if we must delete m_imageList
    bool m_ownsStateImageList;
};

// ----------------------------------------------------------------------------
// notebook event class (used by NOTEBOOK_PAGE_CHANGED/ING events)
// ----------------------------------------------------------------------------

class WXDLLEXPORT CMuleNotebookEvent : public wxNotifyEvent
{
public:
    CMuleNotebookEvent(wxEventType commandType = wxEVT_NULL, int id = 0,
                    int nSel = -1, int nOldSel = -1)
        : wxNotifyEvent(commandType, id)
        {
            m_nSel = nSel;
            m_nOldSel = nOldSel;
        }

    // accessors
        // the currently selected page (-1 if none)
    int GetSelection() const { return m_nSel; }
    void SetSelection(int nSel) { m_nSel = nSel; }
        // the page that was selected before the change (-1 if none)
    int GetOldSelection() const { return m_nOldSel; }
    void SetOldSelection(int nOldSel) { m_nOldSel = nOldSel; }

private:
    int m_nSel,     // currently selected page
        m_nOldSel;  // previously selected page

    DECLARE_DYNAMIC_CLASS(CMuleNotebookEvent)
};

// ----------------------------------------------------------------------------
// event types and macros for them
// ----------------------------------------------------------------------------

#if defined(__BORLANDC__) && defined(__WIN16__)
    // For 16-bit BC++, these 2 would be identical otherwise (truncated)
    #define wxEVT_COMMAND_NOTEBOOK_PAGE_CHANGED wxEVT_COMMAND_NB_PAGE_CHANGED
    #define wxEVT_COMMAND_NOTEBOOK_PAGE_CHANGING wxEVT_COMMAND_NB_PAGE_CHANGING
#endif

BEGIN_DECLARE_EVENT_TYPES()
    DECLARE_EVENT_TYPE(wxEVT_COMMAND_MULENOTEBOOK_PAGE_CHANGED, 802)
    DECLARE_EVENT_TYPE(wxEVT_COMMAND_MULENOTEBOOK_PAGE_CHANGING, 803)
     DECLARE_EVENT_TYPE(wxEVT_COMMAND_MULENOTEBOOK_PAGE_CLOSED,4804)
END_DECLARE_EVENT_TYPES()

typedef void (wxEvtHandler::*CMuleNotebookEventFunction)(CMuleNotebookEvent&);

// Truncation in 16-bit BC++ means we need to define these differently
#if defined(__BORLANDC__) && defined(__WIN16__)
#define EVT_MULENOTEBOOK_PAGE_CHANGED(id, fn)                                   \
  DECLARE_EVENT_TABLE_ENTRY(                                                \
    wxEVT_COMMAND_NB_PAGE_CHANGED,                                          \
    id,                                                                     \
    -1,                                                                     \
    (wxObjectEventFunction)(wxEventFunction)(CMuleNotebookEventFunction) &fn,  \
    NULL                                                                    \
  ),

#define EVT_MULENOTEBOOK_PAGE_CHANGING(id, fn)                                  \
  DECLARE_EVENT_TABLE_ENTRY(                                                \
    wxEVT_COMMAND_NB_PAGE_CHANGING,                                         \
    id,                                                                     \
    -1,                                                                     \
    (wxObjectEventFunction)(wxEventFunction)(CMuleNotebookEventFunction) &fn,  \
    NULL                                                                    \
  ),

#else

#define EVT_MULENOTEBOOK_PAGE_CHANGED(id, fn)                                   \
  DECLARE_EVENT_TABLE_ENTRY(                                                \
    wxEVT_COMMAND_MULENOTEBOOK_PAGE_CHANGED,                                    \
    id,                                                                     \
    -1,                                                                     \
    (wxObjectEventFunction)(wxEventFunction)(CMuleNotebookEventFunction) &fn,  \
    NULL                                                                    \
  ),

#define EVT_MULENOTEBOOK_PAGE_CHANGING(id, fn)                                  \
  DECLARE_EVENT_TABLE_ENTRY(                                                \
    wxEVT_COMMAND_MULENOTEBOOK_PAGE_CHANGING,                                   \
    id,                                                                     \
    -1,                                                                     \
    (wxObjectEventFunction)(wxEventFunction)(CMuleNotebookEventFunction) &fn,  \
    NULL                                                                    \
  ),

#define EVT_MULENOTEBOOK_PAGE_CLOSED(id, fn)                                  \
  DECLARE_EVENT_TABLE_ENTRY(                                                \
    wxEVT_COMMAND_MULENOTEBOOK_PAGE_CLOSED,                                   \
    id,                                                                     \
    -1,                                                                     \
    (wxObjectEventFunction)(wxEventFunction)(CMuleNotebookEventFunction) &fn,  \
    NULL                                                                    \
  ),

#endif

// ----------------------------------------------------------------------------
// CMuleNotebook class itself
// ----------------------------------------------------------------------------

#include "MuleNotebook.h"

#endif // wxUSE_NOTEBOOK

#endif
    // _WX_NOTEBOOK_H_BASE_
