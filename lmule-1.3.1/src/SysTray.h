//this file is part of lMule
//Copyright (C)2002 Tiku
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#include "wintypes.h"
#include "wx/window.h"
#include <gtk/gtk.h>

//START - enkeyDEV(kei-kun) -TaskbarNotifier-
#define TBN_NULL				0
#define TBN_CHAT				1
#define TBN_DLOAD				2
#define TBN_LOG					3
#define TBN_IMPORTANTEVENT		4
#define TBN_NEWVERSION				5
//END - enkeyDEV(kei-kun) -TaskbarNotifier-

typedef struct _tagSIZE {
  int cx,cy;
} SIZE;

class CSysTray {
 public:
  CSysTray(wxWindow* parent,int desktopMode);

  void Show(const wxChar* caption,int nMsgType,DWORD dwTimeToShow=500,DWORD dwTimeToStay=4000,DWORD dwTimeTOHide=200);

  void TraySetToolTip(char* data);
  void TraySetIcon(char** data,bool what=false,int* pVals=NULL);
  bool SetColorLevels(int* pLimits,COLORREF* pColors, int nEntries);

 private:
  void setupProperties();

  void drawMeters(GdkPixmap* pix,GdkBitmap* mask,int* pBarData);
  void DrawIconMeter(GdkPixmap* pix,GdkBitmap* mask,int pBarData,int x);
  COLORREF GetMeterColor(int level);

  int desktopMode;
  wxWindow* parent;
  GtkWidget* status_docklet;
  GtkWidget* status_image;
  GtkTooltips* status_tooltips;
  SIZE m_sDimensions;

  int m_nSpacingWidth;
  int m_nNumBars;
  int m_nMaxVal;
  int *m_pLimits;
  COLORREF *m_pColors;
  int m_nEntries;

};
