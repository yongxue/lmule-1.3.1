//this file is part of eMule
//Copyright (C)2002 Merkur ( merkur-@users.sourceforge.net / http://www.emule-project.net )
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

//#include "stdafx.h"
#include "emule.h"
#include "emuleDlg.h"
#include "opcodes.h"
#include "IPFilter.h"
#include "WebServer.h"
//#include "mdump.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#include <wx/config.h>
#include <wx/clipbrd.h>
#include <wx/socket.h>
#include <wx/splash.h>
#include "muuli_wdr.h"
//#include "splash_wdr.h"
#include "pixmaps/About.jpg.xpm"

#include <wx/filesys.h>
#include <wx/fs_zip.h>
#include "wx/xrc/xmlres.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
 
#include <config.h>

IMPLEMENT_APP(CemuleApp)

static wxBitmap *splashBmp=NULL;

// CemuleApp

//BEGIN_MESSAGE_MAP(CemuleApp, CWinApp)
//	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
//END_MESSAGE_MAP()

#include <sys/time.h>
unsigned long GetTickCount()
{	
  struct timeval aika;
  gettimeofday(&aika,NULL);
  unsigned long secs=aika.tv_sec*1000;
  secs+=(aika.tv_usec/1000);
  return secs;
}

CemuleApp::CemuleApp() {
  splashBmp=new wxBitmap((const char**)About_jpg);
}

CemuleApp::~CemuleApp(){ }
int CemuleApp::OnExit() {
  printf("*** emuleapp deletion start\n");
  delete splashBmp;
  
  theApp.listensocket->Destroy();
  theApp.clientudp->Destroy();
  delete theApp.sharedfiles;
  delete theApp.serverconnect;
  delete theApp.serverlist;
  delete theApp.knownfiles;
  delete theApp.searchlist;
  delete theApp.clientcredits;
  delete theApp.downloadqueue;
  delete theApp.uploadqueue;
  delete theApp.clientlist;
  delete theApp.friendlist;
  delete theApp.glob_prefs;

#if 0
  delete searchlist;
  delete friendlist;
  delete knownfiles;
  delete serverlist;
  delete serverconnect;
  delete sharedfiles;
  delete listensocket;
  delete clientudp;
  delete clientcredits;
  delete downloadqueue;
  delete uploadqueue;
  delete clientlist; 
#endif

  hashing_mut.Unlock();
  pendingMutex.Unlock();

  printf("*** emuleapp deleted\n");
}



//CemuleApp theApp;

// IPC classes
#include <wx/ipc.h>
class MuleConnection : public wxConnection {
public:
  MuleConnection(void):wxConnection() {}
  ~MuleConnection(void) {};
  bool OnAdvise(const wxString& topic,const wxString& item,char* data,int size,wxIPCFormat format) {
    return TRUE;
  }
  virtual char* OnRequest(const wxString& topic,const wxString& item,int* size,wxIPCFormat format) {
    if(item=="STATS") {
      int filecount=theApp.downloadqueue->GetFileCount();
      // get the source count
      int stats[2];
      theApp.downloadqueue->GetDownloadStats(stats);
      static char buffer[1024];
      sprintf(buffer,"%d %d %d %d %d",filecount,stats[0],stats[1],theApp.uploadqueue->GetUploadQueueLength(),theApp.uploadqueue->GetWaitingUserCount());
      return buffer;
    }
    return "";
  }
  bool OnExecute(const wxString& topic,/*const wxString& item,*/char* data,int size,wxIPCFormat format) {
    wxString item="ED2KLINK";
    if(item=="ED2KLINK") {
      // add it to queue
      try {
	wxString edlink=data;
	if(edlink.Right(1)!="/") edlink+="/";
	edlink.Replace("%7c","|");
	edlink.Replace("%7C","|");
	wxString newlink;
	URLDecode(newlink,edlink.GetData());
	printf("Link is %s\n",newlink.GetData());
	CED2KLink* pLink=CED2KLink::CreateLinkFromUrl(newlink);
	switch(pLink->GetKind()) {
	case CED2KLink::kFile:
	  {
	    CED2KFileLink* pFileLink=pLink->GetFileLink();	    
	    theApp.downloadqueue->AddFileLinkToDownload(pFileLink);
	  }
	  break;
	case CED2KLink::kServerList:
	  {
	    CED2KServerListLink* pListLink=pLink->GetServerListLink();
	    CString strAddress=pListLink->GetAddress();
	    if(strAddress.GetLength()!=0)
	      theApp.emuledlg->serverwnd->UpdateServerMetFromURL(strAddress);
	  }
	  break;
	case CED2KLink::kServer:
	  {
	    CString defName;
	    CED2KServerLink* pSrvLink = pLink->GetServerLink();
	    in_addr host;
	    host.s_addr = pSrvLink->GetIP();
	    CServer* pSrv = new CServer(pSrvLink->GetPort(),inet_ntoa(host));
	    pSrvLink->GetDefaultName(defName);
	    pSrv->SetListName((char*)(defName.GetData()));
	    
	    // Barry - Default all new servers to high priority
	    pSrv->SetPreference(PR_HIGH);
	    
	    if (!theApp.emuledlg->serverwnd->serverlistctrl->AddServer(pSrv,true))
	      delete pSrv;
	    else theApp.emuledlg->AddLogLine(true,GetResString(IDS_SERVERADDED)+CString(pSrv->GetListName()));
	  }
	  break;
	default:
	  break;
	}
	delete pLink;
      } catch(...) {
	theApp.emuledlg->AddLogLine(true,GetResString(IDS_LINKNOTADDED));
      }
    }
  }
};

class MuleClient : public wxClient {
public:
  MuleClient(void) {};
  wxConnectionBase* OnMakeConnection(void) { return new MuleConnection; }
};

class MuleServer : public wxServer {
public:
  MuleServer(void) {};
  wxConnectionBase* OnAcceptConnection(const wxString& topic) {return new MuleConnection; }
};

// CemuleApp Initialisierung

extern void InitXmlResource();

bool CemuleApp::OnInit()
{
	//putenv("LANG=en_US");

	// for resources
	wxFileSystem::AddHandler(new wxZipFSHandler);
	wxXmlResource::Get()->InitAllHandlers();
	InitXmlResource();

	printf("Initialising eMule\n");
	close(0);
	//if(ProcessCommandLine())
	//	return FALSE;

	//SetTopWindow(dlg);

	SetVendorName("TikuWarez");
	SetAppName("eMule");

	// see if there is another instance running
	wxString server=getenv("HOME")+wxString("/.lmule/muleconn");
	wxString hostName="localhost";
	MuleClient* client=new MuleClient;
	MuleConnection* conn=(MuleConnection*)client->MakeConnection(hostName,server,"lMule IPC");
	if (!conn) {
		// no connection => spawn server instead.
		MuleServer* ipcserver=new MuleServer;
		ipcserver->Create(server);
		// arguments should be checked here too..
		// (actually, later when app is ready)
	} else {
		// check command line arguments ed2k://
		for (int i=0;i<argc;i++) {
			printf("%i. %s\n", i, argv[i]);
			if (strncmp(argv[i], "version", 7) == 0) {
				printf("%s version %s\n", PACKAGE_NAME, VERSION);
			} else if (strlen(argv[i])>7) {
				// could be ed2k link
				if (strncmp(argv[i],"ed2k://",7)==0) {
					// it most likely is. send it to active instance
					conn->Execute(argv[i]);
				}else if (strncmp(argv[i],"statistics",10)==0) {
					printf("%s\n",conn->Request("STATS",NULL));		
				}
			}
		}
		conn->Disconnect();
		delete conn;
		delete client;
		// don't allow another instance.
		sleep(1); // this will prevent lmule to hang itself
		exit(0); // is this clean.. perhaps not
	}

	m_locale.Init(wxLANGUAGE_DEFAULT);
	m_locale.AddCatalogLookupPathPrefix(LOCALEDIR);
	m_locale.AddCatalog(PACKAGE);

	CemuleDlg* dlg=new CemuleDlg();
	emuledlg=dlg;
	dlg->Show(TRUE);

	glob_prefs = new CPreferences();
	emuledlg->preferenceswnd->SetPrefs(glob_prefs);
	
	emuledlg->createSystray();

	// splashscreen
	if (theApp.glob_prefs->UseSplashScreen() && !theApp.glob_prefs->GetStartMinimized()){
		wxSplashScreen* splsh=new wxSplashScreen(*splashBmp,wxSPLASH_CENTRE_ON_SCREEN|wxSPLASH_TIMEOUT,
							 5000,NULL,-1,wxDefaultPosition,wxDefaultSize,
							 wxSIMPLE_BORDER|wxSTAY_ON_TOP);
	}

	wxIPV4address myaddr;
	myaddr.AnyAddress();
	myaddr.Service(glob_prefs->GetPort());
	printf("(port=%d)\n",glob_prefs->GetPort());

	clientlist=NULL;
	searchlist=NULL;
	friendlist=NULL;
	knownfiles=NULL;
	serverlist=NULL;
	serverconnect=NULL;
	sharedfiles=NULL;
	listensocket=NULL;
	clientudp=NULL;
	clientcredits=NULL;
	downloadqueue=NULL;
	uploadqueue=NULL;
	ipfilter=NULL;

	//setup languag	
	clientlist = new CClientList();
	searchlist = new CSearchList();
	friendlist = new CFriendList();
	knownfiles = new CKnownFileList(glob_prefs->GetAppDir());
	serverlist = new CServerList(glob_prefs);
	serverconnect = new CServerConnect(serverlist,theApp.glob_prefs);
	sharedfiles = new CSharedFileList(glob_prefs,serverconnect,knownfiles);
	listensocket = new CListenSocket(glob_prefs,myaddr);
	myaddr.Service(glob_prefs->GetUDPPort());
	clientudp = new CClientUDPSocket(myaddr);
	clientcredits = new CClientCreditsList(glob_prefs);
	downloadqueue = new CDownloadQueue(glob_prefs,sharedfiles);	// bugfix - do this before creating the uploadqueue
	uploadqueue = new CUploadQueue(glob_prefs);
	ipfilter = new CIPFilter();
	webserver=new CWebServer();	
	//INT_PTR nResponse = dlg.DoModal();
	
	// init statistics stuff, better do it asap
	emuledlg->statisticswnd->Init();
	emuledlg->statisticswnd->ShowInterval();

	if(!listensocket->Ok()) {
		emuledlg->AddLogLine(false,(_("Port %d is not available. You will be LOWID")),glob_prefs->GetPort());
		CString str;
		str.Format(_("Port %d is not available !!\n\nThis will mean that you will be LOWID.\n\nUse netstat to determine when port becomes available\nand try starting lmule again."),glob_prefs->GetPort());
		wxMessageBox(str,_("Error"),wxCENTRE|wxOK|wxICON_ERROR);
	}

	// must do initialisations here.. 
	emuledlg->serverwnd->serverlistctrl->Init(serverlist);
	serverlist->Init();

	// ini. downloadqueue
	theApp.downloadqueue->Init();
	emuledlg->AddLogLine(true,GetResString(IDS_MAIN_READY),CURRENT_VERSION_LONG);
	
	// 
	theApp.sharedfiles->SetOutputCtrl((CSharedFilesCtrl*)emuledlg->sharedfileswnd->FindWindowByName("sharedFilesCt"));

	// then init firend list
	theApp.friendlist->SetWindow((CFriendListCtrl*)theApp.emuledlg->chatwnd->FindWindowById(ID_FRIENDLIST));
	theApp.friendlist->ShowFriends();

	SetTopWindow(dlg);

	// reset statistic values
	theApp.stat_sessionReceivedBytes=0;
	theApp.stat_sessionSentBytes=0;
	theApp.stat_reconnects=0;
	theApp.stat_transferStarttime=0;
	theApp.stat_serverConnectTime=0;

	// Initialize and sort all lists.
	theApp.emuledlg->transferwnd->downloadlistctrl->LoadSettings(CPreferences::tableDownload);
	theApp.emuledlg->transferwnd->downloadlistctrl->InitSort();
	theApp.emuledlg->transferwnd->uploadlistctrl->LoadSettings(CPreferences::tableUpload);
	theApp.emuledlg->transferwnd->uploadlistctrl->InitSort();
	theApp.emuledlg->transferwnd->queuelistctrl->LoadSettings(CPreferences::tableQueue);
	theApp.emuledlg->transferwnd->queuelistctrl->InitSort();
	theApp.emuledlg->serverwnd->serverlistctrl->LoadSettings(CPreferences::tableServer);
	theApp.emuledlg->serverwnd->serverlistctrl->InitSort();
	theApp.emuledlg->sharedfileswnd->sharedfilesctrl->LoadSettings(CPreferences::tableShared);
	theApp.emuledlg->sharedfileswnd->sharedfilesctrl->InitSort();

	emuledlg->m_app_state=APP_STATE_RUNNING;

	// parse possible arguments
	// easy way: just create a client connection and stuff them there :)
	// note: not actually SENDING anything, just passing stuff directly
	conn=new MuleConnection;
	if(conn) {
		// check command line arguments ed2k://
		for(int i=0;i<argc;i++) {
			if(strlen(argv[i])>7) {
	// could be ed2k link
	if(strncmp(argv[i],"ed2k://",7)==0) {
		// it most likely is. send it to active instance
		conn->OnPoke("IPC","ED2KLINK",argv[i],strlen(argv[i]),wxIPC_TEXT);
	} 
			}
		}
		// we'll leave it lying around.. 
		// (can't delete it :)
	}

	// reload shared files
	theApp.sharedfiles->Reload(true, true);

	if (glob_prefs->GetWSIsEnabled()) {
		webserver->ReloadTemplates();
		webserver->StartServer();
	}
	
	// pretty much ready now. start autoconnect if 
	if(theApp.glob_prefs->DoAutoConnect()) {
		wxCommandEvent nullEvt;
		theApp.emuledlg->OnBnConnect(nullEvt);
	}

#if 0
	// set the checkpoint
	wxDebugContext::SetCheckpoint();
#endif
	return TRUE;
}

#if 0
BOOL CemuleApp::InitInstance()
{
#ifdef _DUMP
	MiniDumper dumper(CURRENT_VERSION_LONG);
#endif

	pendinglink = 0;
	if (ProcessCommandline())
		return false;
	// InitCommonControls() ist f�r Windows XP erforderlich, wenn ein Anwendungsmanifest
	// die Verwendung von ComCtl32.dll Version 6 oder h�her zum Aktivieren
	// von visuellen Stilen angibt. Ansonsten treten beim Erstellen von Fenstern Fehler auf.
	InitCommonControls();

	CWinApp::InitInstance();

	if (!AfxSocketInit())
	{
		AfxMessageBox(IDP_SOCKETS_INIT_FAILED);
		return FALSE;
	}

	AfxEnableControlContainer();
	AfxSocketInit();
	CemuleDlg dlg;
	emuledlg = &dlg;
	m_pMainWnd = &dlg;

	// create & initalize all the important stuff 
	glob_prefs = new CPreferences();

	//setup languag	
	clientlist = new CClientList();
	searchlist = new CSearchList();
	knownfiles = new CKnownFileList(glob_prefs->GetAppDir());
	serverlist = new CServerList(glob_prefs);
	serverconnect = new CServerConnect(serverlist,theApp.glob_prefs);
	sharedfiles = new CSharedFileList(glob_prefs,serverconnect,knownfiles);
	listensocket = new CListenSocket(glob_prefs);
	clientcredits = new CClientCreditsList(glob_prefs);
	downloadqueue = new CDownloadQueue(glob_prefs,sharedfiles);	// bugfix - do this before creating the uploadqueue
	uploadqueue = new CUploadQueue(glob_prefs);
	INT_PTR nResponse = dlg.DoModal();


	// reset statistic values
	theApp.stat_sessionReceivedBytes=0;
	theApp.stat_sessionSentBytes=0;
	theApp.stat_reconnects=0;
	theApp.stat_transferStarttime=0;
	theApp.stat_serverConnectTime=0;
	
	return FALSE;
}
#endif

bool CemuleApp::ProcessCommandline(){
#if 0
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);
	
	char buffer[50];
	sprintf(buffer,"eMule V%s",CURRENT_VERSION_LONG);	
	HWND maininst = FindWindow(0,buffer);
	if (cmdInfo.m_nShellCommand == CCommandLineInfo::FileOpen) {
		CString command = cmdInfo.m_strFileName;
	
		//if (maininst){ moved down by Cax2 28/10/02 
			//tagCOPYDATASTRUCT sendstruct; removed by Cax2 28/10/02 
			sendstruct.cbData = command.GetLength()+1; 
			sendstruct.dwData = OP_ED2KLINK; 
			sendstruct.lpData = command.GetBuffer(); 
		if (maininst) { //Cax2 28/10/02 
			SendMessage(maininst,WM_COPYDATA,(WPARAM)0,(LPARAM) (PCOPYDATASTRUCT) &sendstruct); 
			return true; 
		} 
		else 
			pendinglink = new CString(command);
	}

	return maininst;
#endif
}

void CemuleApp::UpdateReceivedBytes(int32 bytesToAdd) {
	SetTimeOnTransfer();
	stat_sessionReceivedBytes+=bytesToAdd;
}
void CemuleApp::UpdateSentBytes(int32 bytesToAdd) {
	SetTimeOnTransfer();
	stat_sessionSentBytes+=bytesToAdd;
}
void CemuleApp::SetTimeOnTransfer() {
	if (stat_transferStarttime>0) return;
	
	stat_transferStarttime=GetTickCount();
}

typedef char* LPTSTR;

wxString CemuleApp::StripInvalidFilenameChars(wxString strText, bool bKeepSpaces)
{
  LPTSTR pszBuffer = (char*)strText.GetData();
  LPTSTR pszSource = pszBuffer;
  LPTSTR pszDest = pszBuffer;
  
  while (*pszSource != '\0')
    {
      if (!((*pszSource <= 31 && *pszSource >= 0)	|| // lots of invalid chars for filenames in windows :=)
	    *pszSource == '\"' || *pszSource == '*' || *pszSource == '<'  || *pszSource == '>' ||
	    *pszSource == '?'  || *pszSource == '|' || *pszSource == '\\' || *pszSource == '/' || 
	    *pszSource == ':') )
	{
	  if (!bKeepSpaces && *pszSource == ' ')
	    *pszDest = '_';
	  else
	    *pszDest = *pszSource;
	  
	  pszDest++;
	}
      pszSource++;
    }
  *pszDest = '\0';
  
  return strText;
}

wxString CemuleApp::CreateED2kLink( CAbstractFile* f )
{
  wxString strLink;
  
  strLink=strLink.Format("ed2k://|file|%s|%d|%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x|/",
			 StripInvalidFilenameChars(f->GetFileName(), true).GetData(),	// spaces to dots
			 f->GetFileSize(),
			 f->GetFileHash()[0],f->GetFileHash()[1],f->GetFileHash()[2],
			 f->GetFileHash()[3],f->GetFileHash()[4],f->GetFileHash()[5],
			 f->GetFileHash()[6],f->GetFileHash()[7],f->GetFileHash()[8],
			 f->GetFileHash()[9],f->GetFileHash()[10],f->GetFileHash()[11],
			 f->GetFileHash()[12],f->GetFileHash()[13],f->GetFileHash()[14],
			 f->GetFileHash()[15]);
  return strLink.GetData();
}

wxString CemuleApp::CreateED2kSourceLink( CAbstractFile* f )
{
        if (!serverconnect->IsConnected() || serverconnect->IsLowID()){
                emuledlg->AddLogLine(true,GetResString(IDS_SOURCELINKFAILED));
                return CString("");
        }
        uint32 dwID = serverconnect->GetClientID();
 
        wxString strLink;
        strLink.Printf("ed2k://|file|%s|%u|%s|/|sources,%i.%i.%i.%i:%i|/",
                StripInvalidFilenameChars(f->GetFileName(), false).GetData(),     // spaces to dots
                f->GetFileSize(),
                EncodeBase16(f->GetFileHash(),16).GetData(),
                (uint8)dwID,(uint8)(dwID>>8),(uint8)(dwID>>16),(uint8)(dwID>>24), glob_prefs->GetPort() );
        return strLink;
}

wxString CemuleApp::CreateHTMLED2kLink( CAbstractFile* f )
{
  wxString strCode = "<a href=\"" + CreateED2kLink(f) + "\">" + StripInvalidFilenameChars(f->GetFileName(), true) + "</a>";
  return strCode;
}

bool CemuleApp::CopyTextToClipboard( wxString strText )
{
  if (wxTheClipboard->Open())
  {
    wxTheClipboard->UsePrimarySelection(TRUE);
    wxTheClipboard->SetData( new wxTextDataObject(strText) );
    wxTheClipboard->Close();
  }
}

void CemuleApp::OnlineSig() // Added By Bouc7 
{ 
  if (!theApp.glob_prefs->IsOnlineSignatureEnabled()) return;

    char* fullpath = new char[strlen(glob_prefs->GetAppDir())+14]; 
    sprintf(fullpath,"%sonlinesig.dat",glob_prefs->GetAppDir()); 
    CFile file; 
    if (!file.Open(fullpath,CFile::write)) {
		theApp.emuledlg->AddLogLine(true,GetResString(IDS_ERROR_SAVEFILE)+wxString(" OnlineSig.dat"));
    } 
    char buffer[20]; 
    if (serverconnect->IsConnected()) 
    { 
      file.Write("1",1); 
      file.Write("|",1); 
      file.Write(serverconnect->GetCurrentServer()->GetListName(),strlen(serverconnect->GetCurrentServer()->GetListName())); 
      // Not : file.Write(serverconnect->GetCurrentServer()->GetListName(),strlen(serverconnect->GetCurrentServer()- >GetRealName())); 

      file.Write("|",1); 
      file.Write(serverconnect->GetCurrentServer()->GetFullIP(),strlen(serverconnect->GetCurrentServer()->GetFullIP())); 
      file.Write("|",1); 
      sprintf(buffer,"%d",serverconnect->GetCurrentServer()->GetPort());
      file.Write(buffer,strlen(buffer)); 
    } 
    else 
      file.Write("0",1); 

    file.Write("\n",1); 
    sprintf(buffer,"%.1f",(float)downloadqueue->GetDatarate()/1024); 
    file.Write(buffer,strlen(buffer)); 
    file.Write("|",1); 
    sprintf(buffer,"%.1f",(float)uploadqueue->GetDatarate()/1024); 
    file.Write(buffer,strlen(buffer)); 
    file.Write("|",1); 
    sprintf(buffer,"%d",uploadqueue->GetWaitingUserCount());
    //itoa(uploadqueue->GetWaitingUserCount(),buffer,10); 
    file.Write(buffer,strlen(buffer)); 

    file.Close(); 
    delete[] fullpath; 
    
} //End Added By Bouc7


#define wxGTK_WINDOW 1
#define SHIFT (8*(sizeof(short int)-sizeof(char)))

#include <gdk/gdk.h>
#include <gtk/gtk.h>
static bool GetColourWidget(int& red,int& green,int& blue,int type)
{
  GtkWidget* widget;
  switch(type) {
  case wxGTK_WINDOW:
    widget=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    break;
  default:
    return FALSE;
  }
  GtkStyle* def=gtk_rc_get_style(widget);
  bool ok=false;
  if(!def)
    def=gtk_widget_get_default_style();
  if(def) {
    GdkColor* col;
    col=def->bg;
    red=col[GTK_STATE_NORMAL].red;
    green=col[GTK_STATE_NORMAL].green;
    blue=col[GTK_STATE_NORMAL].blue;
    ok=true;
  }
  gtk_widget_destroy(widget);
  return ok;
}

// external helper function
wxColour GetColour(wxSystemColour what)
{
  static wxColour* _systemWindowColour=NULL;

  switch(what) {
  case wxSYS_COLOUR_WINDOW:
    if(!_systemWindowColour) {
      int red,green,blue;
      if(!GetColourWidget(red,green,blue,wxGTK_WINDOW)) {
	red=green=blue=0x9c40;
      }
      _systemWindowColour=new wxColour(red>>SHIFT,green>>SHIFT,blue>>SHIFT);
    }
    return *_systemWindowColour;
  }
}
