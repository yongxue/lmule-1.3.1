//this file is part of eMule
//Copyright (C)2002 Merkur ( merkur-@users.sourceforge.net / http://www.emule-project.net )
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


#ifndef SEARCHLIST_H
#define SEARCHLIST_H

#include "opcodes.h"
#include "emule.h"
#include "types.h"
#include "Preferences.h"
#include "KnownFile.h"
#include "packets.h"
#include "SearchListCtrl.h"
#include "otherfunctions.h"

class SearchList;

#if 0
WX_DECLARE_LIST(CSearchFile,SearchListL);

class CSearchFile : public CAbstractFile {
	friend class CPartFile;
public:
	//CSearchFile() {};
	CSearchFile(CMemFile* in_data, uint16 nSearchID);
	~CSearchFile();
	uint32	GetIntTagValue(uint8 tagname);
	char*	GetStrTagValue(uint8 tagname);
	uint32	AddSources(uint32 count);
	uint32	GetSourceCount();
	uint16	GetSearchID() {return m_nSearchID;}

private:
	uint8	clientip[4];
	uint16	clientport;
	uint16	m_nSearchID;
	TagList taglist;
	//CArray<CTag*,CTag*> taglist;
};

#include <wx/hashmap.h>
WX_DECLARE_HASH_MAP(uint32,uint16,wxIntegerHash,wxIntegerEqual,FoundMap);

class CSearchList
{
  friend class CSearchListCtrl;
public:
  CSearchList();
  ~CSearchList();
  void Clear();
  void NewSearch(CSearchListCtrl* in_wnd, wxString resTypes, uint16 nSearchID);
  uint16	ProcessSearchanswer(char* packet, uint32 size, CUpDownClient* Sender = NULL);
  uint16	ProcessUDPSearchanswer(char* packet, uint32 size);
  uint16	GetResultCount();
  uint16	GetResultCount(uint32 nSearchID);
  void	RemoveResults(  uint16 nSearchID );
  void	RemoveResults( CSearchFile* todel );
  void	ShowResults(uint16 nSearchID);

  uint16	GetFoundFiles(uint32 searchID) {
    uint16 returnVal;
    FoundMap::iterator pos=foundFilesCount.find(searchID);
    return pos->second;
  }
  
 private:
  void AddToList(CSearchFile* toadd);
  //CTypedPtrList<CPtrList, CSearchFile*> list;
  SearchListL list;
  FoundMap foundFilesCount;

  CSearchListCtrl*	outputwnd;
  wxString resultType;
  uint16	m_nCurrentSearch;
};
#endif

class CSearchFile : public CAbstractFile {
	friend class CPartFile;
public:
	//CSearchFile() {};
	CSearchFile(CFile* in_data, uint32 nSearchID);
	~CSearchFile();
	uint32	GetIntTagValue(uint8 tagname);
	char*	GetStrTagValue(uint8 tagname);
	uint32	AddSources(uint32 count);
	uint32	GetSourceCount();
	uint32	GetSearchID() {return m_nSearchID;}
private:
	uint8	clientip[4];
	uint16	clientport;
	uint32	m_nSearchID;
	CArray<CTag*,CTag*> taglist;
};

class CSearchList
{
friend class CSearchListCtrl;
public:
	CSearchList();
	~CSearchList();
	void	Clear();
	void	NewSearch(CSearchListCtrl* in_wnd, CString resTypes, uint16 nSearchID);
	uint16	ProcessSearchanswer(char* packet, uint32 size, CUpDownClient* Sender = NULL);
	uint16	ProcessUDPSearchanswer(char* packet, uint32 size);
	uint16	GetResultCount();
	uint16	GetResultCount(uint32 nSearchID);
	void	SetOutputWnd(CSearchListCtrl* in_wnd)		{outputwnd = in_wnd;}
	void	RemoveResults(  uint32 nSearchID );
	void	RemoveResults( CSearchFile* todel );
	void	ShowResults(uint32 nSearchID);
	
	uint16	GetFoundFiles(uint32 searchID);

private:
	bool AddToList(CSearchFile* toadd, bool bClientResponse = false);
	CTypedPtrList<CPtrList, CSearchFile*> list;
	CMap<uint32, uint32, uint16, uint16> foundFilesCount;

	CString myHashList;
	CSearchListCtrl*	outputwnd;
	CString resultType;
	uint32	m_nCurrentSearch;
};

#endif
