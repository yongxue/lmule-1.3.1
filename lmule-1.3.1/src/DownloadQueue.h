//Copyright (C)2002 Merkur ( merkur-@users.sourceforge.net / http://www.emule-project.net )
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#ifndef DOWNLOADQUEUE_H
#define DOWNLOADQUEUE_H

//#include "stdafx.h"
#include "PartFile.h"
#include "opcodes.h"
#include "types.h"
#include "SharedFileList.h"
#include "Preferences.h"

class CDownloadQueue
{
	friend class CAddFileThread;
	friend class CServerSocket;
public:
	CDownloadQueue(CPreferences* in_prefs,CSharedFileList* in_sharedfilelist);
	~CDownloadQueue();
	void	Process();
	void	Init();
	void	AddSearchToDownload(CSearchFile* toadd);
	void	AddSearchToDownload(CString link);
	void	AddFileLinkToDownload(class CED2KFileLink* pLink);
	bool	IsFileExisting(uchar* fileid);
	bool	IsPartFile(void* totest);
	CPartFile*	GetFileByID(uchar* filehash);
	CPartFile* GetFileByIndex(int idx);
	void    CheckAndAddSource(CPartFile* sender,CUpDownClient* source);
	void    CheckAndAddKnownSource(CPartFile* sender,CUpDownClient* source);
	bool	RemoveSource(CUpDownClient* toremove, bool updatewindow = true);
	void	DeleteAll();
	void	RemoveFile(CPartFile* toremove);
	uint32	GetDatarate()			{return datarate;}
	void	SortByPriority();
	void	StopUDPRequests();
	CServer*	cur_udpserver;
	void	GetDownloadStats(int results[]);
	void	AddPartFilesToShare();
	void	AddDownload(CPartFile* newfile);
	CUpDownClient* 	GetDownloadClientByIP(uint32 dwIP);
	void	UpdateDisplayedInfo(bool force=false);
	void	StartNextFile();
	void	AddDownDataOverheadSourceExchange(uint32 data)	{ m_nDownDataRateMSOverhead += data;
															  m_nDownDataOverheadSourceExchange += data;
															  m_nDownDataOverheadSourceExchangePackets++;}
	void	AddDownDataOverheadFileRequest(uint32 data)		{ m_nDownDataRateMSOverhead += data;
															  m_nDownDataOverheadFileRequest += data;
															  m_nDownDataOverheadFileRequestPackets++;}
	void	AddDownDataOverheadServer(uint32 data)			{ m_nDownDataRateMSOverhead += data;
															  m_nDownDataOverheadServer += data;
															  m_nDownDataOverheadServerPackets++;}
	void	AddDownDataOverheadOther(uint32 data)			{ m_nDownDataRateMSOverhead += data;
															  m_nDownDataOverheadOther += data;
															  m_nDownDataOverheadOtherPackets++;}
	uint32	GetDownDatarateOverhead()			{return m_nDownDatarateOverhead;}
	uint64	GetDownDataOverheadSourceExchange()	{return m_nDownDataOverheadSourceExchange;}
	uint64	GetDownDataOverheadFileRequest()	{return m_nDownDataOverheadFileRequest;}
	uint64	GetDownDataOverheadServer()			{return m_nDownDataOverheadServer;}
	uint64	GetDownDataOverheadOther()			{return m_nDownDataOverheadOther;}
	uint64	GetDownDataOverheadSourceExchangePackets()	{return m_nDownDataOverheadSourceExchangePackets;}
	uint64	GetDownDataOverheadFileRequestPackets()		{return m_nDownDataOverheadFileRequestPackets;}
	uint64	GetDownDataOverheadServerPackets()			{return m_nDownDataOverheadServerPackets;}
	uint64	GetDownDataOverheadOtherPackets()			{return m_nDownDataOverheadOtherPackets;}
	void	CompDownDatarateOverhead();
	int		GetFileCount()						{return filelist.GetCount();}
	void	SavePartFiles(bool del = false);	// InterCeptor
protected:
	bool	SendNextUDPPacket();
private:
	CTypedPtrList<CPtrList, CPartFile*> filelist;
	CSharedFileList* sharedfilelist;
	CPreferences*	 app_prefs;
	uint16	filesrdy;
	uint32	datarate;
	
	CPartFile*	lastfile;
	uint32		lastudpsearchtime;
	uint32		lastudpstattime;
	uint8		udcounter;

	uint32		m_nDownDatarateOverhead;
	uint32		m_nDownDataRateMSOverhead;
	uint64		m_nDownDataOverheadSourceExchange;
	uint64		m_nDownDataOverheadSourceExchangePackets;
	uint64		m_nDownDataOverheadFileRequest;
	uint64		m_nDownDataOverheadFileRequestPackets;
	uint64		m_nDownDataOverheadServer;
	uint64		m_nDownDataOverheadServerPackets;
	uint64		m_nDownDataOverheadOther;
	uint64		m_nDownDataOverheadOtherPackets;
	CList<int,int>	m_AvarageDDRO_list;

    DWORD m_lastRefreshedDLDisplay;
};


#endif
