//this file is part of eMule
//Copyright (C)2002 Merkur ( merkur-@users.sourceforge.net / http://www.emule-project.net )
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#ifndef CLIENTUDP_H
#define CLIENTUDP_H

#include "wintypes.h"
#include "types.h"
#include "packets.h"

#include <wx/socket.h>


#pragma pack(1)
struct UDPPack{
	Packet* packet;
	uint32 dwIP;
	uint16 nPort;
};
#pragma pack()

// CClientUDPSocket command target

class CClientUDPSocket : public wxDatagramSocket
{
  DECLARE_DYNAMIC_CLASS(CClientUDPSocket)
    CClientUDPSocket() : wxDatagramSocket(useless2) {};
public:
	CClientUDPSocket(wxIPV4address address);
	virtual ~CClientUDPSocket();
	bool	SendPacket(Packet* packet, uint32 dwIP, uint16 nPort);
	bool	IsBusy()	{return m_bWouldBlock;}
	bool	Create();
protected:
	bool	ProcessPacket(char* packet, int16 size, int8 opcode, char* host, uint16 port);
	
 public:
	virtual void	OnSend(int nErrorCode);	
	virtual void	OnReceive(int nErrorCode);
private:
	void	ClearQueues();	
	int		SendTo(char* lpBuf,int nBufLen,uint32 dwIP, uint16 nPort);
	bool	m_bWouldBlock;

	CTypedPtrList<CPtrList, UDPPack*> controlpacket_queue;
	wxIPV4address  useless2;
};




#endif
