#ifndef PREFERDLG_H
#define PREFERDLG_H

#include "PPgGeneral.h"
#include "PPgConnection.h"
#include "PPgServer.h"
#include "PPgDirectories.h"
#include "PPgFiles.h"
#include "PPgStats.h"
#include "PPgNotify.h"
#include "PPgIrc.h"
#include "PPgTweaks.h"
#include "otherfunctions.h"

// CPreferencesDlg

class CPreferencesDlg : public wxDialog //CPropertySheet
{
  //DECLARE_DYNAMIC(CPreferencesDlg)
  DECLARE_DYNAMIC_CLASS(CPreferencesDlg)

    CPreferencesDlg() {};

public:
	CPreferencesDlg(wxWindow* parent,CPreferences* prefs);
	virtual ~CPreferencesDlg();
	
protected:
	UINT m_nActiveWnd;
	//DECLARE_MESSAGE_MAP()
	DECLARE_EVENT_TABLE()
public:
	CPPgGeneral*		m_wndGeneral;
	CPPgConnection*	m_wndConnection;
	CPPgServer*		m_wndServer;
	CPPgDirectories*	m_wndDirectories;
	CPPgFiles*		m_wndFiles;
	CPPgStats*		m_wndStats;
	CPPgNotify*		m_wndNotify;
	CPPgIRC*			m_wndIRC;
	CPPgTweaks*		m_wndTweaks;
	CPreferences	*app_prefs;

	virtual int ShowModal();

	void SetPrefs(CPreferences* in_prefs)
	{
		app_prefs = in_prefs;
		m_wndGeneral->SetPrefs(in_prefs);
		m_wndConnection->SetPrefs(in_prefs);
		m_wndServer->SetPrefs(in_prefs);
		m_wndDirectories->SetPrefs(in_prefs);
		m_wndFiles->SetPrefs(in_prefs);
		m_wndStats->SetPrefs(in_prefs);
		m_wndNotify->SetPrefs(in_prefs);
		//m_wndIRC->SetPrefs(in_prefs);
		m_wndTweaks->SetPrefs(in_prefs);
	}
	//afx_msg void OnDestroy();
	//virtual BOOL OnInitDialog();

	void OnBtnOk(wxEvent& evt);
	void OnBtnCancel(wxEvent& evt);
	void OnBtnWizard(wxEvent& evt);

	void Localize(void)
	{
#if 0
		// start changed by InterCeptor (localization) 11.11.02 
		TC_ITEM item; 
		item.mask = TCIF_TEXT; 

		CStringArray buffer; 
		buffer.Add(GetResString(IDS_PW_GENERAL)); 
		buffer.Add(GetResString(IDS_PW_CONNECTION)); 
		buffer.Add(GetResString(IDS_PW_SERVER)); 
		buffer.Add(GetResString(IDS_PW_DIR)); 
		buffer.Add(GetResString(IDS_PW_FILES)); 
		buffer.Add(GetResString(IDS_PW_EKDEV_OPTIONS)); 
		buffer.Add(GetResString(IDS_STATSSETUPINFO)); 
		buffer.Add("IRC"); 

		for (int i = 0; i <= GetTabControl()->GetItemCount()-1; i++) { 
		item.pszText = buffer[i].GetBuffer(); 
		GetTabControl()->SetItem (i, &item); 
		buffer[i].ReleaseBuffer(); 
		} 
		// end changed by InterCeptor (localization) 11.11.02 
#endif
		//SetWindowText(GetResString(IDS_PREFERENCES_CAPTION));
		m_wndGeneral->Localize();
		m_wndConnection->Localize();
		m_wndServer->Localize();
		m_wndDirectories->Localize();
		m_wndFiles->Localize();
		m_wndStats->Localize();
		m_wndNotify->Localize();
		//m_wndIRC->Localize();
		m_wndTweaks->Localize();
	}
};


#endif
