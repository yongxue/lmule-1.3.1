// PgTweaks.cpp : implementation file
//

//#include "stdafx.h"
#include "wintypes.h"
#include "emule.h"
#include "PPgTweaks.h"

#include <wx/slider.h>
#include "wx/xrc/xmlres.h"
#include "wx/xrc/xh_all.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#include <wx/notebook.h>
#include <wx/checkbox.h>

#define GetDlgItem(x,clas) XRCCTRL(*this,#x,clas)
#define IsDlgButtonChecked(x) XRCCTRL(*this,#x,wxCheckBox)->GetValue()
#define CheckDlgButton(x,y) XRCCTRL(*this,#x,wxCheckBox)->SetValue(y)

// CPPgTweaks dialog

//IMPLEMENT_DYNAMIC(CPPgTweaks, CPropertyPage)
IMPLEMENT_DYNAMIC_CLASS(CPPgTweaks,wxPanel)

BEGIN_EVENT_TABLE(CPPgTweaks,wxPanel)
  EVT_SCROLL(CPPgTweaks::OnHScroll)
END_EVENT_TABLE()

CPPgTweaks::CPPgTweaks(wxWindow* parent)
  : wxPanel(parent,CPPgTweaks::IDD)
{
  wxNotebook* book=(wxNotebook*)parent;

  wxPanel* page1=_panel=wxXmlResource::Get()->LoadPanel(this,"DLG_PPG_TWEAKS");
  book->AddPage(this,_("Tweaks"));
  
  //SetSize(wxSize(320,200));

  // looks stupid? it is :)
  SetSize(page1->GetSize().GetWidth(),page1->GetSize().GetHeight()+60);
  page1->SetSize(GetSize());
}

CPPgTweaks::~CPPgTweaks()
{
}

#if 0
void CPPgTweaks::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CPPgTweaks, CPropertyPage)

	ON_EN_CHANGE(IDC_MAXCON5SEC, OnSettingsChange)
	ON_BN_CLICKED(IDC_VERBOSE, OnSettingsChange)
	ON_BN_CLICKED(IDC_AUTOTAKEED2KLINKS, OnSettingsChange)
	ON_BN_CLICKED(IDC_UPDATEQUEUE, OnSettingsChange)
	ON_BN_CLICKED(IDC_SHOWRATEONTITLE, OnSettingsChange)
	ON_WM_HSCROLL()

END_MESSAGE_MAP()

#endif

// CPPgTweaks message handlers


BOOL CPPgTweaks::OnInitDialog()
{
#if 0
	CPropertyPage::OnInitDialog();

	LoadSettings();
	Localize();

#endif
	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}


void CPPgTweaks::LoadSettings(void)
{
	if(1)
	{
		CString strBuffer;

		strBuffer.Format("%d", app_prefs->GetMaxConperFive());
		GetDlgItem(IDC_MAXCON5SEC,wxTextCtrl)->SetValue(strBuffer);
	}
	if(app_prefs->prefs->m_bVerbose)
		CheckDlgButton(IDC_VERBOSE,1);
	else
		CheckDlgButton(IDC_VERBOSE,0);
	
	if(app_prefs->prefs->showRatesInTitle)
		CheckDlgButton(IDC_SHOWRATEONTITLE,1);
	else
		CheckDlgButton(IDC_SHOWRATEONTITLE,0);

	if(app_prefs->prefs->m_bupdatequeuelist)
		CheckDlgButton(IDC_UPDATEQUEUE,1);
	else
		CheckDlgButton(IDC_UPDATEQUEUE,0);
	// Barry
	if(app_prefs->prefs->autotakeed2klinks)
		CheckDlgButton(IDC_AUTOTAKEED2KLINKS,1);
	else
		CheckDlgButton(IDC_AUTOTAKEED2KLINKS,0);
	((wxSlider*)GetDlgItem(IDC_FILEBUFFERSIZE,wxSlider))->SetRange(1,100);
	((wxSlider*)GetDlgItem(IDC_FILEBUFFERSIZE,wxSlider))->SetValue(app_prefs->prefs->m_iFileBufferSize);
	m_iFileBufferSize = app_prefs->prefs->m_iFileBufferSize;
	((wxSlider*)GetDlgItem(IDC_QUEUESIZE,wxSlider))->SetRange(20,100);
	((wxSlider*)GetDlgItem(IDC_QUEUESIZE,wxSlider))->SetValue(app_prefs->prefs->m_iQueueSize);
	m_iQueueSize = app_prefs->prefs->m_iQueueSize;

	// must fake two scroll events.. setvalue won't cause any events
	wxScrollEvent evt1(0,XRCID("IDC_QUEUESIZE"),app_prefs->prefs->m_iQueueSize);
	evt1.SetEventObject(XRCCTRL(*this,"IDC_QUEUESIZE",wxObject));
	OnHScroll(evt1);

	wxScrollEvent evt2(0,XRCID("IDC_FILEBUFFERSIZE"),app_prefs->prefs->m_iFileBufferSize);
	evt2.SetEventObject(XRCCTRL(*this,"IDC_FILEBUFFERSIZE",wxObject));
	OnHScroll(evt2);
}

BOOL CPPgTweaks::OnApply()
{
  wxString buffer;
	if(GetDlgItem(IDC_MAXCON5SEC,wxTextCtrl)->GetValue().Length())
	{
	  buffer=GetDlgItem(IDC_MAXCON5SEC,wxTextCtrl)->GetValue();//GetWindowText(buffer,20);
	  app_prefs->SetMaxConsPerFive((atoi(buffer)) ?
					      atoi(buffer) : 20);
	}
	if(IsDlgButtonChecked(IDC_VERBOSE))
		app_prefs->prefs->m_bVerbose = true;
	else
		app_prefs->prefs->m_bVerbose = false;

	if(IsDlgButtonChecked(IDC_UPDATEQUEUE))
		app_prefs->prefs->m_bupdatequeuelist = true;
	else
		app_prefs->prefs->m_bupdatequeuelist = false;

	if(IsDlgButtonChecked(IDC_SHOWRATEONTITLE))
		app_prefs->prefs->showRatesInTitle= true;
	else
		app_prefs->prefs->showRatesInTitle= false;

	// Barry
	app_prefs->prefs->autotakeed2klinks = (int8)IsDlgButtonChecked(IDC_AUTOTAKEED2KLINKS);
	app_prefs->prefs->m_iFileBufferSize = m_iFileBufferSize;
	app_prefs->prefs->m_iQueueSize = m_iQueueSize;
	LoadSettings();
	//SetModified(FALSE);
	//theApp.emuledlg->serverwnd.ToggleDebugWindow();

	if (!theApp.glob_prefs->ShowRatesOnTitle()) {
	  //sprintf(buffer,"eMule v%s",CURRENT_VERSION_LONG);
	  //theApp.emuledlg->SetWindowText(buffer);
	}

	return TRUE;
}

void CPPgTweaks::OnHScroll(wxScrollEvent& evt)// UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
  //SetModified(TRUE);
	
  wxSlider* slider=(wxSlider*)evt.GetEventObject();
  //CSliderCtrl* slider =(CSliderCtrl*)pScrollBar;
  CString temp;
  if (slider==XRCCTRL(*this,"IDC_FILEBUFFERSIZE",wxSlider)) {//GetDlgItem(IDC_FILEBUFFERSIZE)) {
    m_iFileBufferSize = evt.GetPosition();
    temp.Format( GetResString(IDS_FILEBUFFERSIZE), m_iFileBufferSize*15000 );
    GetDlgItem(IDC_FILEBUFFERSIZE_STATIC,wxControl)->SetLabel(temp);
  }
  else if (slider==XRCCTRL(*this,"IDC_QUEUESIZE",wxSlider)) { //GetDlgItem(IDC_QUEUESIZE)) {
    m_iQueueSize = evt.GetPosition();
    temp.Format( GetResString(IDS_QUEUESIZE), m_iQueueSize*100 );
    GetDlgItem(IDC_QUEUESIZE_STATIC,wxControl)->SetLabel(temp);
  }

}

void CPPgTweaks::Localize(void)
{	
	if(1)
	{
	  //SetWindowText(GetResString(IDS_PW_TWEAK));
	  GetDlgItem(IDC_MAXCON5SECLABEL,wxControl)->SetLabel(GetResString(IDS_MAXCON5SECLABEL));
	  GetDlgItem(IDC_WARNING,wxControl)->SetLabel(GetResString(IDS_WARNING));
	  GetDlgItem(IDC_UPDATEQUEUE,wxControl)->SetLabel(GetResString(IDS_UPDATEQUEUE));
	  GetDlgItem(IDC_AUTOTAKEED2KLINKS,wxControl)->SetLabel(GetResString(IDS_AUTOTAKEED2KLINKS));
	  GetDlgItem(IDC_VERBOSE,wxControl)->SetLabel(GetResString(IDS_VERBOSE));
	  CString temp;
	  temp.Format( GetResString(IDS_FILEBUFFERSIZE), m_iFileBufferSize*15000 );
	  GetDlgItem(IDC_FILEBUFFERSIZE_STATIC,wxControl)->SetLabel(temp);
	  temp.Format( GetResString(IDS_QUEUESIZE), m_iQueueSize*100 );
	  GetDlgItem(IDC_QUEUESIZE_STATIC,wxControl)->SetLabel(temp);
	  
	  GetDlgItem(IDC_SHOWRATEONTITLE,wxControl)->SetLabel(GetResString(IDS_SHOWRATEONTITLE));
	}
}

