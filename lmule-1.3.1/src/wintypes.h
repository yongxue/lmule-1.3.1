#ifndef _WINTYPES_H
#define _WINTYPES_H

typedef unsigned int UINT;
typedef UINT* UINT_PTR;

typedef char* LPCSTR;
typedef unsigned char BYTE;
typedef unsigned long DWORD;
typedef unsigned long BOOL;
typedef signed long LONG;
typedef unsigned long ULONG;
typedef unsigned long long ULONGLONG;
typedef BYTE* LPBYTE;
typedef int INT;
typedef unsigned short WORD;
typedef unsigned char* LPCTSTR;
typedef DWORD COLORREF;
typedef void VOID;
typedef void* LPVOID;
typedef void* PVOID;

typedef struct _POINT {
  UINT x,y;
} POINT;

typedef struct _RECT {
  UINT left,top,right,bottom;
} RECT;

typedef RECT* LPRECT;

typedef struct _WINDOWPLACEMENT {
    UINT length;
    UINT flags;
    UINT showCmd;
    POINT ptMinPosition;
    POINT ptMaxPosition;
    RECT rcNormalPosition;
} WINDOWPLACEMENT;

#define min(a,b) ((a)<(b)?(a):(b))

#include "mfc.h"
#endif
