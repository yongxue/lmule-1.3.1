//this file is part of eMule
//Copyright (C)2002 Merkur ( merkur-@users.sourceforge.net / http://www.emule-project.net )
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


#ifndef _SLIST_CTRL
#define _SLIST_CTRL 1

#include "opcodes.h"
#include "emule.h"
#include "types.h"
#include "Preferences.h"
#include "KnownFile.h"
#include "SearchList.h"
#include "MuleListCtrl.h"

// CSearchListCtrl
class CSearchList;
class CSearchFile;
class CSearchListCtrl : public CMuleListCtrl
{
  //DECLARE_DYNAMIC(CSearchListCtrl)

public:
	CSearchListCtrl();
	CSearchListCtrl(wxWindow*& parent,int id,const wxPoint& pos,wxSize siz,int flags);

	virtual ~CSearchListCtrl();
	void	Init(CSearchList* in_searchlist);
	void	UpdateSources(CSearchFile* toupdate);
	void	AddResult(CSearchFile* toshow);
	void	RemoveResult( CSearchFile* toremove);
	void	Localize();
	void	ShowResults(uint32 nResultsID);
	//void OnRclick(wxListEvent& evt);
	void OnNMRclick(wxMouseEvent& evt);
	void OnLDclick(wxMouseEvent& evt);
	uint16	GetSearchId() { return m_nResultsID; }

protected:
	static int SortProc(long lParam1, long lParam2, long lParamSort);
	//afx_msg void OnCustomdraw ( NMHDR* pNMHDR, LRESULT* pResult);
	//afx_msg	void OnColumnClick( NMHDR* pNMHDR, LRESULT* pResult);
	//afx_msg void OnContextMenu(CWnd* /*pWnd*/, CPoint /*point*/);
	//virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	void OnColumnClick(wxListEvent& evt);
	//DECLARE_MESSAGE_MAP()

	bool ProcessEvent(wxEvent& evt);

	DECLARE_EVENT_TABLE()
private:
	void UpdateColor(long index,long count);

	//CTitleMenu	 m_SearchFileMenu;
	wxMenu* m_SearchFileMenu;
	CSearchList* searchlist;
	uint32		 m_nResultsID;
	bool		 asc_sort[7];	 
};


#endif

