//this file is part of eMule
//Copyright (C)2002 Merkur ( merkur-@users.sourceforge.net / http://www.emule-project.net )
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#ifndef OTHERSTRUCTS_H
#define OTHERSTRUCTS_H

#include "wintypes.h"
#include "types.h"
#include "CMemFile.h"
#include <zlib.h>

#if 0
/* winemaker: #pragma pack(1) */
//#include <pshpack1.h>
//			SERVER TO CLIENT

struct Header_Struct{
	int8	eDonkeyID __attribute__ ((packed));
	int32	packetlength __attribute__ ((packed));
	int8	command __attribute__ ((packed));
};
struct UDP_Header_Struct{
	int8	eDonkeyID __attribute__ ((packed));
	int8	command __attribute__ ((packed));
};

struct LoginAnswer_Struct {
	uint32	clientid __attribute__ ((packed));
};

struct Requested_Block_Struct{
	uint32	StartOffset __attribute__ ((packed));
	uint32	EndOffset __attribute__ ((packed));
	uint32	packedsize __attribute__ ((packed));
	uchar	FileID[16] __attribute__ ((packed));
};

struct Requested_File_Struct{
	uchar	  fileid[16] __attribute__ ((packed));
	uint32	  lastasked __attribute__ ((packed));
	uint8	  badrequests __attribute__ ((packed));
};

struct Pending_Block_Struct{
  Requested_Block_Struct*	block __attribute__ ((packed));
  CMemFile*				buffer __attribute__ ((packed));
};

struct Gap_Struct{
	uint32 start __attribute__ ((packed));
	uint32 end __attribute__ ((packed));
};

struct ServerMet_Struct {
	uint32	ip __attribute__ ((packed));
	uint16	port __attribute__ ((packed));
	uint32	tagcount __attribute__ ((packed));
};
#endif


//			SERVER TO CLIENT
#pragma pack(1)
struct Header_Struct{
	int8	eDonkeyID;
	int32	packetlength;
	int8	command;
};
#pragma pack()

#pragma pack(1)
struct UDP_Header_Struct{
	int8	eDonkeyID;
	int8	command;
};
#pragma pack()

#pragma pack(1)
struct LoginAnswer_Struct {
	uint32	clientid;
};
#pragma pack()

#pragma pack(1)
struct Requested_Block_Struct{
	uint32	StartOffset;
	uint32	EndOffset;
	uint32	packedsize;
	uchar	FileID[16];
	uint32  transferred; // Barry - This counts bytes completed
};
#pragma pack()

#pragma pack(1)
struct Requested_File_Struct{
	uchar	  fileid[16];
	uint32	  lastasked;
	uint8	  badrequests;
};
#pragma pack()

struct Pending_Block_Struct{
	Requested_Block_Struct*	block;
	z_stream               *zStream;       // Barry - Used to unzip packets
	uint32                  totalUnzipped; // Barry - This holds the total unzipped bytes for all packets so far
};

struct Gap_Struct{
	uint32 start;
	uint32 end;
};
#pragma pack(1)
struct ServerMet_Struct {
	uint32	ip;
	uint16	port;
	uint32	tagcount;
};
#pragma pack()


#endif
