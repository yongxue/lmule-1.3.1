#pragma once
#include "types.h"

class CIrcMain;
class CIrcSocket : public CAsyncSocket
{
public:
	CIrcSocket(CIrcMain* pIrcMain);
	virtual ~CIrcSocket();
	int SendString(CString message);

public:
	void Connect();
	public:
	virtual void OnConnect(int nErrorCode);
	virtual void OnReceive(int nErrorCode);
	virtual void OnClose(int nErrorCode);
private:
	CIrcMain*	m_pIrcMain;
};
