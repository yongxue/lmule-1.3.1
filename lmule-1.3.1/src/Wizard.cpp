// Wizard.cpp : implementation file
//

//#include "stdafx.h"
#include "wintypes.h"
#include "emule.h"
#include "Wizard.h"
#include "muuli_wdr.h"

#include <wx/listctrl.h>

// Wizard dialog

//IMPLEMENT_DYNAMIC(Wizard, CDialog)
Wizard::Wizard(wxWindow* pParent /*=NULL*/)
  //: CDialog(Wizard::IDD, pParent)
  : wxDialog(pParent,Wizard::IDD,_("Wizard"),wxDefaultPosition,wxDefaultSize,
	     wxDEFAULT_DIALOG_STYLE|wxSYSTEM_MENU)
{
	m_iOS = 0;
	m_iTotalDownload = 0;

	wxSizer* content=connWizDlg(this,TRUE);
	content->Show(this,TRUE);

	Centre();

	m_provider=wxStaticCast(FindWindowById(ID_PROVIDER),wxListCtrl);
}

Wizard::~Wizard()
{
}

#if 0
void Wizard::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PROVIDERS, m_provider);
	DDX_Radio(pDX, IDC_WIZ_XP_RADIO, m_iOS);
	DDX_Radio(pDX, IDC_WIZ_LOWDOWN_RADIO, m_iTotalDownload);
}
#endif

BEGIN_EVENT_TABLE(Wizard,wxDialog)
  EVT_BUTTON(ID_APPLY,Wizard::OnBnClickedApply)
  EVT_BUTTON(ID_CANCEL,Wizard::OnBnClickedCancel)
  EVT_RADIOBUTTON(ID_LOWDOWN,Wizard::OnBnClickedWizLowdownloadRadio)
  EVT_RADIOBUTTON(ID_MEDIUMDOWN,Wizard::OnBnClickedWizMediumdownloadRadio)
  EVT_RADIOBUTTON(ID_HIGHDOWN,Wizard::OnBnClickedWizHighdownloadRadio)
  EVT_LIST_ITEM_SELECTED(ID_PROVIDER,Wizard::OnNMClickProviders)
END_EVENT_TABLE()

#if 0
BEGIN_MESSAGE_MAP(Wizard, CDialog)
	ON_BN_CLICKED(IDC_WIZ_APPLY_BUTTON, OnBnClickedApply)
	ON_BN_CLICKED(IDC_WIZ_CANCEL_BUTTON, OnBnClickedCancel)
	ON_BN_CLICKED(IDC_WIZ_XP_RADIO, OnBnClickedWizRadioOsNtxp)
	ON_BN_CLICKED(IDC_WIZ_ME_RADIO, OnBnClickedWizRadioUs98me)
	ON_BN_CLICKED(IDC_WIZ_LOWDOWN_RADIO, OnBnClickedWizLowdownloadRadio)
	ON_BN_CLICKED(IDC_WIZ_MEDIUMDOWN_RADIO, OnBnClickedWizMediumdownloadRadio)
	ON_BN_CLICKED(IDC_WIZ_HIGHDOWN_RADIO, OnBnClickedWizHighdownloadRadio)
	ON_NOTIFY(NM_CLICK, IDC_PROVIDERS, OnNMClickProviders)
END_MESSAGE_MAP()
#endif


// Wizard message handlers

#include <wx/radiobut.h>
#define GetDlgItem(a,b) wxStaticCast(FindWindowById((a)),b)
#define IsDlgButtonChecked(a) GetDlgItem(a,wxRadioButton)->GetValue()

void Wizard::OnBnClickedApply(wxEvent& evt)
{
	char buffer[510];
	int upload, download;
	if(GetDlgItem(ID_TRUEDOWNLOAD,wxTextCtrl)->GetValue().Length())
	{ 
	  wxString tmp=GetDlgItem(ID_TRUEDOWNLOAD,wxTextCtrl)->GetValue();//GetWindowText(buffer,20);
	  download = atoi(tmp.GetData());
	}
	else
	{
		download = 0;
 	}
	if(GetDlgItem(ID_TRUEUPLOAD,wxTextCtrl)->GetValue().Length())
	{ 
	  wxString tmp=GetDlgItem(ID_TRUEUPLOAD,wxTextCtrl)->GetValue();//GetWindowText(buffer,20);
	  upload = atoi(tmp.GetData());
	}
	else
	{
		upload = 0;
	}

	if(IsDlgButtonChecked(ID_KBITS)==1) {upload/=8;download/=8;}

	if( upload > 0 && download > 0 ){
		app_prefs->prefs->maxupload = (uint16)(upload*.8);
		if( upload < 4 && download > upload*3 ){
			app_prefs->prefs->maxdownload = app_prefs->prefs->maxupload * 3;
			download = upload*3;
		}
		if( upload < 10 && download > upload*4 ){
			app_prefs->prefs->maxdownload = app_prefs->prefs->maxupload * 4;
			download = upload*4;
		}
		else
			app_prefs->prefs->maxdownload = (uint16)(download*.9);

		app_prefs->prefs->maxGraphDownloadRate = app_prefs->prefs->maxdownload;
		app_prefs->prefs->maxGraphUploadRate = app_prefs->prefs->maxupload;
		theApp.emuledlg->statisticswnd->SetARange(false,app_prefs->prefs->maxGraphUploadRate);
		theApp.emuledlg->statisticswnd->SetARange(true,app_prefs->prefs->maxGraphDownloadRate);

		if( m_iOS == 1 )
			app_prefs->prefs->maxconnections = 50;
		else{
		if( upload <= 7 )	
			app_prefs->prefs->maxconnections = 80;
		else if( upload < 12 )
			app_prefs->prefs->maxconnections = 200;	
		else if( upload < 25 )
			app_prefs->prefs->maxconnections = 400;
		else if( upload < 37 )
			app_prefs->prefs->maxconnections = 600;
		else
			app_prefs->prefs->maxconnections = 800;	

		}
		if( m_iOS == 1 )
			download = download/2;

		if( download <= 7 ){
			switch( m_iTotalDownload ){
				case 0:
					app_prefs->prefs->maxsourceperfile = 100;
				break;
				case 1:
					app_prefs->prefs->maxsourceperfile = 60;
				break;
				case 2:
					app_prefs->prefs->maxsourceperfile = 40;
				break;
			}
		}
		else if( download < 62 ){
			switch( m_iTotalDownload ){
				case 0:
					app_prefs->prefs->maxsourceperfile = 300;
				break;
				case 1:
					app_prefs->prefs->maxsourceperfile = 200;
				break;
				case 2:
					app_prefs->prefs->maxsourceperfile = 100;
				break;
			}
		}
		else if( download < 187 ){
			switch( m_iTotalDownload ){
				case 0:
					app_prefs->prefs->maxsourceperfile = 500;
				break;
				case 1:
					app_prefs->prefs->maxsourceperfile = 400;
				break;
				case 2:
					app_prefs->prefs->maxsourceperfile = 350;
				break;
			}
		}
		else if( download <= 312 ){
			switch( m_iTotalDownload ){
				case 0:
					app_prefs->prefs->maxsourceperfile = 800;
				break;
				case 1:
					app_prefs->prefs->maxsourceperfile = 600;
				break;
				case 2:
					app_prefs->prefs->maxsourceperfile = 400;
				break;
			}
		}
		else {
			switch( m_iTotalDownload ){
			case 0:
				app_prefs->prefs->maxsourceperfile = 1000;
				break;
			case 1:
				app_prefs->prefs->maxsourceperfile = 750;
				break;
			case 2:
				app_prefs->prefs->maxsourceperfile = 500;
				break;
			}
		}
	}
	theApp.emuledlg->preferenceswnd->m_wndConnection->LoadSettings();
	theApp.emuledlg->preferenceswnd->m_wndTweaks->LoadSettings();
	printf("TODO: TWEAKS missing \n");
	//CDialog::OnOK();
	EndModal(0);
}

void Wizard::OnBnClickedCancel(wxEvent& evt)
{
  //CDialog::OnCancel();
  EndModal(1);
}

void Wizard::OnBnClickedWizRadioOsNtxp(wxEvent& evt)
{
	m_iOS = 0;
}

void Wizard::OnBnClickedWizRadioUs98me(wxEvent& evt)
{
	m_iOS = 1;
}

void Wizard::OnBnClickedWizLowdownloadRadio(wxEvent& evt)
{
	m_iTotalDownload = 0;
}

void Wizard::OnBnClickedWizMediumdownloadRadio(wxEvent& evt)
{
	m_iTotalDownload = 1;
}

void Wizard::OnBnClickedWizHighdownloadRadio(wxEvent& evt)
{
	m_iTotalDownload = 2;
}

void Wizard::OnBnClickedWizResetButton(wxEvent& evt)
{
	CString strBuffer;
	strBuffer.Format("%i", 0);
	GetDlgItem(ID_TRUEDOWNLOAD,wxTextCtrl)->SetValue(strBuffer); 
	GetDlgItem(ID_TRUEUPLOAD,wxTextCtrl)->SetValue(strBuffer); 
}

#define CheckDlgButton(a,b) GetDlgItem(a,wxRadioButton)->SetValue(b)

BOOL Wizard::OnInitDialog(){
  //CDialog::OnInitDialog();

  //CheckDlgButton(IDC_WIZ_XP_RADIO,1);
  CheckDlgButton(ID_LOWDOWN,1);
  CheckDlgButton(ID_KBITS,1);

  printf("void olla %lx\n",GetDlgItem(ID_TRUEDOWNLOAD,wxTextCtrl));
  CString temp;
  temp.Format("%u",app_prefs->prefs->maxGraphDownloadRate *8);	GetDlgItem(ID_TRUEDOWNLOAD,wxTextCtrl)->SetValue(temp); 
  temp.Format("%u",app_prefs->prefs->maxGraphUploadRate*8);GetDlgItem(ID_TRUEUPLOAD,wxTextCtrl)->SetValue(temp); 

  m_provider->InsertColumn(0,GetResString(IDS_PW_CONNECTION),wxLIST_FORMAT_LEFT, 160);
  m_provider->InsertColumn(1,GetResString(IDS_WIZ_DOWN),wxLIST_FORMAT_LEFT, 90);
  m_provider->InsertColumn(2,GetResString(IDS_WIZ_UP),wxLIST_FORMAT_LEFT, 90);

  int i=0;
  i=m_provider->InsertItem(0,GetResString(IDS_WIZARD_CUSTOM) );m_provider->SetItem(0,1,GetResString(IDS_WIZARD_ENTERBELOW));m_provider->SetItem(0,2,GetResString(IDS_WIZARD_ENTERBELOW));
  i=m_provider->InsertItem(1,_("56-k Modem"));m_provider->SetItem(1,1,"56");m_provider->SetItem(1,2,"56");
  i=m_provider->InsertItem(2,_("ISDN"));m_provider->SetItem(2,1,"64");m_provider->SetItem(2,2,"64");
  i=m_provider->InsertItem(3,_("ISDN 2x"));m_provider->SetItem(3,1,"128");m_provider->SetItem(3,2,"128");
  i=m_provider->InsertItem(4 ,_("DSL"));m_provider->SetItem(4,1,"256");m_provider->SetItem(4,2,"128");
  i=m_provider->InsertItem(5,_("DSL"));m_provider->SetItem(5,1,"384");m_provider->SetItem(5,2,"91");
  i=m_provider->InsertItem(6,_("DSL"));m_provider->SetItem(6,1,"512");m_provider->SetItem(6,2,"91");
  i=m_provider->InsertItem(7 ,_("DSL"));m_provider->SetItem(7,1,"512");m_provider->SetItem(7,2,"128");
  i=m_provider->InsertItem(8,_("DSL"));m_provider->SetItem(8,1,"640");m_provider->SetItem(8,2,"90");
  i=m_provider->InsertItem(9,_("DSL (T-DSL, newDSL, 1&1-DSL"));m_provider->SetItem(9,1,"768");m_provider->SetItem(9,2,"128");
  i=m_provider->InsertItem(10,_("DSL (QDSL, NGI-DSL"));m_provider->SetItem(10,1,"1024");m_provider->SetItem(10,2,"256");
  i=m_provider->InsertItem(11,_("DSL 1500 ('TDSL 1500')"));m_provider->SetItem(11,1,"1500");m_provider->SetItem(11,2,"192");
  i=m_provider->InsertItem(12,_("DSL 1600"));m_provider->SetItem(12,1,"1600");m_provider->SetItem(12,2,"90");
  i=m_provider->InsertItem(13,_("DSL 2000"));m_provider->SetItem(13,1,"2000");m_provider->SetItem(13,2,"300");
  i=m_provider->InsertItem(14,_("Cable"));m_provider->SetItem(14,1,"187");m_provider->SetItem(14,2,"32");
  i=m_provider->InsertItem(15,_("Cable"));m_provider->SetItem(15,1,"187");m_provider->SetItem(15,2,"64");
  i=m_provider->InsertItem(16,_("T1"));m_provider->SetItem(16,1,"1500");m_provider->SetItem(16,2,"1500");
  i=m_provider->InsertItem(17,_("T3+"));m_provider->SetItem(17,1,"44 Mbps");m_provider->SetItem(17,2,"44 Mbps");
  
  //m_provider.SetSelectionMark(0);

	Localize();

	return TRUE;
}

void Wizard::Localize(void){
#if 0
	GetDlgItem(IDC_WIZ_OS_FRAME)->SetWindowText(GetResString(IDS_WIZ_OS_FRAME));
	GetDlgItem(IDC_WIZ_TRUEUPLOAD_TEXT)->SetWindowText(GetResString(IDS_WIZ_TRUEUPLOAD_TEXT));
	GetDlgItem(IDC_WIZ_TRUEDOWNLOAD_TEXT)->SetWindowText(GetResString(IDS_WIZ_TRUEDOWNLOAD_TEXT));
	GetDlgItem(IDC_WIZ_APPLY_BUTTON)->SetWindowText(GetResString(IDS_PW_APPLY));
	GetDlgItem(IDC_WIZ_CANCEL_BUTTON)->SetWindowText(GetResString(IDS_CANCEL));
	GetDlgItem(IDC_WIZ_HOTBUTTON_FRAME)->SetWindowText(GetResString(IDS_WIZ_CTFRAME));
	GetDlgItem(IDC_CTINFO)->SetWindowText(GetResString(IDS_CTINFO));

	GetDlgItem(IDC_CTINFO)->SetWindowText(GetResString(IDS_CTINFO));
	GetDlgItem(IDC_CTINFO)->SetWindowText(GetResString(IDS_CTINFO));

	GetDlgItem(IDC_WIZ_CONCURENTDOWN_FRAME)->SetWindowText(GetResString(IDS_CONCURDWL));
	GetDlgItem(IDC_UNIT)->SetWindowText(GetResString(IDS_UNIT));
#endif
}

void Wizard::SetCustomItemsActivation() {
	BOOL active=(m_provider->GetNextItem(-1,wxLIST_NEXT_ALL,wxLIST_STATE_SELECTED)<1);

	GetDlgItem(ID_TRUEUPLOAD,wxTextCtrl)->Enable(active);
	GetDlgItem(ID_TRUEDOWNLOAD,wxTextCtrl)->Enable(active);
	GetDlgItem(ID_KBITS,wxRadioButton )->Enable(active);
	GetDlgItem(ID_KBYTES,wxRadioButton )->Enable(active);
}

void Wizard::OnNMClickProviders(wxListEvent& evt)
{
  //LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	

  SetCustomItemsActivation();
  uint16 up,down;
  int cursel=m_provider->GetNextItem(-1,wxLIST_NEXT_ALL,wxLIST_STATE_SELECTED);
  switch (cursel) {
  case 1 : down=56;up=33; break;
  case 2 : down=64;up=64; break;
  case 3 : down=128;up=128; break;
  case 4 : down=256;up=128; break;
  case 5 : down=384;up=91; break;
  case 6 : down=512;up=91; break;
  case 7 : down=512;up=128; break;
  case 8 : down=640;up=90; break;
  case 9 : down=768;up=128; break;
  case 10 : down=1024;up=256; break;
  case 11 : down=1500;up=192; break;
  case 12: down=1600;up=90; break;
  case 13: down=2000;up=300; break;
  case 14: down=187;up=32; break;
  case 15: down=187;up=64; break;
  case 16: down=1500;up=1500; break;
  case 17: down=44000;up=44000; break;
    
  default: return;
  }
  CString temp;
  temp.Format("%u",down);	GetDlgItem(ID_TRUEDOWNLOAD,wxTextCtrl)->SetValue(temp); 
  temp.Format("%u",up);GetDlgItem(ID_TRUEUPLOAD,wxTextCtrl)->SetValue(temp); 
  CheckDlgButton(ID_KBITS,1);
  CheckDlgButton(ID_KBYTES,0);
  
  //*pResult = 0;
}

