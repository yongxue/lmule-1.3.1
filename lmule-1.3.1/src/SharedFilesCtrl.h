//this file is part of eMule
//Copyright (C)2002 Merkur ( merkur-@users.sourceforge.net / http://www.emule-project.net )
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


#ifndef _SHAREDFILESTRL_H
#define _SHAREDFILESTRL_H

#include "SharedFileList.h"
#include "KnownFile.h"
#include "types.h"
#include "MuleListCtrl.h"
#include "wx/menu.h"

// CSharedFilesCtrl
class CSharedFileList;
class CSharedFilesCtrl : public CMuleListCtrl
{
  //	DECLARE_DYNAMIC(CSharedFilesCtrl)

public:
	CSharedFilesCtrl();
	CSharedFilesCtrl(wxWindow*& parent,int id,const wxPoint& pos,wxSize siz,int flags);
	virtual ~CSharedFilesCtrl();
	void	Init();
	void InitSort();
	void	ShowFileList(CSharedFileList* in_sflist);
	void	ShowFile(CKnownFile* file);
	void	ShowFile(CKnownFile* file,uint32 itemnr);
	void	RemoveFile(CKnownFile* toremove);
	void	UpdateFile(CKnownFile* file,uint32 pos);
	void	UpdateItem(CKnownFile* toupdate);
	void	Localize();
	void	ShowFilesCount();
protected:
	static int  SortProc(long lParam1, long lParam2, long lParamSort);
	//afx_msg	void OnColumnClick( NMHDR* pNMHDR, LRESULT* pResult);
	void OnColumnClick(wxListEvent& evt);
	//DECLARE_MESSAGE_MAP()
	void OnNMRclick(wxListEvent& evt);
	DECLARE_EVENT_TABLE()

	virtual bool ProcessEvent(wxEvent& evt);
private:
	//CTitleMenu	   m_SharedFilesMenu;
	wxMenu* m_SharedFilesMenu;
	wxMenu		   m_PrioMenu;
	wxMenu		   m_PermMenu;
	CSharedFileList* sflist;
	bool		   sortstat[3];
public:
	//afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
protected:
	//virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
};
#endif
