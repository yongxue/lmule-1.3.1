//this file is part of eMule
//Copyright (C)2002 Merkur ( merkur-@users.sourceforge.net / http://www.emule-project.net )
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

// ClientDetailDialog.cpp : implementation file
//

//#include "stdafx.h"
#include "wintypes.h"
#include "emule.h"
#include "ClientDetailDialog.h"
#include "otherfunctions.h"
#include "muuli_wdr.h"

#include "wx/xrc/xmlres.h"
#include "wx/xrc/xh_all.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

// CClientDetailDialog dialog
#define GetDlgItem(x,clas) XRCCTRL(*this,#x,clas)
#define IsDlgButtonChecked(x) XRCCTRL(*this,#x,wxCheckBox)->GetValue()
#define CheckDlgButton(x,y) XRCCTRL(*this,#x,wxCheckBox)->SetValue(y)

BEGIN_EVENT_TABLE(CClientDetailDialog,wxDialog)
  EVT_BUTTON(ID_CLOSEWND,CClientDetailDialog::OnBnClose)
END_EVENT_TABLE()

//IMPLEMENT_DYNAMIC(CClientDetailDialog, CDialog)
CClientDetailDialog::CClientDetailDialog(wxWindow* parent,CUpDownClient* client)
  : wxDialog(parent,9997,"Client Details",wxDefaultPosition,wxDefaultSize,
	     wxDEFAULT_DIALOG_STYLE|wxSYSTEM_MENU)
  //: CDialog(CClientDetailDialog::IDD, 0)
{
	m_client = client;

	wxSizer* content=clientDetails(this,TRUE);
	content->Show(this,TRUE);

	Centre();

	OnInitDialog();
}

CClientDetailDialog::~CClientDetailDialog()
{
}

#if 0 
void CClientDetailDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}
#endif

void CClientDetailDialog::OnBnClose(wxEvent& evt)
{
  EndModal(0);
}

#define GetDlgItem(a,b) wxStaticCast(FindWindowById((a)),b)

BOOL CClientDetailDialog::OnInitDialog(){
  //CDialog::OnInitDialog();
	Localize();
	char buffer[100];
	if (m_client->GetUserName())
		GetDlgItem(ID_DNAME,wxStaticText)->SetLabel(m_client->GetUserName());
	else
		GetDlgItem(ID_DNAME,wxStaticText)->SetLabel("?");
	
	if (m_client->GetUserName()){
		buffer[0] = 0;
		for (uint16 i = 0;i != 16;i++)
			sprintf(buffer,"%s%02X",buffer,m_client->GetUserHash()[i]);
		GetDlgItem(ID_DHASH,wxStaticText)->SetLabel(buffer);
	}
	else
		GetDlgItem(ID_DHASH,wxStaticText)->SetLabel("?");
	
	switch(m_client->GetClientSoft()){
		case SO_UNKNOWN:
			GetDlgItem(ID_DSOFT,wxStaticText)->SetLabel("?");
			GetDlgItem(ID_DVERSION,wxStaticText)->SetLabel("?");
			break;
		case SO_EMULE:
		case SO_OLDEMULE:
			GetDlgItem(ID_DSOFT,wxStaticText)->SetLabel(_("eMule"));
			if (m_client->GetMuleVersion()){
				sprintf(buffer,"v0.%02X",m_client->GetMuleVersion());
				GetDlgItem(ID_DVERSION,wxStaticText)->SetLabel(buffer);
			}
			else	
				GetDlgItem(ID_DVERSION,wxStaticText)->SetLabel("?");
		break;
		case SO_LMULE:
			GetDlgItem(ID_DSOFT,wxStaticText)->SetLabel(_("lMule"));
			if (m_client->GetMuleVersion()){
				sprintf(buffer,"v0.%02X",m_client->GetMuleVersion());
				GetDlgItem(ID_DVERSION,wxStaticText)->SetLabel(buffer);
			}
			else	
				GetDlgItem(ID_DVERSION,wxStaticText)->SetLabel("?");
		break;
		case SO_EDONKEY:
			GetDlgItem(ID_DSOFT,wxStaticText)->SetLabel(_("eDonkey"));
			sprintf(buffer,"v%i",m_client->GetVersion());
			GetDlgItem(ID_DVERSION,wxStaticText)->SetLabel(buffer);
		break;
		case SO_MLDONKEY:
			GetDlgItem(ID_DSOFT,wxStaticText)->SetLabel(_("Old MlDonkey"));
			sprintf(buffer,"v%i",m_client->GetVersion());
			GetDlgItem(ID_DVERSION,wxStaticText)->SetLabel(buffer);
		break;
		case SO_NEW_MLDONKEY:
			GetDlgItem(ID_DSOFT,wxStaticText)->SetLabel(_("New MlDonkey"));
			sprintf(buffer,"v%i",m_client->GetVersion());
			GetDlgItem(ID_DVERSION,wxStaticText)->SetLabel(buffer);
		break;
	}

	sprintf(buffer,"%u (%s)",m_client->GetUserID(),(m_client->HasLowID() ? GetResString(IDS_PRIOLOW).GetData():GetResString(IDS_PRIOHIGH).GetData()));
	GetDlgItem(ID_DID,wxStaticText)->SetLabel(buffer);
	
	sprintf(buffer,"%s:%i",m_client->GetFullIP(),m_client->GetUserPort());
	GetDlgItem(ID_DIP,wxStaticText)->SetLabel(buffer);

	if (m_client->GetServerIP()){
		in_addr server;
		server.s_addr = m_client->GetServerIP();
		GetDlgItem(ID_DSIP,wxStaticText)->SetLabel(inet_ntoa(server));
		
		CServer* cserver = theApp.serverlist->GetServerByAddress(inet_ntoa(server), m_client->GetServerPort()); 
		if (cserver)
			GetDlgItem(ID_DSNAME,wxStaticText)->SetLabel(cserver->GetListName());
		else
			GetDlgItem(ID_DSNAME,wxStaticText)->SetLabel("?");
	}
	else{
		GetDlgItem(ID_DSIP,wxStaticText)->SetLabel("?");
		GetDlgItem(ID_DSNAME,wxStaticText)->SetLabel("?");
	}

	CKnownFile* file = theApp.sharedfiles->GetFileByID(m_client->reqfileid);
	if (file)
		GetDlgItem(ID_DDOWNLOADING,wxStaticText)->SetLabel(file->GetFileName());
	else
		GetDlgItem(ID_DDOWNLOADING,wxStaticText)->SetLabel("-");


	GetDlgItem(ID_DDUP,wxStaticText)->SetLabel(CastItoXBytes(m_client->GetTransferedDown()));
       
	GetDlgItem(ID_DDOWN,wxStaticText)->SetLabel(CastItoXBytes(m_client->GetTransferedUp()));

	sprintf(buffer,"%.1f %s",(float)m_client->GetDownloadDatarate()/1024,GetResString(IDS_KBYTESEC).GetData());
	GetDlgItem(ID_DAVUR,wxStaticText)->SetLabel(buffer);

	sprintf(buffer,"%.1f %s",(float)m_client->GetDatarate()/1024,GetResString(IDS_KBYTESEC).GetData());
	GetDlgItem(ID_DAVDR,wxStaticText)->SetLabel(buffer);
	
	if (m_client->Credits()){		
		GetDlgItem(ID_DUPTOTAL,wxStaticText)->SetLabel(CastItoXBytes(m_client->Credits()->GetDownloadedTotal()));		
		GetDlgItem(ID_DDOWNTOTAL,wxStaticText)->SetLabel(CastItoXBytes(m_client->Credits()->GetUploadedTotal()));
		sprintf(buffer,"%.1f",(float)m_client->Credits()->GetScoreRatio());
		GetDlgItem(ID_DRATIO,wxStaticText)->SetLabel(buffer);
	}
	else{
		GetDlgItem(ID_DDOWNTOTAL,wxStaticText)->SetLabel("?");
		GetDlgItem(ID_DUPTOTAL,wxStaticText)->SetLabel("?");
		GetDlgItem(ID_DRATIO,wxStaticText)->SetLabel("?");
	}

	if (m_client->GetUserName()){
		sprintf(buffer,"%.1f",(float)m_client->GetScore(m_client->IsDownloading(),true));
		GetDlgItem(ID_DRATING,wxStaticText)->SetLabel(buffer);
	}
	else
		GetDlgItem(ID_DRATING,wxStaticText)->SetLabel("?");;

	if (m_client->GetUploadState() != US_NONE){
		sprintf(buffer,"%u",m_client->GetScore(m_client->IsDownloading(),false));
		GetDlgItem(ID_DSCORE,wxStaticText)->SetLabel(buffer);
	}
	else
		GetDlgItem(ID_DSCORE,wxStaticText)->SetLabel("-");
	return true;
}

#if 0
BEGIN_MESSAGE_MAP(CClientDetailDialog, CDialog)
END_MESSAGE_MAP()
#endif

// CClientDetailDialog message handlers
void CClientDetailDialog::Localize(){
#if 0
	GetDlgItem(IDC_STATIC30)->SetWindowText(GetResString(IDS_CD_GENERAL));
	GetDlgItem(IDC_STATIC31)->SetWindowText(GetResString(IDS_CD_UNAME));
	GetDlgItem(IDC_STATIC32)->SetWindowText(GetResString(IDS_CD_UHASH));
	GetDlgItem(IDC_STATIC33)->SetWindowText(GetResString(IDS_CD_CSOFT));
	GetDlgItem(IDC_STATIC34)->SetWindowText(GetResString(IDS_CD_UIP));
	GetDlgItem(IDC_STATIC35)->SetWindowText(GetResString(IDS_CD_SIP));
	GetDlgItem(IDC_STATIC36)->SetWindowText(GetResString(IDS_CD_VERSION));
	GetDlgItem(IDC_STATIC37)->SetWindowText(GetResString(IDS_CD_UID));
	GetDlgItem(IDC_STATIC38)->SetWindowText(GetResString(IDS_CD_SNAME));

	GetDlgItem(IDC_STATIC40)->SetWindowText(GetResString(IDS_CD_TRANS));
	GetDlgItem(IDC_STATIC41)->SetWindowText(GetResString(IDS_CD_CDOWN));
	GetDlgItem(IDC_STATIC42)->SetWindowText(GetResString(IDS_CD_DOWN));
	GetDlgItem(IDC_STATIC43)->SetWindowText(GetResString(IDS_CD_ADOWN));
	GetDlgItem(IDC_STATIC44)->SetWindowText(GetResString(IDS_CD_TDOWN));
	GetDlgItem(IDC_STATIC45)->SetWindowText(GetResString(IDS_CD_UP));
	GetDlgItem(IDC_STATIC46)->SetWindowText(GetResString(IDS_CD_AUP));
	GetDlgItem(IDC_STATIC47)->SetWindowText(GetResString(IDS_CD_TUP));

	GetDlgItem(IDC_STATIC50)->SetWindowText(GetResString(IDS_CD_SCORES));
	GetDlgItem(IDC_STATIC51)->SetWindowText(GetResString(IDS_CD_MOD));
	GetDlgItem(IDC_STATIC52)->SetWindowText(GetResString(IDS_CD_RATING));
	GetDlgItem(IDC_STATIC53)->SetWindowText(GetResString(IDS_CD_USCORE));

	GetDlgItem(IDOK)->SetWindowText(GetResString(IDS_FD_CLOSE));

	SetWindowText(GetResString(IDS_CD_TITLE));
#endif
}
