#ifndef PPGSTATS_H
#define PPGSTATS_H

// CPPgStats dialog
#include <wx/panel.h>
#include <wx/combobox.h>
#include "Preferences.h"

class CPPgStats : public wxPanel //CPropertyPage
{
  //DECLARE_DYNAMIC(CPPgStats)
  DECLARE_DYNAMIC_CLASS(CPPgStats)
    CPPgStats() {};
public:
	CPPgStats(wxWindow* parent);
	virtual ~CPPgStats();

	void SetPrefs(CPreferences* in_prefs) {	app_prefs = in_prefs; }

// Dialog Data
	enum { IDD = IDD_PPG_STATS };
protected:
	CPreferences *app_prefs;
protected:
#if 0
	//{{AFX_MSG(CSpinToolBar)
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	//}}AFX_MSG
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	DECLARE_MESSAGE_MAP()
#endif
	  DECLARE_EVENT_TABLE();
	wxComboBox* m_colors;
	void OnHScroll(wxScrollEvent& evt);
	void OnCbnSelchangeColorselector(wxCommandEvent& evt);
	void ShowInterval();
	void OnChangeColor(wxEvent& evt);
public:
	//virtual BOOL OnInitDialog();
private:
	int mystats1,mystats2,mystats3;
public:
	virtual BOOL OnApply();
	void Localize(void);
	void LoadSettings();
};

#endif
