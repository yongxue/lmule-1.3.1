// PPgServer.cpp : implementation file
//

//#include "stdafx.h"
#include "wintypes.h"
#include "emule.h"
#include "PPgServer.h"
#include "EditServerListDlg.h"

#include <wx/checkbox.h>
#include <wx/notebook.h>
#include <wx/slider.h>
#include "wx/xrc/xmlres.h"
#include "wx/xrc/xh_all.h"

// CPPgServer dialog
#define GetDlgItem(x,clas) XRCCTRL(*this,#x,clas)
#define IsDlgButtonChecked(x) XRCCTRL(*this,#x,wxCheckBox)->GetValue()
#define CheckDlgButton(x,y) XRCCTRL(*this,#x,wxCheckBox)->SetValue(y)

//IMPLEMENT_DYNAMIC(CPPgServer, CPropertyPage)
IMPLEMENT_DYNAMIC_CLASS(CPPgServer,wxPanel)

CPPgServer::CPPgServer(wxWindow* parent)
  :wxPanel(parent,CPPgServer::IDD) //: CPropertyPage(CPPgServer::IDD)
{
  wxNotebook* book=(wxNotebook*)parent;

  wxPanel*page1=wxXmlResource::Get()->LoadPanel(this,"DLG_PPG_SERVER");
  book->AddPage(this,_("Server"));

  // looks stupid? it is :)
  SetSize(page1->GetSize().GetWidth(),page1->GetSize().GetHeight()+48);
  page1->SetSize(GetSize());

  Localize();
}

CPPgServer::~CPPgServer()
{
}

#if 0
void CPPgServer::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
}
#endif

BEGIN_EVENT_TABLE(CPPgServer,wxPanel)
	EVT_BUTTON(XRCID("IDC_EDITADR"), CPPgServer::OnBnClickedEditadr)
END_EVENT_TABLE()

#if 0
BEGIN_MESSAGE_MAP(CPPgServer, CPropertyPage)
	ON_EN_CHANGE(IDC_SERVERRETRIES, OnEnChangeServerretries)
	ON_BN_CLICKED(IDC_AUTOSERVER, OnBnClickedAutoserver)
	ON_BN_CLICKED(IDC_UPDATESERVERCONNECT, OnBnClickedUpdateserverconnect)
	ON_BN_CLICKED(IDC_UPDATESERVERCLIENT, OnBnClickedUpdateserverclient)
	ON_BN_CLICKED(IDC_FILTER, OnBnClickedFilter)
	ON_BN_CLICKED(IDC_SCORE, OnBnClickedScore)
	ON_BN_CLICKED(IDC_REMOVEDEAD, OnBnClickedRemovedead)
END_MESSAGE_MAP()
#endif

// CPPgServer message handlers

#if 0
void CPPgServer::OnBnClickedCheck1()
{
	// TODO: Add your control notification handler code here
}

BOOL CPPgServer::OnInitDialog()
{
	CPropertyPage::OnInitDialog();

	LoadSettings();
	Localize();

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}
#endif

#include <wx/checkbox.h>

void CPPgServer::LoadSettings(void)
{
//	switch(app_prefs->prefs->deadserver)
//	{
//		case 1:	this->CheckDlgButton(IDC_MARKDEAD,1);	break;
//		case 2:	this->CheckDlgButton(IDC_DELETEDEAD,1);	break;
//	}
  
  CString strBuffer;
  strBuffer.Format("%d", app_prefs->prefs->deadserverretries);
  GetDlgItem(IDC_SERVERRETRIES,wxTextCtrl)->SetValue(strBuffer);
  
  
  if(app_prefs->IsSafeServerConnectEnabled())
    CheckDlgButton(IDC_SAFESERVERCONNECT,1);
  else
    CheckDlgButton(IDC_SAFESERVERCONNECT,0);
  
  if(app_prefs->prefs->m_bmanualhighprio)
    CheckDlgButton(IDC_MANUALSERVERHIGHPRIO,1);
  else
    CheckDlgButton(IDC_MANUALSERVERHIGHPRIO,0);
  
  if(app_prefs->GetSmartIdCheck())
    CheckDlgButton(IDC_SMARTIDCHECK,1);
  else
    CheckDlgButton(IDC_SMARTIDCHECK,0);
  
  if(app_prefs->prefs->deadserver)
    CheckDlgButton(IDC_REMOVEDEAD,1);
  else
    CheckDlgButton(IDC_REMOVEDEAD,0);
  
  if(app_prefs->prefs->autoserverlist)
    CheckDlgButton(IDC_AUTOSERVER,1);
  else
    CheckDlgButton(IDC_AUTOSERVER,0);
  
  if(app_prefs->prefs->addserversfromserver)
    CheckDlgButton(IDC_UPDATESERVERCONNECT, 1);
  else
    CheckDlgButton(IDC_UPDATESERVERCONNECT, 0);
  
  if(app_prefs->prefs->addserversfromclient)
    CheckDlgButton(IDC_UPDATESERVERCLIENT, 1);
  else
    CheckDlgButton(IDC_UPDATESERVERCLIENT, 0);
  
  if(app_prefs->prefs->filterBadIP)
    CheckDlgButton(IDC_FILTER,1);
  else
    CheckDlgButton(IDC_FILTER,0);
  
  if(app_prefs->prefs->scorsystem)
    CheckDlgButton(IDC_SCORE,1);
  else
    CheckDlgButton(IDC_SCORE,0);
  
  // Barry
  if(app_prefs->prefs->autoconnectstaticonly)
    CheckDlgButton(IDC_AUTOCONNECTSTATICONLY,1);
  else
    CheckDlgButton(IDC_AUTOCONNECTSTATICONLY,0);
}

void CPPgServer::OnApply()
{	
  wxString buffer;
	
  //	if(IsDlgButtonChecked(IDC_MARKDEAD))
  //		app_prefs->prefs->deadserver = 1;
  //	else
  //		app_prefs->prefs->deadserver = 2;
  app_prefs->SetSafeServerConnectEnabled((int8)IsDlgButtonChecked(IDC_SAFESERVERCONNECT));
  
  if(IsDlgButtonChecked(IDC_SMARTIDCHECK))
    app_prefs->prefs->smartidcheck = true;
  else
    app_prefs->prefs->smartidcheck = false;
  
  if(IsDlgButtonChecked(IDC_MANUALSERVERHIGHPRIO))
    app_prefs->prefs->m_bmanualhighprio = true;
  else
    app_prefs->prefs->m_bmanualhighprio = false;
  
  app_prefs->prefs->deadserver = (int8)IsDlgButtonChecked(IDC_REMOVEDEAD);
  
  if(GetDlgItem(IDC_SERVERRETRIES,wxTextCtrl)->GetValue().Length())
    {
      buffer=GetDlgItem(IDC_SERVERRETRIES,wxTextCtrl)->GetValue();
      app_prefs->prefs->deadserverretries = (atoi(buffer)) ? atoi(buffer) : 5;
    }
  if(app_prefs->prefs->deadserverretries < 1) 
    app_prefs->prefs->deadserverretries = 5;
  
  app_prefs->prefs->scorsystem = (int8)IsDlgButtonChecked(IDC_SCORE);
  app_prefs->prefs->autoserverlist = (int8)IsDlgButtonChecked(IDC_AUTOSERVER);
  app_prefs->prefs->addserversfromserver = (int8)IsDlgButtonChecked(IDC_UPDATESERVERCONNECT);
  app_prefs->prefs->addserversfromclient = (int8)IsDlgButtonChecked(IDC_UPDATESERVERCLIENT);
  app_prefs->prefs->filterBadIP = (int8)IsDlgButtonChecked(IDC_FILTER);
  // Barry
  app_prefs->prefs->autoconnectstaticonly = (int8)IsDlgButtonChecked(IDC_AUTOCONNECTSTATICONLY);
  
  //	app_prefs->Save();
  LoadSettings();
  
  //SetModified();
  //return CPropertyPage::OnApply();
}


void CPPgServer::Localize(void)
{
  if(1)
    {
      //SetWindowText(GetResString(IDS_PW_SERVER));
      
      GetDlgItem(IDC_REMOVEDEAD,wxControl)->SetLabel(GetResString(IDS_PW_RDEAD));
      GetDlgItem(IDC_RETRIES_LBL,wxControl)->SetLabel(GetResString(IDS_PW_RETRIES));
      GetDlgItem(IDC_UPDATESERVERCONNECT,wxControl)->SetLabel(GetResString(IDS_PW_USC));
      GetDlgItem(IDC_UPDATESERVERCLIENT,wxControl)->SetLabel(GetResString(IDS_PW_UCC));
      GetDlgItem(IDC_FILTER,wxControl)->SetLabel(GetResString(IDS_PW_FILTER));
      GetDlgItem(IDC_AUTOSERVER,wxControl)->SetLabel(GetResString(IDS_PW_USS));
      GetDlgItem(IDC_SMARTIDCHECK,wxControl)->SetLabel(GetResString(IDS_SMARTLOWIDCHECK));
      GetDlgItem(IDC_SAFESERVERCONNECT,wxControl)->SetLabel(GetResString(IDS_PW_FASTSRVCON));
      GetDlgItem(IDC_SCORE,wxControl)->SetLabel(GetResString(IDS_PW_SCORE));
      GetDlgItem(IDC_MANUALSERVERHIGHPRIO,wxControl)->SetLabel(GetResString(IDS_MANUALSERVERHIGHPRIO));
      GetDlgItem(IDC_EDITADR,wxControl)->SetLabel(GetResString(IDS_EDITLIST));
      
      // Barry
      GetDlgItem(IDC_AUTOCONNECTSTATICONLY,wxControl)->SetLabel(GetResString(IDS_PW_AUTOCONNECTSTATICONLY));
    }
}

void CPPgServer::OnBnClickedEditadr(wxEvent& evt)
{
  char* fullpath = new char[strlen(theApp.glob_prefs->GetAppDir())+13];
  sprintf(fullpath,"%sadresses.dat",theApp.glob_prefs->GetAppDir());

  EditServerListDlg* test=new EditServerListDlg(this,
		  				_("Edit Serverlist"),
		                                _("Add here URL's to download server.met files.\nOnly one url on each line."),
						fullpath);
  test->ShowModal();
  
  delete[] fullpath;
  delete test;
}
