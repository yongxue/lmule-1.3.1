// PPgStats.cpp : implementation file
//

//#include "stdafx.h"
#include "wintypes.h"
#include "emule.h"
#include "PPgStats.h"

#include <wx/checkbox.h>
#include <wx/notebook.h>
#include <wx/slider.h>
#include "wx/xrc/xmlres.h"
#include "wx/xrc/xh_all.h"

#define GetDlgItem(x,clas) XRCCTRL(*this,#x,clas)
#define IsDlgButtonChecked(x) XRCCTRL(*this,#x,wxCheckBox)->GetValue()
#define CheckDlgButton(x,y) XRCCTRL(*this,#x,wxCheckBox)->SetValue(y)
#define GetRValue(rgb) (((rgb)>>16)&0xff)
#define GetGValue(rgb) (((rgb)>>8)&0xff)
#define GetBValue(rgb) ((rgb)&0xff)

// CPPgStats dialog

IMPLEMENT_DYNAMIC_CLASS(CPPgStats,wxPanel)

//IMPLEMENT_DYNAMIC(CPPgStats, CPropertyPage)
CPPgStats::CPPgStats(wxWindow* parent)
	: wxPanel(parent,CPPgStats::IDD)
{
  wxNotebook* book=(wxNotebook*)parent;

  wxPanel* page1=wxXmlResource::Get()->LoadPanel(this,"DLG_PPG_STATS");
  book->AddPage(this,_("Statistics"));

  //SetSize(wxSize(320,200));

  // looks stupid? it is :)
  SetSize(page1->GetSize().GetWidth(),page1->GetSize().GetHeight()+48);
  page1->SetSize(GetSize());

  Localize();
}

CPPgStats::~CPPgStats()
{
}

BEGIN_EVENT_TABLE(CPPgStats,wxPanel)
  EVT_SCROLL(CPPgStats::OnHScroll)
  EVT_BUTTON(XRCID("IDC_COLOR_BUTTON"),CPPgStats::OnChangeColor)
  EVT_COMBOBOX(XRCID("IDC_COLORSELECTOR"),CPPgStats::OnCbnSelchangeColorselector)
END_EVENT_TABLE()

#include <wx/colordlg.h>
void CPPgStats::OnChangeColor(wxEvent& evt)
{
  wxColour newcol=wxGetColourFromUser(this,XRCCTRL(*this,"IDC_COLOR_BUTTON",wxButton)->GetBackgroundColour());
  if(newcol.Ok()) {
    XRCCTRL(*this,"IDC_COLOR_BUTTON",wxButton)->SetBackgroundColour(newcol);
    // hmm.. ei liiemmin peruta..
    theApp.glob_prefs->SetStatsColor(XRCCTRL(*this,"IDC_COLORSELECTOR",wxComboBox)->GetSelection(),
				     RGB(newcol.Red(),newcol.Green(),newcol.Blue()));
  }
}

#if 0
void CPPgStats::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CPPgStats, CPropertyPage)
	ON_WM_HSCROLL()
END_MESSAGE_MAP()


BOOL CPPgStats::OnInitDialog()
{
	CPropertyPage::OnInitDialog();

	((CSliderCtrl*)GetDlgItem(IDC_SLIDER))->SetPos(app_prefs->GetTrafficOMeterInterval());
	((CSliderCtrl*)GetDlgItem(IDC_SLIDER2))->SetPos(app_prefs->GetStatsInterval()-4);
	((CSliderCtrl*)GetDlgItem(IDC_SLIDER3))->SetPos(app_prefs->GetStatsAverageMinutes()-1);
	mystats1=app_prefs->GetTrafficOMeterInterval();
	mystats2=app_prefs->GetStatsInterval();
	mystats3=app_prefs->GetStatsAverageMinutes();

	Localize();

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}
#endif

BOOL CPPgStats::OnApply()
{
	theApp.glob_prefs->SetTrafficOMeterInterval(mystats1);
	theApp.glob_prefs->SetStatsInterval(mystats2);
	theApp.glob_prefs->SetStatsAverageMinutes(mystats3);
#warning TODO: STATISTICWND->LOCALIZE	
	//theApp.emuledlg->statisticswnd->Localize();
	theApp.emuledlg->statisticswnd->ShowInterval();
	//SetModified(FALSE);
	//return CPropertyPage::OnApply();
	return TRUE;
}


void CPPgStats::Localize(void)
{
  if(1)
    {
      GetDlgItem(IDC_GRAPHS,wxControl)->SetLabel(GetResString(IDS_GRAPHS));
      GetDlgItem(IDC_STREE,wxControl)->SetLabel(GetResString(IDS_STREE));
      //SetWindowText(GetResString(IDS_STATSSETUPINFO));
      
      GetDlgItem(IDC_PREFCOLORS,wxControl)->SetLabel(GetResString(IDS_COLORS));
      
      m_colors=XRCCTRL(*this,"IDC_COLORSELECTOR",wxComboBox);

      while (m_colors->GetCount()>0) m_colors->Delete(0);
      m_colors->Append(GetResString(IDS_SP_BACKGROUND));
      m_colors->Append(GetResString(IDS_SP_GRID));
      m_colors->Append(GetResString(IDS_SP_DL1));
      m_colors->Append(GetResString(IDS_SP_DL2));
      m_colors->Append(GetResString(IDS_SP_DL3));
      m_colors->Append(GetResString(IDS_SP_UL1));
      m_colors->Append(GetResString(IDS_SP_UL2));
      m_colors->Append(GetResString(IDS_SP_UL3));
      m_colors->Append(GetResString(IDS_SP_ACTCON));
      m_colors->Append(GetResString(IDS_SP_ACTDL));
      m_colors->Append(GetResString(IDS_SP_ACTUL));
      m_colors->Append(GetResString(IDS_SP_ICONBAR));
      
      //m_ctlColor.CustomText = _T(GetResString(IDS_COL_MORECOLORS));
      //m_ctlColor.DefaultText = NULL;	
      GetDlgItem(IDC_COLOR_BUTTON,wxControl)->SetLabel(GetResString(IDS_COL_MORECOLORS));
      m_colors->SetSelection(0);

      //SetWindowText(GetResString(IDS_STATSSETUPINFO));
      
      //ShowInterval();
    }
}

void CPPgStats::OnHScroll(wxScrollEvent& evt)
{
  //SetModified(TRUE);
	
  wxSlider* slider =(wxSlider*)evt.GetEventObject();
  int position = evt.GetPosition();//slider->GetPos();
  
  if (slider==GetDlgItem(IDC_SLIDER,wxSlider)) {
    mystats1=position;
  } else if (slider==GetDlgItem(IDC_SLIDER2,wxSlider)) {
    if (position>0) position+=4;
    mystats2=position;
  } else mystats3=position+1;
  
  ShowInterval();
}

void CPPgStats::ShowInterval() {
	CString m_SliderValue;
	
	if (mystats1==0) m_SliderValue.Format(GetResString(IDS_DISABLED));
		else m_SliderValue.Format(GetResString(IDS_STATS_UPDATELABEL), mystats1);
	GetDlgItem(IDC_SLIDERINFO,wxControl)->SetLabel(m_SliderValue);

	if (mystats2==0) m_SliderValue.Format(GetResString(IDS_DISABLED));
		else m_SliderValue.Format(GetResString(IDS_STATS_UPDATELABEL), mystats2);
	GetDlgItem(IDC_SLIDERINFO2,wxControl)->SetLabel(m_SliderValue);

	m_SliderValue.Format(GetResString(IDS_STATS_AVGLABEL), mystats3);
	GetDlgItem(IDC_SLIDERINFO3,wxControl)->SetLabel(m_SliderValue);
}

void CPPgStats::LoadSettings() {
  GetDlgItem(IDC_SLIDER,wxSlider)->SetValue(app_prefs->GetTrafficOMeterInterval());
  GetDlgItem(IDC_SLIDER2,wxSlider)->SetValue(app_prefs->GetStatsInterval()-4);
  GetDlgItem(IDC_SLIDER3,wxSlider)->SetValue(app_prefs->GetStatsAverageMinutes()-1);
  mystats1=app_prefs->GetTrafficOMeterInterval();
  mystats2=app_prefs->GetStatsInterval();
  mystats3=app_prefs->GetStatsAverageMinutes();

  wxCommandEvent nullEvt;
  OnCbnSelchangeColorselector(nullEvt);
  ShowInterval();
}

void CPPgStats::OnCbnSelchangeColorselector(wxCommandEvent& evt)
{
  int sel=m_colors->GetSelection();
  COLORREF selcolor=theApp.glob_prefs->GetStatsColor(sel);
  //m_ctlColor.SetColor(selcolor);
  XRCCTRL(*this,"IDC_COLOR_BUTTON",wxButton)->SetBackgroundColour(wxColour(GetRValue(selcolor),GetGValue(selcolor),GetBValue(selcolor)));
#warning COLOR IS NOT SET
}
