//this file is part of eMule
//Copyright (C)2002 Merkur ( merkur-@users.sourceforge.net / http://www.emule-project.net )
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


#ifndef _SHAREDFILESLIST_H
#define _SHAREDFILESLIST_H

#include "wintypes.h"
#include "opcodes.h"
#include "emule.h"
#include "types.h"
#include "Preferences.h"
#include "KnownFile.h"
#include "KnownFileList.h"
#include "SharedFilesCtrl.h"
#include "sockets.h"
#include "PartFile.h"
#include "MapKey.h"
#include <wx/thread.h>

struct UnknownFile_Struct{
	char* name;
	char* directory;
};

class CKnownFileList;

#if 0
WX_DECLARE_LIST(UnknownFileStruct,UnknownList);

class CFilePtrList : public UnknownList
{
public:
  CFilePtrList() : UnknownList()
    { };
  virtual ~CFilePtrList() 
    { m_lockFilePtrList.Unlock(); };
  
  int GetCount()	{ int nResult;
  wxMutexLocker locker(m_lockFilePtrList);
  nResult = UnknownList::GetCount();
  return nResult; };
  
  bool IsEmpty()		{ bool bResult;
  wxMutexLocker locker(m_lockFilePtrList);
  bResult = UnknownList::IsEmpty();
  return bResult; };
  
  UnknownList::Node* AddTail(UnknownFileStruct* pNewElement) 
    { UnknownList::Node* pos;
    wxMutexLocker locker(m_lockFilePtrList);
    pos = UnknownList::Append(pNewElement);
    return pos; };
  
  UnknownFileStruct* RemoveHead() 
    { UnknownFileStruct* pFile;
    wxMutexLocker locker(m_lockFilePtrList);
    pFile = UnknownList::GetFirst()->GetData();
    UnknownList::DeleteNode(UnknownList::GetFirst());
    return pFile; };
  
  //CRITICAL_SECTION 	m_lockFilePtrList;
  wxMutex m_lockFilePtrList;
};
#endif

class CSharedFileList{
	friend class CSharedFilesCtrl;
	friend class CClientReqSocket;
public:
	CSharedFileList(CPreferences* in_prefs,CServerConnect* in_server, CKnownFileList* in_filelist);
	~CSharedFileList();
	void	SendListToServer();
	void	Reload(bool sendtoserver = true, bool firstload = false);
	void	SafeAddKFile(CKnownFile* toadd, bool bOnlyAdd = false);
	void	SetOutputCtrl(CSharedFilesCtrl* in_ctrl);
	void	RemoveFile(CKnownFile* toremove);
	wxMutex	list_mut;
	CKnownFile*	GetFileByID(uchar* filehash);
	short	GetFilePriorityByID(uchar* filehash);
	CKnownFile*     GetFileByIndex(int index);
	CKnownFileList*		filelist;
	void	CreateOfferedFilePacket(CKnownFile* cur_file,CMemFile* files);
	uint64	GetDatasize();
	uint16	GetCount()	{return m_Files_map.GetCount(); }
	void	UpdateItem(CKnownFile* toupdate);
private:
	void	FindSharedFiles();
	void	AddFilesFromDirectory(char* directory);
	void	HashNextFile();

	CMap<CCKey,CCKey,CKnownFile*,CKnownFile*&> m_Files_map;
	CTypedPtrList<CPtrList, UnknownFile_Struct*> waitingforhash_list;
	CPreferences*		app_prefs;
	CServerConnect*		server;
	CSharedFilesCtrl*	output;
};

#if 0
WX_DECLARE_HASH_MAP(wxString,CKnownFile*,wxStringHash,wxStringEqual,FilesMap);
WX_DECLARE_LIST(UnknownFileStruct,WaitingForList);

class CSharedFileList{
	friend class CSharedFilesCtrl;
	friend class CClientReqSocket;
public:
	CSharedFileList(CPreferences* in_prefs,CServerConnect* in_server, CKnownFileList* in_filelist);
	~CSharedFileList();
	void	SendListToServer();
	void	Reload(bool sendtoserver = true);
	void	SafeAddKFile(CKnownFile* toadd, bool bOnlyAdd = false);
	void	SetOutputCtrl(CSharedFilesCtrl* in_ctrl);
	void	RemoveFile(CKnownFile* toremove);
	wxMutex	list_mut;
	CKnownFile*	GetFileByID(uchar* filehash);
	short	GetFilePriorityByID(uchar* filehash);
	CKnownFileList*		filelist;
	void	CreateOfferedFilePacket(CKnownFile* cur_file,CMemFile* files);
	uint64	GetDatasize();
	uint16	GetCount()	{return m_Files_map.size(); }
	void	UpdateItem(CKnownFile* toupdate);
private:
	void	FindSharedFiles();
	void	AddFilesFromDirectory(char* directory);
	void	HashNextFile();

	//CMap<CCKey,CCKey&,CKnownFile*,CKnownFile*> m_Files_map;
	FilesMap m_Files_map;
	//CTypedPtrList<CPtrList, UnknownFileStruct*> waitingforhash_list;
	WaitingForList waitingforhash_list;
	CPreferences*		app_prefs;
	CServerConnect*		server;
	CSharedFilesCtrl*	output;
};
#endif

//class CPartFile;
#include "wx/thread.h"
class CAddFileThread : public wxThread
{
  //		DECLARE_DYNCREATE(CAddFileThread)
public:
	CAddFileThread();
public:
	virtual	BOOL	InitInstance() {return true;}
	//virtual int		Run();	
	virtual ExitCode Entry();
	virtual void OnExit() {printf("CAddFileThread::OnExit()\n");}

	void	SetValues(CSharedFileList* pOwner, char* directory, char* filename, CPartFile* in_partfile_Owner = 0,wxFrame* parent=NULL);
private:
	CSharedFileList* m_pOwner;
	char*			 directory;
	char*			 filename;
	CPartFile*		 partfile_Owner;
	wxFrame* parent;
};
#endif
