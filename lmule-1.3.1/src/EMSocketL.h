//this file is part of eMule
//Copyright (C)2002 Merkur ( merkur-@users.sourceforge.net / http://www.emule-project.net )
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#ifndef EMSOCKET_H
#define EMSOCKET_H

//#include "afxsock.h"
#include "types.h"
#include "packets.h"
#include "wx/socket.h"

#define ERR_WRONGHEADER		0x01
#define ERR_TOOBIG			0x02

#define	ES_DISCONNECTED		0xFF
#define	ES_NOTCONNECTED		0x00
#define	ES_CONNECTED		0x01

WX_DECLARE_LIST(Packet,PacketListL);

class CEMSocket :
	public wxSocketClient
{
  DECLARE_DYNAMIC_CLASS(CEMSocket)
    
public:
	CEMSocket(void);
	~CEMSocket(void);
	bool	SendPacket(Packet* packet, bool delpacket = true,bool controlpacket = true);// controlpackets have a higher priority
	bool	IsBusy()	{return sendbuffer;}
	bool	IsConnected()	{return byConnected == ES_CONNECTED;}
	void	SetDownloadLimit(uint32 limit);
	void	DisableDownloadLimit();
	BOOL	AsyncSelect(long lEvent);
	//protected:
 public:
	virtual void	PacketReceived(Packet* packet)		{}
	virtual void	OnError(int nErrorCode)				{}
	virtual void	OnClose(int nErrorCode);
	virtual void	OnSend(int nErrorCode);	
	virtual void	OnReceive(int nErrorCode);
	uint8	byConnected;

private:
	void	ClearQueues();	
	int		Send(char* lpBuf,int nBufLen,int nFlags = 0);
	virtual int Receive(void* lpBuf, int nBufLen, int nFlags = 0);

	uint32	downloadlimit;
	bool	limitenabled;
	char*	readbuffer;
	uint32	dataneeded;
	uint32	datawaiting;
	Packet* receivedpacket;
	char	header[6];
	char*	sendbuffer;
	uint32	sendblen;
	uint32	sent;
	bool	m_bLinkedPackets;

	PacketListL controlpacket_queue;
	PacketListL standartpacket_queue;
	//CTypedPtrList<CPtrList, Packet*> controlpacket_queue;
	//CTypedPtrList<CPtrList, Packet*> standartpacket_queue;
};
#endif
