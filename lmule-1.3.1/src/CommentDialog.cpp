// CommentDialog.cpp : implementation file 
// 

//#include "stdafx.h" 
#include "wintypes.h"
#include "emule.h" 
#include "CommentDialog.h" 
#include "muuli_wdr.h"

// CommentDialog dialog 

#include "wx/xrc/xmlres.h"
#include "wx/xrc/xh_all.h"

#define GetDlgItem(x,clas) XRCCTRL(*this,#x,clas)
#define IsDlgButtonChecked(x) XRCCTRL(*this,#x,wxCheckBox)->GetValue()
#define CheckDlgButton(x,y) XRCCTRL(*this,#x,wxCheckBox)->SetValue(y)

//IMPLEMENT_DYNAMIC(CCommentDialog, CDialog) 
CCommentDialog::CCommentDialog(wxWindow* parent,CKnownFile* file) 
  //: CDialog(CCommentDialog::IDD, 0) 
  : wxDialog(parent,CCommentDialog::IDD,_("File Comments"),
	     wxDefaultPosition,wxDefaultSize,wxDEFAULT_DIALOG_STYLE|wxSYSTEM_MENU)
{ 
   m_file = file; 
   //wxSizer* content=commentDlg(this,TRUE);
   //content->Show(this,TRUE);
   wxPanel* content=wxXmlResource::Get()->LoadPanel(this,"DLG_COMMENT");
   wxSize koko=content->GetSize();
   SetSize(content->GetSize());

   content->SetBackgroundColour(GetColour(wxSYS_COLOUR_WINDOW));
   Center();

   ratebox=XRCCTRL(*this,"IDC_RATELIST",wxComboBox);

   OnInitDialog();
} 

CCommentDialog::~CCommentDialog() 
{ 
} 

BEGIN_EVENT_TABLE(CCommentDialog,wxDialog)
  EVT_BUTTON(XRCID("IDCOK"),CCommentDialog::OnBnClickedApply)
  EVT_BUTTON(XRCID("IDCCANCEL"),CCommentDialog::OnBnClickedCancel)
END_EVENT_TABLE()

#if 0
void CCommentDialog::DoDataExchange(CDataExchange* pDX) 
{ 
   CDialog::DoDataExchange(pDX); 
   DDX_Control(pDX, IDC_RATELIST, ratebox);//for rate 
} 

BEGIN_MESSAGE_MAP(CCommentDialog, CDialog) 
   ON_BN_CLICKED(IDCOK, OnBnClickedApply) 
   ON_BN_CLICKED(IDCCANCEL, OnBnClickedCancel) 
END_MESSAGE_MAP() 
#endif


void CCommentDialog::OnBnClickedApply(wxEvent& evt) 
{ 
   wxString SValue; 
   SValue=GetDlgItem(IDC_CMT_TEXT,wxTextCtrl)->GetValue();//(SValue); 
   m_file->SetFileComment(CString(SValue)); 
   m_file->SetFileRate((int8)ratebox->GetSelection());//for Rate// 
   //CDialog::OnOK(); 
   EndModal(0);
} 

void CCommentDialog::OnBnClickedCancel(wxEvent& evt) 
{ 
  //CDialog::OnCancel(); 
  EndModal(0);
} 

BOOL CCommentDialog::OnInitDialog(){ 
  //CDialog::OnInitDialog(); 
   Localize(); 

   GetDlgItem(IDC_CMT_TEXT,wxTextCtrl)->SetValue(m_file->GetFileComment()); 
   //((CEdit*)GetDlgItem(IDC_CMT_TEXT))->SetLimitText(50);
   GetDlgItem(IDC_CMT_TEXT,wxTextCtrl)->SetMaxLength(50);
   return TRUE; 
} 

void CCommentDialog::Localize(void){ 
   GetDlgItem(IDCOK,wxControl)->SetLabel(GetResString(IDS_PW_APPLY)); 
   GetDlgItem(IDCCANCEL,wxControl)->SetLabel(GetResString(IDS_CANCEL)); 

   GetDlgItem(IDC_CMT_LQUEST,wxControl)->SetLabel(GetResString(IDS_CMT_QUEST)); 
   GetDlgItem(IDC_CMT_LAIDE,wxControl)->SetLabel(GetResString(IDS_CMT_AIDE)); 

   //for rate// 
   GetDlgItem(IDC_RATEQUEST,wxControl)->SetLabel(GetResString(IDS_CMT_RATEQUEST)); 
   GetDlgItem(IDC_RATEHELP,wxControl)->SetLabel(GetResString(IDS_CMT_RATEHELP)); 

   while (ratebox->GetCount()>0) ratebox->Delete(0); 
   
   ratebox->Append(GetResString(IDS_CMT_NOTRATED)); 
   ratebox->Append(GetResString(IDS_CMT_FAKE)); 
   ratebox->Append(GetResString(IDS_CMT_POOR)); 
   ratebox->Append(GetResString(IDS_CMT_GOOD)); 
   ratebox->Append(GetResString(IDS_CMT_FAIR)); 
   ratebox->Append(GetResString(IDS_CMT_EXCELLENT)); 
   ratebox->SetSelection(m_file->GetFileRate());//==CB_ERR)ratebox.SetCurSel(0) ; 
   //--end rate--//
   wxString strTitle; 

   strTitle.Printf(GetResString(IDS_CMT_TITLE),m_file->GetFileName());
   //AfxFormatString1(title,IDS_CMT_TITLE,m_file->GetFileName()); 

   //SetWindowText(strTitle); 
   SetTitle(strTitle);
} 
