#pragma once
#include "ResizableLib/ResizableDialog.h"
#include "afxcmn.h"
#include "afxwin.h"
#include "resource.h"
#include "IrcMain.h"
#include "HyperTextCtrl.h"
#

#define Irc_Op			1
#define Irc_DeOp		2
#define Irc_Voice		3
#define Irc_DeVoice		4
#define Irc_HalfOp		5
#define Irc_DeHalfOp	6
#define Irc_Kick		7
#define Irc_Slap		8
#define Irc_Join		9
#define Irc_Close		10
#define Irc_Priv		11
#define Irc_AddFriend	12

// CIrcWnd dialog

struct ChannelList{
	CString name;
	CString users;
	CString desc;
};

struct Channel{
	CString	name;
	CPreparedHyperText log;
	CString title;
	CPtrList nicks;
	uint8 type;
	// Type is mainly so that we can use this for IRC and the eMule Messages.. But, I'm not to good with guis, so I'm going to temp use it for other stuff also.
	// 1-Status, 2-Channel list, 3-Preference, 4-Channel, 5-Private Channel, 6-eMule Message(Add later)
};

struct Nick{
	CString nick;
	CString mode;
};

class CIrcWnd : public CResizableDialog
{

	DECLARE_DYNAMIC(CIrcWnd)

public:
	CIrcWnd(CWnd* pParent = NULL);   // standard constructor
	virtual ~CIrcWnd();
	void		AddStatus( CString recieved, ... );
	void		AddInfoMessage( CString channelName, CString recieved, ... );
	void		AddMessage( CString channelName, CString targetname, CString line,...);
	void		SetConnectStatus( bool connected );
	Channel*	FindChannelByName(CString name);
	Nick*		FindNickByName(CString channel, CString name);
	Channel*	NewChannel(CString name, uint8 type);
	Nick*		NewNick(CString channel, CString nick);
	void		RefreshNickList( CString channel );
	bool		RemoveNick( CString channel, CString nick );
	void		RemoveChannel( CString channel );
	void		DeleteAllChannel();
	void		DeleteAllNick( CString channel );
	void		DeleteNickInAll ( CString nick, CString message );
	void		ChangeAllNick( CString oldnick, CString newnick );
	bool		ChangeNick( CString channel, CString oldnick, CString newnick );
	bool		ChangeMode( CString channel, CString nick, CString mode );
	void		ParseChangeMode( CString channel, CString changer, CString commands, CString names );
	void		NoticeMessage( CString source, CString message );
	void		SetTitle( CString channel, CString title );
	void		ResetServerChannelList();
	void		AddChannelToList( CString name, CString user, CString description );
	void		SetActivity( CString channel, bool flag);
	void		JoinChannels();
	CString		StripMessageOfFontCodes( CString temp );
	CString		StripMessageOfColorCodes( CString temp );
	void		Localize();
	void		SendString( CString send )	{m_pIrcMain->SendString( send );}
	bool		GetLoggedIn()				{return m_bLoggedIn;}
	void		SetLoggedIn( bool flag )	{m_bLoggedIn = flag;}
	void		SetSendFileString( CString in_file )	{m_sSendString = in_file;}
	CString		GetSendFileString()						{return m_sSendString;}
//	void		SetNick( CString in_nick );


// Dialog Data
	enum { IDD = IDD_IRC };

protected:
	static	int		CALLBACK SortProcNick(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort);
	static	int		CALLBACK SortProcChanL(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort);
	afx_msg	void	OnColumnClickNick( NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg	void	OnColumnClickChanL( NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg	void	OnNMRclickNick(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg	void	OnNMRclickStatusWindow(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg	void	OnNMRclickChanL(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void	OnBnClickedBnIrcconnect();
	afx_msg void	OnBnClickedChatsend();
	afx_msg void	OnBnClickedClosechat();
	afx_msg void	OnTcnSelchangeTab2(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void	OnNMDblclkserverChannelList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void	OnNMClickNicklist(NMHDR *pNMHDR, LRESULT *pResult);

	virtual void	DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL	OnInitDialog();
	virtual BOOL	OnCommand(WPARAM wParam,LPARAM lParam );
	virtual BOOL	PreTranslateMessage(MSG* pMsg);
	virtual void	OnSize(UINT nType, int cx, int cy);
	virtual int		OnCreate(LPCREATESTRUCT lpCreateStruct);
	DECLARE_MESSAGE_MAP()

	bool			m_bConnected;	
	bool			m_bLoggedIn;
	Channel*		m_pCurrentChannel;
	CPtrList		channelPtrList;
	CPtrList		channelLPtrList;
private:
	CImageList		imagelist;
	bool			asc_sort[8];	 
	CIrcMain*		m_pIrcMain;
	CListCtrl		nickList;
	CListCtrl		serverChannelList;
	CEdit			inputWindow;
	CTabCtrl		channelselect;
	CHyperTextCtrl	statusWindow;
	CEdit			titleWindow;
	CString			m_sSendString;
};
