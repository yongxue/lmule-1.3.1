// PPgFiles.cpp : implementation file
//

//#include "stdafx.h"
#include "wintypes.h"
#include "emule.h"
#include "PPgFiles.h"
#include <wx/radiobut.h>
#include <wx/checkbox.h>

#include <wx/checkbox.h>
#include <wx/notebook.h>
#include <wx/slider.h>
#include "wx/xrc/xmlres.h"
#include "wx/xrc/xh_all.h"

// CPPgFiles dialog
#define GetDlgItem(x,clas) XRCCTRL(*this,#x,clas)
#define IsDlgButtonChecked2(x,y) XRCCTRL(*this,#x,y)->GetValue()
#define IsDlgButtonChecked(x) XRCCTRL(*this,#x,wxCheckBox)->GetValue()
#define CheckDlgButton3(x,y,z) XRCCTRL(*this,#x,z)->SetValue(y)
#define CheckDlgButton(x,y) XRCCTRL(*this,#x,wxCheckBox)->SetValue(y)

//IMPLEMENT_DYNAMIC(CPPgFiles, CPropertyPage)
IMPLEMENT_DYNAMIC_CLASS(CPPgFiles,wxPanel)
CPPgFiles::CPPgFiles(wxWindow* parent)
	: wxPanel(parent,CPPgFiles::IDD)
{
  wxNotebook* book=(wxNotebook*)parent;

  wxPanel*page1=wxXmlResource::Get()->LoadPanel(this,"DLG_PPG_FILES");
  book->AddPage(this,_("Files"));

  // looks stupid? it is :)
  SetSize(page1->GetSize().GetWidth(),page1->GetSize().GetHeight()+48);
  page1->SetSize(GetSize());

  Localize();
}

CPPgFiles::~CPPgFiles()
{
}

#if 0
void CPPgFiles::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CPPgFiles, CPropertyPage)

	ON_BN_CLICKED(IDC_SEESHARE1, OnBnClickedSeeshare1)
	ON_BN_CLICKED(IDC_SEESHARE2, OnBnClickedSeeshare2)
	ON_BN_CLICKED(IDC_SEESHARE3, OnBnClickedSeeshare3)
	ON_BN_CLICKED(IDC_ICH, OnBnClickedIch)
END_MESSAGE_MAP()


BOOL CPPgFiles::OnInitDialog()
{
	CPropertyPage::OnInitDialog();

	LoadSettings();
	Localize();

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}
#endif

void CPPgFiles::LoadSettings(void)
{	
	switch(app_prefs->prefs->m_iSeeShares)
	{
		case 0:  CheckDlgButton3(IDC_SEESHARE1,1,wxRadioButton); break;
		case 1:	 CheckDlgButton3(IDC_SEESHARE2,1,wxRadioButton); break;
		default: CheckDlgButton3(IDC_SEESHARE3,1,wxRadioButton);	break;
	}

	if(app_prefs->prefs->addnewfilespaused)
		CheckDlgButton(IDC_ADDNEWFILESPAUSED,1);
	else
		CheckDlgButton(IDC_ADDNEWFILESPAUSED,0);
	
	if(app_prefs->prefs->ICH)
		CheckDlgButton(IDC_ICH,1);
	else
		CheckDlgButton(IDC_ICH,0);

	if(app_prefs->prefs->m_bpreviewprio)
		CheckDlgButton(IDC_PREVIEWPRIO,1);
	else
		CheckDlgButton(IDC_PREVIEWPRIO,0);

	if(app_prefs->prefs->m_btransferfullchunks)
		CheckDlgButton(IDC_FULLCHUNKTRANS,1);
	else
		CheckDlgButton(IDC_FULLCHUNKTRANS,0);

	if(app_prefs->prefs->m_bstartnextfile)
		CheckDlgButton(IDC_STARTNEXTFILE,1);
	else
		CheckDlgButton(IDC_STARTNEXTFILE,0);

	if(app_prefs->prefs->m_bUAP) CheckDlgButton(IDC_UAP, 1);
}

void CPPgFiles::OnApply()
{
  if(IsDlgButtonChecked2(IDC_SEESHARE1,wxRadioButton))
    app_prefs->prefs->m_iSeeShares = 0;
  else if(IsDlgButtonChecked2(IDC_SEESHARE2,wxRadioButton))
    app_prefs->prefs->m_iSeeShares = 1;
  else
    app_prefs->prefs->m_iSeeShares = 2;
  
  if(IsDlgButtonChecked(IDC_PREVIEWPRIO))
    app_prefs->prefs->m_bpreviewprio = true;
  else
    app_prefs->prefs->m_bpreviewprio = false;
  
  if(IsDlgButtonChecked(IDC_STARTNEXTFILE))
    app_prefs->prefs->m_bstartnextfile = true;
  else
    app_prefs->prefs->m_bstartnextfile = false;
  
  if(IsDlgButtonChecked(IDC_FULLCHUNKTRANS))
    app_prefs->prefs->m_btransferfullchunks = true;
  else
    app_prefs->prefs->m_btransferfullchunks = false;
  
  app_prefs->prefs->addnewfilespaused = (int8)IsDlgButtonChecked(IDC_ADDNEWFILESPAUSED);
  
  app_prefs->prefs->ICH = (int8)IsDlgButtonChecked(IDC_ICH);
  app_prefs->prefs->m_bUAP = IsDlgButtonChecked(IDC_UAP);
  
  LoadSettings();
}


void CPPgFiles::Localize(void)
{
  if(1)
    {
      //SetWindowText(GetResString(IDS_PW_FILES));
      GetDlgItem(IDC_ICH_FRM,wxControl)->SetLabel(GetResString(IDS_PW_ICH));
      GetDlgItem(IDC_ICH,wxControl)->SetLabel(GetResString(IDS_PW_FILE_ICH));
      GetDlgItem(IDC_SEEMYSHARE_FRM,wxControl)->SetLabel(GetResString(IDS_PW_SHARE));
      GetDlgItem(IDC_SEESHARE1,wxControl)->SetLabel(GetResString(IDS_PW_EVER));
      GetDlgItem(IDC_SEESHARE2,wxControl)->SetLabel(GetResString(IDS_PW_FRIENDS));
      GetDlgItem(IDC_SEESHARE3,wxControl)->SetLabel(GetResString(IDS_PW_NOONE));
      GetDlgItem(IDC_UAP,wxControl)->SetLabel(GetResString(IDS_PW_UAP));
      GetDlgItem(IDC_PREVIEWPRIO,wxControl)->SetLabel(GetResString(IDS_DOWNLOADMOVIECHUNKS));
      GetDlgItem(IDC_ADDNEWFILESPAUSED,wxControl)->SetLabel(GetResString(IDS_ADDNEWFILESPAUSED));
      
      GetDlgItem(IDC_FULLCHUNKTRANS,wxControl)->SetLabel(GetResString(IDS_FULLCHUNKTRANS));
      GetDlgItem(IDC_STARTNEXTFILE,wxControl)->SetLabel(GetResString(IDS_STARTNEXTFILE));
    }
}
