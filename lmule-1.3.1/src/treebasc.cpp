/////////////////////////////////////////////////////////////////////////////
// Name:        treebase.cpp
// Purpose:     Base wxTreeCtrl classes
// Author:      Julian Smart
// Created:     01/02/97
// Modified:
// Id:          $Id: treebasc.cpp,v 1.2 2003/03/15 20:25:31 tiku Exp $
// Copyright:   (c) 1998 Robert Roebling, Julian Smart et al
// Licence:     wxWindows licence
/////////////////////////////////////////////////////////////////////////////

// =============================================================================
// declarations
// =============================================================================

// -----------------------------------------------------------------------------
// headers
// -----------------------------------------------------------------------------

#ifdef __GNUG__
  #pragma implementation "treebasc.h"
#endif

// For compilers that support precompilation, includes "wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
    #pragma hdrstop
#endif

#if wxUSE_TREECTRL

#include "treebasc.h"
#include "wx/settings.h"
#include "wx/log.h"
#include "wx/intl.h"
#include "wx/dynarray.h"
#include "wx/arrimpl.cpp"
#include "wx/dcclient.h"
#include "wx/msgdlg.h"


// ----------------------------------------------------------------------------
// events
// ----------------------------------------------------------------------------

DEFINE_EVENT_TYPE(wxEVT_COMMAND_CTREE_BEGIN_DRAG)
DEFINE_EVENT_TYPE(wxEVT_COMMAND_CTREE_BEGIN_RDRAG)
DEFINE_EVENT_TYPE(wxEVT_COMMAND_CTREE_BEGIN_LABEL_EDIT)
DEFINE_EVENT_TYPE(wxEVT_COMMAND_CTREE_END_LABEL_EDIT)
DEFINE_EVENT_TYPE(wxEVT_COMMAND_CTREE_DELETE_ITEM)
DEFINE_EVENT_TYPE(wxEVT_COMMAND_CTREE_GET_INFO)
DEFINE_EVENT_TYPE(wxEVT_COMMAND_CTREE_SET_INFO)
DEFINE_EVENT_TYPE(wxEVT_COMMAND_CTREE_ITEM_EXPANDED)
DEFINE_EVENT_TYPE(wxEVT_COMMAND_CTREE_ITEM_EXPANDING)
DEFINE_EVENT_TYPE(wxEVT_COMMAND_CTREE_ITEM_COLLAPSED)
DEFINE_EVENT_TYPE(wxEVT_COMMAND_CTREE_ITEM_COLLAPSING)
DEFINE_EVENT_TYPE(wxEVT_COMMAND_CTREE_SEL_CHANGED)
DEFINE_EVENT_TYPE(wxEVT_COMMAND_CTREE_SEL_CHANGING)
DEFINE_EVENT_TYPE(wxEVT_COMMAND_CTREE_KEY_DOWN)
DEFINE_EVENT_TYPE(wxEVT_COMMAND_CTREE_ITEM_ACTIVATED)
DEFINE_EVENT_TYPE(wxEVT_COMMAND_CTREE_ITEM_RIGHT_CLICK)
DEFINE_EVENT_TYPE(wxEVT_COMMAND_CTREE_ITEM_MIDDLE_CLICK)
DEFINE_EVENT_TYPE(wxEVT_COMMAND_CTREE_END_DRAG)
DEFINE_EVENT_TYPE(wxEVT_COMMAND_CTREE_ITEM_LEFT_CLICK)

// ----------------------------------------------------------------------------
// Tree event
// ----------------------------------------------------------------------------

IMPLEMENT_DYNAMIC_CLASS(wxCTreeEvent, wxNotifyEvent)


wxCTreeEvent::wxCTreeEvent(wxEventType commandType, int id)
           : wxNotifyEvent(commandType, id)
{
    m_itemOld = 0l;
    m_editCancelled = FALSE;
}

#endif // wxUSE_CTRE_CTRL

