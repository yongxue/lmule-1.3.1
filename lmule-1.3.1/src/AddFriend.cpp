//this file is part of eMule
//Copyright (C)2002 Merkur ( merkur-@users.sourceforge.net / http://www.emule-project.net )
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

// AddFriend.cpp : implementation file
//

//#include "stdafx.h"
#include "wintypes.h"
#include "emule.h"
#include "AddFriend.h"
#include "Friend.h"
#include "otherfunctions.h"
#include "muuli_wdr.h"

#ifdef _DEBUG

#undef THIS_FILE

static char THIS_FILE[]=__FILE__;

#define new DEBUG_NEW

#endif


// CAddFriend dialog

//IMPLEMENT_DYNAMIC(CAddFriend, CDialog)
IMPLEMENT_DYNAMIC_CLASS(CAddFriend,wxDialog)

CAddFriend::CAddFriend(wxWindow* parent)
  //: CDialog(CAddFriend::IDD, 0)
  : wxDialog(parent,9995,_("Add a Friend"),wxDefaultPosition,wxDefaultSize,
	     wxDEFAULT_DIALOG_STYLE|wxSYSTEM_MENU)
{
  wxSizer* content=addFriendDlg(this,TRUE);
  content->Show(this,TRUE);
  OnInitDialog();  
}

CAddFriend::~CAddFriend()
{
}

#if 0
void CAddFriend::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}
#endif

BOOL CAddFriend::OnInitDialog(){
  //CDialog::OnInitDialog();
  Localize();
	

  return true;
}

#if 0
BEGIN_MESSAGE_MAP(CAddFriend, CDialog)
	ON_BN_CLICKED(IDC_ADD, OnAddBtn)
END_MESSAGE_MAP()
#endif

BEGIN_EVENT_TABLE(CAddFriend,wxDialog)
  EVT_BUTTON(ID_ADDFRIEND,CAddFriend::OnAddBtn)
  EVT_BUTTON(ID_CLOSEDLG,CAddFriend::OnCloseBtn)
END_EVENT_TABLE()

// CAddFriend message handlers
void CAddFriend::Localize(){
#if 0
	SetWindowText(GetResString(IDS_ADDAFRIEND));
	GetDlgItem(IDC_INFO1)->SetWindowText(GetResString(IDS_PAF_REQINFO));
	GetDlgItem(IDC_INFO2)->SetWindowText(GetResString(IDS_PAF_MOREINFO));

	GetDlgItem(IDC_ADD)->SetWindowText(GetResString(IDS_ADD));
	GetDlgItem(IDCANCEL)->SetWindowText(GetResString(IDS_CANCEL));

	GetDlgItem(IDC_STATIC31)->SetWindowText(GetResString(IDS_CD_UNAME));
	GetDlgItem(IDC_STATIC32)->SetWindowText(GetResString(IDS_CD_UHASH));
	GetDlgItem(IDC_STATIC34)->SetWindowText(GetResString(IDS_CD_UIP));
	GetDlgItem(IDC_STATIC35)->SetWindowText(GetResString(IDS_PORT)+":");
#endif
}

#define GetDlgItem(a,b) wxStaticCast(FindWindowById((a)),b)

void CAddFriend::OnAddBtn(wxEvent& evt) {


//	if( GetDlgItem(IDC_USERNAME)->GetWindowTextLength()==0
//		|| GetDlgItem(IDC_USERHASH)->GetWindowTextLength()!=32	){
//		
//			MessageBox(GetResString(IDS_ERR_NOVALIDFRIENDINFO));
//			return;
//	}

	char buffer[256];
	CString name,userhash, fullip;
	uint32 ip;
	uint16 port;
	ip=port=0;
	if( GetDlgItem(ID_USERNAME,wxTextCtrl)->GetValue().Length() ){
	  //GetDlgItem(IDC_USERNAME)->GetWindowText(buffer,255);
	  name.Format("%s",GetDlgItem(ID_USERNAME,wxTextCtrl)->GetValue().GetData());
	}
	if(GetDlgItem(ID_IPADDRESS,wxTextCtrl)->GetValue().Length()){
	  //GetDlgItem(IDC_IP)->GetWindowText(buffer,20);
	  fullip.Format("%s", GetDlgItem(ID_IPADDRESS,wxTextCtrl)->GetValue().GetData());
	}
	else{
	  //MessageBox(GetResString(IDS_ERR_NOVALIDFRIENDINFO));
	  wxMessageBox(GetResString(IDS_ERR_NOVALIDFRIENDINFO));
	  return;
	}
	if(GetDlgItem(ID_IPORT,wxTextCtrl)->GetValue().Length()) 
	{
	  //GetDlgItem(IDC_PORT)->GetWindowText(buffer,20);
	  wxString buff=GetDlgItem(ID_IPORT,wxTextCtrl)->GetValue();
	  port = (atoi(buff.GetData())) ? atoi(buff.GetData()) : 0;
	}
	else{
	  //MessageBox(GetResString(IDS_ERR_NOVALIDFRIENDINFO));
	  wxMessageBox(GetResString(IDS_ERR_NOVALIDFRIENDINFO));
	  return;
	}
	
#if 0
	int counter = 0;
	CString temp;
	for( int i = 0; i < 4; i++){
		fullip.Tokenize(".",counter);
		if( counter == -1 ){
		  //MessageBox(GetResString(IDS_ERR_NOVALIDFRIENDINFO));
		  wxMessageBox(GetResString(IDS_ERR_NOVALIDFRIENDINFO));
		  return;
		}
	}
	counter=0;
	for(int i=0; i<4; i++){ 
		temp = fullip.Tokenize(".",counter);
		ip += atoi(temp) * pow(256,i);
	}
#endif
	int a,b,c,d;
	a=b=c=d=0;
	sscanf(fullip.GetData(),"%d.%d.%d.%d",&a,&b,&c,&d);
	ip=a*(uint32)pow(256.0,0)+b*(uint32)pow(256.0,1)+c*(uint32)pow(256.0,2)+d*(uint32)pow(256.0,3);
	theApp.friendlist->AddFriend(NULL, 0, ip, port, 0, name, 0 );
	
	//OnCloseBtn(evt);
	EndModal(1);
	//OnCancel();
}

void CAddFriend::OnCloseBtn(wxEvent& evt)
{
  EndModal(0);
}
