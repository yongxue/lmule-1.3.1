#ifndef CMEMFILE_H
#define CMEMFILE_H 1

// something that looks like the MFC CMemFile

#include "wintypes.h"
#include "CFile.h"

class CMemFile : public CFile
{
 public:  
  CMemFile(unsigned int growBytes=1024);
  CMemFile(BYTE* buffer,unsigned int bufferSize,unsigned int growBytes=0);
  void Attach(BYTE* buffer,unsigned int buffserSize,unsigned int growBytes=0);
  BYTE* Detach();
  virtual ~CMemFile();
  virtual unsigned long GetPosition() {return fPosition;};
  virtual bool GetStatus(unsigned long none) const {return 1;};
  off_t Seek(off_t offset,wxSeekMode from=wxFromStart);
  virtual void SetLength(unsigned long newLen);
  unsigned long GetLength() { return fFileSize; };
  off_t Read(void* buf,off_t length);
  size_t Write(const void* buf,size_t length);
  //virtual void Abort();
  //virtual void Flush();
  virtual bool Close();

 private:
  void enlargeBuffer(unsigned long size);

  unsigned long fLength;
  unsigned int fGrowBytes;
  unsigned long fPosition;
  unsigned long fBufferSize;
  unsigned long fFileSize;
  int deleteBuffer;
  BYTE* fBuffer;
};

#endif
