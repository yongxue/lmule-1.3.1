#ifndef PPGIRC_H
#define PPGIRC_H

// CPPgIRC dialog

#include "Preferences.h"

class CPPgIRC : public wxWindow //CPropertyPage
{
  //DECLARE_DYNAMIC(CPPgIRC)

public:
	CPPgIRC();
	virtual ~CPPgIRC();

	void SetPrefs(CPreferences* in_prefs) {	app_prefs = in_prefs; }

// Dialog Data
	enum { IDD = IDD_PPG_IRC };
protected:
	CPreferences *app_prefs;
protected:
	//virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	//DECLARE_MESSAGE_MAP()
public:
	//virtual BOOL OnInitDialog();
private:
	void LoadSettings(void);
	bool m_bnickModified;
public:
#if 0
	virtual BOOL OnApply();
	afx_msg void OnEnChangeServer()					{ SetModified(); }
	afx_msg void OnEnChangeNick()					{ SetModified(); m_bnickModified = true;}
	afx_msg void OnBnClickedAddTimeStamp()			{ SetModified(); }
	afx_msg void OnBnClickedUseFilter()				{ SetModified(); }
	afx_msg void OnBnClickedUsePerform()			{ SetModified(); }
	afx_msg void OnBnClickedListOnConnect()			{ SetModified(); }
	afx_msg void OnEnChangeName()					{ SetModified(); }
	afx_msg void OnEnChangeUser()					{ SetModified(); }
	afx_msg void OnEnChangePerformString()			{ SetModified(); }
	afx_msg void OnBnClickedAcceptLinks()			{ SetModified(); }
#endif
	void Localize(void);
};

#endif
