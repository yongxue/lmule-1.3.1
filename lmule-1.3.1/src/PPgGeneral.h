#ifndef PPGGEN_H
#define PPGGEN_H

#include "Preferences.h"
//#include "afxwin.h"

// CPPgGeneral dialog
#include "wx/panel.h"
#include "wx/combobox.h"

class CPPgGeneral : public wxPanel //CPropertyPage
{
  //DECLARE_DYNAMIC(CPPgGeneral)
  DECLARE_DYNAMIC_CLASS(CPPgGeneral)
    CPPgGeneral() {};

public:
	CPPgGeneral(wxWindow* parent);
	virtual ~CPPgGeneral();

	void SetPrefs(CPreferences* in_prefs) {	app_prefs = in_prefs; }

// Dialog Data
	enum { IDD = IDD_PPG_GENERAL };
protected:
	CPreferences *app_prefs;
protected:
	//virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	//DECLARE_MESSAGE_MAP()
	void OnHScroll(wxScrollEvent& evt);
	DECLARE_EVENT_TABLE()
public:
	//virtual BOOL OnInitDialog();
 public:
	void LoadSettings(void);
public:
	void OnApply();
#if 0
	virtual BOOL OnApply();
	afx_msg void OnEnChangeNick()					{ SetModified(); }
	afx_msg void OnCbnSelchangeLangs()				{ SetModified(); }
	afx_msg void OnBnClickedFlat()					{ SetModified(); }
	afx_msg void OnBnClickedMintray()				{ SetModified(); }
	afx_msg void OnBnClickedBeeper()				{ SetModified(); }
	afx_msg void OnBnClickedExit()					{ SetModified(); }
	afx_msg void OnBnClickedSplashon()				{ SetModified(); }
	afx_msg void OnBnClickedDblclick()				{ SetModified(); }
	afx_msg void OnBnClickedBringtoforeground()		{ SetModified(); }
	afx_msg void OnBnClickedNotify()				{ SetModified(); }
	afx_msg void OnEnChangeTooltipdelay()			{ SetModified(); }
	afx_msg void OnBnClickedOnlinesig()				{ SetModified(); }
	afx_msg void OnBnClickedEd2kfix();
#endif
	void OnDesktopmode(wxEvent& evt);
protected:
	wxComboBox m_language;
public:
	void Localize(void);
};


#endif
