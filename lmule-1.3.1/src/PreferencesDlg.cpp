// PreferencesDlg.cpp : implementation file
//

//#include "stdafx.h"
#include "wintypes.h"
#include "emule.h"
#include "PreferencesDlg.h"
#include "muuli_wdr.h"
#include "Wizard.h"
#include "wx/xrc/xmlres.h"
#include "wx/xrc/xh_all.h"

// CPreferencesDlg

//IMPLEMENT_DYNAMIC(CPreferencesDlg, CPropertySheet)
IMPLEMENT_DYNAMIC_CLASS(CPreferencesDlg,wxDialog)

BEGIN_EVENT_TABLE(CPreferencesDlg,wxDialog)
  EVT_BUTTON(ID_OK,CPreferencesDlg::OnBtnOk)
  EVT_BUTTON(ID_CANCEL,CPreferencesDlg::OnBtnCancel)
END_EVENT_TABLE()

#if 0
void _setFont(wxWindow* w)
{
  for(wxWindowList::Node* node=w->GetChildren().GetFirst();node;node=node->GetNext()) {
    wxWindow* win=(wxWindow*)node->GetData();
    _setFont(win);
    wxString lab=win->GetLabel();

    win->SetFont(wxFont(10,wxSWISS,wxNORMAL,wxNORMAL));
  }
}
#endif

CPreferencesDlg::CPreferencesDlg(wxWindow* parent,CPreferences* prefs)
  : wxDialog(parent,9999,_("Preferences"),wxDefaultPosition,wxDefaultSize,wxDEFAULT_DIALOG_STYLE|wxSYSTEM_MENU)
{
  //SetFont(wxFont(8,wxSWISS,wxNORMAL,wxNORMAL));

  // here comes brave new resource system
  wxNotebook* book=new wxNotebook(this,7773,wxPoint(0,0),wxSize(200,200));
  wxPanel* page1;

  // load pages
#if 0
  page1=wxXmlResource::Get()->LoadPanel(book,"DLG_PPG_IRC");
  book->AddPage(page1,"IRC");
#endif

  m_wndGeneral=new CPPgGeneral(book);
  m_wndConnection=new CPPgConnection(book);
  m_wndServer=new CPPgServer(book);
  m_wndFiles=new CPPgFiles(book);
  m_wndDirectories=new CPPgDirectories(book);
  m_wndStats=new CPPgStats(book);
  m_wndNotify=new CPPgNotify(book);
  //m_wndIRC=new CPPgIRC();
  m_wndIRC=NULL;
  m_wndTweaks=new CPPgTweaks(book);

  wxSize pageSize(520,460);
  pageSize=m_wndGeneral->GetSize();
  SetSize(pageSize.GetWidth(),pageSize.GetHeight()+40);
  book->SetSize(pageSize);

  // then the ok/cancel buttons please
  wxButton* mybut=new wxButton(this,ID_OK,_("OK"),wxPoint(pageSize.GetWidth()-2*80-16,pageSize.GetHeight()+10),wxSize(80,24));
  wxButton* otbut=new wxButton(this,ID_CANCEL,_("Cancel"),wxPoint(pageSize.GetWidth()-80-8,pageSize.GetHeight()+10),wxSize(80,24));

  SetPrefs(prefs);

  Centre();

  // force font small enought to show the pages
  //_setFont(this);

  // now call page initializators
  // no. bad idea. no preferences yet.
  // do it at showmodal instead!
  //m_wndGeneral.LoadSettings();

#if 0
	this->m_psh.dwFlags &= ~PSH_HASHELP;
	m_wndGeneral.m_psp.dwFlags &= ~PSH_HASHELP;
	m_wndConnection.m_psp.dwFlags &= ~PSH_HASHELP;
	m_wndServer.m_psp.dwFlags &= ~PSH_HASHELP;
	m_wndDirectories.m_psp.dwFlags &= ~PSH_HASHELP;
	m_wndFiles.m_psp.dwFlags &= ~PSH_HASHELP;
	m_wndStats.m_psp.dwFlags &= ~PSH_HASHELP;
	m_wndIRC.m_psp.dwFlags &= ~PSH_HASHELP;
	
	AddPage(&m_wndGeneral);
	AddPage(&m_wndConnection);
	AddPage(&m_wndServer);
	AddPage(&m_wndDirectories);
	AddPage(&m_wndFiles);
	AddPage(&m_wndNotify);
	AddPage(&m_wndStats);
	AddPage(&m_wndIRC);
#endif

	Localize();
}

CPreferencesDlg::~CPreferencesDlg()
{
}

int CPreferencesDlg::ShowModal()
{
  // setup pages
  m_wndGeneral->LoadSettings();
  m_wndFiles->LoadSettings();
  m_wndServer->LoadSettings();
  m_wndConnection->LoadSettings();
  m_wndDirectories->LoadSettings();
  m_wndStats->LoadSettings();
  m_wndNotify->LoadSettings();
  m_wndTweaks->LoadSettings();
  // and then do the show
  return wxDialog::ShowModal();
}

#if 0
BEGIN_MESSAGE_MAP(CPreferencesDlg, CPropertySheet)
	ON_WM_DESTROY()
END_MESSAGE_MAP()
#endif

void CPreferencesDlg::OnBtnWizard(wxEvent& evt)
{
  Wizard* test=new Wizard(this);
  test->SetPrefs(theApp.glob_prefs);//app_prefs);
  test->OnInitDialog();
  test->ShowModal();
  delete test;
  printf("** done\n");
}

#if 0
void CPreferencesDlg::OnDestroy()
{
	CPropertySheet::OnDestroy();
	app_prefs->Save();
	m_nActiveWnd = GetActiveIndex();
}

BOOL CPreferencesDlg::OnInitDialog()
{
	BOOL bResult = CPropertySheet::OnInitDialog();
	
	/*
	CString title = GetResString(IDS_EM_PREFS);
	title.Remove('&');
	SetTitle(title);
	*/

	m_nActiveWnd = 0;
	SetActivePage(m_nActiveWnd);
	Localize();

	return bResult;
}
#endif

void CPreferencesDlg::OnBtnOk(wxEvent& evt)
{
  // apply all pages
  m_wndGeneral->OnApply();
  m_wndConnection->OnApply();
  m_wndServer->OnApply();
  m_wndFiles->OnApply();
  m_wndDirectories->OnApply();
  m_wndStats->OnApply();
  m_wndNotify->OnApply();
  m_wndTweaks->OnApply();
  app_prefs->Save();
  EndModal(0);
}

void CPreferencesDlg::OnBtnCancel(wxEvent& evt)
{
  EndModal(0);
}
