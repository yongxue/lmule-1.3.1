//this file is part of eMule
//Copyright (C)2002 Merkur ( merkur-@users.sourceforge.net / http://www.emule-project.net )
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

//#include "StdAfx.h"
#include "Friend.h"
#include "packets.h"
#include "emule.h"

CFriend::CFriend(void)
{
	m_dwLastSeen = 0;
	m_dwLastUsedIP = 0;
	m_nLastUsedPort = 0;
	m_dwLastChatted = 0;
	m_strName = "";
	m_LinkedClient = 0;
	m_dwHasHash = 0;
	memset( m_abyUserhash, 0, 16);
}

//Added this to work with the IRC.. Probably a better way to do it.. But wanted this in the release..
CFriend::CFriend( uchar tm_abyUserhash[16], uint32 tm_dwLastSeen, uint32 tm_dwLastUsedIP, uint32 tm_nLastUsedPort, uint32 tm_dwLastChatted, wxString tm_strName, uint32 tm_dwHasHash){
	m_dwLastSeen = tm_dwLastSeen;
	m_dwLastUsedIP = tm_dwLastUsedIP;
	m_nLastUsedPort = tm_nLastUsedPort;
	m_dwLastChatted = tm_dwLastChatted;
	if( tm_dwHasHash ){
		memcpy(m_abyUserhash,tm_abyUserhash,16);
		m_dwHasHash = 1;
	}
	else
		m_dwHasHash = 0;
	m_strName = tm_strName;
	m_LinkedClient = 0;
}

CFriend::CFriend(CUpDownClient* client){
	assert ( client );
	//CTime lwtime;
	m_dwLastSeen = time(NULL);//mktime(lwtime.GetLocalTm());
	m_dwLastUsedIP = client->GetIP();
	m_nLastUsedPort = client->GetUserPort();
	m_dwLastChatted = 0;
	if (client->GetUserName())
		m_strName = client->GetUserName();
	else
		m_strName = "";
	memcpy(m_abyUserhash,client->GetUserHash(),16);
	m_LinkedClient = client;
	m_dwHasHash = 1;
}

CFriend::~CFriend(void)
{
}

void CFriend::LoadFromFile(CFile* file){
	file->Read(m_abyUserhash,16);
	file->Read(&m_dwLastUsedIP,4);
	file->Read(&m_nLastUsedPort,2);
	file->Read(&m_dwLastSeen,4);
	file->Read(&m_dwLastChatted,4);
	m_dwHasHash = 1;
	uint32 tagcount;
	file->Read(&tagcount,4);
	for (uint32 j = 0; j != tagcount;j++){
		CTag* newtag = new CTag(file);
		switch(newtag->tag->specialtag){
			case FF_NAME:{
				m_strName= newtag->tag->stringvalue;
				delete newtag;
				break;
			}
		}	
	}
}

void CFriend::WriteToFile(CFile* file){
//	if (!m_dwHasHash)
//		for( int i = 0; i < 16; i++ )
//			m_abyUserhash[i] = 1;
	file->Write(m_abyUserhash,16);
	file->Write(&m_dwLastUsedIP,4);
	file->Write(&m_nLastUsedPort,2);
	file->Write(&m_dwLastSeen,4);
	file->Write(&m_dwLastChatted,4);

	uint32 tagcount = 0;
	if (!m_strName.IsEmpty())
		tagcount++;
	file->Write(&tagcount,4);
	if (!m_strName.IsEmpty()){
		CTag nametag(FF_NAME,(char*)m_strName.GetData());
		nametag.WriteTagToFile(file);
	}
}
