#ifndef PPGSERVER_H
#define PPGSERVER_H

#include "Preferences.h"

#include <wx/panel.h>

// CPPgServer dialog

class CPPgServer : public wxPanel //CPropertyPage
{
  //DECLARE_DYNAMIC(CPPgServer)
  DECLARE_DYNAMIC_CLASS(CPPgServer)
    CPPgServer() {};

public:
	CPPgServer(wxWindow* parent);
	virtual ~CPPgServer();

	void SetPrefs(CPreferences* in_prefs) {	app_prefs = in_prefs; }

// Dialog Data
	enum { IDD = IDD_PPG_SERVER };
protected:
	CPreferences *app_prefs;
protected:
	//virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	DECLARE_EVENT_TABLE()
	//DECLARE_MESSAGE_MAP()
public:
	//afx_msg void OnBnClickedCheck1();
	//virtual BOOL OnInitDialog();
 public:
	void LoadSettings(void);
public:
	void OnApply();
#if 0
	virtual BOOL OnApply();

	afx_msg void OnEnChangeServerretries()			{ SetModified(); }
	afx_msg void OnBnClickedRemovedead()			{ SetModified(); }
	afx_msg void OnBnClickedAutoserver()			{ SetModified(); }
	afx_msg void OnBnClickedUpdateserverconnect()	{ SetModified(); }
	afx_msg void OnBnClickedUpdateserverclient()	{ SetModified(); }
	afx_msg void OnBnClickedFilter()				{ SetModified(); }
	afx_msg void OnBnClickedScore()					{ SetModified(); }
#endif
	void Localize(void);
	void OnBnClickedEditadr(wxEvent& evt);
};
#endif
