//this file is part of eMule
//Copyright (C)2002 Merkur ( merkur-@users.sourceforge.net / http://www.emule-project.net )
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#ifndef CLIENTCRED_H
#define CLIENTCRED_H

#include "types.h"
#include "Preferences.h"
#include "MapKey.h"

/* winemaker: #pragma pack(1) */
//#include <pshpack1.h>

#pragma pack(1)
struct CreditStruct{
	uchar		abyKey[16];
	uint32		nUploaded;	//uploaded TO him
	uint32		nDownloaded; // downloaded from him
	uint32		nLastSeen;
	uint32		nReserved1;
	uint32		nReserved2;
	uint16		nReserved3;
};
#pragma pack()

class CClientCredits{
public:
	CClientCredits(CreditStruct* in_credits);
	CClientCredits(uchar* key);
	~CClientCredits();

	uchar*	GetKey()						{return m_pCredits->abyKey;}
	CreditStruct* GetDataStruct()			{return m_pCredits;}
	void	AddDownloaded(uint32 bytes)		{m_pCredits->nDownloaded += bytes;}
	void	AddUploaded(uint32 bytes)		{m_pCredits->nUploaded += bytes;}
	uint32	GetUploadedTotal()				{return m_pCredits->nUploaded;} //uploaded TO him
	uint32	GetDownloadedTotal()			{return m_pCredits->nDownloaded; } // downloaded from him
	float	GetScoreRatio();
	void	SetLastSeen()					{m_pCredits->nLastSeen = time(NULL);}

private:
	CreditStruct*	m_pCredits;
};

class CClientCreditsList{
public:
	CClientCreditsList(CPreferences* in_prefs);
	~CClientCreditsList();
	
	CClientCredits* GetCredit(uchar* key);
	void	Process();
protected:
	void	LoadList();
	void	SaveList();
private:
	CMap<CCKey, CCKey, CClientCredits*, CClientCredits*> m_mapClients;
	CPreferences*	m_pAppPrefs;
	uint32			m_nLastSaved;
};

#endif
