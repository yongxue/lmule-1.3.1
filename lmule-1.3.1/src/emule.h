//this file is part of eMule
//Copyright (C)2002 Merkur ( merkur-@users.sourceforge.net / http://www.emule-project.net )
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#ifndef EMULE_H
#define EMULE_H

//#ifndef __AFXWIN_H__
//	#error include 'stdafx.h' before including this file for PCH
//#endif

#include "wx/app.h"
#include <wx/thread.h>
#include <wx/intl.h>

#include "resource.h"
#include "emuleDlg.h"
#include "KnownFileList.h"
#include "Preferences.h"
#include "sockets.h"
#include "ServerList.h"
#include "SharedFileList.h"
#include "SearchList.h"
#include "ListenSocket.h"
#include "UploadQueue.h"
#include "DownloadQueue.h"
#include "ClientList.h"
#include "ClientCredits.h"
#include "FriendList.h"
#include "ClientUDPSocket.h"
#include "IPFilter.h"
#include "WebServer.h"
//#include <afxmt.h>

class CSearchList;
class CUploadQueue;
class CListenSocket;
class CDownloadQueue;
class CSharedFileList;

#define G_BLEND(a,b) ( ((int)a*b)/100>255?255:((int)a*b)/100 ) 

class CemuleApp : public wxApp
{
public:

	CemuleApp();
	virtual ~CemuleApp();
	int OnExit();

	CemuleDlg*			emuledlg;
	CClientList*		clientlist;
	CKnownFileList*		knownfiles;
	CPreferences*		glob_prefs;
	CServerConnect*		serverconnect;
	CServerList*		serverlist;	
	CSharedFileList*	sharedfiles;
	CSearchList*		searchlist;
	CListenSocket*		listensocket;
	CUploadQueue*		uploadqueue;
	CDownloadQueue*		downloadqueue;
	CClientCreditsList*	clientcredits;
	CClientUDPSocket*       clientudp;
	CFriendList*		friendlist;
	CWebServer*             webserver;
	wxMutex				hashing_mut;
	wxMutex pendingMutex;
	//virtual BOOL		InitInstance();
	virtual bool 		OnInit();
	wxString*			pendinglink;
	//tagCOPYDATASTRUCT  sendstruct; //added by Cax2 28/10/02 
	CIPFilter*			ipfilter;

	uint64				stat_sessionReceivedBytes;
	uint64				stat_sessionSentBytes;
	uint16				stat_reconnects;
	DWORD				stat_transferStarttime;
	DWORD				stat_serverConnectTime;
	uint16				stat_filteredclients;

// Implementierung
	// ed2k link functions
	wxString		StripInvalidFilenameChars(wxString strText, bool bKeepSpaces = true);
	wxString		CreateED2kLink( CAbstractFile* f );
	wxString		CreateHTMLED2kLink( CAbstractFile* f );
        wxString         CreateED2kSourceLink( CAbstractFile* f );
	bool		CopyTextToClipboard( wxString strText );
	void		OnlineSig(); 
	void		UpdateReceivedBytes(int32 bytesToAdd);
	void		UpdateSentBytes(int32 bytesToAdd);

	//DECLARE_MESSAGE_MAP()
protected:
	bool ProcessCommandline();
	void SetTimeOnTransfer();

	wxLocale m_locale;

};
//extern CemuleApp theApp;
#define theApp (*((CemuleApp*)wxTheApp))

//#include <dmalloc.h>

// helper
wxColour GetColour(wxSystemColour what);


#endif
