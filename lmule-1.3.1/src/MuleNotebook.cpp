/////////////////////////////////////////////////////////////////////////////
// Name:        notebook.cpp
// Purpose:
// Author:      Robert Roebling
// Id:          $Id: MuleNotebook.cpp,v 1.5 2003/03/24 20:10:07 un-thesis Exp $
// Copyright:   (c) 1998 Robert Roebling, Vadim Zeitlin
// Licence:     wxWindows licence
/////////////////////////////////////////////////////////////////////////////

#ifdef __GNUG__
#pragma implementation "MuleNotebook.h"
#endif

#include "CMuleNotebookBase.h"

#if wxUSE_NOTEBOOK
#include "wx/bitmap.h"
#include "wx/panel.h"
#include "wx/utils.h"
#include "wx/imaglist.h"
#include "wx/intl.h"
#include "wx/log.h"

//#include "wx/gtk/private.h"
#include <gtk/gtk.h>
#include "wx/gtk/win_gtk.h"

#include <gdk/gdkkeysyms.h>

// borrowed from private.h
#ifdef __WXGTK20__
#if wxUSE_UNICODE
    #define wxGTK_CONV(s) wxConvUTF8.cWX2MB(s)
    #define wxGTK_CONV_BACK(s) wxConvUTF8.cMB2WX(s)
#else
    #define wxGTK_CONV(s) wxConvUTF8.cWC2MB( wxConvLocal.cWX2WC(s) )
    #define wxGTK_CONV_BACK(s)  wxConvLocal.cWC2WX( (wxConvUTF8.cMB2WC( s ) ) )
#endif
#else
    #define wxGTK_CONV(s) s.c_str()
    #define wxGTK_CONV_BACK(s) s
#endif

// this GtkNotebook struct field has been renamed
#ifdef __WXGTK20__
    #define NOTEBOOK_PANEL(nb)  GTK_NOTEBOOK(nb)->event_window
#else
    #define NOTEBOOK_PANEL(nb)  GTK_NOTEBOOK(nb)->panel
#endif

// ----------------------------------------------------------------------------
// events
// ----------------------------------------------------------------------------

DEFINE_EVENT_TYPE(wxEVT_COMMAND_MULENOTEBOOK_PAGE_CHANGED)
DEFINE_EVENT_TYPE(wxEVT_COMMAND_MULENOTEBOOK_PAGE_CHANGING)
DEFINE_EVENT_TYPE(wxEVT_COMMAND_MULENOTEBOOK_PAGE_CLOSED)

//-----------------------------------------------------------------------------
// idle system
//-----------------------------------------------------------------------------

extern void wxapp_install_idle_handler();
extern bool g_isIdle;

//-----------------------------------------------------------------------------
// data
//-----------------------------------------------------------------------------

extern bool g_blockEventsOnDrag;

//-----------------------------------------------------------------------------
// wxGtkMuleNotebookPage
//-----------------------------------------------------------------------------

// VZ: this is rather ugly as we keep the pages themselves in an array (it
//     allows us to have quite a few functions implemented in the base class)
//     but the page data is kept in a separate list, so we must maintain them
//     in sync manually... of course, the list had been there before the base
//     class which explains it but it still would be nice to do something
//     about this one day

class wxGtkMuleNotebookPage: public wxObject
{
public:
  wxGtkMuleNotebookPage()
  {
    m_image = -1;
    m_page = (GtkNotebookPage *) NULL;
    m_box = (GtkWidget *) NULL;
  }

  wxString           m_text;
  int                m_image;
  unsigned long      m_itemData;
  GtkNotebookPage   *m_page;
  GtkLabel          *m_label;
  GtkWidget         *m_box;     // in which the label and image are packed
};

#include "wx/listimpl.cpp"
WX_DEFINE_LIST(wxGtkMuleNotebookPagesList);

int getPage(CMuleNotebook* nb,GtkWidget* parent)
{
  // try to find which page was actually clicked..
  int currentPage=-1;
  int count=nb->GetPageCount();
  for(int i=0;i<count;i++) {
    GtkWidget* page=gtk_notebook_get_nth_page(GTK_NOTEBOOK(nb->m_widget),i);
    if(gtk_notebook_get_tab_label(GTK_NOTEBOOK(nb->m_widget),page)==parent) {
      currentPage=i;
      break;
    }
  }
  return currentPage;
}

//
// "enter-notify
// 
static gboolean gtk_notebook_enter_cb(GtkWidget* widget,GdkEventCrossing* event,
				      gpointer user_data)
{
  // collect data
  CMuleNotebook* nb=(CMuleNotebook*)user_data;
  GtkPixmap* pixmapwid=GTK_PIXMAP(gtk_object_get_data(GTK_OBJECT(widget),"myPixmap"));

  // locate page...
  GtkWidget* parent=GTK_WIDGET(gtk_object_get_data(GTK_OBJECT(widget),"myParent"));

  int currentPage=getPage(nb,parent);
  if(currentPage<0) {
    return true;
  }

  // then get from active image list
  int curimg=nb->GetPageImage(currentPage);
  if(curimg>=0) {
    const wxBitmap *bmp = nb->GetStateImageList()->GetBitmap(curimg);
    GdkPixmap *pixmap = bmp->GetPixmap();
    GdkBitmap *mask = (GdkBitmap*) NULL;
    if ( bmp->GetMask() )
      {
	mask = bmp->GetMask()->GetBitmap();
      }
    // does this loose the old pixmap?
    gtk_pixmap_set(pixmapwid,pixmap,mask);
  }
  return true;
}

static gboolean gtk_notebook_leave_cb(GtkWidget* widget,GdkEventCrossing* event,
				      gpointer user_data)
{
  // collect data
  CMuleNotebook* nb=(CMuleNotebook*)user_data;
  GtkPixmap* pixmapwid=GTK_PIXMAP(gtk_object_get_data(GTK_OBJECT(widget),"myPixmap"));

  // locate page...
  GtkWidget* parent=GTK_WIDGET(gtk_object_get_data(GTK_OBJECT(widget),"myParent"));

  int currentPage=getPage(nb,parent);
  if(currentPage<0) {
    return true;
  }

  // then get from active image list
  int curimg=nb->GetPageImage(currentPage);
  if(curimg>=0) {
    const wxBitmap *bmp = nb->GetImageList()->GetBitmap(curimg);
    GdkPixmap *pixmap = bmp->GetPixmap();
    GdkBitmap *mask = (GdkBitmap*) NULL;
    if ( bmp->GetMask() )
      {
	mask = bmp->GetMask()->GetBitmap();
      }
    // does this loose the old pixmap?
    gtk_pixmap_set(pixmapwid,pixmap,mask);
  }
  return true;
}

//
// "click" (on a image)
//
static gboolean gtk_notebook_button_cb(GtkWidget* widget,GdkEventButton* event,
				       gpointer user_data)
{
  if(event->button==1) {
    CMuleNotebook* notebook=(CMuleNotebook*)user_data;
    // this is here because user can click close on a tab that is not current page.
    // button press event will come _before_ the notebook handles it
    // (ok, ok, I think there was a neat way to let notebook handle the event first
    //  and after that run the callbak, but I didn't remember it while writing this)
    // so we'll first locate which tab user clicked and then pass that to the receiver.
    GtkWidget* parent=GTK_WIDGET(gtk_object_get_data(GTK_OBJECT(widget),"myParent"));
    // try to find which page was actually clicked..
    int currentPage=getPage(notebook,parent);
    // send event (only if match was found)
    if(currentPage>=0) {
      CMuleNotebookEvent eventChanged( wxEVT_COMMAND_MULENOTEBOOK_PAGE_CLOSED,
				       notebook->GetId(), 
				       currentPage);
      eventChanged.SetEventObject( notebook );
      notebook->GetEventHandler()->ProcessEvent( eventChanged );
    }
  }
  return false;
}

//-----------------------------------------------------------------------------
// "switch_page"
//-----------------------------------------------------------------------------

static void gtk_notebook_page_change_callback(GtkNotebook *WXUNUSED(widget),
                                              GtkNotebookPage *WXUNUSED(page),
                                              gint page,
                                              CMuleNotebook *notebook )
{
    // are you trying to call SetSelection() from a notebook event handler?
    // you shouldn't!
    wxCHECK_RET( !notebook->m_inSwitchPage,
                 _T("gtk_notebook_page_change_callback reentered") );

    notebook->m_inSwitchPage = TRUE;
    if (g_isIdle)
        wxapp_install_idle_handler();

    int old = notebook->GetSelection();

    CMuleNotebookEvent eventChanging( wxEVT_COMMAND_MULENOTEBOOK_PAGE_CHANGING,
                                   notebook->GetId(), page, old );
    eventChanging.SetEventObject( notebook );

    if ( (notebook->GetEventHandler()->ProcessEvent(eventChanging)) &&
         !eventChanging.IsAllowed() )
    {
        /* program doesn't allow the page change */
        gtk_signal_emit_stop_by_name( GTK_OBJECT(notebook->m_widget),
                                      "switch_page" );
    }
    else // change allowed
    {
        // make CMuleNotebook::GetSelection() return the correct (i.e. consistent
        // with CMuleNotebookEvent::GetSelection()) value even though the page is
        // not really changed in GTK+
        notebook->m_selection = page;

        CMuleNotebookEvent eventChanged( wxEVT_COMMAND_MULENOTEBOOK_PAGE_CHANGED,
                                      notebook->GetId(), page, old );
        eventChanged.SetEventObject( notebook );
        notebook->GetEventHandler()->ProcessEvent( eventChanged );
    }

    notebook->m_inSwitchPage = FALSE;
}

//-----------------------------------------------------------------------------
// "size_allocate"
//-----------------------------------------------------------------------------

static void gtk_page_size_callback( GtkWidget *WXUNUSED(widget), GtkAllocation* alloc, wxWindow *win )
{
    if (g_isIdle)
        wxapp_install_idle_handler();

    if ((win->m_x == alloc->x) &&
        (win->m_y == alloc->y) &&
        (win->m_width == alloc->width) &&
        (win->m_height == alloc->height))
    {
        return;
    }

    win->SetSize( alloc->x, alloc->y, alloc->width, alloc->height );

    /* GTK 1.2 up to version 1.2.5 is broken so that we have to call allocate
       here in order to make repositioning after resizing to take effect. */
    if ((gtk_major_version == 1) &&
        (gtk_minor_version == 2) &&
        (gtk_micro_version < 6) &&
        (win->m_wxwindow) &&
        (GTK_WIDGET_REALIZED(win->m_wxwindow)))
    {
        gtk_widget_size_allocate( win->m_wxwindow, alloc );
    }
}

//-----------------------------------------------------------------------------
// "realize" from m_widget
//-----------------------------------------------------------------------------

static gint
gtk_notebook_realized_callback( GtkWidget * WXUNUSED(widget), wxWindow *win )
{
    if (g_isIdle)
        wxapp_install_idle_handler();

    /* GTK 1.2 up to version 1.2.5 is broken so that we have to call a queue_resize
       here in order to make repositioning before showing to take effect. */
    gtk_widget_queue_resize( win->m_widget );

    return FALSE;
}

//-----------------------------------------------------------------------------
// "key_press_event"
//-----------------------------------------------------------------------------

static gint gtk_notebook_key_press_callback( GtkWidget *widget, GdkEventKey *gdk_event, CMuleNotebook *win )
{
    if (g_isIdle)
        wxapp_install_idle_handler();

    if (!win->m_hasVMT) return FALSE;
    if (g_blockEventsOnDrag) return FALSE;

    /* win is a control: tab can be propagated up */
    if ((gdk_event->keyval == GDK_Tab) || (gdk_event->keyval == GDK_ISO_Left_Tab))
    {
        int sel = win->GetSelection();
        wxGtkMuleNotebookPage *nb_page = win->GetNotebookPage(sel);
        wxCHECK_MSG( nb_page, FALSE, _T("invalid selection in CMuleNotebook") );

        wxNavigationKeyEvent event;
        event.SetEventObject( win );
        /* GDK reports GDK_ISO_Left_Tab for SHIFT-TAB */
        event.SetDirection( (gdk_event->keyval == GDK_Tab) );
        /* CTRL-TAB changes the (parent) window, i.e. switch notebook page */
        event.SetWindowChange( (gdk_event->state & GDK_CONTROL_MASK) );
        event.SetCurrentFocus( win );

        CMuleNotebookPage *client = win->GetPage(sel);
        if ( !client->GetEventHandler()->ProcessEvent( event ) )
        {
             client->SetFocus();
        }

        gtk_signal_emit_stop_by_name( GTK_OBJECT(widget), "key_press_event" );
        return TRUE;
    }

    return FALSE;
}

//-----------------------------------------------------------------------------
// InsertChild callback for CMuleNotebook
//-----------------------------------------------------------------------------

static void wxInsertChildInNotebook( CMuleNotebook* WXUNUSED(parent), wxWindow* WXUNUSED(child) )
{
    /* we don't do anything here but pray */
}

//-----------------------------------------------------------------------------
// CMuleNotebook
//-----------------------------------------------------------------------------

IMPLEMENT_DYNAMIC_CLASS(CMuleNotebook,wxControl)

BEGIN_EVENT_TABLE(CMuleNotebook, wxControl)
    EVT_NAVIGATION_KEY(CMuleNotebook::OnNavigationKey)
END_EVENT_TABLE()

void CMuleNotebook::Init()
{
    m_padding = 0;
    m_inSwitchPage = FALSE;

    m_imageList = (wxImageList *) NULL;
    m_pagesData.DeleteContents( TRUE );
    m_selection = -1;
    m_themeEnabled = TRUE;
}

CMuleNotebook::CMuleNotebook()
{
    Init();
}

CMuleNotebook::CMuleNotebook( wxWindow *parent, wxWindowID id,
      const wxPoint& pos, const wxSize& size,
      long style, const wxString& name )
{
    Init();
    Create( parent, id, pos, size, style, name );
}

CMuleNotebook::~CMuleNotebook()
{
    /* don't generate change page events any more */
    gtk_signal_disconnect_by_func( GTK_OBJECT(m_widget),
      GTK_SIGNAL_FUNC(gtk_notebook_page_change_callback), (gpointer) this );

    DeleteAllPages();
}

bool CMuleNotebook::Create(wxWindow *parent, wxWindowID id,
                        const wxPoint& pos, const wxSize& size,
                        long style, const wxString& name )
{
    m_needParent = TRUE;
    m_acceptsFocus = TRUE;
    m_insertCallback = (wxInsertChildFunction)wxInsertChildInNotebook;

    if (!PreCreation( parent, pos, size ) ||
        !CreateBase( parent, id, pos, size, style, wxDefaultValidator, name ))
    {
        wxFAIL_MSG( wxT("wxNoteBook creation failed") );
        return FALSE;
    }


    m_widget = gtk_notebook_new();

    gtk_notebook_set_scrollable( GTK_NOTEBOOK(m_widget), 1 );

    gtk_signal_connect( GTK_OBJECT(m_widget), "switch_page",
      GTK_SIGNAL_FUNC(gtk_notebook_page_change_callback), (gpointer)this );

    m_parent->DoAddChild( this );

    if (m_windowStyle & wxNB_RIGHT)
        gtk_notebook_set_tab_pos( GTK_NOTEBOOK(m_widget), GTK_POS_RIGHT );
    if (m_windowStyle & wxNB_LEFT)
        gtk_notebook_set_tab_pos( GTK_NOTEBOOK(m_widget), GTK_POS_LEFT );
    if (m_windowStyle & wxNB_BOTTOM)
        gtk_notebook_set_tab_pos( GTK_NOTEBOOK(m_widget), GTK_POS_BOTTOM );

    gtk_signal_connect( GTK_OBJECT(m_widget), "key_press_event",
      GTK_SIGNAL_FUNC(gtk_notebook_key_press_callback), (gpointer)this );

    PostCreation();

    SetFont( parent->GetFont() );

    gtk_signal_connect( GTK_OBJECT(m_widget), "realize",
                            GTK_SIGNAL_FUNC(gtk_notebook_realized_callback), (gpointer) this );

    Show( TRUE );

    return TRUE;
}

int CMuleNotebook::GetSelection() const
{
    wxCHECK_MSG( m_widget != NULL, -1, wxT("invalid notebook") );

    if ( m_selection == -1 )
    {
        GList *nb_pages = GTK_NOTEBOOK(m_widget)->children;

        if (g_list_length(nb_pages) != 0)
        {
            GtkNotebook *notebook = GTK_NOTEBOOK(m_widget);

            gpointer cur = notebook->cur_page;
            if ( cur != NULL )
            {
                wxConstCast(this, CMuleNotebook)->m_selection =
                    g_list_index( nb_pages, cur );
            }
        }
    }

    return m_selection;
}

wxString CMuleNotebook::GetPageText( int page ) const
{
    wxCHECK_MSG( m_widget != NULL, wxT(""), wxT("invalid notebook") );

    wxGtkMuleNotebookPage* nb_page = GetNotebookPage(page);
    if (nb_page)
        return nb_page->m_text;
    else
        return wxT("");
}

unsigned long CMuleNotebook::GetUserData(int page) const
{
    wxGtkMuleNotebookPage* nb_page = GetNotebookPage(page);
    if (nb_page)
      return nb_page->m_itemData;
    else return 0;
}

int CMuleNotebook::GetPageImage( int page ) const
{
    wxCHECK_MSG( m_widget != NULL, -1, wxT("invalid notebook") );

    wxGtkMuleNotebookPage* nb_page = GetNotebookPage(page);
    if (nb_page)
        return nb_page->m_image;
    else
        return -1;
}

wxGtkMuleNotebookPage* CMuleNotebook::GetNotebookPage( int page ) const
{
    wxCHECK_MSG( m_widget != NULL, (wxGtkMuleNotebookPage*) NULL, wxT("invalid notebook") );

    wxCHECK_MSG( page < (int)m_pagesData.GetCount(), (wxGtkMuleNotebookPage*) NULL, wxT("invalid notebook index") );

    return m_pagesData.Item(page)->GetData();
}

int CMuleNotebook::SetSelection( int page )
{
    wxCHECK_MSG( m_widget != NULL, -1, wxT("invalid notebook") );

    wxCHECK_MSG( page < (int)m_pagesData.GetCount(), -1, wxT("invalid notebook index") );

    int selOld = GetSelection();

    // cache the selection
    m_selection = page;
    gtk_notebook_set_page( GTK_NOTEBOOK(m_widget), page );

    CMuleNotebookPage *client = GetPage(page);
    if ( client )
        client->SetFocus();

    return selOld;
}

bool CMuleNotebook::SetPageText( int page, const wxString &text )
{
    wxCHECK_MSG( m_widget != NULL, FALSE, wxT("invalid notebook") );

    wxGtkMuleNotebookPage* nb_page = GetNotebookPage(page);

    wxCHECK_MSG( nb_page, FALSE, wxT("SetPageText: invalid page index") );

    nb_page->m_text = text;

    gtk_label_set( nb_page->m_label, wxGTK_CONV( nb_page->m_text ) );

    return TRUE;
}

void CMuleNotebook::SetUserData(int page,unsigned long itemData)
{
    wxGtkMuleNotebookPage* nb_page = GetNotebookPage(page);
    nb_page->m_itemData=itemData;
}

bool CMuleNotebook::SetPageImage( int page, int image )
{
    /* HvdH 28-12-98: now it works, but it's a bit of a kludge */

    wxGtkMuleNotebookPage* nb_page = GetNotebookPage(page);

    if (!nb_page) return FALSE;

    /* Optimization posibility: return immediately if image unchanged.
     * Not enabled because it may break existing (stupid) code that
     * manipulates the imagelist to cycle images */

    /* if (image == nb_page->m_image) return TRUE; */

    /* For different cases:
       1) no image -> no image
       2) image -> no image
       3) no image -> image
       4) image -> image */

    if (image == -1 && nb_page->m_image == -1)
        return TRUE; /* Case 1): Nothing to do. */

    GtkWidget *pixmapwid = (GtkWidget*) NULL;

    if (nb_page->m_image != -1)
    {
        /* Case 2) or 4). There is already an image in the gtkhbox. Let's find it */

        GList *child = gtk_container_children(GTK_CONTAINER(nb_page->m_box));
        while (child)
        {
            if (GTK_IS_PIXMAP(child->data))
            {
                pixmapwid = GTK_WIDGET(child->data);
                break;
            }
            child = child->next;
        }

        /* We should have the pixmap widget now */
        wxASSERT(pixmapwid != NULL);

        if (image == -1)
        {
            /* If there's no new widget, just remove the old from the box */
            gtk_container_remove(GTK_CONTAINER(nb_page->m_box), pixmapwid);
            nb_page->m_image = -1;

            return TRUE; /* Case 2) */
        }
    }

    /* Only cases 3) and 4) left */
    wxASSERT( m_imageList != NULL ); /* Just in case */

    /* Construct the new pixmap */
    const wxBitmap *bmp = m_imageList->GetBitmap(image);
    GdkPixmap *pixmap = bmp->GetPixmap();
    GdkBitmap *mask = (GdkBitmap*) NULL;
    if ( bmp->GetMask() )
    {
        mask = bmp->GetMask()->GetBitmap();
    }

    if (pixmapwid == NULL)
    {
        /* Case 3) No old pixmap. Create a new one and prepend it to the hbox */
        pixmapwid = gtk_pixmap_new (pixmap, mask );

        /* CHECKME: Are these pack flags okay? */
        gtk_box_pack_start(GTK_BOX(nb_page->m_box), pixmapwid, FALSE, FALSE, m_padding);
        gtk_widget_show(pixmapwid);
    }
    else
    {
        /* Case 4) Simply replace the pixmap */
        gtk_pixmap_set(GTK_PIXMAP(pixmapwid), pixmap, mask);
    }

    nb_page->m_image = image;

    return TRUE;
}

void CMuleNotebook::SetPageSize( const wxSize &WXUNUSED(size) )
{
    wxFAIL_MSG( wxT("CMuleNotebook::SetPageSize not implemented") );
}

void CMuleNotebook::SetPadding( const wxSize &padding )
{
    wxCHECK_RET( m_widget != NULL, wxT("invalid notebook") );

    m_padding = padding.GetWidth();

    int i;
    for (i=0; i<int(GetPageCount()); i++)
    {
        wxGtkMuleNotebookPage* nb_page = GetNotebookPage(i);
        wxASSERT(nb_page != NULL);

        if (nb_page->m_image != -1)
        {
            // gtk_box_set_child_packing sets padding on BOTH sides
            // icon provides left padding, label provides center and right
            int image = nb_page->m_image;
            SetPageImage(i,-1);
            SetPageImage(i,image);
        }
        wxASSERT(nb_page->m_label);
        gtk_box_set_child_packing(GTK_BOX(nb_page->m_box),
                                  GTK_WIDGET(nb_page->m_label),
                                  FALSE, FALSE, m_padding, GTK_PACK_END);
    }
}

void CMuleNotebook::SetTabSize(const wxSize& WXUNUSED(sz))
{
    wxFAIL_MSG( wxT("CMuleNotebook::SetTabSize not implemented") );
}

bool CMuleNotebook::DeleteAllPages()
{
    wxCHECK_MSG( m_widget != NULL, FALSE, wxT("invalid notebook") );

    while (m_pagesData.GetCount() > 0)
        DeletePage( m_pagesData.GetCount()-1 );

    wxASSERT_MSG( GetPageCount() == 0, _T("all pages must have been deleted") );

    return CMuleNotebookBase::DeleteAllPages();
}

bool CMuleNotebook::DeletePage( int page )
{
    // GTK sets GtkNotebook.cur_page to NULL before sending the switch page
    // event so we have to store the selection internally
    if ( m_selection == -1 )
    {
        m_selection = GetSelection();
        if ( m_selection == (int)m_pagesData.GetCount() - 1 )
        {
            // the index will become invalid after the page is deleted
            m_selection = -1;
        }
    }

    // it will call our DoRemovePage() to do the real work
    return CMuleNotebookBase::DeletePage(page);
}

CMuleNotebookPage *CMuleNotebook::DoRemovePage( int page )
{
    CMuleNotebookPage *client = CMuleNotebookBase::DoRemovePage(page);
    if ( !client )
        return NULL;

    gtk_widget_ref( client->m_widget );
    gtk_widget_unrealize( client->m_widget );
    gtk_widget_unparent( client->m_widget );

    gtk_notebook_remove_page( GTK_NOTEBOOK(m_widget), page );

    m_pagesData.DeleteObject(GetNotebookPage(page));

    return client;
}

bool CMuleNotebook::InsertPage( int position,
                             CMuleNotebookPage* win,
                             const wxString& text,
                             bool select,
                             int imageId,
				unsigned long itemData)
{
    wxCHECK_MSG( m_widget != NULL, FALSE, wxT("invalid notebook") );

    wxCHECK_MSG( win->GetParent() == this, FALSE,
               wxT("Can't add a page whose parent is not the notebook!") );

    wxCHECK_MSG( position >= 0 && position <= GetPageCount(), FALSE,
                 _T("invalid page index in CMuleNotebookPage::InsertPage()") );

    /* don't receive switch page during addition */
    gtk_signal_disconnect_by_func( GTK_OBJECT(m_widget),
      GTK_SIGNAL_FUNC(gtk_notebook_page_change_callback), (gpointer) this );

    if (m_themeEnabled)
        win->SetThemeEnabled(TRUE);

    GtkNotebook *notebook = GTK_NOTEBOOK(m_widget);

    wxGtkMuleNotebookPage *nb_page = new wxGtkMuleNotebookPage();

    if ( position == GetPageCount() )
        m_pagesData.Append( nb_page );
    else
        m_pagesData.Insert( m_pagesData.Item( position ), nb_page );

    m_pages.Insert(win, position);

    nb_page->m_box = gtk_hbox_new( FALSE, 1 );
    nb_page->m_itemData=itemData;
    gtk_container_border_width( GTK_CONTAINER(nb_page->m_box), 2 );

    gtk_signal_connect( GTK_OBJECT(win->m_widget), "size_allocate",
      GTK_SIGNAL_FUNC(gtk_page_size_callback), (gpointer)win );

    if (position < 0)
        gtk_notebook_append_page( notebook, win->m_widget, nb_page->m_box );
    else
        gtk_notebook_insert_page( notebook, win->m_widget, nb_page->m_box, position );

    nb_page->m_page = (GtkNotebookPage*) g_list_last(notebook->children)->data;

    /* set the label image */
    nb_page->m_image = imageId;

    if (imageId != -1)
    {
        wxASSERT( m_imageList != NULL );

        const wxBitmap *bmp = m_imageList->GetBitmap(imageId);
        GdkPixmap *pixmap = bmp->GetPixmap();
        GdkBitmap *mask = (GdkBitmap*) NULL;
        if ( bmp->GetMask() )
        {
            mask = bmp->GetMask()->GetBitmap();
        }

        GtkWidget *pixmapwid = gtk_pixmap_new (pixmap, mask );

	GtkEventBox* ebox=GTK_EVENT_BOX(gtk_event_box_new());

	// add pixmap into an event box
	gtk_container_add(GTK_CONTAINER(ebox),pixmapwid);

	// and then add the event box to notebook label
	gtk_box_pack_start(GTK_BOX(nb_page->m_box), GTK_WIDGET(ebox), FALSE, FALSE, m_padding);
        //gtk_box_pack_start(GTK_BOX(nb_page->m_box), pixmapwid, FALSE, FALSE, m_padding);

        gtk_widget_show(pixmapwid);
	gtk_widget_show(GTK_WIDGET(ebox));

	gtk_signal_connect( GTK_OBJECT(ebox),"button_press_event",
			    GTK_SIGNAL_FUNC(gtk_notebook_button_cb),(gpointer)this);

	gtk_signal_connect(GTK_OBJECT(ebox),"enter_notify_event",
			   GTK_SIGNAL_FUNC(gtk_notebook_enter_cb),(gpointer)this);

	gtk_signal_connect(GTK_OBJECT(ebox),"leave_notify_event",
			   GTK_SIGNAL_FUNC(gtk_notebook_leave_cb),(gpointer)this);

	gtk_object_set_data(GTK_OBJECT(ebox),"myParent",nb_page->m_box);
	// direct pointer to the pixmap widget
	gtk_object_set_data(GTK_OBJECT(ebox),"myPixmap",pixmapwid);

    }

    /* set the label text */
    nb_page->m_text = text;
    if (nb_page->m_text.IsEmpty()) nb_page->m_text = wxT("");
  
    nb_page->m_label = GTK_LABEL( gtk_label_new(nb_page->m_text.mbc_str()) );
    gtk_box_pack_end( GTK_BOX(nb_page->m_box), GTK_WIDGET(nb_page->m_label), FALSE, FALSE, m_padding );
  
    /* show the label */
    gtk_widget_show( GTK_WIDGET(nb_page->m_label) );
    if (select && (m_pagesData.GetCount() > 1))
    {
        if (position < 0)
            SetSelection( GetPageCount()-1 );
        else
            SetSelection( position );
    }

    gtk_signal_connect( GTK_OBJECT(m_widget), "switch_page",
      GTK_SIGNAL_FUNC(gtk_notebook_page_change_callback), (gpointer)this );

    return TRUE;
}

void CMuleNotebook::OnNavigationKey(wxNavigationKeyEvent& event)
{
    if (event.IsWindowChange())
        AdvanceSelection( event.GetDirection() );
    else
        event.Skip();
}

#if wxUSE_CONSTRAINTS

// override these 2 functions to do nothing: everything is done in OnSize
void CMuleNotebook::SetConstraintSizes( bool WXUNUSED(recurse) )
{
    // don't set the sizes of the pages - their correct size is not yet known
    wxControl::SetConstraintSizes(FALSE);
}

bool CMuleNotebook::DoPhase( int WXUNUSED(nPhase) )
{
    return TRUE;
}

#endif

void CMuleNotebook::ApplyWidgetStyle()
{
    // TODO, font for labels etc

    SetWidgetStyle();
    gtk_widget_set_style( m_widget, m_widgetStyle );
}

bool CMuleNotebook::IsOwnGtkWindow( GdkWindow *window )
{
    return ((m_widget->window == window) ||
            (NOTEBOOK_PANEL(m_widget) == window));
}

//-----------------------------------------------------------------------------
// CMuleNotebookEvent
//-----------------------------------------------------------------------------

IMPLEMENT_DYNAMIC_CLASS(CMuleNotebookEvent, wxNotifyEvent)

#endif
