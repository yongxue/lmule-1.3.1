///////////////////////////////////////////////////////////////////////////////
// Name:        src/common/nbkbase.cpp
// Purpose:     common wxNotebook methods
// Author:      Vadim Zeitlin
// Modified by:
// Created:     02.07.01
// RCS-ID:      $Id: CMuleNotebookBase.cpp,v 1.1 2003/03/16 12:09:13 tiku Exp $
// Copyright:   (c) 2001 Vadim Zeitlin
// Licence:     wxWindows licence
///////////////////////////////////////////////////////////////////////////////

// ============================================================================
// declarations
// ============================================================================

// ----------------------------------------------------------------------------
// headers
// ----------------------------------------------------------------------------

#ifdef __GNUG__
    #pragma implementation "CMuleNotebookBase.h"
#endif

// For compilers that support precompilation, includes "wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
    #pragma hdrstop
#endif

#if wxUSE_NOTEBOOK

#ifndef WX_PRECOMP
#endif //WX_PRECOMP

#include "wx/imaglist.h"
#include "CMuleNotebookBase.h"

// ============================================================================
// implementation
// ============================================================================

// ----------------------------------------------------------------------------
// constructors and destructors
// ----------------------------------------------------------------------------

void CMuleNotebookBase::Init()
{
    m_imageList = NULL;
    m_ownsImageList = FALSE;
    m_stateImageList=NULL;
    m_ownsStateImageList=FALSE;
}

CMuleNotebookBase::~CMuleNotebookBase()
{
    if ( m_ownsImageList )
    {
        // may be NULL, ok
        delete m_imageList;
    }
    if(m_ownsStateImageList)
      {
	delete m_stateImageList;
      }
}

// ----------------------------------------------------------------------------
// image list
// ----------------------------------------------------------------------------

void CMuleNotebookBase::SetImageList(wxImageList* imageList)
{
    if ( m_ownsImageList )
    {
        // may be NULL, ok
        delete m_imageList;

        m_ownsImageList = FALSE;
    }

    m_imageList = imageList;
}

void CMuleNotebookBase::AssignImageList(wxImageList* imageList)
{
    SetImageList(imageList);
    m_ownsImageList = TRUE;
}

void CMuleNotebookBase::SetStateImageList(wxImageList* imageList)
{
    if ( m_ownsStateImageList )
    {
        // may be NULL, ok
        delete m_stateImageList;

        m_ownsStateImageList = FALSE;
    }

    m_stateImageList = imageList;
}

void CMuleNotebookBase::AssignStateImageList(wxImageList* imageList)
{
    SetStateImageList(imageList);
    m_ownsStateImageList = TRUE;
}

// ----------------------------------------------------------------------------
// geometry
// ----------------------------------------------------------------------------

wxSize CMuleNotebookBase::CalcSizeFromPage(const wxSize& sizePage)
{
    // this was just taken from CMuleNotebookSizer::CalcMin() and is, of
    // course, totally bogus - just like the original code was
    wxSize sizeTotal = sizePage;
    
    if ( HasFlag(wxNB_LEFT) || HasFlag(wxNB_RIGHT) )
    {
        sizeTotal.x += 90;
        sizeTotal.y += 10;
    }
    else
    {
        sizeTotal.x += 10;
        sizeTotal.y += 40;
    }

    return sizeTotal;
}

// ----------------------------------------------------------------------------
// pages management
// ----------------------------------------------------------------------------

bool CMuleNotebookBase::DeletePage(int nPage)
{
    CMuleNotebookPage *page = DoRemovePage(nPage);
    if ( !page )
        return FALSE;

    delete page;

    return TRUE;
}

CMuleNotebookPage *CMuleNotebookBase::DoRemovePage(int nPage)
{
    wxCHECK_MSG( nPage >= 0 && (size_t)nPage < m_pages.GetCount(), NULL,
                 _T("invalid page index in CMuleNotebookBase::DoRemovePage()") );

    CMuleNotebookPage *pageRemoved = m_pages[nPage];
    m_pages.RemoveAt(nPage);

    return pageRemoved;
}

int CMuleNotebookBase::GetNextPage(bool forward) const
{
    int nPage;

    int nMax = GetPageCount();
    if ( nMax-- ) // decrement it to get the last valid index
    {
        int nSel = GetSelection();

        // change selection wrapping if it becomes invalid
        nPage = forward ? nSel == nMax ? 0
                                       : nSel + 1
                        : nSel == 0 ? nMax
                                    : nSel - 1;
    }
    else // notebook is empty, no next page
    {
        nPage = -1;
    }

    return nPage;
}

#endif // wxUSE_NOTEBOOK

