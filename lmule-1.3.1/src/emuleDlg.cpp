//this file is part of eMule
//Copyright (C)2002 Merkur ( merkur-@users.sourceforge.net / http://www.emule-project.net )
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


// emuleDlg.cpp : Implementierungsdatei
//

//#include "stdafx.h"
#include "emule.h"
#include "emuleDlg.h"
#include "sockets.h"
#include "KnownFileList.h"
#include "KnownFile.h"
#include "ServerList.h"
#include "opcodes.h"
#include "SharedFileList.h"
#include "CreditsDlg.h"
#include "ED2KLink.h"
#include "UDPSocket.h"
#include "math.h"
//#include "SplashScreen.h"
#pragma comment(lib, "winmm.lib")
//#include "mmsystem.h"
//#include "EnBitmap.h"

#include "mmMultiButton.h"
#include "muuli_wdr.h"
#include <wx/splash.h>
#include <wx/toolbar.h>

#include "pixmaps/mule_TrayIcon.ico.xpm"
#include "pixmaps/mule_Tr_grey.ico.xpm"

// connection images

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#include "ServerSocket.h"
#if 0
#define ID_BUTTONCONNECT 42110
#define ID_BUTTONSERVERS 42111
#define ID_BUTTONSEARCH 42112
#define ID_BUTTONTRANSFER 42113
#define ID_BUTTONPREFERENCES 42114
#define ID_BUTTONSHARED 42115
#define ID_BUTTONSTATS 42116
#endif

#define ID_UQTIMER 59742
#define TM_DNSDONE 17851

BEGIN_EVENT_TABLE(CemuleDlg,wxFrame)
  EVT_SOCKET(ID_SOKETTI, CemuleDlg::socketHandler)
  EVT_TOOL(ID_BUTTONSERVERS,CemuleDlg::btnServers)
  EVT_TOOL(ID_BUTTONSEARCH,CemuleDlg::btnSearch)
  EVT_TOOL(ID_BUTTONTRANSFER,CemuleDlg::btnTransfer)
  EVT_TOOL(ID_BUTTONPREFERENCES,CemuleDlg::btnPreferences)
  EVT_TOOL(ID_BUTTONCONNECT,CemuleDlg::OnBnConnect)
  EVT_TOOL(ID_BUTTONSHARED,CemuleDlg::OnBnShared)
  EVT_TOOL(ID_BUTTONSTATISTICS,CemuleDlg::OnBnStats)
  EVT_TOOL(ID_BUTTONMESSAGES,CemuleDlg::OnBnMessages)
  EVT_TIMER(ID_UQTIMER,CemuleDlg::OnUQTimer)
  EVT_TIMER(4322,CemuleDlg::OnUDPTimer)
  EVT_TIMER(4333,CemuleDlg::OnSocketTimer)
  EVT_MENU(TM_FINISHEDHASHING,CemuleDlg::OnFinishedHashing)
  EVT_MENU(TM_DNSDONE,CemuleDlg::OnDnsDone)
  EVT_CLOSE(CemuleDlg::OnClose)
END_EVENT_TABLE()


void CemuleDlg::OnDnsDone(wxCommandEvent& evt)
{
  CUDPSocket* socket=(CUDPSocket*)evt.GetClientData();
  struct sockaddr_in *si=(struct sockaddr_in*)evt.GetInt();
  socket->DnsLookupDone(si);
}

void CemuleDlg::OnFinishedHashing(wxCommandEvent& evt)
{
  //wxMutexGuiEnter();

  CKnownFile* result = (CKnownFile*)evt.GetClientData();//(CKnownFile*)lParam;
  if (evt.GetInt()){
    CPartFile* requester = (CPartFile*)evt.GetInt(); //wParam;
    requester->PartFileHashFinished(result);
  }
  else{
    theApp.sharedfiles->filelist->SafeAddKFile(result);
    theApp.sharedfiles->SafeAddKFile(result);	
  }

  //wxMutexGuiLeave();
  return;
}

void muleDefaultToolbar( wxToolBar *parent )
{
    parent->SetMargins( 2, 2 );
    
    parent->AddTool( ID_BUTTONCONNECT, _("Connect"),connButImg( 0 ), _("Connect to server") );
    parent->AddSeparator();
    parent->AddCheckTool( ID_BUTTONSERVERS, _("Servers"),emuleDlgImages( 3 ), wxNullBitmap,  _("Server list") );
    parent->AddCheckTool( ID_BUTTONTRANSFER, _("Transfer"),emuleDlgImages( 1 ), wxNullBitmap, _("File transfer") );
    parent->AddCheckTool( ID_BUTTONSEARCH, _("Search"),emuleDlgImages( 2 ), wxNullBitmap, _("Search files") );
    parent->AddCheckTool( ID_BUTTONSHARED, _("Shared Files"),emuleDlgImages( 18 ), wxNullBitmap,_("Show shared files") );
    parent->AddCheckTool( ID_BUTTONMESSAGES, _("Messages"),emuleDlgImages( 21 ), wxNullBitmap,  _("Messages") );
    parent->AddCheckTool( ID_BUTTONIRC, _("IRC"),emuleDlgImages( 19 ), wxNullBitmap, _("Internet Relay Chat")); 
    parent->AddCheckTool( ID_BUTTONSTATISTICS, _("Statistics"),emuleDlgImages( 4 ), wxNullBitmap, _("Show current statistics"));
    parent->AddSeparator();
    parent->AddTool( ID_BUTTONPREFERENCES, _("Preferences"),emuleDlgImages( 5 ), _("Modify preferences") );
    
    parent->Realize();
}

// CemuleDlg Dialog
#include <signal.h>

#if wxMINOR_VERSION > 4
#define CLOSEBOX wxCLOSE_BOX
#else
#define CLOSEBOX 0
#endif

CemuleDlg::CemuleDlg(wxWindow* pParent /*=NULL*/) 
  : wxFrame(pParent,CemuleDlg::IDD,_("eMule for Linux"),wxDefaultPosition,wxSize(800,520),
	    wxCAPTION|wxRESIZE_BORDER|wxSYSTEM_MENU|wxDIALOG_NO_PARENT|wxTHICK_FRAME|wxMINIMIZE_BOX|wxMAXIMIZE_BOX|CLOSEBOX)
{
  m_app_state=APP_STATE_STARTING;
  srand(time(NULL));

  // get rid of sigpipe
  signal(SIGPIPE,SIG_IGN);
  
  //: CTrayDialog(CemuleDlg::IDD, pParent){
  wxSizer* dlg=muleDlg(this,false,true);

  serverwnd=new CServerWnd(this);
  searchwnd=new CSearchDlg(this);
  transferwnd=new CTransferWnd(this);
  preferenceswnd=new CPreferencesDlg(this,theApp.glob_prefs);
  sharedfileswnd=new CSharedFilesWnd(this);
  statisticswnd=new CStatisticsDlg(this);
  chatwnd=new CChatWnd(this);

  transicons[0]=dlStatusImages(0);
  transicons[1]=dlStatusImages(1);
  transicons[2]=dlStatusImages(2);
  transicons[3]=dlStatusImages(3);

#define ID_BITMAPBUTTON (-1)

#if 0
  // create toolbar at the top
  wxBitmap* tikubmp=new wxBitmap(connButImg(0));
  mmMultiButton* tikutest=new mmMultiButton((wxWindow*)this,ID_BUTTONCONNECT,
					    wxT("Connect"),*tikubmp,wxDefaultPosition,
					    wxSize(70,70),mmMB_AUTODRAW);
  buttonToolbar->Add(tikutest,0,wxALIGN_LEFT|wxALL,5);

  wxStaticLine* fufusep=new wxStaticLine((wxWindow*)this,-1,wxDefaultPosition,wxDefaultSize,wxSL_VERTICAL);
  buttonToolbar->Add(fufusep,0,wxGROW|wxALIGN_LEFT|wxALL,5);

  tikubmp=new wxBitmap(emuleDlgImages(3));
  tikutest=new mmMultiButton((wxWindow*)this,ID_BUTTONSERVERS,wxT("Servers"),*tikubmp,wxDefaultPosition,
					    wxSize(70,70),mmMB_AUTODRAW|mmMB_TOGGLE);
  buttonToolbar->Add(tikutest,0,wxALIGN_CENTRE|wxALL,5);
  tikutest->SetToggleDown(true);
  lastbutton=ID_BUTTONSERVERS;

  tikubmp=new wxBitmap(emuleDlgImages(1));
  tikutest=new mmMultiButton((wxWindow*)this,ID_BUTTONTRANSFER,wxT("Transfer"),*tikubmp,wxDefaultPosition,
					    wxSize(70,70),mmMB_AUTODRAW|mmMB_TOGGLE);
  buttonToolbar->Add(tikutest,0,wxALIGN_CENTRE|wxALL,5);

  tikubmp=new wxBitmap(emuleDlgImages(2));
  tikutest=new mmMultiButton((wxWindow*)this,ID_BUTTONSEARCH,wxT("Search"),*tikubmp,wxDefaultPosition,
					    wxSize(70,70),mmMB_AUTODRAW|mmMB_TOGGLE);
  buttonToolbar->Add(tikutest,0,wxALIGN_CENTRE|wxALL,5);

  tikubmp=new wxBitmap(emuleDlgImages(18));
  tikutest=new mmMultiButton((wxWindow*)this,ID_BUTTONSHARED,wxT("Shared Files"),*tikubmp,wxDefaultPosition,
					    wxSize(70,70),mmMB_AUTODRAW|mmMB_TOGGLE);
  buttonToolbar->Add(tikutest,0,wxALIGN_CENTRE|wxALL,5);

  tikubmp=new wxBitmap(emuleDlgImages(21));
  tikutest=new mmMultiButton((wxWindow*)this,ID_BITMAPBUTTON,wxT("Messages"),*tikubmp,wxDefaultPosition,
					    wxSize(70,70),mmMB_AUTODRAW);
  buttonToolbar->Add(tikutest,0,wxALIGN_CENTRE|wxALL,5);

  tikubmp=new wxBitmap(emuleDlgImages(19));
  tikutest=new mmMultiButton((wxWindow*)this,ID_BITMAPBUTTON,wxT("IRC"),*tikubmp,wxDefaultPosition,
					    wxSize(70,70),mmMB_AUTODRAW);
  buttonToolbar->Add(tikutest,0,wxALIGN_CENTRE|wxALL,5);

  tikubmp=new wxBitmap(emuleDlgImages(4));
  tikutest=new mmMultiButton((wxWindow*)this,ID_BUTTONSTATS,wxT("Statistics"),*tikubmp,wxDefaultPosition,
					    wxSize(70,70),mmMB_AUTODRAW|mmMB_TOGGLE);
  buttonToolbar->Add(tikutest,0,wxALIGN_CENTRE|wxALL,5);

  fufusep=new wxStaticLine((wxWindow*)this,-1,wxDefaultPosition,wxDefaultSize,wxSL_VERTICAL);
  buttonToolbar->Add(fufusep,0,wxGROW|wxALIGN_LEFT|wxALL,5);

  tikubmp=new wxBitmap(emuleDlgImages(5));
  tikutest=new mmMultiButton((wxWindow*)this,ID_BUTTONPREFERENCES,wxT("Preferences"),*tikubmp,wxDefaultPosition,
					    wxSize(70,70),mmMB_AUTODRAW);
  buttonToolbar->Add(tikutest,0,wxALIGN_RIGHT|wxALL,5);
#endif
  wxToolBar* newbar=new wxToolBar(this,-1,wxDefaultPosition,wxDefaultSize,wxTB_HORIZONTAL|wxNO_BORDER|wxTB_TEXT);
  muleDefaultToolbar(newbar);
  buttonToolbar->Add(newbar,0,wxGROW|wxALL,5);
  buttonToolbar->Layout();
  m_wndToolbar=newbar;

  // initially server page is visible
  m_wndToolbar->ToggleTool(ID_BUTTONSERVERS,TRUE);
  lastbutton=ID_BUTTONSERVERS;

  // these are not implemented
  m_wndToolbar->EnableTool(ID_BUTTONIRC,FALSE);
  //m_wndToolbar->EnableTool(ID_BUTTONMESSAGES,FALSE);
  dlg->Show(this,TRUE);

  contentSizer->Add(serverwnd,1,wxALIGN_LEFT|wxEXPAND|wxALL,5);
  serverwnd->Show(FALSE);
  activewnd=serverwnd;
  searchwnd->Show(FALSE);
  transferwnd->Show(FALSE);
  sharedfileswnd->Show(FALSE);
  statisticswnd->Show(FALSE);
  chatwnd->Show(FALSE);

  SetActiveDialog(serverwnd);
#if 0
  serverwnd.Show(FALSE);
  contentSizer->Remove(&serverwnd);
  contentSizer->Add(&searchwnd,1,wxALIGN_LEFT|wxEXPAND|wxALL,5);
  searchwnd.Show(TRUE);
  activewnd=&searchwnd;
#endif

  // can't init now
  //theApp.serverlist->Init();

#if 0
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	ready = false;
	lastuprate = 0;
	lastdownrate = 0;
	connicons[0] = (HICON)::LoadImage(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDI_NOTCONNECTED),IMAGE_ICON,16,16,0);
	connicons[1] = (HICON)::LoadImage(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDI_CONNECTED),IMAGE_ICON,16,16,0);
	connicons[2] = (HICON)::LoadImage(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDI_CONNECTEDHIGH),IMAGE_ICON,16,16,0);
	transicons[0] = (HICON)::LoadImage(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDI_UPDOWN),IMAGE_ICON,16,16,0);
	transicons[1] = (HICON)::LoadImage(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDI_UP0DOWN1),IMAGE_ICON,16,16,0);
	transicons[2] = (HICON)::LoadImage(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDI_UP1DOWN0),IMAGE_ICON,16,16,0);
	transicons[3] = (HICON)::LoadImage(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDI_UP1DOWN1),IMAGE_ICON,16,16,0);
	imicons[0] = 0;
	imicons[1] = (HICON)::LoadImage(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDI_MESSAGE),IMAGE_ICON,16,16,0);
	imicons[2] = (HICON)::LoadImage(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDI_MPENDING),IMAGE_ICON,16,16,0);
	sourceTrayIcon= (HICON)LoadImage(AfxGetInstanceHandle() ,MAKEINTRESOURCE(IDR_TRAYICON),IMAGE_ICON,16,16,LR_DEFAULTCOLOR);
	sourceTrayIconGrey= (HICON)LoadImage(AfxGetInstanceHandle() ,MAKEINTRESOURCE(IDR_TRAYICON_GREY),IMAGE_ICON,16,16,LR_DEFAULTCOLOR);
	usericon = (HICON)::LoadImage(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDI_USER),IMAGE_ICON,16,16,0);
#endif
}

class QueryDlg : public wxDialog {
public:
  QueryDlg(wxWindow* parent) : wxDialog(parent,21373,_("Desktop integration"),wxDefaultPosition,
					wxDefaultSize,wxDEFAULT_DIALOG_STYLE|wxSYSTEM_MENU)
  {
    wxSizer* content=desktopDlg(this,TRUE);
    content->Show(this,TRUE);
    Centre();
  };
protected:
  void OnOk(wxEvent& evt) {EndModal(0);};

  DECLARE_EVENT_TABLE();
};

BEGIN_EVENT_TABLE(QueryDlg,wxDialog)
  EVT_BUTTON(ID_OK,QueryDlg::OnOk)
END_EVENT_TABLE()

void CemuleDlg::changeDesktopMode()
{
    QueryDlg query(this);
    query.ShowModal();
    // then determine what choice is selected
    int mode=0;
    if(wxStaticCast(query.FindWindowById(ID_GNOME2),wxRadioButton)->GetValue()) {
      mode=1;
    } else if(wxStaticCast(query.FindWindowById(ID_KDE3),wxRadioButton)->GetValue()) {
      mode=2;
    } else if(wxStaticCast(query.FindWindowById(ID_KDE2),wxRadioButton)->GetValue()) {
      mode=3;
    } else {
      mode=4;
    }
    theApp.glob_prefs->SetDesktopMode(mode);
}

void CemuleDlg::createSystray()
{
  // create the docklet (at this point we already have preferences!)
  if(theApp.glob_prefs->GetDesktopMode()==0) {    
    // ok, it's not set yet.
    changeDesktopMode();
  }
  m_wndTaskbarNotifier=new CSysTray(this,theApp.glob_prefs->GetDesktopMode());
}

extern void TimerProc();
void CemuleDlg::OnUQTimer(wxTimerEvent& evt)
{
  // call timerproc
  if(!IsRunning())
    return;

  TimerProc();
}

void CemuleDlg::InitDialog()
{
  theApp.serverlist->Init();
}

bool CemuleDlg::IsRunning()
{
  return (m_app_state == APP_STATE_RUNNING);
}

void CemuleDlg::OnBnStats(wxEvent& ev)
{
  //mmMultiButton* butn=wxStaticCast(FindWindowById(lastbutton),mmMultiButton);
  if(lastbutton == ID_BUTTONSTATISTICS)
    //butn->SetToggleDown(true);
    m_wndToolbar->ToggleTool(lastbutton,TRUE);
  else
    {    
      //butn->SetToggleDown(false);
      m_wndToolbar->ToggleTool(lastbutton,FALSE);
      lastbutton=ID_BUTTONSTATISTICS;
      SetActiveDialog(statisticswnd);
    }
}

void CemuleDlg::OnBnShared(wxEvent& ev)
{
  //mmMultiButton* butn=wxStaticCast(FindWindowById(lastbutton),mmMultiButton);
  if(lastbutton == ID_BUTTONSHARED)
    //butn->SetToggleDown(true);
    m_wndToolbar->ToggleTool(lastbutton,TRUE);
  else
    {    
      //butn->SetToggleDown(false);
      m_wndToolbar->ToggleTool(lastbutton,FALSE);
      lastbutton=ID_BUTTONSHARED;
      SetActiveDialog(sharedfileswnd);
    }
}

void CemuleDlg::btnServers(wxEvent& ev)
{
  //mmMultiButton* butn=wxStaticCast(FindWindowById(lastbutton),mmMultiButton);
  if(lastbutton == ID_BUTTONSERVERS)
    //butn->SetToggleDown(true);
    m_wndToolbar->ToggleTool(lastbutton,TRUE);
  else
    {    
      //butn->SetToggleDown(false);
      m_wndToolbar->ToggleTool(lastbutton,FALSE);
      lastbutton=ID_BUTTONSERVERS;
      SetActiveDialog(serverwnd);
    }
}

void CemuleDlg::btnSearch(wxEvent& ev)
{
  //mmMultiButton* butn=wxStaticCast(FindWindowById(lastbutton),mmMultiButton);
  if(lastbutton == ID_BUTTONSEARCH)
    //butn->SetToggleDown(true);
    m_wndToolbar->ToggleTool(lastbutton,TRUE);
  else
    {    
      //butn->SetToggleDown(false);
      m_wndToolbar->ToggleTool(lastbutton,FALSE);
      lastbutton=ID_BUTTONSEARCH;
      SetActiveDialog(searchwnd);
    }

}

void CemuleDlg::btnTransfer(wxEvent& ev)
{
  //mmMultiButton* butn=wxStaticCast(FindWindowById(lastbutton),mmMultiButton);
  if(lastbutton == ID_BUTTONTRANSFER)
    //butn->SetToggleDown(true);
    m_wndToolbar->ToggleTool(lastbutton,TRUE);
  else
    {    
      //butn->SetToggleDown(false);
      m_wndToolbar->ToggleTool(lastbutton,FALSE);
      lastbutton=ID_BUTTONTRANSFER;
      SetActiveDialog(transferwnd);
    }
}

void CemuleDlg::OnBnMessages(wxEvent& ev)
{
  //mmMultiButton* butn=wxStaticCast(FindWindowById(lastbutton),mmMultiButton);
  if(lastbutton == ID_BUTTONMESSAGES)
    //butn->SetToggleDown(true);
    m_wndToolbar->ToggleTool(lastbutton,TRUE);
  else
    {    
      //butn->SetToggleDown(false);
      m_wndToolbar->ToggleTool(lastbutton,FALSE);
      lastbutton=ID_BUTTONMESSAGES;
      SetActiveDialog(chatwnd);
    }
}

void CemuleDlg::btnPreferences(wxEvent& ev)
{
  // do preferences
  preferenceswnd->ShowModal();
}

#include "UDPSocket.h"
#include <errno.h>
void CemuleDlg::socketHandler(wxSocketEvent& event)
{
  if(!event.GetSocket())
    return;

  if(!IsRunning()) {
    // we are not mentally ready to receive anything
    return;
  }

  if(event.GetSocket()->IsKindOf(CLASSINFO(CListenSocket))) {
    CListenSocket* soc=(CListenSocket*)event.GetSocket();
    switch(event.GetSocketEvent()) {
    case wxSOCKET_CONNECTION:
      soc->OnAccept(0);
      break;
    default:
      // shouldn't get other than connection events...
      break;
    }
    return;
  }

  if(event.GetSocket()->IsKindOf(CLASSINFO(CClientReqSocket))) {
    CClientReqSocket* soc=(CClientReqSocket*)event.GetSocket();
    //printf("request at clientreqsocket\n");
    switch(event.GetSocketEvent()) {
    case wxSOCKET_LOST:
      soc->OnError(errno);
      break;
    case wxSOCKET_INPUT:
      soc->OnReceive(0);
      break;
    case wxSOCKET_OUTPUT:
      soc->OnSend(0);
      break;
    default:
      // connection requests should not arrive here..
      break;
    }
    return;
  }

  if(event.GetSocket()->IsKindOf(CLASSINFO(CUDPSocket))) {
    CUDPSocket* soc=(CUDPSocket*)event.GetSocket();
    switch(event.GetSocketEvent()) {
    case wxSOCKET_INPUT:
      soc->OnReceive(0);
      break;
    default:
      break;
    }
    return;
  }

  if(event.GetSocket()->IsKindOf(CLASSINFO(CServerSocket))) {
    CServerSocket* soc=(CServerSocket*)event.GetSocket();
    switch(event.GetSocketEvent()) {
    case wxSOCKET_CONNECTION:
      soc->OnConnect(0);
      break;
    case wxSOCKET_LOST:
      soc->OnError(errno);
      break;
    case wxSOCKET_INPUT:
      soc->OnReceive(0);
      break;
    case wxSOCKET_OUTPUT:
      soc->OnSend(0);
      break;
    default:
      break;
    }
    return;
  }

  if(event.GetSocket()->IsKindOf(CLASSINFO(CClientUDPSocket))) {
    CClientUDPSocket* soc=(CClientUDPSocket*)event.GetSocket();
    switch(event.GetSocketEvent()) {
    case wxSOCKET_INPUT:
      soc->OnReceive(0);
      break;
    case wxSOCKET_OUTPUT:
      soc->OnSend(0);
      break;
    default:
      break;
    }
    return;
  }

  printf("*** SHOULD NOT END UP HERE\n");
  printf("** class is %s\n",event.GetSocket()->GetClassInfo()->GetClassName());
}

CemuleDlg::~CemuleDlg()
{
#if 0
	::DestroyIcon(m_hIcon);
	::DestroyIcon(connicons[0]);
	::DestroyIcon(connicons[1]);
	::DestroyIcon(connicons[2]);
	::DestroyIcon(transicons[0]);
	::DestroyIcon(transicons[1]);
	::DestroyIcon(transicons[2]);
	::DestroyIcon(transicons[3]);
	::DestroyIcon(imicons[1]);
	::DestroyIcon(imicons[2]);
	::DestroyIcon(sourceTrayIcon);
	::DestroyIcon(sourceTrayIconGrey);
	::DestroyIcon(usericon);
#endif

}
#if 0
void CemuleDlg::DoDataExchange(CDataExchange* pDX){
	CTrayDialog::DoDataExchange(pDX);
	
	// Masterbuttons
	DDX_Control(pDX, IDC_BUTTON2, m_btnConnect);
	DDX_Control(pDX, IDC_BN_DOWNLOADS, m_btnDownloads);
	DDX_Control(pDX, IDC_BN_SERVERS, m_btnServers);
	DDX_Control(pDX, IDC_BN_SEARCH, m_btnSearch);
	DDX_Control(pDX, IDC_BN_FILES, m_btnFiles);
	DDX_Control(pDX, IDC_BN_PREFERENCES, m_btnPreferences );
	DDX_Control(pDX, IDC_BN_MESSAGES, m_btnMessages );
	DDX_Control(pDX, IDC_BN_STATISTICS , m_btnStatistics );
	DDX_Control(pDX, IDC_BN_IRC, m_btnIrc );
}

BEGIN_MESSAGE_MAP(CemuleDlg, CTrayDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON2, OnBnClickedButton2)

	ON_BN_CLICKED(IDC_BN_DOWNLOADS , OnBnClicked2)
	ON_BN_CLICKED(IDC_BN_SERVERS , OnBnClicked3)
	ON_BN_CLICKED(IDC_BN_SEARCH , OnBnClicked4)
	ON_BN_CLICKED(IDC_BN_FILES , OnBnClicked5)
	ON_BN_CLICKED(IDC_BN_MESSAGES , OnBnClicked6)
	ON_BN_CLICKED(IDC_BN_STATISTICS, OnBnClicked7)
	ON_BN_CLICKED(IDC_BN_PREFERENCES , OnBnClicked8)
	ON_BN_CLICKED(IDC_BN_IRC , OnBnClicked9)

	ON_WM_SIZE()
	ON_MESSAGE(WM_COPYDATA, OnWMData)
	ON_MESSAGE(TM_FINISHEDHASHING,OnFileHashed)
	ON_WM_CLOSE()
	ON_COMMAND(MP_RESTORE, RestoreWindow)
	ON_COMMAND(MP_CONNECT, StartConnection)
	ON_COMMAND(MP_DISCONNECT, CloseConnection)
	ON_COMMAND(MP_EXIT, OnClose )
	ON_WM_CREATE()
END_MESSAGE_MAP()
#endif

// CemuleDlg eventhandler

#if 0
BOOL CemuleDlg::OnInitDialog(){	
	CTrayDialog::OnInitDialog();
	Localize();
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);
	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL){
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty()){
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	SetIcon(m_hIcon, TRUE);			
	SetIcon(m_hIcon, FALSE);

	// Setting up IconButtons
	m_btnConnect.SetIcon(IDI_BN_CONNECT);		m_btnConnect.SetAlign(CButtonST::ST_ALIGN_VERT);
	m_btnDownloads.SetIcon(IDI_BN_DOWNLOAD);	m_btnDownloads.SetAlign(CButtonST::ST_ALIGN_VERT);
	m_btnServers.SetIcon(IDI_BN_SERVER );		m_btnServers.SetAlign(CButtonST::ST_ALIGN_VERT);
	m_btnSearch.SetIcon(IDI_BN_SEARCH);			m_btnSearch.SetAlign(CButtonST::ST_ALIGN_VERT);
	m_btnFiles.SetIcon(IDI_BN_FILES);			m_btnFiles.SetAlign(CButtonST::ST_ALIGN_VERT);
	m_btnStatistics.SetIcon(IDI_BN_STATISTICS);	m_btnStatistics.SetAlign(CButtonST::ST_ALIGN_VERT);
	m_btnIrc.SetIcon(IDI_BN_MESSAGES);			m_btnIrc.SetAlign(CButtonST::ST_ALIGN_VERT);
	m_btnMessages.SetIcon(IDI_BN_MESSAGES);		m_btnMessages.SetAlign(CButtonST::ST_ALIGN_VERT);
	m_btnPreferences.SetIcon(IDI_BN_PREFERENCES);m_btnPreferences.SetAlign(CButtonST::ST_ALIGN_VERT);

	//set title
	char buffer[50];
	sprintf(buffer,"eMule V%s",CURRENT_VERSION_LONG);
	SetWindowText(buffer);

	//START - enkeyDEV(kei-kun) -TaskbarNotifier-
	m_wndTaskbarNotifier.Create(this);
	CEnBitmap m_imgTaskbar;
	VERIFY (m_imgTaskbar.LoadImage(IDR_TASKBAR,"GIF"));
	m_wndTaskbarNotifier.SetBitmap(&m_imgTaskbar, 255, 0, 255);
	m_wndTaskbarNotifier.SetTextFont("Arial",70,TN_TEXT_NORMAL,TN_TEXT_UNDERLINE);
	m_wndTaskbarNotifier.SetTextColor(RGB(255,255,255),RGB(255,255,255));		
	m_wndTaskbarNotifier.SetTextRect(CRect(5,45,m_wndTaskbarNotifier.m_nBitmapWidth-80, m_wndTaskbarNotifier.m_nBitmapHeight-35));
	//END - enkeyDEV(kei-kun) -TaskbarNotifier-

	// set statusbar
	statusbar.Create(WS_CHILD|WS_VISIBLE|CCS_BOTTOM|SBARS_SIZEGRIP,CRect(0,0,0,0), this, IDC_STATUSBAR) ;
	statusbar.EnableToolTips(true);
	CRect rect;
	statusbar.GetClientRect(&rect);
	int widths[5] = { rect.right-460, rect.right-350, rect.right-190,rect.right-25, -1 };
	statusbar.SetParts(5, widths);
	statusbar.SetIcon(1,usericon);
	statusbar.SetText(GetResString(IDS_UUSERS)+": 0",1,0);



	// create dialog pages
	preferenceswnd.SetPrefs(theApp.glob_prefs);
	serverwnd.Create(IDD_SERVER);
	sharedfileswnd.Create(IDD_FILES);
	searchwnd.Create(IDD_SEARCH);
	chatwnd.Create(IDD_CHAT);
	transferwnd.Create(IDD_TRANSFER);
	statisticswnd.Create(IDD_STATISTICS);
	ircwnd.Create(IDD_IRC);
	activewnd = &transferwnd;

	// set updateintervall of graphic rate display (in seconds)
	ShowConnectionState(false);

	// anchors
	AddAnchor(serverwnd,TOP_LEFT,BOTTOM_RIGHT);
	AddAnchor(transferwnd,TOP_LEFT,BOTTOM_RIGHT);
	AddAnchor(sharedfileswnd,TOP_LEFT,BOTTOM_RIGHT);
	AddAnchor(searchwnd,TOP_LEFT,BOTTOM_RIGHT);
	AddAnchor(chatwnd,TOP_LEFT,BOTTOM_RIGHT);
	AddAnchor(ircwnd,TOP_LEFT,BOTTOM_RIGHT);
	AddAnchor(statusbar,BOTTOM_LEFT,BOTTOM_RIGHT);
	AddAnchor(statisticswnd,TOP_LEFT,BOTTOM_RIGHT);
	AddAnchor(m_btnPreferences,TOP_RIGHT);
	ShowTransferRate();
	statisticswnd.ShowInterval();
	// tray icon
	TraySetMinimizeToTray(theApp.glob_prefs->GetMinTrayPTR());

	serverwnd.ShowWindow(SW_SHOW);
	activewnd = &serverwnd;

	theApp.serverlist->Init();
	ready = true;

	// start listining on edonkeyport
	if (!theApp.listensocket->StartListening())
		AddLogLine(false,GetResString(IDS_MAIN_SOCKETERROR),theApp.glob_prefs->GetPort());

	// ini. downloadqueue
	theApp.downloadqueue->Init();

	AddLogLine(true,GetResString(IDS_MAIN_READY),CURRENT_VERSION_LONG);

	if (theApp.pendinglink){
		OnWMData(NULL,(LPARAM) &theApp.sendstruct);//changed by Cax2 28/10/02
		delete theApp.pendinglink;
	}

	if (theApp.glob_prefs->DoAutoConnect())
		OnBnClickedButton2();

	// Restore saved window placement
	WINDOWPLACEMENT wp;wp.length=sizeof(wp);
	wp=theApp.glob_prefs->GetEmuleWindowPlacement();
	SetWindowPlacement(&wp);

	// splashscreen
	if (theApp.glob_prefs->UseSplashScreen()){
		CSplashScreen splash(this);
		splash.DoModal();
	}


	return TRUE;
}

void CemuleDlg::OnSysCommand(UINT nID, LPARAM lParam){
	if ((nID & 0xFFF0) == IDM_ABOUTBOX){
		CCreditsDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else{
		CTrayDialog::OnSysCommand(nID, lParam);
	}
}

void CemuleDlg::OnPaint() {
	if (IsIconic()){
		CPaintDC dc(this);

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		dc.DrawIcon(x, y, m_hIcon);
	}
	else{
		CTrayDialog::OnPaint();
	}
}


HCURSOR CemuleDlg::OnQueryDragIcon(){
	return static_cast<HCURSOR>(m_hIcon);
}

void CemuleDlg::OnBnClickedButton2(){
  if (!theApp.serverconnect->IsConnected()) 
    //connect if not currently connected
	if (!theApp.serverconnect->IsConnecting()  ) StartConnection(); else {
		theApp.serverconnect->StopConnectionTry();
		ShowConnectionState(false);
	}
  else
    //disconnect if currently connected
    CloseConnection();
}
#endif

void CemuleDlg::OnBnConnect(wxEvent& evt)
{
  if (!theApp.serverconnect->IsConnected()) 
    //connect if not currently connected
    if (!theApp.serverconnect->IsConnecting()  ) StartConnection(); else {
      theApp.serverconnect->StopConnectionTry();
      ShowConnectionState(false);
    }
  else
    //disconnect if currently connected
    CloseConnection();
}

void CemuleDlg::ResetLog(){
	logtext="";
	//serverwnd.logbox.SetWindowText(logtext);
	serverwnd->logbox.Clear();
}

void CemuleDlg::ResetDebugLog(){
	logtext="";
	serverwnd->logbox.Clear(); //SetWindowText(logtext);
}

void CemuleDlg::AddLogLine(bool addtostatusbar,const wxChar* line,...){
	char osDate[30],osTime[30]; //<<--9/21/02
	char temp[1060]; //<<--9/21/02
	char bufferline[1000];

	va_list argptr;
	va_start(argptr, line);
	vsnprintf(bufferline, 1000, line, argptr);
	va_end(argptr);
	if (addtostatusbar) {
	//  statusbar.SetStatusText(bufferline,0); //,0);
	  wxStaticText* text=(wxStaticText*)FindWindow("infoLabel");
	  text->SetLabel(bufferline);
	}
	
	time_t joskus=time(NULL);
	struct tm* nyt=localtime(&joskus);
	strftime(osDate,29,"%F",nyt);
	strftime(osTime,29,"%T",nyt);

	//strtime( osTime ); //<<--9/21/02
	//strdate( osDate ); //<<--9/21/02
	sprintf(temp,"%s %s: %s",osDate,osTime,bufferline);//<<--9/21/02 
	logtext += temp; //<<--9/21/02
	logtext += "\n";

	wxTextCtrl* ct=(wxTextCtrl*)serverwnd->FindWindowByName("logView");
	if(ct) {
	  ct->AppendText(logtext);
	  //ct->ShowPosition(ct->GetLastPosition());
	  ct->ShowPosition(ct->GetValue().Length()-1);
	}

#if 0
	if (ready){
	  //serverwnd.logbox.SetWindowText(logtext);
	  serverwnd.logbox.Clear();
	  serverwnd.logbox.AppendText(logtext);
	  //serverwnd.logbox.LineScroll(serverwnd.logbox.GetLineCount());
	  serverwnd.logbox.ShowPosition(serverwnd.logbox.GetLastPosition());
	  ShowNotifier(bufferline, TBN_LOG, false);
	}
#endif
	//printf("***LOG: %s\n",logtext);
}

void CemuleDlg::AddDebugLogLine(bool addtostatusbar,const wxChar* line,...){
#if 0
	if( !theApp.glob_prefs->GetVerbose() )
		return;
	char osDate[30],osTime[30]; //<<--9/21/02
	char temp[1060]; //<<--9/21/02
	char bufferline[1000];

	va_list argptr;
	va_start(argptr, line);
	_vsnprintf(bufferline, 1000, line, argptr);
	va_end(argptr);
	if (addtostatusbar)
		statusbar.SetText(bufferline,0,0);
	
	_strtime( osTime ); //<<--9/21/02
	_strdate( osDate ); //<<--9/21/02
	sprintf(temp,"%s %s: %s",osDate,osTime,bufferline);//<<--9/21/02 
	debuglog += temp; //<<--9/21/02
	debuglog += "\r\n";
	if (ready){
		serverwnd.debuglog.SetWindowText(debuglog);
		serverwnd.debuglog.LineScroll(serverwnd.debuglog.GetLineCount());
	}
#endif
}

void CemuleDlg::AddServerMessageLine(char* line,...){
	wxString content;
	va_list argptr;
	char bufferline[500];
	va_start(argptr, line);
	vsnprintf(bufferline, 500, line, argptr);
	va_end(argptr);

	wxTextCtrl* cv=(wxTextCtrl*)serverwnd->FindWindowByName("serverInfo");
	if(cv) {
  	  cv->AppendText(wxString(bufferline)+wxString("\n"));
	  cv->ShowPosition(cv->GetValue().Length()-1);
	}
	//serverwnd.servermsgbox.AppendText(CString(bufferline)+CString("\n"));
}

void CemuleDlg::ShowConnectionState(bool connected){ShowConnectionState(connected,"");}

#define STAT_LOWID 1
#define STAT_HIGHID 2
#define STAT_CONNECTING 3
#define STAT_NOTCONNECTED 4

void CemuleDlg::ShowConnectionState(bool connected, wxString server,bool iconOnly){
  static int __oldstatus=(-1);
  bool changed=false;

  theApp.emuledlg->serverwnd->UpdateMyInfo();

  if (connected){
    wxStaticBitmap* bmp=(wxStaticBitmap*)FindWindowByName("connImage");
    if(theApp.serverconnect->IsLowID()) {
      if(__oldstatus!=STAT_LOWID) {
	bmp->SetBitmap(connImages(1));	    
	__oldstatus=STAT_LOWID;
	changed=true;
      }
    } else {
      if(__oldstatus!=STAT_HIGHID) {
	bmp->SetBitmap(connImages(3));
	__oldstatus=STAT_HIGHID;
	changed=true;
      }
    }

    //butn->SetLabel(GetResString(IDS_MAIN_BTN_DISCONNECT));
    //butn->SetDefaultBitmap(connButImg(1));
    // we can't modify existing button. so we delete the old and create a new one
    if(changed) {
      m_wndToolbar->DeleteTool(ID_BUTTONCONNECT);
      m_wndToolbar->InsertTool(0,ID_BUTTONCONNECT,GetResString(IDS_MAIN_BTN_DISCONNECT),connButImg(1),GetResString(IDS_MAIN_BTN_DISCONNECT_TOOLTIP));
      m_wndToolbar->Realize();
    }

    /*
      this->GetDlgItem(IDC_BUTTON2)->SetWindowText(GetResString(IDS_MAIN_BTN_DISCONNECT));
      m_btnConnect.SetIcon(IDI_BN_DISCONNECT); m_btnConnect.SetTooltipText(GetResString(IDS_MAIN_BTN_DISCONNECT_TOOLTIP));
      
      if (theApp.serverconnect->IsLowID()) 
      statusbar.SetIcon(3,connicons[1]);
      else 
      statusbar.SetIcon(3,connicons[2]);
    */
    //statusbar.SetText(GetResString(IDS_MAIN_CONNECTEDTO) +CString(server),0,0);
    wxStaticText* tx=(wxStaticText*)FindWindow("infoLabel");
    tx->SetLabel(GetResString(IDS_MAIN_CONNECTEDTO)+wxString(server));
    ((wxStaticText*)FindWindow("connLabel"))->SetLabel(server);
    //statusbar.SetTipText(3,server);
    //statusbar.SetText(server,3,0);
  }
  else{
    
    wxStaticBitmap* bmp=(wxStaticBitmap*)FindWindowByName("connImage");
    
    if (theApp.serverconnect->IsConnecting()) {		  
      if(__oldstatus!=STAT_CONNECTING) {
	bmp->SetBitmap(connImages(0));
	//butn->SetLabel(GetResString(IDS_MAIN_BTN_CANCEL));
	//butn->SetDefaultBitmap(connButImg(2));
	// once again.. no modify
	m_wndToolbar->DeleteTool(ID_BUTTONCONNECT);
	m_wndToolbar->InsertTool(0,ID_BUTTONCONNECT,GetResString(IDS_MAIN_BTN_CANCEL),connButImg(2),
				 GetResString(IDS_MAIN_BTN_CANCEL_TOOLTIP));
	m_wndToolbar->Realize();
	__oldstatus=STAT_CONNECTING;
      }

      /*this->GetDlgItem(IDC_BUTTON2)->SetWindowText(GetResString(IDS_MAIN_BTN_CANCEL));
	m_btnConnect.SetIcon(IDI_BN_STOPCONNECTING); m_btnConnect.SetTooltipText(GetResString(IDS_MAIN_BTN_CANCEL_TOOLTIP));
	statusbar.SetIcon(3,connicons[0]);*/
      
      // EI INFOLABEL
      ((wxStaticText*)FindWindowByName("connLabel"))->SetLabel(GetResString(IDS_CONNECTING));
      //statusbar.SetText(GetResString(IDS_CONNECTING),3,0);
      //statusbar.SetTipText(3,"");
      ShowUserCount(0,0);
      
    } else {
      if(__oldstatus!=STAT_NOTCONNECTED) {
	bmp->SetBitmap(connImages(0));
	((wxStaticText*)FindWindowByName("connLabel"))->SetLabel(GetResString(IDS_NOTCONNECTED));
      //butn->SetLabel(GetResString(IDS_MAIN_BTN_CONNECT));
      //butn->SetDefaultBitmap(connButImg(0));
      // and again...
	m_wndToolbar->DeleteTool(ID_BUTTONCONNECT);
	m_wndToolbar->InsertTool(0,ID_BUTTONCONNECT,GetResString(IDS_MAIN_BTN_CONNECT),connButImg(0),
				 GetResString(IDS_CONNECTTOANYSERVER));
	m_wndToolbar->Realize();
	__oldstatus=STAT_NOTCONNECTED;
      }
      /*
	this->GetDlgItem(IDC_BUTTON2)->SetWindowText(GetResString(IDS_MAIN_BTN_CONNECT));
	m_btnConnect.SetIcon(IDI_BN_CONNECT);	m_btnConnect.SetTooltipText(GetResString(IDS_CONNECTTOANYSERVER));
	
	statusbar.SetIcon(3,connicons[0]);
	statusbar.SetText(GetResString(IDS_NOTCONNECTED),3,0);
      */
      ((wxStaticText*)FindWindow("connLabel"))->SetLabel(GetResString(IDS_NOTCONNECTED));
      theApp.emuledlg->AddLogLine(true,GetResString(IDS_DISCONNECTED));
      //statusbar.SetTipText(3,"");
      ShowUserCount(0,0);
    }
    
  }
}

void CemuleDlg::ShowUserCount(uint32 user_toshow,uint32 file_toshow){
  uint32 totaluser, totalfile;
  totaluser = totalfile = 0;
  if( user_toshow || file_toshow )
    theApp.serverlist->GetUserFileStatus( totaluser, totalfile );
  char buffer[100];
  sprintf(buffer, "%s: %s(%s) | %s: %s(%s)", GetResString(IDS_UUSERS).GetData(), CastItoIShort(user_toshow).GetData(), CastItoIShort(totaluser).GetData(), GetResString(IDS_FILES).GetData(), CastItoIShort(file_toshow).GetData(), CastItoIShort(totalfile).GetData());
  wxStaticCast(FindWindow("userLabel"),wxStaticText)->SetLabel(buffer);
  //statusbar.SetText(buffer,1,0);
}

void CemuleDlg::ShowMessageState(uint8 iconnr){
  //statusbar.SetIcon(4,imicons[iconnr]);
}

void CemuleDlg::ShowTransferRate(bool foreceAll){
	char buffer[50];
	char buffer2[100];

	lastuprate = theApp.uploadqueue->GetDatarate();
	lastdownrate= theApp.downloadqueue->GetDatarate();
	int lastuprateoverhead = theApp.uploadqueue->GetUpDatarateOverhead();
	int lastdownrateoverhead = theApp.downloadqueue->GetDownDatarateOverhead();

	if( theApp.glob_prefs->ShowOverhead() )
		sprintf(buffer,GetResString(IDS_UPDOWN),(float)lastuprate/1024, (float)lastuprateoverhead/1024, (float)lastdownrate/1024, (float)lastdownrateoverhead/1024);
	else
		sprintf(buffer,GetResString(IDS_UPDOWNSMALL),(float)lastuprate/1024, (float)lastdownrate/1024);
	//statusbar.SetStatusText(buffer,2); //,0);
	((wxStaticText*)FindWindowByName("speedLabel"))->SetLabel(buffer);

	// set trayicon-icon
	int DownRateProcent=(int)ceil ( (lastdownrate/10.24)/ theApp.glob_prefs->GetMaxGraphDownloadRate());
	if (DownRateProcent>100) DownRateProcent=100;
	UpdateTrayIcon(DownRateProcent);

	if (theApp.serverconnect->IsConnected()) 
		sprintf(buffer2,wxString(_("eMule (%s | ")+GetResString(IDS_CONNECTED)+")").GetData(),buffer);
	else
		sprintf(buffer2,wxString(_("eMule (%s | ")+GetResString(IDS_DISCONNECTED)+")").GetData(),buffer);

	m_wndTaskbarNotifier->TraySetToolTip(buffer2);

	wxStaticBitmap* bmp=(wxStaticBitmap*)FindWindowByName("transferImg");
	if(lastuprate&&lastdownrate)
	  bmp->SetBitmap(transicons[3]);
	else if(lastuprate)
	  bmp->SetBitmap(transicons[2]);
	else if(lastdownrate)
	  bmp->SetBitmap(transicons[1]);
	else
	  bmp->SetBitmap(transicons[0]);
#if 0
	if (lastuprate && lastdownrate)
		statusbar.SetIcon(2,transicons[3]);
	else if (lastuprate)
		statusbar.SetIcon(2,transicons[2]);
	else if (lastdownrate)
		statusbar.SetIcon(2,transicons[1]);
	else
		statusbar.SetIcon(2,transicons[0]);
#endif
}

#if 0
void CemuleDlg::OnCancel(){

if (*theApp.glob_prefs->GetMinTrayPTR()) {
  //TrayShow();
  //		ShowWindow(SW_HIDE);
	} else {
	  //	ShowWindow(SW_MINIMIZE);
	}

};
#endif

void CemuleDlg::SetActiveDialog(wxWindow* dlg){
  activewnd->Show(FALSE);
  contentSizer->Remove(activewnd);
  contentSizer->Add(dlg,1,wxALIGN_LEFT|wxEXPAND|wxALL,5);
  dlg->Show(TRUE);
  activewnd=dlg;
  Layout();
}

#if 0
void CemuleDlg::OnSize(UINT nType,int cx,int cy){
	CTrayDialog::OnSize(nType,cx,cy);
	CRect rect;
	statusbar.GetClientRect(&rect);
	int widths[5] = { rect.right-460, rect.right-350, rect.right-190,rect.right-25, -1 };
	statusbar.SetParts(5, widths);
}
#endif

extern void URLDecode(wxString& result, const char* buff);

#if 0
LRESULT CemuleDlg::OnWMData(WPARAM wParam,LPARAM lParam){
	tagCOPYDATASTRUCT* data = (tagCOPYDATASTRUCT*)lParam;
	if (data->dwData == OP_ED2KLINK){
		FlashWindow(true);
		if( theApp.glob_prefs->IsBringToFront() ){
			if (IsIconic())
				ShowWindow(SW_SHOWNORMAL);
			else if (TrayHide()){
				ShowWindow(SW_SHOW);
			}
		else
			SetForegroundWindow();
		}
		try {
			CString link2;
			CString link;
			link2 = (char*)data->lpData;
			link2.Replace("%7c","|");
			URLDecode(link,(const char*)link2.GetBuffer());
			CED2KLink* pLink = CED2KLink::CreateLinkFromUrl(link);
			_ASSERT( pLink !=0 );
			switch (pLink->GetKind()) {
			case CED2KLink::kFile:
				{
					CED2KFileLink* pFileLink = pLink->GetFileLink();
					_ASSERT(pFileLink !=0);
					theApp.downloadqueue->AddFileLinkToDownload(pFileLink);
				}
				break;
			case CED2KLink::kServerList:
				{
					CED2KServerListLink* pListLink = pLink->GetServerListLink(); 
					_ASSERT( pListLink !=0 ); 
					CString strAddress = pListLink->GetAddress(); 
					if(strAddress.GetLength() != 0)
						theApp.emuledlg->serverwnd.UpdateServerMetFromURL(strAddress);
				}
				break;
			case CED2KLink::kServer:
				{
					CString defName;
					CED2KServerLink* pSrvLink = pLink->GetServerLink();
					_ASSERT( pSrvLink !=0 );
					in_addr host;
					host.S_un.S_addr = pSrvLink->GetIP();
					CServer* pSrv = new CServer(pSrvLink->GetPort(),inet_ntoa(host));
					_ASSERT( pSrv !=0 );
					pSrvLink->GetDefaultName(defName);
					pSrv->SetListName(defName.GetBuffer());
					if (!theApp.emuledlg->serverwnd.serverlistctrl.AddServer(pSrv,true)) 
						delete pSrv; 
					else theApp.emuledlg->AddLogLine(true,GetResString(IDS_SERVERADDED)+CString(pSrv->GetListName()));
				}
				break;
			default:
				break;
			}
			delete pLink;
		} catch(...) {
			AddLogLine(true, GetResString(IDS_LINKNOTADDED));
		}
	}
	return true;
}
#endif

#if 0
LRESULT CemuleDlg::OnFileHashed(WPARAM wParam,LPARAM lParam){
	CKnownFile* result = (CKnownFile*)lParam;
	if (wParam){
		CPartFile* requester = (CPartFile*)wParam;
		requester->PartFileHashFinished(result);
	}
	else{
		theApp.sharedfiles->filelist->SafeAddKFile(result);
		theApp.sharedfiles->SafeAddKFile(result);	
	}
	return true;
}
#endif

void CemuleDlg::OnClose(wxCloseEvent& evt)
{
  if (evt.CanVeto() && theApp.glob_prefs->IsConfirmExitEnabled() )
    if (wxNO==wxMessageBox(GetResString(IDS_MAIN_EXIT),GetResString(IDS_MAIN_EXITTITLE),wxYES_NO)) {
      evt.Veto();
      return;
    }

  // we are going DOWN
  theApp.emuledlg->m_app_state=APP_STATE_SHUTINGDOWN;

  theApp.OnlineSig(); // Added By Bouc7 
  
  // TODO: Add your message handler code here and/or call default
  //WINDOWPLACEMENT wp;wp.length=sizeof(wp);GetWindowPlacement(&wp);
  //theApp.glob_prefs->SetWindowLayout(wp);
  
  // saving data & stuff
  theApp.knownfiles->Save();
  transferwnd->downloadlistctrl->SaveSettings(CPreferences::tableDownload);
  transferwnd->uploadlistctrl->SaveSettings(CPreferences::tableUpload);
  transferwnd->queuelistctrl->SaveSettings(CPreferences::tableQueue);
//  if(searchwnd->searchlistctrl) searchwnd->searchlistctrl->SaveSettings(CPreferences::tableSearch);
  sharedfileswnd->sharedfilesctrl->SaveSettings(CPreferences::tableShared);
  serverwnd->serverlistctrl->SaveSettings(CPreferences::tableServer);
  
  theApp.glob_prefs->Add2TotalDownloaded(theApp.stat_sessionReceivedBytes);
  theApp.glob_prefs->Add2TotalUploaded(theApp.stat_sessionSentBytes);

  // save friends
  theApp.friendlist->SaveList();

  theApp.glob_prefs->Save();
  
  transferwnd->downloadlistctrl->DeleteAllItems();
  //emuledlg->chatwnd->chatselector->DeleteAllItems();
  theApp.clientlist->DeleteAll();
  if(searchwnd->searchlistctrl) searchwnd->searchlistctrl->DeleteAllItems();

  // and destroy the window
  Destroy();
  //CTrayDialog::OnCancel();
}

#if 0
void CemuleDlg::OnTrayRButtonDown(CPoint pt){
	if (trayPopup) return;
	trayPopup.CreatePopupMenu(); 

	UINT flagsC;
	UINT flagsD;
	
	// set connect/disconnect to enabled and disabled
	if (!theApp.serverconnect->IsConnected()) {
		flagsD=MF_STRING || MF_DISABLED; flagsC=MF_STRING;
	} else {
		flagsC=MF_STRING || MF_DISABLED; flagsD=MF_STRING;
	}

	trayPopup.AddMenuTitle((CString)"eMule V"+(CString)CURRENT_VERSION_LONG);

	trayPopup.AppendMenu(	MF_STRING ,MP_RESTORE, GetResString(IDS_MAIN_POPUP_RESTORE) ); 
	trayPopup.SetDefaultItem(MP_RESTORE);
	trayPopup.AppendMenu(MF_SEPARATOR);

	trayPopup.AppendMenu(flagsC,MP_CONNECT, GetResString(IDS_CONNECTTOANYSERVER));
	trayPopup.AppendMenu(flagsD ,MP_DISCONNECT,   GetResString(IDS_MAIN_BTN_DISCONNECT)); 

	trayPopup.AppendMenu(MF_STRING,MP_EXIT, GetResString(IDS_EXIT)); 
	SetForegroundWindow();
	trayPopup.TrackPopupMenu(TPM_LEFTALIGN |TPM_RIGHTBUTTON, pt.x, pt.y, this); 

	trayPopup.DestroyMenu();

}
#endif

void CemuleDlg::StartConnection(){
	AddLogLine(true, GetResString(IDS_CONNECTING));
	theApp.serverconnect->ConnectToAnyServer();
	ShowConnectionState(false);
}

void CemuleDlg::CloseConnection(){
	theApp.serverconnect->Disconnect();
	theApp.OnlineSig(); // Added By Bouc7 
}
void CemuleDlg::RestoreWindow(){
  //if (TrayIsVisible()) TrayHide();	
  //ShowWindow(SW_SHOW);
}

#if 0
void CemuleDlg::OnBnClicked2(){ SetActiveDialog(&transferwnd); }
void CemuleDlg::OnBnClicked3(){ SetActiveDialog(&serverwnd);}
void CemuleDlg::OnBnClicked4(){ SetActiveDialog(&searchwnd);}
void CemuleDlg::OnBnClicked5(){ SetActiveDialog(&sharedfileswnd);}
void CemuleDlg::OnBnClicked6(){ SetActiveDialog(&chatwnd);}
void CemuleDlg::OnBnClicked7(){ SetActiveDialog(&statisticswnd);}
void CemuleDlg::OnBnClicked8(){ preferenceswnd.DoModal();}
void CemuleDlg::OnBnClicked9(){ SetActiveDialog(&ircwnd);}
#endif

void CemuleDlg::ShowStatistics() {
  	statisticswnd->ShowStatistics();	
}

void CemuleDlg::UpdateTrayIcon(int procent)
{
	//HICON mytrayIcon;
	//if (mytrayIcon!=NULL) DestroyIcon(mytrayIcon);

	// set the limits of where the bar color changes (low-high)
	int pLimits16[1] = {100};

	// set the corresponding color for each level
	COLORREF pColors16[1] = {theApp.glob_prefs->GetStatsColor(11)};

#if 0
	// start it up
	if (theApp.serverconnect->IsConnected())
		trayIcon.Init(sourceTrayIcon,100,1,1,16,16,RGB(21,97,110));
	else trayIcon.Init(sourceTrayIconGrey,100,1,1,16,16,RGB(21,97,110));
#endif

	// load our limit and color info
	m_wndTaskbarNotifier->SetColorLevels(pLimits16,pColors16,1);

	// generate the icon (destroy these icon using DestroyIcon())
	int pVals16[1] = {procent};

#if 0
	mytrayIcon = trayIcon.Create(pVals16);
	ASSERT (mytrayIcon != NULL);
	if (mytrayIcon)
		m_wndTaskbarNotifier->TraySetIcon(mytrayIcon,true);
	//TrayUpdate();
#endif
	// ei hienostelua. tarvii kuitenki pelleill� gtk:n kanssa 
	char** data;
	if(!theApp.serverconnect)
	  data=mule_Tr_grey_ico;
	else {
	  if (theApp.serverconnect->IsConnected())
	    data=mule_TrayIcon_ico;
	  else data=mule_Tr_grey_ico;
	}

	m_wndTaskbarNotifier->TraySetIcon(data,true,pVals16);
}
#if 0
int CemuleDlg::OnCreate(LPCREATESTRUCT lpCreateStruct){
	CTrayDialog::OnCreate(lpCreateStruct);
	/*if (theApp.glob_prefs->UseSplashScreen()){
		ModifyStyleEx(0,WS_EX_LAYERED);
		SetLayeredWindowAttributes(0, 0, LWA_ALPHA);
	}*/
	return 0;

}
#endif

//START - enkeyDEV(kei-kun) -TaskbarNotifier-
void CemuleDlg::ShowNotifier(wxString Text, int MsgType, bool ForceSoundOFF) {
#if 0
	bool ShowIt = false;
	switch(MsgType) {
		case TBN_CHAT:
            if (theApp.glob_prefs->GetUseChatNotifier()) {
				m_wndTaskbarNotifier->Show(Text, MsgType);
				ShowIt = true;
			}
			break;
		case TBN_DLOAD:
            if (theApp.glob_prefs->GetUseDownloadNotifier()) {
				m_wndTaskbarNotifier->Show(Text, MsgType);
				ShowIt = true;			
			}
			break;
		case TBN_LOG:
            if (theApp.glob_prefs->GetUseLogNotifier()) {
				m_wndTaskbarNotifier->Show(Text, MsgType);
				ShowIt = true;			
			}
			break;
		case TBN_IMPORTANTEVENT:
			if (theApp.glob_prefs->GetNotifierPopOnImportantError()) {
				m_wndTaskbarNotifier->Show(Text, MsgType);
				ShowIt = true;			
			}
			break;	// added by InterCeptor (bugfix) 27.11.02
		case TBN_NEWVERSION:
			if (theApp.glob_prefs->GetNotifierPopOnNewVersion()) {
				m_wndTaskbarNotifier->Show(Text, MsgType);
				ShowIt = true;			
			}
			break;
		case TBN_NULL:
            m_wndTaskbarNotifier->Show(Text, MsgType);
			ShowIt = true;			
			break;
	}
	
#if 0
    if (theApp.glob_prefs->GetUseSoundInNotifier() && !ForceSoundOFF && ShowIt == true)
        PlaySound(theApp.glob_prefs->GetNotifierWavSoundPath(), NULL, SND_FILENAME | SND_NOSTOP | SND_NOWAIT | SND_ASYNC);
#endif
#endif
}


#if 0
LRESULT CemuleDlg::OnTaskbarNotifierClicked(WPARAM wParam,LPARAM lParam)
{
	int msgType = TBN_NULL;
	msgType = m_wndTaskbarNotifier.GetMessageType();
    switch(msgType)
	{
	case TBN_CHAT:
		RestoreWindow();
		SetActiveDialog(&chatwnd);
		break;
	case TBN_DLOAD:
		RestoreWindow();
		SetActiveDialog(&transferwnd);
	//	{TODO: aprire la cartella incoming?}
		break;
	case TBN_LOG:
		RestoreWindow();
		SetActiveDialog(&serverwnd);	
		break;
	}
    return 0;
}
#endif
//END - enkeyDEV(kei-kun) -TaskbarNotifier-

void CemuleDlg::Localize(){
#if 0
	GetDlgItem(IDC_BN_DOWNLOADS)->SetWindowText(GetResString(IDS_EM_TRANS));
	GetDlgItem(IDC_BN_SERVERS)->SetWindowText(GetResString(IDS_EM_SERVER));
	GetDlgItem(IDC_BN_SEARCH)->SetWindowText(GetResString(IDS_EM_SEARCH));
	GetDlgItem(IDC_BN_FILES)->SetWindowText(GetResString(IDS_EM_FILES));
	GetDlgItem(IDC_BN_MESSAGES)->SetWindowText(GetResString(IDS_EM_MESSAGES));
	GetDlgItem(IDC_BN_STATISTICS)->SetWindowText(GetResString(IDS_EM_STATISTIC));
	GetDlgItem(IDC_BN_PREFERENCES)->SetWindowText(GetResString(IDS_EM_PREFS));
#endif
}

void CemuleDlg::OnUDPTimer(wxTimerEvent& evt)
{
  if(!IsRunning()) 
    return;
  theApp.serverlist->SendNextPacket();
}

void CemuleDlg::OnSocketTimer(wxTimerEvent& evt)
{
  if(!IsRunning())
    return;

	CServerConnect *_this= theApp.serverconnect; 
	_this->StopConnectionTry();
	if (_this->IsConnected()) return; 

	_this->ConnectToAnyServer(); 
}

void CemuleDlg::OnQLTimer(wxTimerEvent& evt)
{
  printf("QLTimer\n");
  if(!IsRunning())
    return;
  CQueueListCtrl::QueueUpdateTimer();
}
