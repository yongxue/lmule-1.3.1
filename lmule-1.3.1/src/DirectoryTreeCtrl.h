#ifndef DIRTREECTRL_H
#define DIRTREECTRL_H 1

/////////////////////////////////////////////
// written by robert rostek - tecxx@rrs.at //
/////////////////////////////////////////////

// CDirectoryTreeCtrl
//#include "TitleMenu.h"

#define USRMSG_ITEMSTATECHANGED		(47101) + 16
#define MP_SHAREDFOLDERS_FIRST	46901

#include <wx/window.h>
#include "treebasc.h"
#include "treectlc.h"

#undef HTREEITEM
#define HTREEITEM wxCTreeItemId

class CDirectoryTreeCtrl : public wxCTreeCtrl
{
//DECLARE_DYNAMIC(CDirectoryTreeCtrl)
  DECLARE_DYNAMIC_CLASS(CDirectoryTreeCtrl) 

    CDirectoryTreeCtrl() {};
public:
	CDirectoryTreeCtrl(wxWindow*& parent,int id,const wxPoint& pos,wxSize siz,int flags);

	// initialize control
	void Init(void);
	// get all shared directories
	void GetSharedDirectories(wxArrayString* list);
	// set shared directories
	void SetSharedDirectories(wxArrayString* list);

private:
	wxImageList m_image; 
	// add a new item
	HTREEITEM AddChildItem(HTREEITEM hRoot, wxString strText);
	// add subdirectory items
	void AddSubdirectories(HTREEITEM hRoot, wxString strDir);
	// return the full path of an item (like C:\abc\somewhere\inheaven\)
	wxString GetFullPath(HTREEITEM hItem);
	// returns true if strDir has at least one subdirectory
	bool HasSubdirectories(wxString strDir);
	// check status of an item has changed
	void CheckChanged(HTREEITEM hItem, bool bChecked);
	// returns true if a subdirectory of strDir is shared
	bool HasSharedSubdirectory(wxString strDir);
	// when sharing a directory, make all parent directories bold
	void UpdateParentItems(HTREEITEM hChild);

	// share list access
	bool IsShared(wxString strDir);
	void AddShare(wxString strDir);
	void DelShare(wxString strDir);
	void MarkChilds(HTREEITEM hChild,bool mark);

	wxArrayString m_lstShared;
	  //CTitleMenu  m_SharedMenu;
	wxString m_strLastRightClicked;
	bool m_bSelectSubDirs;

public:
	// construction / destruction
	//CDirectoryTreeCtrl();
	virtual ~CDirectoryTreeCtrl();
	  //virtual BOOL OnCommand(WPARAM wParam,LPARAM lParam );
	virtual bool ProcessEvent(wxEvent& evt);

protected:
	void OnTvnItemexpanding(wxCTreeEvent& evt);
	void OnLButtonDown(wxCTreeEvent& evt);
#if 0
	afx_msg void OnNMRclickSharedList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnTvnItemexpanding(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnTvnGetdispinfo(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	DECLARE_MESSAGE_MAP()
#endif
	  DECLARE_EVENT_TABLE()
};

#undef HTREEITEM

#endif
