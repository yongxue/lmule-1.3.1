//this file is part of eMule
//Copyright (C)2002 Merkur ( merkur-@users.sourceforge.net / http://www.emule-project.net )
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


#ifndef KNOWNFILELIST_H
#define KNOWNFILELIST_H

#include "KnownFile.h"
//#include <afxmt.h>
#include "emule.h"

#include <wx/list.h>
WX_DECLARE_LIST(CKnownFile,KnownFileList);

//class CKnownFileList : public CArray<CKnownFile*,CKnownFile*>
class CKnownFileList : public KnownFileList
{
	friend class CSharedFilesWnd;
	friend class CFileStatistic;
public:
	CKnownFileList(char* in_appdir);
	~CKnownFileList();
	void	SafeAddKFile(CKnownFile* toadd);
	bool	Init();
	void	Save();
	void	Clear();
	CKnownFile*	FindKnownFile(char* filename,uint32 in_date,uint32 in_size);
	wxMutex	list_mut;
private:
	char*	appdir;
	uint16 requested;
	uint32 transfered;
	uint16 accepted;
};



#endif
