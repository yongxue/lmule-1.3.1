//this file is part of eMule
//Copyright (C)2002 Merkur ( merkur-@users.sourceforge.net / http://www.emule-project.net )
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

//#include "stdafx.h"
#include "wintypes.h"
#include "otherfunctions.h"
#include "emule.h"

#include "strings.en"

#define itemsof(x) (sizeof(x)/sizeof(x[0]))

CString GetResString(UINT uStringID) {
  //return GetResString(uStringID,theApp.glob_prefs->GetLanguageID());
  int i;
  for(i=0;i<itemsof(_resStrings);i++) {
    if(_resStrings[i].id==uStringID) {
      return CString(wxGetTranslation(_resStrings[i].resstr));
    }
  }
  return "This String is Neat";
}

// Base chars for encode an decode functions
static byte base16Chars[17] = "0123456789ABCDEF";
static byte base32Chars[33] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567";
#define BASE16_LOOKUP_MAX 23
static byte base16Lookup[BASE16_LOOKUP_MAX][2] = {
    { '0', 0x0 },
    { '1', 0x1 },
    { '2', 0x2 },
    { '3', 0x3 },
    { '4', 0x4 },
    { '5', 0x5 },
    { '6', 0x6 },
    { '7', 0x7 },
    { '8', 0x8 },
    { '9', 0x9 },
	{ ':', 0x9 },
    { ';', 0x9 },
    { '<', 0x9 },
    { '=', 0x9 },
    { '>', 0x9 },
    { '?', 0x9 },
    { '@', 0x9 },
    { 'A', 0xA },
    { 'B', 0xB },
    { 'C', 0xC },
    { 'D', 0xD },
    { 'E', 0xE },
    { 'F', 0xF }
};

CString CastItoXBytes(uint64 count){
	CString buffer;
	if (count < 1024)
		buffer.Format("%.0f %s",(float)count,GetResString(IDS_BYTES).GetData());
	else if (count < 1048576)
		buffer.Format("%.0f %s",(float)count/1024,GetResString(IDS_KBYTES).GetData());
	else if (count < 1073741824)
		buffer.Format("%.2f %s",(float)count/1048576,GetResString(IDS_MBYTES).GetData());
	else if (count < 1099511627776LL)
		buffer.Format("%.2f %s",(float)count/1073741824,GetResString(IDS_GBYTES).GetData());
	else 
		buffer.Format("%.3f %s",(float)count/1099511627776LL,GetResString(IDS_TBYTES).GetData());
	return buffer;
}

CString CastItoIShort(uint64 count){
	CString output;
	if (count < 1000)
		output.Format("%i",count);
	else if (count < 1000000)
		output.Format("%.0f%s",(float)count/1000, GetResString(IDS_KILO).GetBuffer());
	else if (count < 1000000000)
		output.Format("%.2f%s",(float)count/1000000, GetResString(IDS_MEGA).GetBuffer());
	else if (count < 1000000000000LL)
		output.Format("%.2f%s",(float)count/1000000000LL, GetResString(IDS_GIGA).GetBuffer());
	else if (count < 1000000000000000LL)
		output.Format("%.2f%s",(float)count/1000000000000LL, GetResString(IDS_TERRA).GetBuffer());
	return output;
}

CString CastSecondsToHM(sint32 count){
	CString buffer;
	if (count < 0)
		buffer = "?"; 
	else if (count < 60)
		buffer.Format("%i %s",count,GetResString(IDS_SECS).GetData()); 
	else if (count < 3600) 
		buffer.Format("%i:%s %s",count/60,LeadingZero(count-(count/60)*60).GetData(),GetResString(IDS_MINS).GetData());
	else if (count < 86400) 
		buffer.Format("%i:%s %s",count/3600,LeadingZero((count-(count/3600)*3600)/60).GetData(),GetResString(IDS_HOURS).GetData());
	else 
		buffer.Format("%i %s %i %s",count/86400,GetResString(IDS_DAYS).GetData(),(count-(count/86400)*86400)/3600,GetResString(IDS_HOURS).GetData()); 
	return buffer;
} 

CString LeadingZero(uint32 units) {
	CString temp;
	if (units<10) temp.Format("0%i",units); else temp.Format("%i",units);
	return temp;
}

//<<--9/21/02
void ShellOpenFile(wxString name){ 
  //ShellExecute(NULL, "open", name, NULL, NULL, SW_SHOW); 
  printf("todo. shellopen\n");
} 

namespace {
	bool IsHexDigit(int c) {
		switch (c) {
		case '0': return true;
		case '1': return true;
		case '2': return true;
		case '3': return true;
		case '4': return true;
		case '5': return true;
		case '6': return true;
		case '7': return true;
		case '8': return true;
		case '9': return true;
		case 'A': return true;
		case 'B': return true;
		case 'C': return true;
		case 'D': return true;
		case 'E': return true;
		case 'F': return true;
		case 'a': return true;
		case 'b': return true;
		case 'c': return true;
		case 'd': return true;
		case 'e': return true;
		case 'f': return true;
		default: return false;
		}
	}
}

void URLDecode(wxString& result, const char* buff)
{
	int buflen = (int)strlen(buff);
	int x;
	int y;
	char* buff2 = nstrdup(buff); // length of buff2 will be less or equal to length of buff
	for (x = 0, y = 0; x < buflen ; ++x )
	{
		if ( buff[x] == '%' && x+2 < buflen && IsHexDigit(buff[x+1]) && IsHexDigit(buff[x+2]) ) {
			char hexstr[3];
			// Copy the two bytes following the %
			strncpy(hexstr, &buff[x + 1], 2);

			// Skip over the hex
			x = x + 2;

			// Convert the hex to ASCII
			buff2[y++] = (unsigned char)strtoul(hexstr, NULL, 16);
		}
		else {
			buff2[y++] = buff[x];
			break;
		}
	}
	result = buff2;
	free(buff2);
}

wxString URLEncode(wxString sIn){
    wxString sOut;
	
    const int nLen = sIn.Length() + 1;

    register LPBYTE pOutTmp = NULL;
    LPBYTE pOutBuf = NULL;
    register LPBYTE pInTmp = NULL;
    LPBYTE pInBuf =(LPBYTE)sIn.GetWriteBuf(nLen); //GetData(); //GetBuffer(nLen);
    BYTE b = 0;
	
    //alloc out buffer
    pOutBuf = (LPBYTE)sOut.GetWriteBuf(nLen*3-2); //GetData(); //GetBuffer(nLen  * 3 - 2);//new BYTE [nLen  * 3];

    if(pOutBuf)
    {
        pInTmp	= pInBuf;
	pOutTmp = pOutBuf;
		
	// do encoding
	while (*pInTmp)
	{
	    if(isalnum(*pInTmp))
	        *pOutTmp++ = *pInTmp;
	    else
	        if(isspace(*pInTmp))
		    *pOutTmp++ = '+';
		else
		{
		    *pOutTmp++ = '%';
		    *pOutTmp++ = toHex(*pInTmp>>4);
		    *pOutTmp++ = toHex(*pInTmp%16);
		}
	    pInTmp++;
	}
	*pOutTmp = '\0';
	sOut=pOutBuf;
	//delete [] pOutBuf;
	//Out.ReleaseBuffer();
	sOut.UngetWriteBuf();
    }
    //sIn.ReleaseBuffer();
    sIn.UngetWriteBuf();
    return sOut;
}

inline BYTE toHex(const BYTE &x){
	return x > 9 ? x + 55: x + 48;
}

// Returns the Typename, examining the extention of the given filename
wxString GetFiletypeByName(wxString infile) {
	wxString ext;

	//if (infile.ReverseFind('.')==-1) return GetResString(IDS_SEARCH_ANY);
	if(infile.Find('.',TRUE)==-1) return GetResString(IDS_SEARCH_ANY);

	ext= infile.Right(infile.Length()-infile.Find('.',TRUE)).MakeLower() ;
	
	if (ext==".mpc" || ext==".mp4" || ext==".aac" || ext==".ape" || ext==".mp3" || 
	    ext==".mp2" || ext==".wav" || ext==".au" || ext==".ogg" || ext==".wma") return "Audio"; //GetResString(IDS_SEARCH_AUDIO);

	if (ext==".jpg" || ext==".jpeg" || ext==".bmp" || ext==".gif" || ext==".tif" ||
	    ext==".png") return "Picture"; //GetResString(IDS_SEARCH_PICS);

	if (ext==".avi" || ext==".mpg" || ext==".mpeg" || ext==".ram" || ext==".rm" || ext==".asf" ||
	    ext==".vob" || ext==".divx" || ext==".vivo" || ext==".ogm" || ext==".mov" || ext==".wmv") return GetResString(IDS_SEARCH_VIDEO);

	if (ext==".gz" || ext==".zip" || ext==".ace" || ext==".rar") return GetResString(IDS_SEARCH_ARC);

	if (ext==".exe" || ext==".com") return "Program"; //GetResString(IDS_SEARCH_PRG);

	if (ext==".ccd"|| ext==".sub" || ext==".cue" || ext==".bin" || ext==".iso" || ext==".nrg" ||
	    ext==".img" || ext==".bwa" || ext==".bwi" || ext==".bws" || ext==".bwt" || ext==".mds" || ext==".mdf")
	  return GetResString(IDS_SEARCH_CDIMG);
  
	return "Any";
	//return GetResString(IDS_SEARCH_ANY);
}


inline char* nstrdup(const char* todup) { 
   size_t len = strlen(todup)+1; 
   return (char*)memcpy(new char[len], todup, len); 
} 

CString MakeStringEscaped(CString in) {
	in.Replace("&","&&");
	
	return in;
}

bool Ask4RegFix(bool checkOnly){
#if 0
	// check registry if ed2k links is assigned to emule
	CRegKey regkey;
	regkey.Create(HKEY_CLASSES_ROOT,"ed2k\\shell\\open\\command");
	ULONG maxsize = 500;
	TCHAR rbuffer[500];
	char modbuffer[490];
	char regbuffer[520];
	regkey.QueryStringValue(0,rbuffer,&maxsize);
	::GetModuleFileName(0,modbuffer, 490);
	sprintf(regbuffer,"\"%s\" \"%%1\"",modbuffer);
	if (strcmp(rbuffer,regbuffer)){
		if (checkOnly) return true;
		if (MessageBox(0,GetResString(IDS_ASSIGNED2K),GetResString(IDS_ED2KLINKFIX),MB_ICONQUESTION|MB_YESNO) == IDYES){
			regkey.SetStringValue(0,regbuffer);	
			regkey.Create(HKEY_CLASSES_ROOT,"ed2k\\DefaultIcon" );// Added Shrink 
			regkey.SetStringValue(0,modbuffer);
			regkey.Create(HKEY_CLASSES_ROOT,"ed2k" );
			regkey.SetStringValue(0,"URL: ed2k Protocol");
			regkey.SetStringValue("URL Protocol","" );
		}
	}
	regkey.Close();
	return false;
#endif
}

int GetMaxConnections() {
#if 0
	OSVERSIONINFOEX osvi;
	ZeroMemory(&osvi, sizeof(OSVERSIONINFOEX));
	osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);

	if(!GetVersionEx((OSVERSIONINFO*)&osvi)) {
		//if OSVERSIONINFOEX doesn't work, try OSVERSIONINFO
		osvi.dwOSVersionInfoSize = sizeof (OSVERSIONINFO);
		if(!GetVersionEx((OSVERSIONINFO*)&osvi))
			return -1;  //shouldn't ever happen
	}

	if(osvi.dwPlatformId == VER_PLATFORM_WIN32_NT) // Windows NT product family
		return -1;  //no limits

	if(osvi.dwPlatformId == VER_PLATFORM_WIN32_WINDOWS) { // Windows 95 product family

		if (osvi.dwMajorVersion == 4 && osvi.dwMinorVersion == 0) { //old school 95
			HKEY hKey;
			DWORD dwValue;
			DWORD dwLength = sizeof(dwValue);
			LONG lResult;

			RegOpenKeyEx(HKEY_LOCAL_MACHINE, "System\\CurrentControlSet\\Services\\VxD\\MSTCP",
				0, KEY_QUERY_VALUE, &hKey);
			lResult = RegQueryValueEx(hKey, TEXT("MaxConnections"), NULL, NULL,
				(LPBYTE)&dwValue, &dwLength);
			RegCloseKey(hKey);

			if(lResult != ERROR_SUCCESS || lResult < 1)
				return 100;  //the default for 95 is 100

			return dwValue;

		} else { //98 or ME
			HKEY hKey;
			TCHAR szValue[32];
			DWORD dwLength = sizeof(szValue);
			LONG lResult;

			RegOpenKeyEx(HKEY_LOCAL_MACHINE, "System\\CurrentControlSet\\Services\\VxD\\MSTCP",
				0, KEY_QUERY_VALUE, &hKey);
			lResult = RegQueryValueEx(hKey, TEXT("MaxConnections"), NULL, NULL,
				(LPBYTE)szValue, &dwLength);
			RegCloseKey(hKey);

			LONG lMaxConnections;
			if(lResult != ERROR_SUCCESS || (lMaxConnections = atoi(szValue)) < 1)
				return 100;  //the default for 98/ME is 100

			return lMaxConnections;
		}         
	}

	return -1;  //give the user the benefit of the doubt, most use NT+ anyway
#endif
	return -1;
}

CString   GetRateString(uint16 rate)   { 
      switch (rate){ 
      case 0: 
         return GetResString(IDS_CMT_NOTRATED); 
         break; 
      case 1: 
         return GetResString(IDS_CMT_FAKE); 
         break; 
      case 2: 
         return GetResString(IDS_CMT_POOR); 
         break; 
      case 3: 
         return GetResString(IDS_CMT_GOOD); 
         break; 
      case 4: 
         return GetResString(IDS_CMT_FAIR); 
         break; 
      case 5: 
         return GetResString(IDS_CMT_EXCELLENT); 
         break; 
      } 
      return GetResString(IDS_CMT_NOTRATED); 
}


WORD DetectWinVersion()
{
  // we are at least this good
  return _WINVER_XP_;
}


// Returns a BASE32 encoded byte array
//
// [In]
//   buffer: Pointer to byte array
//   bufLen: Lenght of buffer array
//
// [Return]
//   CString object with BASE32 encoded byte array
CString EncodeBase32(const unsigned char* buffer, unsigned int bufLen)
{
	CString Base32Buff;
    
	unsigned int i, index;
    unsigned char word;
	int pos = 0;

    for(i = 0, index = 0; i < bufLen;) {

		// Is the current word going to span a byte boundary?
        if (index > 3) {
            word = (buffer[i] & (0xFF >> index));
            index = (index + 5) % 8;
            word <<= index;
            if (i < bufLen - 1)
                word |= buffer[i + 1] >> (8 - index);

            i++;
        } else {
            word = (buffer[i] >> (8 - (index + 5))) & 0x1F;
            index = (index + 5) % 8;
            if (index == 0)
               i++;
        }

		Base32Buff += (char) base32Chars[word];
    }

    return Base32Buff;
}

// Returns a BASE16 encoded byte array
//
// [In]
//   buffer: Pointer to byte array
//   bufLen: Lenght of buffer array
//
// [Return]
//   CString object with BASE16 encoded byte array
CString EncodeBase16(const unsigned char* buffer, unsigned int bufLen)
{
	CString Base16Buff;

	for(unsigned int i = 0; i < bufLen; i++) {
		Base16Buff += base16Chars[buffer[i] >> 4];
		Base16Buff += base16Chars[buffer[i] & 0xf];
	}

    return Base16Buff;
}

// Decodes a BASE16 string into a byte array
//
// [In]
//   base16Buffer: String containing BASE16
//   base16BufLen: Lenght BASE16 coded string's length
//
// [Out]
//   buffer: byte array containing decoded string
void DecodeBase16(const char *base16Buffer, unsigned int base16BufLen, byte *buffer)
{
    memset(buffer, 0, DecodeLengthBase16(base16BufLen));
  
    for(unsigned int i = 0; i < base16BufLen; i++) {
		int lookup = toupper(base16Buffer[i]) - '0';

        // Check to make sure that the given word falls inside a valid range
		byte word = 0;
        
		if ( lookup < 0 || lookup >= BASE16_LOOKUP_MAX)
           word = 0xFF;
        else
           word = base16Lookup[lookup][1];

		if(i % 2 == 0) {
			buffer[i/2] = word << 4;
		} else {
			buffer[(i-1)/2] |= word;
		}
	}
}

// Calculates length to decode from BASE16
//
// [In]
//   base16Length: Actual length of BASE16 string
//
// [Return]
//   New length of byte array decoded
int	DecodeLengthBase16(int base16Length)
{
	return base16Length / 2;
}


#if 0
void UpdateURLMenu(CMenu &menu,int &counter){
	counter=0;
	theApp.webservices.RemoveAll();
	CString name,url,sbuffer;
	char buffer[1024];
	int lenBuf = 1024;

	FILE* readFile= fopen(CString(theApp.glob_prefs->GetAppDir())+"webservices.dat", "r");
	if (readFile!=NULL) {
		while (!feof(readFile)) {
			if (fgets(buffer,lenBuf,readFile)==0) break;
			sbuffer=buffer;
			
			// ignore comments & too short lines
			if (sbuffer.GetAt(0) == '#' || sbuffer.GetAt(0) == '/' || sbuffer.GetLength()<5)
				continue;
			
			int pos=sbuffer.Find(',');
			if (pos>0) {
				counter++;
				menu.AppendMenu(MF_STRING,MP_WEBURL+(counter-1), sbuffer.Left(pos).Trim() );
				theApp.webservices.Add(sbuffer.Right(sbuffer.GetLength()-pos-1).Trim() );
			}
		}
		fclose(readFile);
	}
}
#endif

void RunURL(CAbstractFile* file, CString urlpattern)
{
#if 0
	// Convert hash to hexadecimal text and add it to the URL
	urlpattern.Replace("#hashid", EncodeBase16(file->GetFileHash(), 16));

	// Add file size to the URL
	CString temp;
	temp.Format("%u",file->GetFileSize());
	urlpattern.Replace("#filesize", temp);

	// add filename to the url
	urlpattern.Replace("#filename",URLEncode(file->GetFileName()));

	// Open URL
	ShellExecute(NULL, NULL, urlpattern, NULL, theApp.glob_prefs->GetAppDir(), SW_SHOWDEFAULT);
#endif
}

#ifdef __FreeBSD__   
long long atoll( char * s ){
return  strtoll(s, (char **)NULL, 10);
}
#endif /* __FreeBSD__ */

void MakeFoldername(char* path){
	wxString string(path);
	if (string.Length()>0) if (string.Right(1)=='\\') string=string.Left(string.Length()-1);
	sprintf(path,"%s",string.GetData());
}
