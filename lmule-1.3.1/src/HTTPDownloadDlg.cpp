//this file is part of LMULE
//Copyright (C)2002 Tiku ( )
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

// ClientDetailDialog.cpp : implementation file
//

//#include "stdafx.h"
#include "wintypes.h"
#include "emule.h"
#include "HTTPDownloadDlg.h"
#include "otherfunctions.h"
#include "animate.h"
#include "muuli_wdr.h"
#include "inetdownload.h"

BEGIN_EVENT_TABLE(CHTTPDownloadDlg,wxDialog)
  EVT_BUTTON(ID_CANCEL,CHTTPDownloadDlg::OnBtnCancel)
END_EVENT_TABLE()

CHTTPDownloadDlg::CHTTPDownloadDlg(wxWindow* parent,wxString url,wxString tempName)
  : wxDialog(parent,1025,_("Downloading..."),wxDefaultPosition,wxDefaultSize,wxDEFAULT_DIALOG_STYLE|wxSYSTEM_MENU)
{
  wxSizer* content=downloadDlg(this,TRUE);

  wxGIFAnimationCtrl *ani=(wxGIFAnimationCtrl*)FindWindowById(ID_ANIMATE);
  //ani->LoadFile("pixmaps/tiku.gif");
  ani->LoadData((char*)inetDownload,sizeof(inetDownload));
  //ani->GetPlayer().UseParentBackground(true);
  ani->GetPlayer().UseBackgroundColour(true);
  ani->Play();

  content->Show(this,TRUE);

  Centre();

  process=new myWGetProcess(this);

  wxString cmd=wxString::Format("wget -O '%s' '%s'",tempName.GetData(),url.GetData());
  if(wxExecute(cmd,wxEXEC_ASYNC,process)<1) {
    wxMessageBox(_("Can't spawn wget. You should have it\nin your $PATH."),_("Error"),wxCENTRE|wxOK|wxICON_ERROR);
  }
}

void CHTTPDownloadDlg::OnBtnCancel(wxEvent& evt)
{
  // terminate wget
  if(process) {
    process->Detach();
  }
  EndModal(ID_CANCEL);
}

void myWGetProcess::OnTerminate(int pid,int status)
{
  // ok. it's time to close the window
/*  This code causes a continuous loop if wget is unsuccessful.
  if(status!=0) {
    // error
    wxString mystr=wxString::Format(_("wget returned error %d.\nServerlist was not updated."),status);
    wxMessageBox(mystr,_("Error"),wxCENTRE|wxOK|wxICON_ERROR);
  }*/

  myDlg->EndModal(status);
}

