#ifndef COMMENTDLG_H

#include <wx/dialog.h>
#include <wx/choice.h>

// CCommentDialog dialog 

class CCommentDialog : public wxDialog
{ 
  //DECLARE_DYNAMIC(CCommentDialog) 

public: 
   CCommentDialog(wxWindow* pParent,CKnownFile* file);   // standard constructor 
   virtual ~CCommentDialog(); 
   void Localize(); 
   virtual BOOL OnInitDialog(); 

    

// Dialog Data 
   enum { IDD = IDD_COMMENT }; 
protected: 
   //virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support 
   //DECLARE_MESSAGE_MAP() 
   DECLARE_EVENT_TABLE()
public: 
   void OnBnClickedApply(wxEvent& evt); 
   void OnBnClickedCancel(wxEvent& evt); 
private: 
   //CComboBox   ratebox;//For rate 
   wxComboBox* ratebox;
   CKnownFile* m_file; 
}; 

#endif
