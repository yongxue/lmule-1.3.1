#ifndef WIZARD_H
#define WIZARD_H


#include <wx/dialog.h>
// Wizard dialog

class Wizard : public wxDialog
{
  //DECLARE_DYNAMIC(Wizard)

public:
	Wizard(wxWindow* pParent = NULL);   // standard constructor
	virtual ~Wizard();
	void SetPrefs(CPreferences* in_prefs) {	app_prefs = in_prefs; }
	void Localize();
	virtual BOOL OnInitDialog();

	int m_iOS;
	int m_iTotalDownload;

// Dialog Data
	enum { IDD = IDD_WIZARD };
protected:
	CPreferences* app_prefs;
protected:
	//virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//DECLARE_MESSAGE_MAP()
	DECLARE_EVENT_TABLE()
public:
	void OnBnClickedApply(wxEvent& evt);
	void OnBnClickedCancel(wxEvent& evt);
	void OnBnClickedWizRadioOsNtxp(wxEvent& evt);
	void OnBnClickedWizRadioUs98me(wxEvent& evt);
	void OnBnClickedWizLowdownloadRadio(wxEvent& evt);
	void OnBnClickedWizMediumdownloadRadio(wxEvent& evt);
	void OnBnClickedWizHighdownloadRadio(wxEvent& evt);
	void OnBnClickedWizResetButton(wxEvent& evt);
protected:
	//CListCtrl m_provider;
	wxListCtrl* m_provider;
private:
	void SetCustomItemsActivation();
public:
	//afx_msg void OnNMClickProviders(NMHDR *pNMHDR, LRESULT *pResult);
	void OnNMClickProviders(wxListEvent& evt);

};

#endif
