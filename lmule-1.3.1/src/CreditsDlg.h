#ifndef CREDITSDLG_H
#define CREDITSDLG_H

// TVCreditsDlg.h : header file
//

//#include "GDIThread.h"
#include "resource.h"
//#include "EnBitmap.h"

/////////////////////////////////////////////////////////////////////////////
// CCreditsDlg dialog

class CCreditsDlg : public wxDialog
{
// Construction
public:
	void KillThread();
	void StartThread();
	CCreditsDlg(wxWindow* pParent = NULL);   // standard constructor
	CCreditsDlg::~CCreditsDlg();

#if 0
	CClientDC*	m_pDC;
	CRect		m_rectScreen;

	CGDIThread* m_pThread;
#endif

// Dialog Data
	//{{AFX_DATA(CCreditsDlg)
	enum { IDD = IDD_ABOUTBOX };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCreditsDlg)
	protected:
	//virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCreditsDlg)
#if 0
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	virtual BOOL OnInitDialog();
	virtual void OnPaint();
	afx_msg void OnDestroy();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
#endif
private:
	//CEnBitmap m_imgSplash;
};

#endif
