// XBMDraw.h: interface for the XBMDraw class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_XBMDRAW_H__79B4E994_2752_4A68_A15E_29B04736D719__INCLUDED_)
#define AFX_XBMDRAW_H__79B4E994_2752_4A68_A15E_29B04736D719__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class XBMDraw  
{
public:
	bool Line(int x1, int y1, int x2, int y2, bool bXOR = false);
	CString GetImageTag();
	bool Plot(int x, int y, bool bXOR = false);
	bool GetImage(CString &sImage);
	bool CreateImage(CString sName, int nWidth, int nHeight, BYTE bBackground = 0x00);
	XBMDraw();
	virtual ~XBMDraw();
private:
	CString	m_sName;
	int		m_nWidth;
	int		m_nHeight;
	BYTE*	m_pImage;
};

#endif // !defined(AFX_XBMDRAW_H__79B4E994_2752_4A68_A15E_29B04736D719__INCLUDED_)
