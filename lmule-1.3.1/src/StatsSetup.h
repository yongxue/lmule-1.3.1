//this file is part of eMule
//Copyright (C)2002 Merkur ( merkur-@users.sourceforge.net / http://www.emule-project.net )
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


#pragma once
#include "afxwin.h"
#include "IconStatic.h"

// CStatsSetup dialog
class CStatsSetup : public CDialog
{
	DECLARE_DYNAMIC(CStatsSetup)

public:
	CStatsSetup();   // standard constructor
	virtual ~CStatsSetup();
	void Localize();
// Dialog Data
	enum { IDD = IDD_STATSSETUP };

protected:
	//{{AFX_MSG(CSpinToolBar)
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void SaveColors();
	//}}AFX_MSG
	CSliderCtrl m_Slider1;
	CSliderCtrl m_Slider2;
	CString	m_SliderValue1;
	CString	m_SliderValue2;
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	void OnDestroy();
	CComboBox m_color;
	DECLARE_MESSAGE_MAP()
	void ShowInterval();
	void ShowColors(int nr);

private:
	void OnSelection();
};
