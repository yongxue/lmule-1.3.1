#ifndef MULELISTCTRL_H
#define MULELISTCTRL_H

#include "resource.h"
//////////////////////////////////
// CMuleListCtrl

#include <wx/event.h>
//#include "wx/listctrl.h"
#include "./listctrl.h"

class CMuleListCtrl : public wxODListCtrl
{
  //  	DECLARE_DYNAMIC(CMuleListCtrl)
  DECLARE_DYNAMIC_CLASS(CMuleListCtrl)

public:
	CMuleListCtrl();
	CMuleListCtrl(wxWindow*& parent,int id,const wxPoint& pos,wxSize siz,int flags);
	virtual ~CMuleListCtrl();

	// Sets the list name, used for hide/show menu
	void SetNameMule(LPCTSTR lpszName);

	// Forces the control to repaint a specific item.
	BOOL Update(int nItem);

	// Hide the column
	void HideColumn(int iColumn);

	// Unhide the column
	void ShowColumn(int iColumn);

	// check to see if the column is hidden
	BOOL IsColumnHidden(int iColumn) const {
		if(iColumn < 1 || iColumn >= m_iColumnsTracked)
			return false;

		return m_aColumns[iColumn].bHidden;
	}

	// Get the correct column width even if column is hidden
	int GetColumnWidth(int iColumn) const {
		if(iColumn < 0 || iColumn >= m_iColumnsTracked)
			return 0;
		
		if(m_aColumns[iColumn].bHidden)
			return m_aColumns[iColumn].iWidth;
		else
			return wxODListCtrl::GetColumnWidth(iColumn);
	}

	//save to preferences
	void SaveSettings(CPreferences::Table tID);

	//load from preferences
	void LoadSettings(CPreferences::Table tID);

	enum ArrowType { arrowDown = IDB_DOWN, arrowUp = IDB_UP,
		arrowDoubleDown = IDB_DOWN2X, arrowDoubleUp = IDB_UP2X };
	DECLARE_EVENT_TABLE()
protected:
	virtual bool ProcessEvent(wxEvent& evt);
	virtual void PreSubclassWindow();
	//virtual BOOL OnWndMsg(UINT message, WPARAM wParam, LPARAM lParam, LRESULT* pResult);
	//virtual BOOL OnChildNotify(UINT message, WPARAM wParam, LPARAM lParam, LRESULT* pResult);

#if 0
	DECLARE_MESSAGE_MAP()
	afx_msg void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnSysColorChange();
#endif

	void         SetColors();
	void         SetSortArrow(int iColumn, ArrowType atType);
	void         SetSortArrow(int iColumn, bool bAscending) {
		SetSortArrow(iColumn, bAscending ? arrowUp : arrowDown);
	}
#if 0
	DWORD        SetExtendedStyle(DWORD dwNewStyle) {
		return CListCtrl::SetExtendedStyle(dwNewStyle | LVS_EX_HEADERDRAGDROP);
	}
#endif

	wxString          m_Name;
	//NMLVCUSTOMDRAW   m_lvcd;
	wxMenu *m_ColumnMenu;
	bool             m_bCustomDraw;
	COLORREF         m_crWindow;
	COLORREF         m_crWindowText;
	COLORREF         m_crHighlight;
	COLORREF         m_crFocusLine;
	COLORREF         m_crNoHighlight;
	COLORREF         m_crNoFocusLine;
	void OnColumnRclick(wxListEvent& evt);
private:
	//static int IndexToOrder(CHeaderCtrl* pHeader, int iIndex);

	struct MULE_COLUMN {
		int iWidth;
		int iLocation;
		bool bHidden;
	};

	int          m_iColumnsTracked;
	MULE_COLUMN *m_aColumns;

	int GetHiddenColumnCount() const {
		int iHidden = 0;
		for(int i = 0; i < m_iColumnsTracked; i++)
			if(m_aColumns[i].bHidden)
				iHidden++;
		return iHidden;
	}

	int       m_iCurrentSortItem;
	ArrowType m_atSortArrow;

	int m_col_minsize;
};
#endif
