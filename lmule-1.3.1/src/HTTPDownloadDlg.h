//this file is part of LMULE
//Copyright (C)2002 Tiku ( )
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#ifndef HTTP_DOWNLOADDLG_H
#define HTTP_DOWNLOADDLG_H

#include "wintypes.h"
#include "types.h"
#include "updownclient.h"

#include <wx/dialog.h>
#include <wx/process.h>

class CHTTPDownloadDlg;

class myWGetProcess : public wxProcess
{
 public:
  myWGetProcess(wxEvtHandler* parent) 
    : wxProcess(parent) {
    myDlg=(CHTTPDownloadDlg*)parent;
  };

  void OnTerminate(int pid,int status);
 private:
  CHTTPDownloadDlg* myDlg;
};

class CHTTPDownloadDlg : public wxDialog
{
 public:
  CHTTPDownloadDlg(wxWindow*parent,wxString url,wxString tempName);
  ~CHTTPDownloadDlg() {
    if(process) {
      process->Detach();
    }
  }

 private:
  DECLARE_EVENT_TABLE()

  void OnBtnCancel(wxEvent& evt);
  myWGetProcess *process;
};

#endif
