/////////////////////////////////////////////////////////////////////////////
// Name:        wx/generic/treectlg.h
// Purpose:     wxCTreeCtrl class
// Author:      Robert Roebling
// Modified by:
// Created:     01/02/97
// RCS-ID:      $Id: treectlc.h,v 1.1 2003/03/09 21:06:06 tiku Exp $
// Copyright:   (c) 1997,1998 Robert Roebling
// Licence:     wxWindows license
/////////////////////////////////////////////////////////////////////////////

#ifndef _GENERIC_TREECTRLC_H_
#define _GENERIC_TREECTRLC_H_

#if defined(__GNUG__) && !defined(__APPLE__)
  #pragma interface "treectlc.h"
#endif

#if 1

#include "wx/scrolwin.h"
#include "wx/pen.h"
#include "wx/imaglist.h"

// -----------------------------------------------------------------------------
// forward declaration
// -----------------------------------------------------------------------------

class WXDLLEXPORT wxGenericCTreeItem;

class WXDLLEXPORT wxCTreeItemData;

class WXDLLEXPORT wxCTreeRenameTimer;
class WXDLLEXPORT wxCTreeFindTimer;
class WXDLLEXPORT wxCTreeTextCtrl;
class WXDLLEXPORT wxTextCtrl;

// -----------------------------------------------------------------------------
// wxGenericCTreeCtrl - the tree control
// -----------------------------------------------------------------------------

class WXDLLEXPORT wxGenericCTreeCtrl : public wxScrolledWindow
{
public:
    // creation
    // --------
    wxGenericCTreeCtrl() { Init(); }

    wxGenericCTreeCtrl(wxWindow *parent, wxWindowID id = -1,
               const wxPoint& pos = wxDefaultPosition,
               const wxSize& size = wxDefaultSize,
               long style = wxTR_DEFAULT_STYLE,
               const wxValidator &validator = wxDefaultValidator,
               const wxString& name = wxCTreeCtrlNameStr)
    {
        Init();
        Create(parent, id, pos, size, style, validator, name);
    }

    virtual ~wxGenericCTreeCtrl();

    bool Create(wxWindow *parent, wxWindowID id = -1,
                const wxPoint& pos = wxDefaultPosition,
                const wxSize& size = wxDefaultSize,
                long style = wxTR_DEFAULT_STYLE,
                const wxValidator &validator = wxDefaultValidator,
                const wxString& name = wxCTreeCtrlNameStr);

    // accessors
    // ---------

        // get the total number of items in the control
    size_t GetCount() const;

        // indent is the number of pixels the children are indented relative to
        // the parents position. SetIndent() also redraws the control
        // immediately.
    unsigned int GetIndent() const { return m_indent; }
    void SetIndent(unsigned int indent);

        // spacing is the number of pixels between the start and the Text
    unsigned int GetSpacing() const { return m_spacing; }
    void SetSpacing(unsigned int spacing);

        // image list: these functions allow to associate an image list with
        // the control and retrieve it. Note that when assigned with
        // SetImageList, the control does _not_ delete
        // the associated image list when it's deleted in order to allow image
        // lists to be shared between different controls. If you use
        // AssignImageList, the control _does_ delete the image list.
        //
        // The normal image list is for the icons which correspond to the
        // normal tree item state (whether it is selected or not).
        // Additionally, the application might choose to show a state icon
        // which corresponds to an app-defined item state (for example,
        // checked/unchecked) which are taken from the state image list.
    wxImageList *GetImageList() const;
    wxImageList *GetStateImageList() const;
    wxImageList *GetButtonsImageList() const;

    void SetImageList(wxImageList *imageList);
    void SetStateImageList(wxImageList *imageList);
    void SetButtonsImageList(wxImageList *imageList);
    void AssignImageList(wxImageList *imageList);
    void AssignStateImageList(wxImageList *imageList);
    void AssignButtonsImageList(wxImageList *imageList);

    // Functions to work with tree ctrl items.

    // accessors
    // ---------

        // retrieve item's label
    wxString GetItemText(const wxCTreeItemId& item) const;
        // get one of the images associated with the item (normal by default)
    int GetItemImage(const wxCTreeItemId& item,
                     wxCTreeItemIcon which = wxCTreeItemIcon_Normal) const;
        // get the data associated with the item
    wxCTreeItemData *GetItemData(const wxCTreeItemId& item) const;

        // get the item's text colour
    wxColour GetItemTextColour(const wxCTreeItemId& item) const;

        // get the item's background colour
    wxColour GetItemBackgroundColour(const wxCTreeItemId& item) const;

        // get the item's font
    wxFont GetItemFont(const wxCTreeItemId& item) const;

    // modifiers
    // ---------

        // set item's label
    void SetItemText(const wxCTreeItemId& item, const wxString& text);
        // get one of the images associated with the item (normal by default)
    void SetItemImage(const wxCTreeItemId& item, int image,
                      wxCTreeItemIcon which = wxCTreeItemIcon_Normal);
        // associate some data with the item
    void SetItemData(const wxCTreeItemId& item, wxCTreeItemData *data);

        // force appearance of [+] button near the item. This is useful to
        // allow the user to expand the items which don't have any children now
        // - but instead add them only when needed, thus minimizing memory
        // usage and loading time.
    void SetItemHasChildren(const wxCTreeItemId& item, bool has = TRUE);

        // the item will be shown in bold
    void SetItemBold(const wxCTreeItemId& item, bool bold = TRUE);

        // set the item's text colour
    void SetItemTextColour(const wxCTreeItemId& item, const wxColour& col);

        // set the item's background colour
    void SetItemBackgroundColour(const wxCTreeItemId& item, const wxColour& col);

        // set the item's font (should be of the same height for all items)
    void SetItemFont(const wxCTreeItemId& item, const wxFont& font);

        // set the window font
    virtual bool SetFont( const wxFont &font );

       // set the styles.  No need to specify a GetWindowStyle here since
       // the base wxWindow member function will do it for us
    void SetWindowStyle(const long styles);

    void SetChecked(const wxCTreeItemId& item,bool mode);

    // item status inquiries
    // ---------------------

        // is the item visible (it might be outside the view or not expanded)?
    bool IsVisible(const wxCTreeItemId& item) const;
        // does the item has any children?
    bool HasChildren(const wxCTreeItemId& item) const
      { return ItemHasChildren(item); }
    bool ItemHasChildren(const wxCTreeItemId& item) const;
        // is the item expanded (only makes sense if HasChildren())?
    bool IsExpanded(const wxCTreeItemId& item) const;
        // is this item currently selected (the same as has focus)?
    bool IsSelected(const wxCTreeItemId& item) const;
        // is item text in bold font?
    bool IsBold(const wxCTreeItemId& item) const;
        // does the layout include space for a button?
    bool IsChecked(const wxCTreeItemId& item) const;

    // number of children
    // ------------------

        // if 'recursively' is FALSE, only immediate children count, otherwise
        // the returned number is the number of all items in this branch
    size_t GetChildrenCount(const wxCTreeItemId& item, bool recursively = TRUE);

    // navigation
    // ----------

    // wxCTreeItemId.IsOk() will return FALSE if there is no such item

        // get the root tree item
    wxCTreeItemId GetRootItem() const { return m_anchor; }

        // get the item currently selected (may return NULL if no selection)
    wxCTreeItemId GetSelection() const { return m_current; }

        // get the items currently selected, return the number of such item
    size_t GetSelections(wxArrayCTreeItemIds&) const;

        // get the parent of this item (may return NULL if root)
    wxCTreeItemId GetItemParent(const wxCTreeItemId& item) const;

#if WXWIN_COMPATIBILITY_2_2
        // deprecated:  Use GetItemParent instead.
    wxCTreeItemId GetParent(const wxCTreeItemId& item) const
    	{ return GetItemParent( item ); }

    	// Expose the base class method hidden by the one above.
    wxWindow *GetParent() const { return wxScrolledWindow::GetParent(); }
#endif  // WXWIN_COMPATIBILITY_2_2

        // for this enumeration function you must pass in a "cookie" parameter
        // which is opaque for the application but is necessary for the library
        // to make these functions reentrant (i.e. allow more than one
        // enumeration on one and the same object simultaneously). Of course,
        // the "cookie" passed to GetFirstChild() and GetNextChild() should be
        // the same!

        // get the first child of this item
    wxCTreeItemId GetFirstChild(const wxCTreeItemId& item, long& cookie) const;
        // get the next child
    wxCTreeItemId GetNextChild(const wxCTreeItemId& item, long& cookie) const;
        // get the last child of this item - this method doesn't use cookies
    wxCTreeItemId GetLastChild(const wxCTreeItemId& item) const;

        // get the next sibling of this item
    wxCTreeItemId GetNextSibling(const wxCTreeItemId& item) const;
        // get the previous sibling
    wxCTreeItemId GetPrevSibling(const wxCTreeItemId& item) const;

        // get first visible item
    wxCTreeItemId GetFirstVisibleItem() const;
        // get the next visible item: item must be visible itself!
        // see IsVisible() and wxCTreeCtrl::GetFirstVisibleItem()
    wxCTreeItemId GetNextVisible(const wxCTreeItemId& item) const;
        // get the previous visible item: item must be visible itself!
    wxCTreeItemId GetPrevVisible(const wxCTreeItemId& item) const;

        // Only for internal use right now, but should probably be public
    wxCTreeItemId GetNext(const wxCTreeItemId& item) const;

    // operations
    // ----------

        // add the root node to the tree
    wxCTreeItemId AddRoot(const wxString& text,
                         int image = -1, int selectedImage = -1,
                         wxCTreeItemData *data = NULL);

        // insert a new item in as the first child of the parent
    wxCTreeItemId PrependItem(const wxCTreeItemId& parent,
                             const wxString& text,
                             int image = -1, int selectedImage = -1,
                             wxCTreeItemData *data = NULL);

        // insert a new item after a given one
    wxCTreeItemId InsertItem(const wxCTreeItemId& parent,
                            const wxCTreeItemId& idPrevious,
                            const wxString& text,
                            int image = -1, int selectedImage = -1,
                            wxCTreeItemData *data = NULL);

        // insert a new item before the one with the given index
    wxCTreeItemId InsertItem(const wxCTreeItemId& parent,
                            size_t index,
                            const wxString& text,
                            int image = -1, int selectedImage = -1,
                            wxCTreeItemData *data = NULL);

        // insert a new item in as the last child of the parent
    wxCTreeItemId AppendItem(const wxCTreeItemId& parent,
                            const wxString& text,
                            int image = -1, int selectedImage = -1,
                            wxCTreeItemData *data = NULL);

        // delete this item and associated data if any
    void Delete(const wxCTreeItemId& item);
        // delete all children (but don't delete the item itself)
        // NB: this won't send wxEVT_COMMAND_TREE_ITEM_DELETED events
    void DeleteChildren(const wxCTreeItemId& item);
        // delete all items from the tree
        // NB: this won't send wxEVT_COMMAND_TREE_ITEM_DELETED events
    void DeleteAllItems();

        // expand this item
    void Expand(const wxCTreeItemId& item);
        // expand this item and all subitems recursively
    void ExpandAll(const wxCTreeItemId& item);
        // collapse the item without removing its children
    void Collapse(const wxCTreeItemId& item);
        // collapse the item and remove all children
    void CollapseAndReset(const wxCTreeItemId& item);
        // toggles the current state
    void Toggle(const wxCTreeItemId& item);

        // remove the selection from currently selected item (if any)
    void Unselect();
    void UnselectAll();
        // select this item
    void SelectItem(const wxCTreeItemId& item, bool unselect_others=TRUE, bool extended_select=FALSE);
        // make sure this item is visible (expanding the parent item and/or
        // scrolling to this item if necessary)
    void EnsureVisible(const wxCTreeItemId& item);
        // scroll to this item (but don't expand its parent)
    void ScrollTo(const wxCTreeItemId& item);
    void AdjustMyScrollbars();

        // The first function is more portable (because easier to implement
        // on other platforms), but the second one returns some extra info.
    wxCTreeItemId HitTest(const wxPoint& point)
        { int dummy; return HitTest(point, dummy); }
    wxCTreeItemId HitTest(const wxPoint& point, int& flags);

        // get the bounding rectangle of the item (or of its label only)
    bool GetBoundingRect(const wxCTreeItemId& item,
                         wxRect& rect,
                         bool textOnly = FALSE) const;

        // Start editing the item label: this (temporarily) replaces the item
        // with a one line edit control. The item will be selected if it hadn't
        // been before.
    void EditLabel( const wxCTreeItemId& item ) { Edit( item ); }
    void Edit( const wxCTreeItemId& item );
        // returns a pointer to the text edit control if the item is being
        // edited, NULL otherwise (it's assumed that no more than one item may
        // be edited simultaneously)
    wxTextCtrl* GetEditControl() const;

    // sorting
        // this function is called to compare 2 items and should return -1, 0
        // or +1 if the first item is less than, equal to or greater than the
        // second one. The base class version performs alphabetic comparaison
        // of item labels (GetText)
    virtual int OnCompareItems(const wxCTreeItemId& item1,
                               const wxCTreeItemId& item2);
        // sort the children of this item using OnCompareItems
        //
        // NB: this function is not reentrant and not MT-safe (FIXME)!
    void SortChildren(const wxCTreeItemId& item);

    // deprecated functions: use Set/GetItemImage directly
        // get the selected item image
    int GetItemSelectedImage(const wxCTreeItemId& item) const
        { return GetItemImage(item, wxCTreeItemIcon_Selected); }
        // set the selected item image
    void SetItemSelectedImage(const wxCTreeItemId& item, int image)
        { SetItemImage(item, image, wxCTreeItemIcon_Selected); }

    // implementation only from now on

    // overridden base class virtuals
    virtual bool SetBackgroundColour(const wxColour& colour);
    virtual bool SetForegroundColour(const wxColour& colour);

    // callbacks
    void OnPaint( wxPaintEvent &event );
    void OnSetFocus( wxFocusEvent &event );
    void OnKillFocus( wxFocusEvent &event );
    void OnChar( wxKeyEvent &event );
    void OnMouse( wxMouseEvent &event );
    void OnIdle( wxIdleEvent &event );

    // implementation helpers
protected:
    friend class wxGenericCTreeItem;
    friend class wxCTreeRenameTimer;
    friend class wxCTreeFindTimer;
    friend class wxCTreeTextCtrl;

    wxFont               m_normalFont;
    wxFont               m_boldFont;

    wxGenericCTreeItem   *m_anchor;
    wxGenericCTreeItem   *m_current,
                        *m_key_current;
    unsigned short       m_indent;
    unsigned short       m_spacing;
    int                  m_lineHeight;
    wxPen                m_dottedPen;
    wxBrush             *m_hilightBrush,
                        *m_hilightUnfocusedBrush;
    bool                 m_hasFocus;
    bool                 m_dirty;
    bool                 m_ownsImageListNormal,
                         m_ownsImageListState,
                         m_ownsImageListButtons;
    bool                 m_isDragging; // true between BEGIN/END drag events
    bool                 m_lastOnSame;  // last click on the same item as prev
    wxImageList         *m_imageListNormal,
                        *m_imageListState,
                        *m_imageListButtons;

    int                  m_dragCount;
    wxPoint              m_dragStart;
    wxGenericCTreeItem   *m_dropTarget;
    wxCursor             m_oldCursor;  // cursor is changed while dragging
    wxGenericCTreeItem   *m_oldSelection;
    wxCTreeTextCtrl      *m_textCtrl;

    wxTimer             *m_renameTimer;

    wxBitmap            *m_arrowRight,
      *m_arrowDown,
      *m_check1,
      *m_check2;

    // incremental search data
    wxString             m_findPrefix;
    wxTimer             *m_findTimer;

    // the common part of all ctors
    void Init();

    // misc helpers
    void SendDeleteEvent(wxGenericCTreeItem *itemBeingDeleted);

    void DrawBorder(const wxCTreeItemId& item);
    void DrawLine(const wxCTreeItemId& item, bool below);
    void DrawDropEffect(wxGenericCTreeItem *item);

    wxCTreeItemId DoInsertItem(const wxCTreeItemId& parent,
                              size_t previous,
                              const wxString& text,
                              int image, int selectedImage,
                              wxCTreeItemData *data);

    // called by wxTextTreeCtrl when it marks itself for deletion
    void ResetTextControl();

    // find the first item starting with the given prefix after the given item
    wxCTreeItemId FindItem(const wxCTreeItemId& id, const wxString& prefix) const;

    bool HasButtons(void) const
        { return (m_imageListButtons != NULL)
              || HasFlag(wxTR_TWIST_BUTTONS|wxTR_HAS_BUTTONS); }

    void CalculateLineHeight();
    int  GetLineHeight(wxGenericCTreeItem *item) const;
    void PaintLevel( wxGenericCTreeItem *item, wxDC& dc, int level, int &y );
    void PaintItem( wxGenericCTreeItem *item, wxDC& dc);

    void CalculateLevel( wxGenericCTreeItem *item, wxDC &dc, int level, int &y );
    void CalculatePositions();
    void CalculateSize( wxGenericCTreeItem *item, wxDC &dc );

    void RefreshSubtree( wxGenericCTreeItem *item );
    void RefreshLine( wxGenericCTreeItem *item );

    // redraw all selected items
    void RefreshSelected();

    // RefreshSelected() recursive helper
    void RefreshSelectedUnder(wxGenericCTreeItem *item);

    void OnRenameTimer();
    bool OnRenameAccept(wxGenericCTreeItem *item, const wxString& value);
    void OnRenameCancelled(wxGenericCTreeItem *item);

    void FillArray(wxGenericCTreeItem*, wxArrayCTreeItemIds&) const;
    void SelectItemRange( wxGenericCTreeItem *item1, wxGenericCTreeItem *item2 );
    bool TagAllChildrenUntilLast(wxGenericCTreeItem *crt_item, wxGenericCTreeItem *last_item, bool select);
    bool TagNextChildren(wxGenericCTreeItem *crt_item, wxGenericCTreeItem *last_item, bool select);
    void UnselectAllChildren( wxGenericCTreeItem *item );

private:
    DECLARE_EVENT_TABLE()
    DECLARE_DYNAMIC_CLASS(wxGenericCTreeCtrl)
};

#if !defined(__WXMSW__) || defined(__WIN16__) || defined(__WXUNIVERSAL__)
/*
 * wxCTreeCtrl has to be a real class or we have problems with
 * the run-time information.
 */

class WXDLLEXPORT wxCTreeCtrl: public wxGenericCTreeCtrl
{
    DECLARE_DYNAMIC_CLASS(wxCTreeCtrl)

public:
    wxCTreeCtrl() {}

    wxCTreeCtrl(wxWindow *parent, wxWindowID id = -1,
               const wxPoint& pos = wxDefaultPosition,
               const wxSize& size = wxDefaultSize,
               long style = wxTR_DEFAULT_STYLE,
               const wxValidator &validator = wxDefaultValidator,
               const wxString& name = wxCTreeCtrlNameStr)
    : wxGenericCTreeCtrl(parent, id, pos, size, style, validator, name)
    {
    }
};
#endif // !__WXMSW__ || __WIN16__ || __WXUNIVERSAL__

#endif // wxUSE_TREECTRL

#endif // _GENERIC_TREECTRL_H_

