//this file is part of eMule
//Copyright (C)2002 Merkur ( merkur-@users.sourceforge.net / http://www.emule-project.net )
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


#ifndef _TRANSFERWND_H
#define _TRANSFERWND_H

#include "UploadListCtrl.h"
#include "DownloadListCtrl.h"
//#include "ResizableLib/ResizableDialog.h"
#include "SplitterControl.h"
#include "QueueListCtrl.h"

#include <wx/panel.h>
#include <wx/tooltip.h>

// CTransferWnd dialog

#include <wx/listctrl.h>
class CTransferWnd : public wxPanel //CResizableDialog
{
  //DECLARE_DYNAMIC(CTransferWnd)
  DECLARE_DYNAMIC_CLASS(CTransferWnd)

public:
	CTransferWnd(wxWindow* pParent = NULL);   // standard constructor
	virtual ~CTransferWnd();
	void	ShowQueueCount(uint32 number);
	void	SwitchUploadList(wxCommandEvent& evt);
	void	Localize();
// Dialog Data
	enum { IDD = IDD_TRANSFER };
	CUploadListCtrl*		uploadlistctrl;
	CDownloadListCtrl*	downloadlistctrl;
	CQueueListCtrl*		queuelistctrl;
	//wxToolTip		m_ttip;
protected:
	void DoResize(int delta);
	void UpdateSplitterRange();
	void SetInitLayout();
	/*
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	virtual LRESULT DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	afx_msg BOOL OnToolTipNotify(UINT id, NMHDR *pNMH, LRESULT *pResult);
	*/

	void DoSplitResize(int delta);
	void UpdateToolTips(void);
	int GetItemUnderMouse(wxListCtrl* ctrl);
	
	CSplitterControl m_wndSplitter;
	
	DECLARE_EVENT_TABLE()

	//DECLARE_MESSAGE_MAP()

private:
	wxString m_strToolTip;
	int m_iOldToolTipItemDown;
	int m_iOldToolTipItemUp;
	int m_iOldToolTipItemQueue;	
	uint8	windowtransferstate;
};
#endif
