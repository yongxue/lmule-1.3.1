#ifndef PPGDIR_H
#define PPGDIR_H

#include "Preferences.h"
#include "DirectoryTreeCtrl.h"
#include <wx/panel.h>

// CPPgDirectories dialog

class CPPgDirectories : public wxPanel //CPropertyPage
{
  //DECLARE_DYNAMIC(CPPgDirectories)
  DECLARE_DYNAMIC_CLASS(CPPgDirectories)
    CPPgDirectories() {};

public:
	CPPgDirectories(wxWindow* parent);									// standard constructor
	virtual ~CPPgDirectories();

	void SetPrefs(CPreferences* in_prefs) {	app_prefs = in_prefs; }

// Dialog Data
	enum { IDD = IDD_PPG_DIRECTORIES };

protected:
	//virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//DECLARE_MESSAGE_MAP()
	DECLARE_EVENT_TABLE()

public:
	//virtual BOOL OnInitDialog();
public:
	CDirectoryTreeCtrl* m_ShareSelector;
	CPreferences* app_prefs;
private:
	bool SelectDir(char* outdir, char* titletext);
public:
	void OnBnClickedSelincdir(wxEvent& e);
	void OnBnClickedSeltempdir(wxEvent& e);
	void BrowseVideoplayer(wxEvent& e);

	virtual BOOL OnApply();
#if 0

	afx_msg void OnBnClickedSelincdir();
	afx_msg void OnBnClickedSeltempdir();
	
	afx_msg void OnEnChangeIncfiles()	{ SetModified(); }
	afx_msg void OnEnChangeTempfiles()	{ SetModified(); }
	afx_msg void OnBnClickedCheck1()	{ SetModified(); }
protected:
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
#endif
public:
	void Localize(void);
	void LoadSettings(void);
};

#endif
