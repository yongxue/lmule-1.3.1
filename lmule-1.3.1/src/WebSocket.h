#ifndef WEBSOCKET_H
#define WEBSOCKET_H 1

class CWebServer;

void StartSockets(CWebServer *pThis);
void StopSockets();

class CWebSocket 
{
public:
	void SetParent(CWebServer *);
	CWebServer* m_pParent;

	class CChunk 
	{
	public:
		char* m_pData;
		char* m_pToSend;
		DWORD m_dwSize;
		CChunk* m_pNext;

		~CChunk() { if (m_pData) delete[] m_pData; }
	};

	CChunk* m_pHead; // tails of what has to be sent
	CChunk* m_pTail;

	char* m_pBuf;
	DWORD m_dwRecv;
	DWORD m_dwBufSize;
	DWORD m_dwHttpHeaderLen;
	DWORD m_dwHttpContentLen;

	bool m_bCanRecv;
	bool m_bCanSend;
	bool m_bValid;
	int  m_hSocket;

	void OnReceived(void* pData, DWORD dwDataSize); // must be implemented
	void SendData(const void* pData, DWORD dwDataSize);
	void SendData(char* szText) { SendData(szText, strlen(szText)); }
	void SendContent(char* szStdResponse, const void* pContent, DWORD dwContentSize);
	void SendTextContent(char* szText) { SendContent("", szText, strlen(szText)); }
	void Disconnect();

	void OnRequestReceived(char* pHeader, DWORD dwHeaderLen, char* pData, DWORD dwDataLen);
};
#endif
