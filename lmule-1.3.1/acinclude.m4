dnl ----------------------------------------------------
dnl CHECK_WX_BUILT_WITH_GTK2
dnl check gtk version wx windows was compiled
dnl ----------------------------------------------------

AC_DEFUN(CHECK_WX_BUILT_WITH_GTK2,
[
  AC_MSG_CHECKING(if wxWindows was linked with GTK2)
  if $wx_config_name --cppflags | grep -q 'gtk2' ; then
     GTK_USEDVERSION=2
     AC_MSG_RESULT(yes)
  else
     AC_MSG_RESULT(no)
  fi

  AC_SUBST(GTK_USEDVERSION)
])



dnl ----------------------------------------------------
dnl CHECK_ZLIB
dnl check if zlib is on the system
dnl ----------------------------------------------------
AC_DEFUN(CHECK_ZLIB,
[
wv_zlib=""
found_zlib="no"

ZLIB_DIR=""
AC_ARG_WITH(zlib,[  --with-zlib=DIR       use zlib in DIR],[
	if [ test "$withval" = "no" ]; then
		AC_MSG_ERROR([zlib is required by lMule])
        elif [ test "$withval" = "yes" ]; then
		zlib=check
        elif [ test "$withval" = "peer" ]; then
		zlib=peer
	else
		zlib=sys
		ZLIB_DIR="$withval"
		wv_zlib="--with-zlib=$withval"
        fi
],[	zlib=check
])

if test $zlib = peer; then
	z=peer
else
	if test $zlib = sys; then
		_cppflags="$CPPFLAGS"
		CPPFLAGS="$CPPFLAGS -I$ZLIB_DIR/include"
	fi
	AC_CHECK_HEADER(zlib.h,[
		z=sys
	],[	if test $zlib = sys; then
			AC_MSG_ERROR([zlib not found in system location])
		fi
		z=peer
	])
	if test $zlib = sys; then
		CPPFLAGS="$_cppflags"
	fi
fi

if test $z = peer; then
	AC_MSG_CHECKING(for zlib in peer directory)
	if test -d ../zlib; then
		if test -r ../zlib/libz.a; then
			AC_MSG_RESULT(yes)
		else
			AC_MSG_RESULT(no)
			AC_MSG_ERROR([unable to use peer zlib - zlib/libz.a not found])
		fi
	else
		AC_MSG_RESULT(no)
		AC_MSG_ERROR([unable to use zlib - no peer found])
	fi

	zlib_message="peer zlib"
	ZLIB_CFLAGS='-I$(top_srcdir)/../zlib'
	ZLIB_LIBS='$(top_srcdir)/../zlib/libz.a'

	wv_cppflags="$wv_cppflags -I$ZLIB_PEERDIR"
else
	if test $zlib = sys; then
		zlib_message="zlib in -L$ZLIB_DIR/lib -lz"
		ZLIB_CFLAGS="-I$ZLIB_DIR/include"
		ZLIB_LIBS="-L$ZLIB_DIR/lib -lz"
	else
		zlib_message="zlib in -lz"
		ZLIB_CFLAGS=""
		ZLIB_LIBS="-lz"
	fi
fi

AC_SUBST(ZLIB_CFLAGS)
AC_SUBST(ZLIB_LIBS)

])

