//this file is part of eMule
//Copyright (C)2002 Merkur ( merkur-@users.sourceforge.net / http://www.emule-project.net )
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#ifndef OTHERFUNS_H
#define OTHERFUNS_H

#include "types.h"
#include <math.h>

#define ROUND(x) (floor((float)x+0.5f))

#define _WINVER_NT4_	0x0004
#define _WINVER_95_		0x0004
#define _WINVER_98_		0x0A04
#define _WINVER_ME_		0x5A04
#define _WINVER_2K_		0x0005
#define _WINVER_XP_		0x0105

#include <wx/string.h>

CString CastItoXBytes(uint64 count);
CString CastItoIShort(uint64 number);
CString CastSecondsToHM(sint32 seconds); //<<--9/21/02
CString LeadingZero(uint32 units);
void ShellOpenFile(CString name); //<<--9/21/02
CString GetResString(UINT uStringID);//<<--10/01/02
CString GetResString(UINT uStringID,WORD languageID);
inline char* nstrdup(const char* todup);
void MakeFoldername(char* path);

void URLDecode(wxString& result, const char* buff);// Make a malloc'd decoded strnig from an URL encoded string (with escaped spaces '%20' and  the like
wxString URLEncode(wxString sIn);
inline BYTE toHex(const BYTE &x);
wxString GetFiletypeByName(wxString infile);
bool Ask4RegFix(bool checkOnly, bool dontAsk = false); // Barry - Allow forced update without prompt
void BackupReg(void); // Barry - Store previous values
void RevertReg(void); // Barry - Restore previous values
int GetMaxConnections();
CString MakeStringEscaped(CString in);
//void RunURL(CAbstractFile* file,CString urlpattern);

WORD	DetectWinVersion();
//_int64	GetFreeDiskSpaceX(PCHAR pDirectory);
//For Rate File 
CString GetRateString(uint16 rate);
//void	UpdateURLMenu(CMenu &menu, int &counter);

// From Gnucleus project [found by Tarod]
CString EncodeBase32(const unsigned char* buffer, unsigned int bufLen);
CString EncodeBase16(const unsigned char* buffer, unsigned int bufLen);
int	DecodeLengthBase16(int base16Length);
void DecodeBase16(const char *base16Buffer, unsigned int base16BufLen, unsigned char *buffer);


// md4cmp -- replacement for memcmp(hash1,hash2,16)
// Like 'memcmp' this function returns 0, if hash1==hash2, and !0, if hash1!=hash2.
// NOTE: Do *NOT* use that function for determining if hash1<hash2 or hash1>hash2.
__inline int md4cmp(const void* hash1, const void* hash2) {
	return !(((uint32*)hash1)[0] == ((uint32*)hash2)[0] &&
		     ((uint32*)hash1)[1] == ((uint32*)hash2)[1] &&
		     ((uint32*)hash1)[2] == ((uint32*)hash2)[2] &&
		     ((uint32*)hash1)[3] == ((uint32*)hash2)[3]);
}

// md4clr -- replacement for memset(hash,0,16)
__inline void md4clr(const void* hash) {
	((uint32*)hash)[0] = ((uint32*)hash)[1] = ((uint32*)hash)[2] = ((uint32*)hash)[3] = 0;
}

// md4cpy -- replacement for memcpy(dst,src,16)
__inline void md4cpy(const void* dst, const void* src) {
	((uint32*)dst)[0] = ((uint32*)src)[0];
	((uint32*)dst)[1] = ((uint32*)src)[1];
	((uint32*)dst)[2] = ((uint32*)src)[2];
	((uint32*)dst)[3] = ((uint32*)src)[3];
}

#define	MAX_HASHSTR_SIZE (16*2+1)
CString md4str(const uchar* hash);
void md4str(const uchar* hash, char* pszHash);

__inline int CompareUnsigned(uint32 uSize1, uint32 uSize2)
{
	if (uSize1 < uSize2)
		return -1;
	if (uSize1 > uSize2)
		return 1;
	return 0;
}

#endif
