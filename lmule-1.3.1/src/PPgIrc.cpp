// PPgIRC.cpp : implementation file
//

//#include "stdafx.h"
#include "wintypes.h"
#include "emule.h"
#include "PPgIrc.h"


// CPPgIRC dialog

//IMPLEMENT_DYNAMIC(CPPgIRC, CPropertyPage)
CPPgIRC::CPPgIRC()
	: wxWindow(0,CPPgIRC::IDD)
{
}

CPPgIRC::~CPPgIRC()
{
}

#if 0
void CPPgIRC::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CPPgIRC, CPropertyPage)
	ON_BN_CLICKED(IDC_IRC_TIMESTAMP, OnBnClickedAddTimeStamp)
	ON_BN_CLICKED(IDC_IRC_USECHANFILTER, OnBnClickedUseFilter)
	ON_BN_CLICKED(IDC_IRC_USEPERFORM, OnBnClickedUsePerform)
	ON_BN_CLICKED(IDC_IRC_ACCEPTLINKS, OnBnClickedAcceptLinks)
	ON_EN_CHANGE(IDC_IRC_NICK_BOX, OnEnChangeNick)
	ON_EN_CHANGE(IDC_IRC_PERFORM_BOX, OnEnChangePerformString)
	ON_EN_CHANGE(IDC_IRC_SERVER_BOX, OnEnChangeServer)
	ON_EN_CHANGE(IDC_IRC_NAME_BOX, OnEnChangeName)
	ON_EN_CHANGE(IDC_IRC_MINUSER_BOX, OnEnChangeUser)
	ON_BN_CLICKED(IDC_IRC_LISTONCONNECT, OnBnClickedListOnConnect)
END_MESSAGE_MAP()


BOOL CPPgIRC::OnInitDialog()
{
	CPropertyPage::OnInitDialog();
	((CEdit*)GetDlgItem(IDC_IRC_NICK_BOX))->SetLimitText(20);
	((CEdit*)GetDlgItem(IDC_IRC_MINUSER_BOX))->SetLimitText(6);
	((CEdit*)GetDlgItem(IDC_IRC_SERVER_BOX))->SetLimitText(40);
	((CEdit*)GetDlgItem(IDC_IRC_NAME_BOX))->SetLimitText(40);
	((CEdit*)GetDlgItem(IDC_IRC_PERFORM_BOX))->SetLimitText(250);
	LoadSettings();
	Localize();
	m_bnickModified = false;

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}
#endif

void CPPgIRC::LoadSettings(void)
{	//What am I doing wrong? why can't I use the app_prefs->prefs like the other tabs?
#if 0
	if(theApp.glob_prefs->GetIRCAddTimestamp())
		this->CheckDlgButton(IDC_IRC_TIMESTAMP,1);
	else
		this->CheckDlgButton(IDC_IRC_TIMESTAMP,0);
	if(theApp.glob_prefs->GetIrcAcceptLinks())
		this->CheckDlgButton(IDC_IRC_ACCEPTLINKS,1);
	else
		this->CheckDlgButton(IDC_IRC_ACCEPTLINKS,0);
	if(theApp.glob_prefs->GetIRCUseChanFilter())
		this->CheckDlgButton(IDC_IRC_USECHANFILTER,1);
	else
		this->CheckDlgButton(IDC_IRC_USECHANFILTER,0);
	if(theApp.glob_prefs->GetIrcUsePerform())
		this->CheckDlgButton(IDC_IRC_USEPERFORM,1);
	else
		this->CheckDlgButton(IDC_IRC_USEPERFORM,0);
	if(theApp.glob_prefs->GetIRCListOnConnect())
		this->CheckDlgButton(IDC_IRC_LISTONCONNECT,1);
	else
		this->CheckDlgButton(IDC_IRC_LISTONCONNECT,0);
	GetDlgItem(IDC_IRC_SERVER_BOX)->SetWindowText(theApp.glob_prefs->GetIRCServer());
	GetDlgItem(IDC_IRC_NICK_BOX)->SetWindowText(theApp.glob_prefs->GetIRCNick());
	GetDlgItem(IDC_IRC_NAME_BOX)->SetWindowText(theApp.glob_prefs->GetIRCChanNameFilter());
	GetDlgItem(IDC_IRC_PERFORM_BOX)->SetWindowText(theApp.glob_prefs->GetIrcPerformString());
	CString strBuffer;
	strBuffer.Format("%d", theApp.glob_prefs->GetIRCChannelUserFilter());
	GetDlgItem(IDC_IRC_MINUSER_BOX)->SetWindowText(strBuffer);
#endif
}


#if 0
BOOL CPPgIRC::OnApply()
{   //What am I doing wrong? why can't I use the app_prefs->prefs like the other tabs?
	if(IsDlgButtonChecked(IDC_IRC_TIMESTAMP))
		theApp.glob_prefs->SetIRCAddTimestamp( true );
	else
		theApp.glob_prefs->SetIRCAddTimestamp( false );
	if(IsDlgButtonChecked(IDC_IRC_ACCEPTLINKS))
		theApp.glob_prefs->SetIrcAcceptLInks( true );
	else
		theApp.glob_prefs->SetIrcAcceptLInks( false );
	if(IsDlgButtonChecked(IDC_IRC_LISTONCONNECT))
		theApp.glob_prefs->SetIRCListonConnect( true );
	else
		theApp.glob_prefs->SetIRCListonConnect( false );
	if(IsDlgButtonChecked(IDC_IRC_USECHANFILTER))
		theApp.glob_prefs->SetIRCUseChanFilter( true );
	else
		theApp.glob_prefs->SetIRCUseChanFilter( false );
	if(IsDlgButtonChecked(IDC_IRC_USEPERFORM))
		theApp.glob_prefs->SetIrcUsePerform( true );
	else
		theApp.glob_prefs->SetIrcUsePerform( false );
	char buffer[510];
	if(GetDlgItem(IDC_IRC_NICK_BOX)->GetWindowTextLength())
	{
		GetDlgItem(IDC_IRC_NICK_BOX)->GetWindowText(buffer,20);
		theApp.glob_prefs->SetIRCNick( buffer );
		if( theApp.emuledlg->ircwnd.GetLoggedIn() && m_bnickModified == true){
			m_bnickModified = false;
			theApp.emuledlg->ircwnd.SendString( (CString)"NICK " + (CString)buffer );
		}
	}

	if(GetDlgItem(IDC_IRC_SERVER_BOX)->GetWindowTextLength())
	{
		GetDlgItem(IDC_IRC_SERVER_BOX)->GetWindowText(buffer,40);
		theApp.glob_prefs->SetIRCServer( buffer );
	}

	if(GetDlgItem(IDC_IRC_NAME_BOX)->GetWindowTextLength())
	{
		GetDlgItem(IDC_IRC_NAME_BOX)->GetWindowText(buffer,40);
		theApp.glob_prefs->SetIRCChanNameFilter( buffer );
	}

	if(GetDlgItem(IDC_IRC_PERFORM_BOX)->GetWindowTextLength())
	{
		GetDlgItem(IDC_IRC_PERFORM_BOX)->GetWindowText(buffer,250);
		theApp.glob_prefs->SetIRCPerformString( buffer );
	}
	else
	{
		theApp.glob_prefs->SetIRCPerformString( " " );
	}

	if(GetDlgItem(IDC_IRC_MINUSER_BOX)->GetWindowTextLength())
	{
		GetDlgItem(IDC_IRC_MINUSER_BOX)->GetWindowText(buffer,6);
		theApp.glob_prefs->SetIRCChanUserFilter( atoi(buffer) );
	}
	else{
		theApp.glob_prefs->SetIRCChanUserFilter( 0 );
	}

	SetModified(FALSE);
	return CPropertyPage::OnApply();
}
#endif

void CPPgIRC::Localize(void)
{
#if 0
	if(m_hWnd)
	{
		GetDlgItem(IDC_IRC_SERVER_FRM)->SetWindowText(GetResString(IDS_PW_SERVER));
		GetDlgItem(IDC_IRC_MISC_FRM)->SetWindowText(GetResString(IDS_PW_MISC));
		GetDlgItem(IDC_IRC_TIMESTAMP)->SetWindowText(GetResString(IDS_IRC_ADDTIMESTAMP));
		GetDlgItem(IDC_IRC_NICK_FRM)->SetWindowText(GetResString(IDS_PW_NICK));
		GetDlgItem(IDC_IRC_NAME_TEXT)->SetWindowText(GetResString(IDS_IRC_NAME));
		GetDlgItem(IDC_IRC_MINUSER_TEXT)->SetWindowText(GetResString(IDS_UUSERS));
		GetDlgItem(IDC_IRC_FILTER_FRM)->SetWindowText(GetResString(IDS_IRC_CHANNELLIST));
		GetDlgItem(IDC_IRC_USECHANFILTER)->SetWindowText(GetResString(IDS_IRC_USEFILTER));
		GetDlgItem(IDC_IRC_PERFORM_FRM)->SetWindowText(GetResString(IDS_IRC_PERFORM));
		GetDlgItem(IDC_IRC_USEPERFORM)->SetWindowText(GetResString(IDS_IRC_USEPERFORM));
		GetDlgItem(IDC_IRC_LISTONCONNECT)->SetWindowText(GetResString(IDS_IRC_LOADCHANNELLISTONCON));
		GetDlgItem(IDC_IRC_ACCEPTLINKS)->SetWindowText(GetResString(IDS_IRC_ACCEPTLINKS));
	}
#endif
}
