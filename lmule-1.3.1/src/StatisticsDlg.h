//this file is part of eMule
//Copyright (C)2002 Merkur ( merkur-@users.sourceforge.net / http://www.emule-project.net )
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#ifndef STAT_DLG_H
#define STAT_DLG_H

//#include "afxdlgs.h"
//#include "ResizableLib/ResizableDialog.h"
#include "OScopeCtrl.h"
#include "otherfunctions.h"
//#include "ColorFrameCtrl.h"
#include <wx/thread.h>
#include <list>
using namespace std;

// CStatisticsDlg dialog

#include <wx/treectrl.h>
#define HTREEITEM wxTreeItemId

class CStatisticsDlg : public wxPanel// CResizableDialog
{
  //DECLARE_DYNAMIC(CStatisticsDlg)
public:
	CStatisticsDlg(wxWindow* pParent = NULL);   // standard constructor
	~CStatisticsDlg();
	enum { IDD = IDD_STATISTICS };

	void Localize();
	void SetCurrentRate(float uploadrate, float downloadrate);
	void ShowInterval();
	void ShowStatistics();
	void SetARange(bool SetDownload,int maxValue);
	void RecordRate();
	float GetAvgDownloadRate(int averageType);
	float GetAvgUploadRate(int averageType);
	void RepaintMeters();
	float GetMaxConperFiveModifier();
	void Init();

	// export can't be done directly in the webserver's thread
	// so it is splitted in two parts	
	wxString ExportHTML();
 private:
	void ExportHTMLEvent(wxEvent& evt);
	wxString exportString;
	wxMutex exportMutex;
	wxCondition* exportCondition;
	wxCriticalSection exportCrit;

private:
    COScopeCtrl* m_DownloadOMeter,*m_UploadOMeter,*m_Statistics;
    //CColorFrameCtrl m_Led1[ 3 ];
    //CColorFrameCtrl m_Led2[ 3 ];
    //CColorFrameCtrl m_Led3[ 4 ];

	wxTreeCtrl* stattree;

	int dl_users,dl_active;
	double m_dPlotDataMore[4];
	int stat_max;
	float maxDownavg;
	float maxDown;
	uint32 m_ilastMaxConnReached;

	// tree-folders
	HTREEITEM h_shared,h_transfer,h_connection,h_clients,h_servers,h_upload,h_download;
	HTREEITEM down1,down2,down3,down4,down5,down6,down7;
	HTREEITEM up1,up2,up3,up4,up5,up6,up7,up8,up9,up10;
	HTREEITEM tran0;
	HTREEITEM con1,con2,con3,con4,con5,con6,con7,con8,con9,con10,con11,con12,con13;
	HTREEITEM shar1,shar2,shar3;
	HTREEITEM cli1,cli2,cli3,cli4,cli5,cli6,cli7,cli8,cli9;
	HTREEITEM cli_versions[12];
	HTREEITEM srv1,srv2,srv3,srv4,srv5,srv6,srv7,srv8,srv9;

	void SetupLegend( int ResIdx, int ElmtIdx, int legendNr);
	void SetStatsRanges(int min, int max);
 public:
	void	UpdateConnectionsStatus();

	const static int AVG_SESSION =0;
	const static int AVG_TIME =1;
	list<uint64> uprateHistory;
	list<uint64> downrateHistory;
    uint32 peakconnections;
	uint32 totalconnectionchecks;
	float averageconnections;
	uint32 activeconnections;
protected:
	DECLARE_EVENT_TABLE()
	//virtual void OnSize(UINT nType,int cx,int cy);
	//virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support	
	//DECLARE_MESSAGE_MAP()
	//virtual BOOL OnInitDialog(); 
	//void	OnShowWindow(BOOL bShow,UINT nStatus);
};
#endif
