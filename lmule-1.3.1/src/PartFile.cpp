//Copyright (C)2002 Merkur ( merkur-@users.sourceforge.net / http://www.emule-project.net )
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif // HAVE_CONFIG_H

// Test if we have _GNU_SOURCE before the next step will mess up 
// setting __USE_GNU 
// (only needed for gcc-2.95 compatibility, gcc 3.2 always defines it)
#include "wx/setup.h"

// Mario Sergio Fujikawa Ferreira <lioux@FreeBSD.org>
// to detect if this is a *BSD system
#if defined(HAVE_SYS_PARAM_H)
#include <sys/param.h>
#endif

#include "PartFile.h"
#include "emule.h"
#include "updownclient.h"
#include <math.h>
#include "ED2KLink.h"
#include "Preview.h"
#include <wx/filename.h>
#include "ini2.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define PROGRESS_HEIGHT 3

//#define nstrdup strdup
extern char* nstrdup(char*);

void dump16f(FILE*f, uchar* d16)
{
  int i;
  for(i=0;i<16;i++) {
    fprintf(f,"%02X",*d16++);
  }
}

void dump16(uchar* d16)
{
  int i;
  for(i=0;i<16;i++) {
    printf("%02X",*d16++);
  }
}

extern void dump16(uchar*);

CBarShader CPartFile::s_LoadBar(PROGRESS_HEIGHT); 
CBarShader CPartFile::s_ChunkBar(16); 

CPartFile::CPartFile(){
	Init();
}

CPartFile::CPartFile(CSearchFile* searchresult){
	Init();
	memcpy(m_abyFileHash, searchresult->GetFileHash(), 16);
	for (int i = 0; i != searchresult->taglist.GetCount();i++){
		switch (searchresult->taglist[i]->tag->specialtag){
			case FT_FILENAME:{
				m_pszFileName = nstrdup(searchresult->taglist[i]->tag->stringvalue);
				break;
			}
			case FT_FILESIZE:{
				m_nFileSize = searchresult->taglist[i]->tag->intvalue;
				break;
			}
			default:
				CTag* newtag = new CTag(searchresult->taglist[i]->tag);
				taglist.Add(newtag);
		}
	}
	CreatePartFile();
}

CPartFile::CPartFile(CString edonkeylink)
{
	CED2KLink* pLink = 0;
	try {
		pLink = CED2KLink::CreateLinkFromUrl(edonkeylink);
		CED2KFileLink* pFileLink = pLink->GetFileLink();
		if (pFileLink==0) 
			throw GetResString(IDS_ERR_NOTAFILELINK);
		InitializeFromLink(pFileLink);
	} catch (CString error) {
		char buffer[200];
		sprintf(buffer,GetResString(IDS_ERR_INVALIDLINK),error.GetBuffer());
		theApp.emuledlg->AddLogLine(true, GetResString(IDS_ERR_LINKERROR), buffer);
		status = PS_ERROR;
	}
	delete pLink;
}

void
CPartFile::InitializeFromLink(CED2KFileLink* fileLink)
{
	Init();
	try{
		m_pszFileName = nstrdup( fileLink->GetName() );
		m_nFileSize = fileLink->GetSize();
		memcpy(m_abyFileHash, fileLink->GetHashKey(), sizeof(m_abyFileHash));
		if (!theApp.downloadqueue->IsFileExisting(m_abyFileHash))
			CreatePartFile();
		else
			status = PS_ERROR;
		}
	catch(CString error){
		char buffer[200];
		sprintf(buffer,GetResString(IDS_ERR_INVALIDLINK),error.GetBuffer());
		theApp.emuledlg->AddLogLine(true, GetResString(IDS_ERR_LINKERROR), buffer);
		status = PS_ERROR;
	}
}

CPartFile::CPartFile(CED2KFileLink* fileLink)
{
	InitializeFromLink(fileLink);
}

void CPartFile::Init(){
	fullname = 0;
	newdate = true;
	lastsearchtime = 0;
	lastpurgetime = ::GetTickCount();
	paused = false;
	status = PS_EMPTY;
	transfered = 0;
	priority = PR_NORMAL;
	srcarevisible = false;
	transferingsrc = 0;
	datarate = 0;
	hashsetneeded = true;
	count = 0;
	percentcompleted = 0;
	partmetfilename = 0;
	completedsize=0;
	m_bPreviewing = false;
	lastseencomplete = 0;//NULL;
	availablePartsCount=0;
	m_ClientSrcAnswered = 0;
	m_LastNoNeededCheck = 0;
	m_bAutoPriority = false ; 
	m_iRate = 0;
	m_strComment = "";
	m_nTotalBufferData = 0;
	m_nLastBufferFlushTime = 0;
	m_bPercentUpdated = false;
	m_bRecoveringArchive = false;
	m_iGainDueToCompression = 0;
	m_iLostDueToCorruption = 0;
	m_iTotalPacketsSavedDueToICH = 0;
	m_nSavedReduceDownload = 0;
	hasRating = false;
	hasComment = false;
}

CPartFile::~CPartFile(){
	// Barry - Ensure all buffered data is written
	FlushBuffer();
	//m_hpartfile.Flush();
	//SavePartFile();
	//m_hpartfile.Close();

	if (fullname)
		delete[] fullname;
	if (partmetfilename)
		delete[] partmetfilename;
	m_SrcpartFrequency.RemoveAll();
	for (POSITION pos = gaplist.GetHeadPosition();pos != 0;gaplist.GetNext(pos))
		delete gaplist.GetAt(pos);
}

// *BSD compatibility
#if (defined(BSD) && (BSD >= 199103))
#define MAX_PATH MAXPATHLEN
#else
#define MAX_PATH 1024
#endif

void CPartFile::CreatePartFile(){
	// use lowest free partfilenumber for free file (InterCeptor)
	int i = 0; 
	CString filename; 	
	do 
	{ 
		i++; 
		filename.Format("%s/%03i.part", theApp.glob_prefs->GetTempDir(), i); 
	
	}while(wxFileName::FileExists(filename)); 
	partmetfilename = new char[15]; 
	sprintf(partmetfilename,"%03i.part.met",i); 

	fullname = new char[strlen(theApp.glob_prefs->GetTempDir())+strlen(partmetfilename)+MAX_PATH];
	sprintf(fullname,"%s/%s",theApp.glob_prefs->GetTempDir(),partmetfilename);
	char* buffer = nstrdup(partmetfilename);
	buffer[strlen(buffer)-4] = 0;
	CTag* partnametag = new CTag(FT_PARTFILENAME,buffer);
	delete[] buffer;
	taglist.Add(partnametag);
	
	Gap_Struct* gap = new Gap_Struct;
	gap->start = 0;
	gap->end = m_nFileSize-1;
	gaplist.AddTail(gap);

	char* partfull = nstrdup(fullname);
	partfull[strlen(partfull)-4] = 0;
	
	
	if (!m_hpartfile.Create(partfull,TRUE)) {
		theApp.emuledlg->AddLogLine(false,GetResString(IDS_ERR_CREATEPARTFILE));
		status = PS_ERROR;
	}
	// jesh.. luotu. nyt se vaan pit�� avata uudestaan read-writeen..
	m_hpartfile.Close();
	if(!m_hpartfile.Open(partfull,CFile::read_write)) {
		theApp.emuledlg->AddLogLine(false,GetResString(IDS_ERR_CREATEPARTFILE));
		status = PS_ERROR;
	}
	delete[] partfull;
	m_SrcpartFrequency.SetSize(GetPartCount());
	for (uint32 i = 0; i != GetPartCount();i++)
		m_SrcpartFrequency.Add(0);
	paused = false;
	//UAP Hunter
	if (theApp.glob_prefs->GetNewAutoUp())
		((CKnownFile*)this)->SetAutoPriority(true);
	else
		((CKnownFile*)this)->SetAutoPriority(false);

	((CKnownFile*)this)->UpdateUploadAutoPriority();
	//end UAP
	SavePartFile();
}

bool CPartFile::LoadPartFile(char* in_directory,char* in_filename){
	CMap<uint16, uint16, Gap_Struct*, Gap_Struct*> gap_map; // Slugfiller
	transfered = 0;
	partmetfilename = nstrdup(in_filename);
	directory = nstrdup(in_directory);
	char* buffer = new char[strlen(directory)+strlen(partmetfilename)+2];
	sprintf(buffer,"%s/%s",directory,partmetfilename);
	fullname = buffer;
	CSafeFile file;
	/*try*/{
		// readfile data form part.met file
		if (!file.Open(fullname,CFile::read)){
			theApp.emuledlg->AddLogLine(false,GetResString(IDS_ERR_OPENMET),partmetfilename,m_pszFileName);
			return false;
		}
		uint8 version;
		file.Read(&version,1);
		if (version != PARTFILE_VERSION){
			file.Close();
			theApp.emuledlg->AddLogLine(false,GetResString(IDS_ERR_BADMETVERSION),partmetfilename,m_pszFileName);
			return false;
		}
		LoadDateFromFile(&file);
		LoadHashsetFromFile(&file,false);

		uint32 tagcount;
		file.Read(&tagcount,4);
		for (uint32 j = 0; j != tagcount;j++){
			CTag* newtag = new CTag(&file);
			switch(newtag->tag->specialtag){
				case FT_FILENAME:{
					if(newtag->tag->stringvalue == NULL) {
						theApp.emuledlg->AddLogLine(true,GetResString(IDS_ERR_METCORRUPT),partmetfilename,m_pszFileName);
						delete newtag;
						return false;
					}
					m_pszFileName = nstrdup(newtag->tag->stringvalue);
					delete newtag;
					break;
				}
				case FT_LASTSEENCOMPLETE: {
					lastseencomplete = newtag->tag->intvalue;
					delete newtag;
					break;
				}
				case FT_FILESIZE:{
					m_nFileSize = newtag->tag->intvalue;
					delete newtag;
					break;
				}
				case FT_TRANSFERED:{
					transfered = newtag->tag->intvalue;
					delete newtag;
					break;
				}
				case FT_PRIORITY:{
					priority = newtag->tag->intvalue;
					if ((priority==PR_AUTO)&&(!theApp.glob_prefs->GetNewAutoUp())) priority=PR_NORMAL;
					delete newtag;
					if (priority == PR_AUTO)
						this->SetAutoPriority(true);
					else
						this->SetAutoPriority(false);
					break;
				}
				case FT_STATUS:{
					paused = newtag->tag->intvalue;
					delete newtag;
					break;
				}
				case FT_ULPRIORITY:{ 
					if ((priority==PR_AUTO)&&(!theApp.glob_prefs->GetNewAutoUp())) priority=PR_NORMAL;
					if ((uint8)newtag->tag->intvalue == PR_AUTO) {
						((CKnownFile*)this)->SetAutoPriority(true);
						((CKnownFile*)this)->UpdateUploadAutoPriority();
					} else {
						((CKnownFile*)this)->SetAutoPriority(false);
						((CKnownFile*)this)->SetPriority((uint8)newtag->tag->intvalue);
					}
					delete newtag;
					break; 
				}
				default:{
					// Start Changes by Slugfiller for better exception handling
					if ((!newtag->tag->specialtag) &&
						(newtag->tag->tagname[0] == FT_GAPSTART ||
							newtag->tag->tagname[0] == FT_GAPEND)){
						Gap_Struct* gap;
						uint16 gapkey = atoi(&newtag->tag->tagname[1]);
						if (!gap_map.Lookup(gapkey, gap))
						{
							gap = new Gap_Struct;
							gap_map.SetAt(gapkey, gap);
							gap->start = (uint32)-1;
							gap->end = (uint32)-1;
						}
						if (newtag->tag->tagname[0] == FT_GAPSTART)
							gap->start = newtag->tag->intvalue;
						if (newtag->tag->tagname[0] == FT_GAPEND)
							gap->end = newtag->tag->intvalue-1;
						delete newtag;
					// End Changes by Slugfiller for better exception handling
					}
					else
						taglist.Add(newtag);
				}
					
			}

		}
		file.Close();
	}
#if 0
	catch(CFileException* error){
		if (error->m_cause == CFileException::endOfFile)
			theApp.emuledlg->AddLogLine(true,GetResString(IDS_ERR_METCORRUPT),partmetfilename,filename);
		else{
			char buffer[150];
			error->GetErrorMessage(buffer,150);
			theApp.emuledlg->AddLogLine(true,GetResString(IDS_ERR_FILEERROR),partmetfilename,filename,error);
		}
		error->Delete();	//memleak fix
		return false;
	}
#endif

	// Now to flush the map into the list (Slugfiller)
	for (POSITION pos = gap_map.GetStartPosition(); pos != NULL; ){
		Gap_Struct* gap;
		uint16 gapkey;
		gap_map.GetNextAssoc(pos, gapkey, gap);
		if (gap->start >= 0 && gap->end >=0 && gap->start <= gap->end)
			gaplist.AddTail(gap); // All tags accounted for
		else
			delete gap; // Some of the tags were missing
	}

	//check if this is a backup
	if(strcasecmp(strrchr(fullname, '.'), ".backup") == 0) {
		char *shorten = strrchr(fullname, '.');
		*shorten = 0;
		fullname = (char*)realloc(fullname, strlen(fullname) + 1);
	}

	// open permanent handle
	char* searchpath = nstrdup(fullname);
	searchpath[strlen(fullname)-4] = 0;
	if (!m_hpartfile.Open(searchpath,/*wxFile::write_append*/CFile::read_write)){
		theApp.emuledlg->AddLogLine(false,GetResString(IDS_ERR_FILEOPEN),fullname,m_pszFileName);
		delete[] searchpath;
		return false;
	}
			

	m_SrcpartFrequency.SetSize(GetPartCount());
	for (uint32 i = 0; i != GetPartCount();i++)
		m_SrcpartFrequency.Add(0);
	status = PS_READY;
	// check hashcount, filesatus etc
	if (hashlist.GetCount() < GetPartCount()){
		status = PS_EMPTY;
		hashsetneeded = true;
		return true;
	}
	else {
		hashsetneeded = false;
		bool empty = true;
		for (int i = 0; i != hashlist.GetSize(); i++){
			if (IsComplete(i*PARTSIZE,((i+1)*PARTSIZE)-1))
				empty = false;
		}
		if (empty)
			status = PS_EMPTY;
	}

	if (gaplist.IsEmpty()){	// is this file complete already?
		CompleteFile(false);
		return true;
	}

	// check date of .part file - if its wrong, rehash file
	//CFileStatus filestatus;
	//m_hpartfile.GetStatus(filestatus);
		struct stat statbuf;
		fstat(m_hpartfile.fd(),&statbuf);
		if ((time_t)date != (time_t)statbuf.st_mtime) {
			theApp.emuledlg->AddLogLine(false,GetResString(IDS_ERR_REHASH),buffer,m_pszFileName);
			// rehash
			status = PS_WAITINGFORHASH;
			CAddFileThread* addfilethread=new CAddFileThread();
			addfilethread->Create();
			addfilethread->SetValues(0,directory,searchpath,this);
			//addfilethread->ResumeThread();
			addfilethread->Run();

			//CAddFileThread* addfilethread = (CAddFileThread*) AfxBeginThread(RUNTIME_CLASS(CAddFileThread), THREAD_PRIORITY_NORMAL,0, CREATE_SUSPENDED);
			//addfilethread->SetValues(0,directory,m_hpartfile.GetFileName().GetBuffer(),this);
			//addfilethread->ResumeThread();	
		}
	delete[] searchpath;

	UpdateCompletedInfos();
	if ( completedsize > transfered )
		m_iGainDueToCompression = completedsize - transfered;
	else if ( completedsize != transfered )
		m_iLostDueToCorruption = transfered - completedsize;
	return true;

	return true;
}

bool CPartFile::SavePartFile(){
	switch (status){
		case PS_WAITINGFORHASH:
		case PS_HASHING:
			return false;
	}
	FILE* file = 0;
	CString BAKName(fullname); // quick hack to make a perm backup of the met file, should probably be changed...
	BAKName.Append(".BAK");
	try{
		//get filedate
		//CFileFind ff;
		char* searchpath = nstrdup(fullname);
		searchpath[strlen(fullname)-4] = 0;
		//bool end = !ff.FindFile(searchpath,0);
		wxString fName=::wxFindFirstFile(searchpath,wxFILE);
		delete[] searchpath;
		//if (!end)
		//		ff.FindNextFile();
		//if (end || ff.IsDirectory())
		//	throw GetResString(IDS_ERR_PART_FNF);
		if(fName.IsEmpty()) {
		  if (file)
		    fclose(file);
		  theApp.emuledlg->AddLogLine(false,GetResString(IDS_ERR_SAVEMET),GetResString(IDS_ERR_PART_FNF).GetData(),partmetfilename,m_pszFileName);
		  return false;
		}
		//CTime lwtime;
		//ff.GetLastWriteTime(lwtime);
		//date = mktime(lwtime.GetLocalTm());
		struct stat sbf;
		stat(fName.GetData(),&sbf);
		date=sbf.st_mtime;
		//ff.Close();
		uint32 lsc = lastseencomplete; //mktime(lastseencomplete.GetLocalTm());

		CString backupName(fullname);
		backupName.Append(".backup");

		remove(backupName);
		rename(fullname, backupName);

		// readfile data form part.met file
		file = fopen(fullname,"wbS");
		if (!file)
			throw GetResString(IDS_ERR_OPENMETFILE);
		//version
		uint8 version = PARTFILE_VERSION;
		fwrite(&version,1,1,file);
		//date
		fwrite(&date,4,1,file);
		//hash
		fwrite(&m_abyFileHash,16,1,file);
		uint16 parts = hashlist.GetCount();
		fwrite(&parts,2,1,file);
		for (int x = 0; x != parts; x++)
			fwrite(hashlist[x],16,1,file);
		//tags
		uint32 tagcount = taglist.GetCount()+7+(gaplist.GetCount()*2);
		fwrite(&tagcount,4,1,file);
		CTag* nametag = new CTag(FT_FILENAME,m_pszFileName);
		nametag->WriteTagToFile(file);
		delete nametag;
		CTag* sizetag = new CTag(FT_FILESIZE,m_nFileSize);
		sizetag->WriteTagToFile(file);
		delete sizetag;
		CTag* transtag = new CTag(FT_TRANSFERED,transfered);
		transtag->WriteTagToFile(file);
		delete transtag;
		CTag* statustag = new CTag(FT_STATUS,(paused)? 1:0);
		statustag->WriteTagToFile(file);
		delete statustag;
		//UAP Hunter
		CTag* prioritytag ;
		if (IsAutoPrioritized()) {
			uint8 aux_priority = PR_AUTO ;
			prioritytag = new CTag(FT_PRIORITY,aux_priority);
		}
		else
			prioritytag = new CTag(FT_PRIORITY,priority);
		prioritytag->WriteTagToFile(file);
		delete prioritytag;
		//end UAP
		CTag* lsctag = new CTag(FT_LASTSEENCOMPLETE,lsc);
		lsctag->WriteTagToFile(file);
		delete lsctag;

		// Modified by Tarod for UAP
		CTag* ulprioritytag;
		if (((CKnownFile*)this)->IsAutoPrioritized())
			ulprioritytag = new CTag(FT_ULPRIORITY, PR_AUTO);
		else
			ulprioritytag = new CTag(FT_ULPRIORITY,m_iPriority);
		ulprioritytag->WriteTagToFile(file);
		delete ulprioritytag;
		// End by Tarod
		for (uint32 j = 0; j != (uint32)taglist.GetCount();j++)
			taglist[j]->WriteTagToFile(file);
		//gaps
		char* namebuffer = new char[10];
		char* number = &namebuffer[1];
		uint16 i_pos = 0;
		for (POSITION pos = gaplist.GetHeadPosition();pos != 0;gaplist.GetNext(pos)){
		  //itoa(i_pos,number,10);
		  sprintf(number,"%d",i_pos);
			namebuffer[0] = FT_GAPSTART;
			CTag* gapstarttag = new CTag(namebuffer,gaplist.GetAt(pos)->start);
			gapstarttag->WriteTagToFile(file);
			// gap start = first missing byte but gap ends = first non-missing byte in edonkey
			// but I think its easier to user the real limits
			namebuffer[0] = FT_GAPEND;
			CTag* gapendtag = new CTag(namebuffer,(gaplist.GetAt(pos)->end)+1);
			gapendtag->WriteTagToFile(file);
			delete gapstarttag;
			delete gapendtag;
			i_pos++;
		}
		delete[] namebuffer;

		if (ferror(file))
			throw CString("unexpected write error");

		remove(backupName);
	}
	catch(char* error){
		if (file)
			fclose(file);
		theApp.emuledlg->AddLogLine(false,GetResString(IDS_ERR_SAVEMET),error,partmetfilename,m_pszFileName);
		return false;
	}
	catch(CString error){
		if (file)
			fclose(file);
		theApp.emuledlg->AddLogLine(false,GetResString(IDS_ERR_SAVEMET),error.GetBuffer(),partmetfilename,m_pszFileName);
		return false;
	}
	fclose(file);
	{
	  char syscmd[512];
	  sprintf(syscmd,"cp \"%s\" \"%s\"",fullname,BAKName.GetData());
	  system(syscmd);
	}
	return true;
}

void CPartFile::PartFileHashFinished(CKnownFile* result){
	newdate = true;
	bool errorfound = false;
	for (uint32 i = 0; i != (uint32)hashlist.GetSize(); i++){
		if (IsComplete(i*PARTSIZE,((i+1)*PARTSIZE)-1)){
			if (!(result->GetPartHash(i) && !memcmp(result->GetPartHash(i),this->GetPartHash(i),16))){
				theApp.emuledlg->AddLogLine(false,GetResString(IDS_ERR_FOUNDCORRUPTION),i+1,m_pszFileName);		
				AddGap(i*PARTSIZE,((((i+1)*PARTSIZE)-1) >= m_nFileSize) ? m_nFileSize-1 : ((i+1)*PARTSIZE)-1);
				errorfound = true;
			}
		}
	}
	delete result;
	if (!errorfound){
		if (status == PS_COMPLETING){
			CompleteFile(true);
			return;
		}
		else
			theApp.emuledlg->AddLogLine(false,GetResString(IDS_HASHINGDONE),m_pszFileName);
	}
	else{
		status = PS_READY;
		SavePartFile();
		return;
	}
	status = PS_READY;
	SavePartFile();
	theApp.sharedfiles->SafeAddKFile(this);
}

void CPartFile::AddGap(uint32 start, uint32 end){
	POSITION pos1, pos2;
	for (pos1 = gaplist.GetHeadPosition();(pos2 = pos1) != NULL;){
		Gap_Struct* cur_gap = gaplist.GetNext(pos1);
		if (cur_gap->start >= start && cur_gap->end <= end){ // this gap is inside the new gap - delete
			gaplist.RemoveAt(pos2);
			delete cur_gap;
			continue;
		}
		else if (cur_gap->start >= start && cur_gap->start <= end){// a part of this gap is in the new gap - extend limit and delete
			end = cur_gap->end;
			gaplist.RemoveAt(pos2);
			delete cur_gap;
			continue;
		}
		else if (cur_gap->end <= end && cur_gap->end >= start){// a part of this gap is in the new gap - extend limit and delete
			start = cur_gap->start;
			gaplist.RemoveAt(pos2);
			delete cur_gap;
			continue;
		}
		else if (start >= cur_gap->start && end <= cur_gap->end){// new gap is already inside this gap - return
			return;
		}
	}
	Gap_Struct* new_gap = new Gap_Struct;
	new_gap->start = start;
	new_gap->end = end;
	gaplist.AddTail(new_gap);
	//theApp.emuledlg->transferwnd.downloadlistctrl.UpdateItem(this);
	UpdateDisplayedInfo();
	newdate = true;
}

bool CPartFile::IsComplete(uint32 start, uint32 end){
	if (end >= m_nFileSize)
		end = m_nFileSize-1;
	for (POSITION pos = gaplist.GetHeadPosition();pos != 0;gaplist.GetNext(pos)){
		Gap_Struct* cur_gap = gaplist.GetAt(pos);
		if ((cur_gap->start >= start && cur_gap->end <= end)||(cur_gap->start >= start 
			&& cur_gap->start <= end)||(cur_gap->end <= end && cur_gap->end >= start)
			||(start >= cur_gap->start && end <= cur_gap->end)){
				return false;	
			}
	}
	return true;
}

bool CPartFile::IsPureGap(uint32 start, uint32 end){
	if (end >= m_nFileSize)
		end = m_nFileSize-1;
	for (POSITION pos = gaplist.GetHeadPosition();pos != 0;gaplist.GetNext(pos)){
		Gap_Struct* cur_gap = gaplist.GetAt(pos);
		if (start >= cur_gap->start  && end <= cur_gap->end ){
			return true;
		}
	}
	return false;
}

bool CPartFile::IsAlreadyRequested(uint32 start, uint32 end){
	for (POSITION pos =  requestedblocks_list.GetHeadPosition();pos != 0; requestedblocks_list.GetNext(pos)){
		Requested_Block_Struct* cur_block =  requestedblocks_list.GetAt(pos);
		if (cur_block->StartOffset == start && cur_block->EndOffset == end)
			return true;
	}
	return false;
}
/*
bool CPartFile::GetNextEmptyBlockInPart(uint16 partnumber,Requested_Block_Struct* result){
	for (uint32 i = 0; i != ceil((float)PARTSIZE/BLOCKSIZE);i++){
		uint32 start = (PARTSIZE*partnumber) + i*BLOCKSIZE;
		uint32 end	 = (PARTSIZE*partnumber) + ((i+1)*BLOCKSIZE)-1;
		if (end >= (uint32)PARTSIZE*(partnumber+1))
			end = (PARTSIZE*(partnumber+1))-1;
		if (start >= GetFileSize())
			break;
		if (end >= GetFileSize())
			end = GetFileSize()-1;
		if ( (!IsComplete(start,end)) && !IsAlreadyRequested(start,end)){
			if (result){
				result->StartOffset = start;
				result->EndOffset = end;
				memcpy(result->FileID,GetFileHash(),16);
			}
			return true;
		}
	}
	return false;
}
*/

bool CPartFile::GetNextEmptyBlockInPart(uint16 partNumber, Requested_Block_Struct *result)
{
	Gap_Struct *firstGap;
	Gap_Struct *currentGap;
	uint32 end;
	uint32 blockLimit;

	// Find start of this part
	uint32 partStart = (PARTSIZE * partNumber);
	uint32 start = partStart;

	// What is the end limit of this block, i.e. can't go outside part (or filesize)
	uint32 partEnd = (PARTSIZE * (partNumber + 1)) - 1;
	if (partEnd >= GetFileSize())
		partEnd = GetFileSize() - 1;

	// Loop until find a suitable gap and return true, or no more gaps and return false
	while (true)
	{
		firstGap = NULL;

		// Find the first gap from the start position
		for (POSITION pos = gaplist.GetHeadPosition(); pos != 0; gaplist.GetNext(pos))
		{
			currentGap = gaplist.GetAt(pos);
			// Want gaps that overlap start<->partEnd
			if ((currentGap->start <= partEnd) && (currentGap->end >= start))
			{
				// Is this the first gap?
				if ((firstGap == NULL) || (currentGap->start < firstGap->start))
					firstGap = currentGap;
			}
		}

		// If no gaps after start, exit
		if (firstGap == NULL)
			return false;

		// Update start position if gap starts after current pos
		if (start < firstGap->start)
			start = firstGap->start;

		// If this is not within part, exit
		if (start > partEnd)
			return false;

		// Find end, keeping within the max block size and the part limit
		end = firstGap->end;
		blockLimit = partStart + (BLOCKSIZE * (((start - partStart) / BLOCKSIZE) + 1)) - 1;
		if (end > blockLimit)
			end = blockLimit;
		if (end > partEnd)
			end = partEnd;
    
		// If this gap has not already been requested, we have found a valid entry
		if (!IsAlreadyRequested(start, end))
		{
			// Was this block to be returned
			if (result != NULL)
			{
				result->StartOffset = start;
				result->EndOffset = end;
				memcpy(result->FileID, GetFileHash(), 16);
				result->transferred = 0;
			}
			return true;
		}
		else
		{
			// Reposition to end of that gap
			start = end + 1;
		}

		// If tried all gaps then break out of the loop
		if (end == partEnd)
			break;
	}

	// No suitable gap found
	return false;
}

void CPartFile::FillGap(uint32 start, uint32 end){
	POSITION pos1, pos2;
	for (pos1 = gaplist.GetHeadPosition();(pos2 = pos1) != NULL;){
		Gap_Struct* cur_gap = gaplist.GetNext(pos1);
		if (cur_gap->start >= start && cur_gap->end <= end){ // our part fills this gap completly
			gaplist.RemoveAt(pos2);
			delete cur_gap;
			continue;
		}
		else if (cur_gap->start >= start && cur_gap->start <= end){// a part of this gap is in the part - set limit
			cur_gap->start = end+1;
		}
		else if (cur_gap->end <= end && cur_gap->end >= start){// a part of this gap is in the part - set limit
			cur_gap->end = start-1;
		}
		else if (start >= cur_gap->start && end <= cur_gap->end){
			uint32 buffer = cur_gap->end;
			cur_gap->end = start-1;
			cur_gap = new Gap_Struct;
			cur_gap->start = end+1;
			cur_gap->end = buffer;
			gaplist.InsertAfter(pos1,cur_gap);
			break; // [Lord KiRon]
		}
	}

	UpdateCompletedInfos();
	//theApp.emuledlg->transferwnd->downloadlistctrl->UpdateItem(this);
	UpdateDisplayedInfo();
	newdate = true;
	//SavePartFile();
}

void CPartFile::UpdateCompletedInfos()
{
   	uint32 allgaps = 0; 

	for (POSITION pos = gaplist.GetHeadPosition(); pos != 0; gaplist.GetNext(pos)){ 
		Gap_Struct* cur_gap = gaplist.GetAt(pos); 
		allgaps += cur_gap->end - cur_gap->start;
	}

	if (gaplist.GetCount() || requestedblocks_list.GetCount()){ 
		percentcompleted = (1.0f-(float)allgaps/m_nFileSize) * 100; 
		completedsize = m_nFileSize - allgaps - 1; 
	} 
	else{
		percentcompleted = 100;
		completedsize = m_nFileSize;
	}
}

#include <wx/gdicmn.h>
typedef DWORD COLORREF;
void CPartFile::DrawStatusBar(wxMemoryDC* dc, wxRect rect, bool bFlat){ 
	COLORREF crProgress;
	COLORREF crHave;
	COLORREF crPending;
	COLORREF crMissing = RGB(255, 0, 0);

	if(bFlat) {
		crProgress = RGB(0, 150, 0);
		crHave = RGB(0, 0, 0);
		crPending = RGB(255,255,100);
	} else {
		crProgress = RGB(0, 224, 0);
		crHave = RGB(104, 104, 104);
		crPending = RGB(255, 208, 0);
	}

	s_ChunkBar.SetHeight(rect.height - rect.y);
	s_ChunkBar.SetWidth(rect.width - rect.x); 
	s_ChunkBar.SetFileSize(m_nFileSize);
	s_ChunkBar.Fill(crHave);

	uint32 allgaps = 0;

	if(status == PS_COMPLETE || status == PS_COMPLETING) {
		s_ChunkBar.FillRange(0, m_nFileSize, crProgress);
		s_ChunkBar.Draw(dc, rect.x, rect.y, bFlat); 
		percentcompleted = 100; completedsize=m_nFileSize;
		return;
	}

	// red gaps
	for (POSITION pos = gaplist.GetHeadPosition();pos !=  0;gaplist.GetNext(pos)){
		Gap_Struct* cur_gap = gaplist.GetAt(pos);
		allgaps += cur_gap->end - cur_gap->start;
		bool gapdone = false;
		uint32 gapstart = cur_gap->start;
		uint32 gapend = cur_gap->end;
		for (uint32 i = 0; i != GetPartCount(); i++){
			if (gapstart >= i*PARTSIZE && gapstart <=  (i+1)*PARTSIZE){ // is in this part?
				if (gapend <= (i+1)*PARTSIZE)
					gapdone = true;
				else{
					gapend = (i+1)*PARTSIZE; // and next part
				}
				// paint
				COLORREF color;
				if (m_SrcpartFrequency.GetCount() >= (int)i && m_SrcpartFrequency[i])  // frequency?
					color = RGB(0,
					(210-(22*(m_SrcpartFrequency[i]-1)) <  0)? 0:210-(22*(m_SrcpartFrequency[i]-1))
					,255);
				else
					color = crMissing;

				s_ChunkBar.FillRange(gapstart, gapend + 1,  color);

				if (gapdone) // finished?
					break;
				else{
					gapstart = gapend;
					gapend = cur_gap->end;
				}
			}
		}
	}

	// yellow pending parts
	for (POSITION pos = requestedblocks_list.GetHeadPosition();pos !=  0;requestedblocks_list.GetNext(pos)){
		Requested_Block_Struct* block =  requestedblocks_list.GetAt(pos);
		s_ChunkBar.FillRange(block->StartOffset, block->EndOffset,  crPending);
	}

	s_ChunkBar.Draw(dc, rect.x, rect.y, bFlat); 

	// green progress
	float blockpixel = (float)(rect.width -  rect.x)/((float)m_nFileSize); 
	RECT gaprect;
	gaprect.top = rect.y; 
	gaprect.bottom = gaprect.top + 4; 
	gaprect.left = rect.x; //->left; 

	if(!bFlat) {
		s_LoadBar.SetWidth((uint32)((float)((float)((m_nFileSize-((allgaps==0)?1:allgaps) )-1))*blockpixel + .5f));
		s_LoadBar.Fill(crProgress);
		s_LoadBar.Draw(dc, gaprect.left, gaprect.top, false);
	} else {
		gaprect.right = rect.x+  (uint32)((float)((float)((m_nFileSize-((allgaps==0)?1:allgaps))-1))*blockpixel +  .5f);
		dc->SetBrush(*(wxTheBrushList->FindOrCreateBrush(wxColour(crProgress),wxSOLID))); //wxBrush(crProgress));
		dc->DrawRectangle(gaprect.left,gaprect.top,gaprect.right,gaprect.bottom);
		//dc->FillRect(&gaprect, &CBrush(crProgress));
		//draw gray progress only if flat
		gaprect.left = gaprect.right;
		gaprect.right = rect.width;
		dc->SetBrush(*(wxTheBrushList->FindOrCreateBrush(wxColour(224,224,224),wxSOLID))); //wxBrush(crPending));
		dc->DrawRectangle(gaprect.left,gaprect.top,gaprect.right,gaprect.bottom);
		//dc->FillRect(&gaprect, &CBrush(RGB(224,224,224)));
	}

	if (	   (gaplist.GetCount() || requestedblocks_list.GetCount())	   )
	{
		percentcompleted = ((1.0f-(float)allgaps/m_nFileSize)) * 100;
		completedsize = m_nFileSize - allgaps - 1;
	} else {percentcompleted = 100; completedsize=m_nFileSize;}

}

void CPartFile::WritePartStatus(CMemFile* file){
	uint16 parts = hashlist.GetCount();
	file->Write(&parts,2);
	uint16 done = 0;
	while (done != parts){
		uint8 towrite = 0;
		for (uint32 i = 0;i != 8;i++){
			if (IsComplete(done*PARTSIZE,((done+1)*PARTSIZE)-1))
				towrite |= (1<<i);
			done++;
			if (done == parts)
				break;
		}
		file->Write(&towrite,1);
	}
}

int CPartFile::GetValidSourcesCount() {
	int counter=0;
	POSITION pos1,pos2;
	for (int sl=0;sl<SOURCESSLOTS;sl++) if (!srclists[sl].IsEmpty())
	for (pos1 = srclists[sl].GetHeadPosition();( pos2 = pos1 ) != NULL;){
		srclists[sl].GetNext(pos1);
		CUpDownClient* cur_src = srclists[sl].GetAt(pos2);
		if (cur_src->GetDownloadState()!=DS_ONQUEUE && cur_src->GetDownloadState()!=DS_DOWNLOADING &&
			cur_src->GetDownloadState()!=DS_NONEEDEDPARTS) counter++;
	}
	return counter;
}



uint16 CPartFile::GetNotCurrentSourcesCount(){
		uint16 counter=0;

		POSITION pos1,pos2;
		for (int sl=0;sl<SOURCESSLOTS;sl++) if (!srclists[sl].IsEmpty())
		for (pos1 = srclists[sl].GetHeadPosition();( pos2 = pos1 ) != NULL;){
			srclists[sl].GetNext(pos1);
			CUpDownClient* cur_src = srclists[sl].GetAt(pos2);
			if (cur_src->GetDownloadState()!=DS_ONQUEUE && cur_src->GetDownloadState()!=DS_DOWNLOADING) counter++;
		}
		return counter;
}

uint8 CPartFile::GetStatus(bool ignorepause){
	if ((!paused) || status == PS_ERROR || ignorepause)
		return status;
	else
		return PS_PAUSED;
}

uint32 CPartFile::Process(uint32 reducedownload/*in percent*/)
{
	DWORD dwCurTick = ::GetTickCount();

	// If buffer size exceeds limit, or if not written within time limit, flush data
	if ((m_nTotalBufferData > theApp.glob_prefs->GetFileBufferSize()) /*|| (dwCurTick > (m_nLastBufferFlushTime + BUFFER_TIME_LIMIT))*/)
	{
		// Avoid flushing while copying preview file
		if (!m_bPreviewing)
			FlushBuffer();
	}

	// check if we want new sources from server
	//uint16 test = theApp.glob_prefs->GetMaxSourcePerFileSoft();
	if (( (!lastsearchtime) || (dwCurTick - lastsearchtime) > SERVERREASKTIME) && theApp.serverconnect->IsConnected()
		&& theApp.glob_prefs->GetMaxSourcePerFileSoft() > GetSourceCount() )
	{
		//local server
		lastsearchtime = dwCurTick;
		Packet* packet = new Packet(OP_GETSOURCES,16);
		memcpy(packet->pBuffer,m_abyFileHash,16);
		theApp.uploadqueue->AddUpDataOverheadServer(packet->size);
		theApp.serverconnect->SendPacket(packet,true);
		theApp.emuledlg->AddDebugLogLine( false, "Send:Source Request Server File(%s)", GetFileName() );
	}

	transferingsrc = 0;
	datarate = 0;
	POSITION pos1, pos2;
	for (uint32 sl = 0; sl < SOURCESSLOTS; sl++)
	{
		if (!srclists[sl].IsEmpty())
		{
			for (pos1 = srclists[sl].GetHeadPosition();( pos2 = pos1 ) != NULL;)
			{
				srclists[sl].GetNext(pos1);
				CUpDownClient* cur_src = srclists[sl].GetAt(pos2);

				switch (cur_src->GetDownloadState())
				{
					case DS_DOWNLOADING:
					{
						transferingsrc++;
						uint32 cur_datarate = cur_src->CalculateDownloadRate();
						datarate += cur_datarate;
#if 0
						if(cur_datarate>0) {
						  printf("Client %s (%lx) downloading (%d)\n",cur_src->GetUserName(),cur_src,cur_datarate);
						}
#endif
						if (reducedownload && cur_src->GetDownloadState() == DS_DOWNLOADING){
							uint32 limit = reducedownload*cur_datarate/1000; //(uint32)(((float)reducedownload/100)*cur_datarate)/10;		
							if (limit < 1000 && reducedownload == 200)
								limit += 1000;
							else if (limit < 1)
								limit = 1;
							cur_src->socket->SetDownloadLimit(limit);
						}
						else {
							cur_src->socket->DisableDownloadLimit();
						}
						break;
					}
					case DS_BANNED:
					case DS_ERROR:
						break;
					case DS_LOWTOLOWIP:	// if we now have a high ip we can ask
						if( ((dwCurTick - lastpurgetime) > 30000) && (this->GetSourceCount() >= (theApp.glob_prefs->GetMaxSourcePerFile()*.8 )) ){
							theApp.downloadqueue->RemoveSource( cur_src );
							lastpurgetime = dwCurTick;
							break;
						}
						if (theApp.serverconnect->IsLowID())
							break;
					case DS_NONEEDEDPARTS:
					{ 
						if( ((dwCurTick - lastpurgetime) > 40000) && (this->GetSourceCount() >= (theApp.glob_prefs->GetMaxSourcePerFile()*.8 )) )
							if( !cur_src->SwapToAnotherFile( cur_src ) ){
								theApp.downloadqueue->RemoveSource( cur_src );
								lastpurgetime = dwCurTick;
								break; //Johnny-B - nothing more to do here (good eye!)
							}
						// doubled reasktime for no needed parts - save connections and traffic
						if (!((!cur_src->GetLastAskedTime()) || (dwCurTick - cur_src->GetLastAskedTime()) > FILEREASKTIME*2))
							break; 
					}
					case DS_ONQUEUE:
					{
						if( cur_src->IsRemoteQueueFull() )
							if( ((dwCurTick - lastpurgetime) > 60000) && (this->GetSourceCount() >= (theApp.glob_prefs->GetMaxSourcePerFile()*.8 )) ){
								theApp.downloadqueue->RemoveSource( cur_src );
								lastpurgetime = dwCurTick;
								break; //Johnny-B - nothing more to do here (good eye!)
							}
						if (theApp.serverconnect->IsConnected() && ((!cur_src->GetLastAskedTime()) || (dwCurTick - cur_src->GetLastAskedTime()) > FILEREASKTIME-20000))
							cur_src->UDPReaskForDownload();
					}
					case DS_CONNECTING:
					case DS_TOOMANYCONNS:
					case DS_CONNECTED:
					case DS_NONE:
					case DS_WAITCALLBACK:
#if 0
					  printf("*** not yet: %d %d %d > %d\n",
						 theApp.serverconnect->IsConnected(),
						 cur_src->GetLastAskedTime(),
						 dwCurTick-cur_src->GetLastAskedTime(),
						 FILEREASKTIME);
#endif
						if (theApp.serverconnect->IsConnected() && ((!cur_src->GetLastAskedTime()) || (dwCurTick - cur_src->GetLastAskedTime()) > FILEREASKTIME))
							cur_src->AskForDownload();
						break;
				}
			}
		}
	}

	// swap No needed partfiles if possible
	if (((!m_LastNoNeededCheck) || (dwCurTick - m_LastNoNeededCheck) > 10000))
	{
		m_LastNoNeededCheck = dwCurTick;
		POSITION pos1, pos2;
		for (int sl = 0; sl < SOURCESSLOTS; sl++)
			if (!srclists[sl].IsEmpty())
				for (pos1 = srclists[sl].GetHeadPosition();( pos2 = pos1 ) != NULL;)
				{
					srclists[sl].GetNext(pos1);
					CUpDownClient* cur_src = srclists[sl].GetAt(pos2);
					if (cur_src->GetDownloadState() == DS_NONEEDEDPARTS)
						cur_src->SwapToAnotherFile(false);
				}
	}

	// calculate datarate, set limit etc.
	count++;
	if (count == 30){
		count = 0;
		//theApp.emuledlg->transferwnd.downloadlistctrl.UpdateItem(this);
		UpdateDisplayedInfo();
		if(m_bPercentUpdated == false)
			UpdateCompletedInfos();
		m_bPercentUpdated = false;
	}
	return datarate;
}

void CPartFile::AddSources(CMemFile* sources,uint32 serverip, uint16 serverport){
	uint8 count;
	uint8 debug_lowiddropped = 0;
	uint8 debug_possiblesources = 0;
	sources->Read(&count,1);
	for (int i = 0;i != count;i++){
		uint32 userid;
		sources->Read(&userid,4);
		uint16 port;
		sources->Read(&port,2);
		// check first if we are this source

		// MOD Note: Do not change this part - Merkur
		if (theApp.serverconnect->GetClientID() < 16777216 && theApp.serverconnect->IsConnected()){
			if ((theApp.serverconnect->GetClientID() == userid) && inet_addr(theApp.serverconnect->GetCurrentServer()->GetFullIP()) == serverip)
				continue;
		}
		else if (theApp.serverconnect->GetClientID() == userid)
			continue;
		else if (userid < 16777216 && !theApp.serverconnect->IsLocalServer(serverip,serverport)){
			debug_lowiddropped++;
			continue;
		}
		// MOD Note - end

		if( theApp.glob_prefs->GetMaxSourcePerFile() > this->GetSourceCount() ){
			debug_possiblesources++;
			CUpDownClient* newsource = new CUpDownClient(port,userid,serverip,serverport,this);
			theApp.downloadqueue->CheckAndAddSource(this,newsource);
		}
	}
	theApp.emuledlg->AddDebugLogLine(false,"RCV: %i sources from server, %i low id dropped, %i possible sources File(%s)",count,debug_lowiddropped,debug_possiblesources, GetFileName());
}

void CPartFile::NewSrcPartsInfo(){
	// Cache part count
	uint16 partcount = GetPartCount();

	// Increase size if necessary
	if(m_SrcpartFrequency.GetSize() < partcount){
		m_SrcpartFrequency.SetSize(partcount);
	}
	// Reset part counters
	for(int i = 0; i < partcount; i++){
		m_SrcpartFrequency[i] = 0;
	}
	CUpDownClient* cur_src;
	
	for(int sl=0; sl<SOURCESSLOTS; ++sl) {
		if (!srclists[sl].IsEmpty()) {
			for (POSITION pos = srclists[sl].GetHeadPosition(); pos != 0; ){
				cur_src = srclists[sl].GetNext(pos);
				for (int i = 0; i != partcount; i++){
					if (cur_src->IsPartAvailable(i))
						m_SrcpartFrequency[i] +=1;
				}
			}
		}
	}
	//theApp.emuledlg->transferwnd.downloadlistctrl.UpdateItem(this);
	UpdateDisplayedInfo();
}

bool CPartFile::GetNextRequestedBlock(CUpDownClient* sender,Requested_Block_Struct** newblocks,uint16* count){
	uint16 requestedCount = *count;
	uint16 newblockcount = 0;
	uint8* partsav = sender->GetPartStatus();
	*count = 0;
	uint16 randomness;
	CList<int,int> liGoodParts;
	CList<int,int> liPossibleParts;
	bool finished = false;
	while (!finished)
	{
		// Need to empty lists to avoid infinite loop when file is smaller than 180k
		// Otherwise it would keep looping to find 3 blocks, there is only one and it is requested
		liGoodParts.RemoveAll();
		liPossibleParts.RemoveAll();
		// Barry - Top priority should be to continue downloading from current blocks (if anything left to download)
		bool foundPriorityPart = false;
		CString gettingParts;
		sender->ShowDownloadingParts(&gettingParts);

		for (int i=(gettingParts.GetLength()-1); i>=0; i--)
		{
			if ((gettingParts.GetChar(i) == 'Y') && (GetNextEmptyBlockInPart(i, 0)))
			{
				liGoodParts.AddHead(i);
				foundPriorityPart = true;
			}
		}
		// Barry - Give priorty to end parts of archives and movies
		if ((!foundPriorityPart) && (IsArchive() || IsMovie()) && theApp.glob_prefs->GetPreviewPrio())
		{
			uint32 partCount = GetPartCount();
			// First part
			if (sender->IsPartAvailable(0) && GetNextEmptyBlockInPart(0, 0))
			{
				liGoodParts.AddHead(0);
				foundPriorityPart = true;
			}
			else if ((partCount > 1))
			{
				// Last part
				if (sender->IsPartAvailable(partCount-1) && GetNextEmptyBlockInPart(partCount-1, 0))
				{
					liGoodParts.AddHead(partCount-1);
					foundPriorityPart = true;
				}
				// Barry - Better to get rarest than these, add to list, but do not exclude all others.
				// These get priority over other parts with same availability.
				else if (partCount > 2)
				{
					// Second part
					if (sender->IsPartAvailable(1) && GetNextEmptyBlockInPart(1, 0))
						liGoodParts.AddHead(1);
					// Penultimate part
					else if (sender->IsPartAvailable(partCount-2) && GetNextEmptyBlockInPart(partCount-2, 0))
						liGoodParts.AddHead(partCount-2);
				}
			}
		}
		if (!foundPriorityPart)
		{
			randomness = (uint16)ROUND(((float)rand()/RAND_MAX)*(GetPartCount()-1));
			for (uint16 i = 0;i != GetPartCount();i++){
				if (sender->IsPartAvailable(randomness))
				{                                        
					if (partsav[randomness] && !IsComplete(randomness*PARTSIZE,((randomness+1)*PARTSIZE)-1)){
						/*if (IsCorruptedPart(randomness)){
						if (GetNextEmptyBlockInPart(randomness,0)){
						goodpart = randomness;
						break;
						}
						}
						else */ 
						if (IsPureGap(randomness*PARTSIZE,((randomness+1)*PARTSIZE)-1))
						{
							if (GetNextEmptyBlockInPart(randomness,0))
								liPossibleParts.AddHead(randomness);
						}
						else if (GetNextEmptyBlockInPart(randomness,0))
							liGoodParts.AddTail(randomness); // Barry - Add after archive/movie entries
					}
				}
				randomness++;
				if (randomness == GetPartCount())
					randomness = 0;
			}
		}
		CList<int,int>* usedlist;
		if (!liGoodParts.IsEmpty())
			usedlist = &liGoodParts;
		else if (!liPossibleParts.IsEmpty())
			usedlist = &liPossibleParts;
		else{
			if (!newblockcount){
				return false;
			}
			else
				break;
		}
		uint16 nRarest = 0xFFFF;
		uint16 usedpart = usedlist->GetHead();
		for (POSITION pos = usedlist->GetHeadPosition();pos != 0;usedlist->GetNext(pos)){
			if (m_SrcpartFrequency.GetCount() >= usedlist->GetAt(pos)
				&& m_SrcpartFrequency[usedlist->GetAt(pos)] < nRarest){
					nRarest = m_SrcpartFrequency[usedlist->GetAt(pos)];
					usedpart = usedlist->GetAt(pos);
				}
		}
		while (true){
			Requested_Block_Struct* block = new Requested_Block_Struct;
			if (GetNextEmptyBlockInPart(usedpart,block)){
				requestedblocks_list.AddTail(block);
				newblocks[newblockcount] = block;
				newblockcount++;
				if (newblockcount == requestedCount){
					finished = true;
					break;
				}
			}
			else
			{
				delete block;
				break;
			}
		}
	} //wend
	*count = newblockcount;
	return true;
}

void  CPartFile::RemoveBlockFromList(uint32 start,uint32 end){
	POSITION pos1,pos2;
	for (pos1 = requestedblocks_list.GetHeadPosition(); ( pos2 = pos1 ) != NULL; ){
	   requestedblocks_list.GetNext(pos1);
		if (requestedblocks_list.GetAt(pos2)->StartOffset <= start && requestedblocks_list.GetAt(pos2)->EndOffset >= end)
			requestedblocks_list.RemoveAt(pos2);
	}
}

void CPartFile::RemoveAllRequestedBlocks(void)
{
	requestedblocks_list.RemoveAll();
}

#include <pthread.h>
pthread_attr_t pattr;

void CPartFile::CompleteFile(bool bIsHashingDone){
	if( this->srcarevisible )
		theApp.emuledlg->transferwnd->downloadlistctrl->HideSources(this);
	status = PS_COMPLETING;
	if (!bIsHashingDone){
		//m_hpartfile.Flush(); // flush the OS buffer before completing...
		datarate = 0;
		char* partfileb = nstrdup(partmetfilename);
		partfileb[strlen(partmetfilename)-4] = 0;
		//CAddFileThread* addfilethread = (CAddFileThread*) AfxBeginThread(RUNTIME_CLASS(CAddFileThread), THREAD_PRIORITY_BELOW_NORMAL,0, CREATE_SUSPENDED);
		CAddFileThread* addfilethread=new CAddFileThread;
		addfilethread->Create();
		addfilethread->SetValues(0,theApp.glob_prefs->GetTempDir(),partfileb,this);
		addfilethread->Run(); //ResumeThread();	
		delete[] partfileb;
		return;
	}
	else{
		StopFile();
		// guess I was wrong about not need to spaw a thread.. It is if the temp and incoming dirs are on different partitions/drives and the file is large...[oz]
		// use pthreads
		pthread_t tid;
		pthread_attr_init(&pattr);
		pthread_attr_setdetachstate(&pattr,PTHREAD_CREATE_DETACHED);
		pthread_create(&tid,&pattr,(void*(*)(void*))CompleteThreadProc,this);
#if 0
		CWinThread *pThread = AfxBeginThread((AFX_THREADPROC)CompleteThreadProc, this, THREAD_PRIORITY_BELOW_NORMAL, 0, CREATE_SUSPENDED); // Lord KiRon - using threads for file completion
		if (pThread)
			pThread->ResumeThread();
		else
			throw CString(_T(GetResString(IDS_ERR_FILECOMPLETIONTHREAD))); // move this to resources
#endif
	}
	theApp.emuledlg->transferwnd->downloadlistctrl->ShowFilesCount();
	UpdateDisplayedInfo(true);
}

UINT CPartFile::CompleteThreadProc(CPartFile* pFile) 
{ 
	if (!pFile)
		return (UINT)-1; 
   	pFile->PerformFileComplete(); 
   	return 0; 
}

// Lord KiRon - using threads for file completion
BOOL CPartFile::PerformFileComplete() 
{ 
  //CSingleLock(&m_FileCompleteMutex,TRUE); // will be unlocked on exit
  wxMutexLocker sLock(m_FileCompleteMutex);
	char* partfilename = nstrdup(fullname);
	partfilename[strlen(fullname)-4] = 0;
	char* newfilename = nstrdup(GetFileName());
	strcpy(newfilename, theApp.StripInvalidFilenameChars(newfilename));
	char* newname = new char[strlen(newfilename)+strlen(theApp.glob_prefs->GetIncomingDir())+MAX_PATH];
	sprintf(newname,"%s/%s",theApp.glob_prefs->GetIncomingDir(),newfilename);

	// close permanent handle
	m_hpartfile.Close();

	bool renamed = false;
	if(wxFileName::FileExists(newname))
	{
		renamed = true;
		int namecount = 0;

		size_t length = strlen(newfilename);

		//the file extension
		char *ext = strrchr(newfilename, '.');
		if(ext == NULL)
			ext = newfilename + length;

		char *last = ext;  //new end is the file name before extension
		last[0] = 0;  //truncate file name

		//serch for matching ()s and check if it contains a number
		if((ext != newfilename) && (strrchr(newfilename, ')') + 1 == last)) {
			char *first = strrchr(newfilename, '(');
			if(first != NULL) {
				first++;
				bool found = true;
				for(char *step = first; step < last - 1; step++)
					if(*step < '0' || *step > '9') {
						found = false;
						break;
					}
					if(found) {
						namecount = atoi(first);
						last = first - 1;
						last[0] = 0;  //truncate again
					}
			}
		}

		CString strTestName;
		do {
			namecount++;
			strTestName.Format("%s/%s(%d).%s", theApp.glob_prefs->GetIncomingDir(),
				newfilename, namecount, min(ext + 1, newfilename + length));
		} while(wxFileName::FileExists(strTestName));
		delete[] newname;
		newname = nstrdup(strTestName);
	}
	delete newfilename;

	char syscmd[4096];
	sprintf(syscmd,"mv \"%s\" \"%s\"",partfilename,newname);
	if (system(syscmd)){	  
		delete[] partfilename;
		delete[] newname;
		wxMutexGuiEnter();
		theApp.emuledlg->AddLogLine(true,GetResString(IDS_ERR_COMPLETIONFAILED),GetFileName());
		wxMutexGuiLeave();
		paused = true;
		status = PS_ERROR;
		wxMutexGuiEnter();
		theApp.downloadqueue->StartNextFile();
		wxMutexGuiLeave();
		return FALSE;
	}
	if (remove(fullname)) {
	  wxMutexGuiEnter();
		theApp.emuledlg->AddLogLine(true,GetResString(IDS_ERR_DELETEFAILED),fullname);
		wxMutexGuiLeave();
	}
	CString BAKName(fullname);
	BAKName.Append(".BAK");
	if (remove(BAKName.GetData()) < 0) {
	  wxMutexGuiEnter();
		theApp.emuledlg->AddLogLine(true,GetResString(IDS_ERR_DELETE), BAKName.GetData());
		wxMutexGuiLeave();
	}

	delete[] partfilename;
	delete[] fullname;
	fullname = newname;
	delete[] directory;
	directory = nstrdup(theApp.glob_prefs->GetIncomingDir());
	status = PS_COMPLETE;
	paused = false;
	wxMutexGuiEnter();
	theApp.emuledlg->AddLogLine(true,GetResString(IDS_DOWNLOADDONE),GetFileName());
	theApp.emuledlg->ShowNotifier(GetResString(IDS_TBN_DOWNLOADDONE)+"\n"+GetFileName(), TBN_DLOAD);
	wxMutexGuiLeave();
	if (renamed) {
	  wxMutexGuiEnter();
		theApp.emuledlg->AddLogLine(true, GetResString(IDS_DOWNLOADRENAMED), strrchr(newname, '/') + 1);
		wxMutexGuiLeave();
	}
	theApp.knownfiles->SafeAddKFile(this);
	((CKnownFile*)this)->SetAutoPriority(false) ; //UAP
	theApp.downloadqueue->RemoveFile(this);
	//theApp.emuledlg->transferwnd.downloadlistctrl.UpdateItem(this);
	wxMutexGuiEnter();
	UpdateDisplayedInfo();
	theApp.emuledlg->transferwnd->downloadlistctrl->ShowFilesCount();
	wxMutexGuiLeave();
	//SHAddToRecentDocs(SHARD_PATH, fullname); // This is a real nasty call that takes ~110 ms on my 1.4 GHz Athlon and isn't really needed afai see...[ozon]

	// Barry - Just in case
	//		transfered = m_nFileSize;
	wxMutexGuiEnter();
	theApp.downloadqueue->StartNextFile();
	wxMutexGuiLeave();

	return TRUE;
}

void  CPartFile::RemoveAllSources(bool bTryToSwap){
	//TODO transfer sources to other downloading files if possible
	POSITION pos1,pos2;
	for (int sl=0;sl<SOURCESSLOTS;sl++) if (!srclists[sl].IsEmpty())
	for( pos1 = srclists[sl].GetHeadPosition(); ( pos2 = pos1 ) != NULL; ){
		srclists[sl].GetNext(pos1);
		if (bTryToSwap){
			if (!srclists[sl].GetAt(pos2)->SwapToAnotherFile(true))
				theApp.downloadqueue->RemoveSource(srclists[sl].GetAt(pos2));
		}
		else
			theApp.downloadqueue->RemoveSource(srclists[sl].GetAt(pos2));
	}
	UpdateFileRatingCommentAvail();
}

void CPartFile::DeleteFile(){

	// Barry - Need to tell any connected clients to stop sending the file
	StopFile();

	theApp.sharedfiles->RemoveFile(this);
	theApp.downloadqueue->RemoveFile(this);
	theApp.emuledlg->transferwnd->downloadlistctrl->RemoveFile(this);

	m_hpartfile.Close();

	if (remove(fullname))
		theApp.emuledlg->AddLogLine(true,GetResString(IDS_ERR_DELETE),fullname);
	char* partfilename = nstrdup(fullname);
	partfilename[strlen(fullname)-4] = 0;
	if (remove(partfilename))
		theApp.emuledlg->AddLogLine(true,GetResString(IDS_ERR_DELETE),partfilename);

	CString BAKName(fullname);
	BAKName.Append(".BAK");
	if (remove(BAKName) < 0)
		theApp.emuledlg->AddLogLine(true,GetResString(IDS_ERR_DELETE), BAKName.GetData());

	delete[] partfilename;
	delete this;
}

bool CPartFile::HashSinglePart(uint16 partnumber){
	if ((GetHashCount() <= partnumber) && (GetPartCount() > 1)){
		theApp.emuledlg->AddLogLine(true,GetResString(IDS_ERR_HASHERRORWARNING),GetFileName());
		this->hashsetneeded = true;
		return true;
	}
	else if(!GetPartHash(partnumber) && GetPartCount() != 1){
		theApp.emuledlg->AddLogLine(true,GetResString(IDS_ERR_INCOMPLETEHASH),GetFileName());
		this->hashsetneeded = true;
		return true;		
	}
	else{
		uchar hashresult[16];
		m_hpartfile.Seek(PARTSIZE*partnumber);
		uint32 length = PARTSIZE;
		if (PARTSIZE*(partnumber+1) > m_hpartfile.Length())
			length = (m_hpartfile.Length()- (PARTSIZE*partnumber));
		CreateHashFromFile(&m_hpartfile,length,hashresult);

		if (GetPartCount() > 1){
			if (memcmp(hashresult,GetPartHash(partnumber),16))
				return false;
			else
				return true;
		}
		else{
			if (memcmp(hashresult,m_abyFileHash,16))
				return false;
			else
				return true;
		}
	}

}

bool CPartFile::IsCorruptedPart(uint16 partnumber){
	return corrupted_list.Find(partnumber);
}

bool CPartFile::IsMovie(){
	//TODO use filetags instead ststr
	return (strstr(GetFileName(),".avi") || strstr(GetFileName(),".mpg") || strstr(GetFileName(),".mpeg") || strstr(GetFileName(),".ogm") || strstr(GetFileName(),".bin"));
}

// Barry - Also want to preview zip/rar files
bool CPartFile::IsArchive()
{
	wxString extension = wxString(GetFileName()).Right(4);
	return ((extension.CmpNoCase(".zip") == 0) || (extension.CmpNoCase(".rar") == 0));
}

void CPartFile::SetPriority(uint8 np){
	//UAP
	if (np == PR_AUTO)
		SetAutoPriority(true) ;
	else
	priority = np;
	//end UAP
	theApp.downloadqueue->SortByPriority();
	//theApp.emuledlg->transferwnd->downloadlistctrl->UpdateItem(this);
	UpdateDisplayedInfo(true);
	SavePartFile();
}

void CPartFile::StopFile(){
	// Barry - Need to tell any connected clients to stop sending the file
	PauseFile();

	RemoveAllSources(true);
	paused = true;
	datarate = 0;
	transferingsrc = 0;
	FlushBuffer();
	//theApp.emuledlg->transferwnd.downloadlistctrl.UpdateItem(this);
	UpdateDisplayedInfo(true);
}

void CPartFile::PauseFile(){
	if (status==PS_COMPLETE || status==PS_COMPLETING) return;
	Packet* packet = new Packet(OP_CANCELTRANSFER,0);
	POSITION pos1,pos2;
	for (int sl=0;sl<SOURCESSLOTS;sl++) if (!srclists[sl].IsEmpty())
	for( pos1 = srclists[sl].GetHeadPosition(); ( pos2 = pos1 ) != NULL; ){
		srclists[sl].GetNext(pos1);
		CUpDownClient* cur_src = srclists[sl].GetAt(pos2);
		if (cur_src->GetDownloadState() == DS_DOWNLOADING){
			theApp.uploadqueue->AddUpDataOverheadOther(packet->size);
			cur_src->socket->SendPacket(packet,false,true);
			cur_src->SetDownloadState(DS_ONQUEUE);
		}
	}
	delete packet;
	paused = true;
	datarate = 0;
	transferingsrc = 0;
	//theApp.emuledlg->transferwnd.downloadlistctrl.UpdateItem(this);
	UpdateDisplayedInfo(true);
	SavePartFile();
}

void CPartFile::ResumeFile(){
	if (status==PS_COMPLETE || status==PS_COMPLETING) return;
	paused = false;
	lastsearchtime = 0;
	//theApp.emuledlg->transferwnd->downloadlistctrl->UpdateItem(this);
	UpdateDisplayedInfo(true);
}

CString CPartFile::getPartfileStatus(){ 
	CString mybuffer=""; 
	if (GetTransferingSrcCount()>0) mybuffer=GetResString(IDS_DOWNLOADING); else mybuffer=GetResString(IDS_WAITING); 
	switch (GetStatus()) {
		case PS_HASHING: 
		case PS_WAITINGFORHASH:
			mybuffer=GetResString(IDS_HASHING);
			break; 
		case PS_COMPLETING:
			mybuffer=GetResString(IDS_COMPLETING);
			break; 
		case PS_COMPLETE:
			mybuffer=GetResString(IDS_COMPLETE);
			break; 
		case PS_PAUSED:
			mybuffer=GetResString(IDS_PAUSED);
			break; 
		case PS_ERROR:
			mybuffer=GetResString(IDS_ERRORLIKE);
			break;
	} 
	return mybuffer; 
} 

int CPartFile::getPartfileStatusRang(){ 
	
	int tempstatus=0;
	if (GetTransferingSrcCount()==0) tempstatus=1;
	switch (GetStatus()) {
		case PS_HASHING: 
		case PS_WAITINGFORHASH:
			tempstatus=3;
			break; 
		case PS_COMPLETING:
			tempstatus=4;
			break; 
		case PS_COMPLETE:
			tempstatus=5;
			break; 
		case PS_PAUSED:
			tempstatus=2;
			break; 
		case PS_ERROR:
			tempstatus=6;
			break;
	} 
	return tempstatus;
} 

sint32 CPartFile::getTimeRemaining(){ 
	if (GetDatarate()==0) return -1;
	
	return( (GetFileSize()-GetCompletedSize())/ GetDatarate());
} 

void CPartFile::PreviewFile(){
#if 0
	if (!PreviewAvailable()){
		return;
	}
	m_bPreviewing = true;
	CPreviewThread* pThread = (CPreviewThread*) AfxBeginThread(RUNTIME_CLASS(CPreviewThread), THREAD_PRIORITY_NORMAL,0, CREATE_SUSPENDED);
	pThread->SetValues(this);
	pThread->ResumeThread();
#endif
	wxString command;

// If no player set in preferences, use mplayer.
	if (theApp.glob_prefs->GetVideoPlayer()=="")
		command.Append(wxT("mplayer"));
	else
		command.Append(theApp.glob_prefs->GetVideoPlayer());
// Need to use quotes in case filename contains spaces.
	command.Append(wxT(" \""));
	command.Append(GetFullName());
// Remove the .met from filename.
	for (int i=0;i<4;i++) command.RemoveLast();
	command.Append(wxT("\""));
        wxShell(command.c_str());
}

bool CPartFile::PreviewAvailable(){
#if 0
	uint64 space;
	space = 0;
	space = GetFreeDiskSpaceX(theApp.glob_prefs->GetTempDir());
#endif
	return (IsMovie() && IsComplete(0, PARTSIZE));
}

void CPartFile::UpdateAvailablePartsCount(){
	uint8 availablecounter = 0;
	bool breakflag = false;
	uint16 iPartCount = GetPartCount();
	for (uint32 ixPart = 0; ixPart < iPartCount; ixPart++){
		breakflag = false;
		for (uint32 sl = 0; sl < SOURCESSLOTS && !breakflag; sl++){
			if (!srclists[sl].IsEmpty()){
				for(POSITION pos = srclists[sl].GetHeadPosition(); pos && !breakflag; ){
					if (srclists[sl].GetNext(pos)->IsPartAvailable(ixPart)){ 
						availablecounter++; 
						breakflag = true;
					}
				}
			}
		}
	}
	if (iPartCount == availablecounter && availablePartsCount < iPartCount)
	  lastseencomplete = time(NULL);//CTime::GetCurrentTime();
	availablePartsCount = availablecounter;
}

Packet*	CPartFile::CreateSrcInfoPacket(CUpDownClient* forClient){
	int sl;
	for (sl=0;sl<SOURCESSLOTS;sl++) if (srclists[sl].IsEmpty()) return 0;

	CMemFile data;
	uint16 nCount = 0;

	data.Write(m_abyFileHash,16);
	data.Write(&nCount,2);
	bool bNeeded;
	for (sl=0;sl<SOURCESSLOTS;sl++) if (!srclists[sl].IsEmpty())
	for (POSITION pos = srclists[sl].GetHeadPosition();pos != 0;srclists[sl].GetNext(pos)){
		bNeeded = false;
		CUpDownClient* cur_src = srclists[sl].GetAt(pos);
		if (cur_src->HasLowID())
			continue;
		// only send source which have needed parts for this client if possible
		if (forClient->reqfile == this && forClient->GetPartStatus() && cur_src->GetPartStatus()){
			uint8* reqstatus = forClient->GetPartStatus();
			uint8* srcstatus = cur_src->GetPartStatus();
			for (int x = 0; x != GetPartCount(); x++){
				if (srcstatus[x] && !reqstatus[x]){
					bNeeded = true;
					break;
				}
			}
		}
		if( bNeeded ){
			nCount++;
			uint32 dwID = cur_src->GetUserID();
			uint16 nPort = cur_src->GetUserPort();
			uint32 dwServerIP = cur_src->GetServerIP();
			uint16 nServerPort = cur_src->GetServerPort();
			data.Write(&dwID, 4);
			data.Write(&nPort, 2);
			data.Write(&dwServerIP, 4);
			data.Write(&nServerPort, 2);
			if (nCount > 500)
				break;
		}
	}
	if (!nCount)
		return 0;
	data.Seek(16);
	data.Write(&nCount, 2);

	Packet* result = new Packet(&data, OP_EMULEPROT);
	result->opcode = OP_ANSWERSOURCES;
	if (nCount > 28)
		result->PackPacket();
	theApp.emuledlg->AddDebugLogLine( false, "Send:Source User(%s) File(%s) Count(%i)", forClient->GetUserName(), GetFileName(), nCount );
	return result;
}

void CPartFile::AddClientSources(CMemFile* sources){
	uint16 nCount;
	sources->Read(&nCount,2);
	for (int i = 0;i != nCount;i++){
		uint32 dwID;
		uint16 nPort;
		uint32 dwServerIP;
		uint16 nServerPort;
		sources->Read(&dwID,4);
		sources->Read(&nPort,2);
		sources->Read(&dwServerIP,4);
		sources->Read(&nServerPort,2);
		// check first if we are this source
		if (theApp.serverconnect->GetClientID() < 16777216 && theApp.serverconnect->IsConnected()){
			if ((theApp.serverconnect->GetClientID() == dwID) && theApp.serverconnect->GetCurrentServer()->GetIP() == dwServerIP)
				continue;
		}
		else if (theApp.serverconnect->GetClientID() == dwID)
			continue;
		else if (dwID < 16777216)
			continue;
		if( theApp.glob_prefs->GetMaxSourcePerFile() > this->GetSourceCount() ){
			CUpDownClient* newsource = new CUpDownClient(nPort,dwID,dwServerIP,nServerPort,this);
			theApp.downloadqueue->CheckAndAddSource(this,newsource);
		} 
	}
}
void CPartFile::UpdateUploadAutoPriority(void)
{
	if (this->IsAutoPrioritized()) {
		CPartFile* partfile = theApp.downloadqueue->GetFileByID(this->GetFileHash()) ;
		if (!partfile) return;
		if (partfile->IsPartFile()) {
			if (partfile->GetSourceCount() <= RARE_FILE)
				this->SetPriority(PR_VERYHIGH) ;
			else if (partfile->GetSourceCount() < 100)
				this->SetPriority(PR_HIGH) ;
			else
				this->SetPriority(PR_NORMAL) ;
		}
	}
}

// making this function return a higher when more sources have the extended
// protocol will force you to ask a larger variety of people for sources
int CPartFile::GetCommonFilePenalty() {
	//TODO: implement, but never return less than MINCOMMONPENALTY!
	return MINCOMMONPENALTY;
}

/* Barry - Replaces BlockReceived() 

           Originally this only wrote to disk when a full 180k block 
           had been received from a client, and only asked for data in 
		   180k blocks.

		   This meant that on average 90k was lost for every connection
		   to a client data source. That is a lot of wasted data.

		   To reduce the lost data, packets are now written to a buffer
		   and flushed to disk regularly regardless of size downloaded.
		   This includes compressed packets.

		   Data is also requested only where gaps are, not in 180k blocks.
		   The requests will still not exceed 180k, but may be smaller to
		   fill a gap.
*/
uint32 CPartFile::WriteToBuffer(uint32 transize, BYTE *data, uint32 start, uint32 end, Requested_Block_Struct *block)
{
	// Increment transfered bytes counter for this file
	transfered += transize;

	// This is needed a few times
	uint32 lenData = end - start + 1;

	if( lenData > transize )
		m_iGainDueToCompression += lenData-transize;

	// Occasionally packets are duplicated, no point writing it twice
	if (IsComplete(start, end))
	{
		theApp.emuledlg->AddDebugLogLine(false, "File '%s' has already been written from %ld to %ld\n", GetFileName(), start, end);
		return 0;
	}

	// Create copy of data as new buffer
	BYTE *buffer = new BYTE[lenData];
	memcpy(buffer, data, lenData);

	// Create a new buffered queue entry
	PartFileBufferedData *item = new PartFileBufferedData;
	item->data = buffer;
	item->start = start;
	item->end = end;
	item->block = block;

	// Add to the queue in the correct position (most likely the end)
	PartFileBufferedData *queueItem;
	bool added = false;
	POSITION pos = m_BufferedData_list.GetTailPosition();
	while (pos != NULL)
	{	
		queueItem = m_BufferedData_list.GetPrev(pos);
		if (item->end > queueItem->end)
		{
			added = true;
			m_BufferedData_list.InsertAfter(pos, item);
			break;
		}
	}
	if (!added)
		m_BufferedData_list.AddHead(item);

	// Increment buffer size marker
	m_nTotalBufferData += lenData;

	// Mark this small section of the file as filled
	FillGap(item->start, item->end);

	// Update the flushed mark on the requested block 
	// The loop here is unfortunate but necessary to detect deleted blocks.
	pos = requestedblocks_list.GetHeadPosition();
	while (pos != NULL)
	{	
		if (requestedblocks_list.GetNext(pos) == item->block)
			item->block->transferred += lenData;
	}

	if (gaplist.IsEmpty()) FlushBuffer();

	// Return the length of data written to the buffer
	return lenData;
}

void CPartFile::FlushBuffer(void)
{
	m_nLastBufferFlushTime = GetTickCount();
	if (m_BufferedData_list.IsEmpty())
		return;

	uint32 partCount = GetPartCount();
	bool *changedPart = new bool[partCount];

//	theApp.emuledlg->AddDebugLogLine(false, "Flushing file %s - buffer size = %ld bytes (%ld queued items) transfered = %ld [time = %ld]\n", GetFileName(), m_nTotalBufferData, m_BufferedData_list.GetCount(), transfered, m_nLastBufferFlushTime);

	try
	{
		// Remember which parts need to be checked at the end of the flush
		for (int partNumber=0; (uint32)partNumber<partCount; partNumber++)
		{
			changedPart[partNumber] = false;
		}

		// Ensure file is big enough to write data to (the last item will be the furthest from the start)
		PartFileBufferedData *item = m_BufferedData_list.GetTail();
		if (m_hpartfile.Length() <= item->end)
		  //m_hpartfile.SetLength(item->end + 1);
		  ftruncate(m_hpartfile.fd(),item->end+1);
		
		// Loop through queue
		for (int i = m_BufferedData_list.GetCount(); i>0; i--)
		{
			// Get top item
			item = m_BufferedData_list.GetHead();

			// This is needed a few times
			uint32 lenData = item->end - item->start + 1;

			int curpart = item->start/PARTSIZE;
			changedPart[curpart] = true;

			// Go to the correct position in file and write block of data			
			m_hpartfile.Seek(item->start);
			m_hpartfile.Write(item->data, lenData);
			
			// Remove item from queue
			m_BufferedData_list.RemoveHead();

			// Decrease buffer size
			m_nTotalBufferData -= lenData;

			// Release memory used by this item
			delete [] item->data;
			delete item;
		}

		// Flush to disk
		m_hpartfile.Flush();

		// Check each part of the file
		uint32 partRange = (m_nFileSize % PARTSIZE) - 1;
		for (int partNumber = partCount-1; partNumber >= 0; partNumber--)
		{
			if (changedPart[partNumber] == false)
			{
				// Any parts other than last must be full size
				partRange = PARTSIZE - 1;
				continue;
			}

			// Is this 9MB part complete
			if ( IsComplete(PARTSIZE * partNumber, (PARTSIZE * (partNumber + 1)) - 1 ) )
			{
				// Is part corrupt
				if (!HashSinglePart(partNumber))
				{
					theApp.emuledlg->AddLogLine(true, GetResString(IDS_ERR_PARTCORRUPT), partNumber, GetFileName());
					AddGap(PARTSIZE*partNumber, (PARTSIZE*partNumber + partRange));
					corrupted_list.AddTail(partNumber);
					// Reduce transfered amount by corrupt amount
					this->m_iLostDueToCorruption += (partRange + 1);
				}
				else
				{
					// Successfully completed part, make it available for sharing
					if (status == PS_EMPTY)
					{
						status = PS_READY;
						theApp.sharedfiles->SafeAddKFile(this);
					}
				}
			}
			else if ( IsCorruptedPart(partNumber) && theApp.glob_prefs->IsICHEnabled())
			{
				// Try to recover with minimal loss
				if (HashSinglePart(partNumber))
				{
					m_iTotalPacketsSavedDueToICH++;
					FillGap(PARTSIZE*partNumber,(PARTSIZE*partNumber+partRange));
					RemoveBlockFromList(PARTSIZE*partNumber,(PARTSIZE*partNumber + partRange));
					theApp.emuledlg->AddLogLine(true,GetResString(IDS_ICHWORKED),partNumber,GetFileName());
				}
			}
			// Any parts other than last must be full size
			partRange = PARTSIZE - 1;
		}

		// Update met file
		SavePartFile();

		// Is this file finished?
		if (gaplist.IsEmpty())
			CompleteFile(false);
	}
#if 0
	catch (CFileException* error)
	{
	  //OUTPUT_DEBUG_TRACE();

		//if (theApp.glob_prefs->IsErrorBeepEnabled()) Beep(800,200);

		if (error->m_cause == CFileException::diskFull) 
		{
			theApp.emuledlg->AddLogLine(true, GetResString(IDS_ERR_OUTOFSPACE), this->GetFileName());
			if (theApp.glob_prefs->GetNotifierPopOnImportantError()) 
			{
				CString msg;
				msg.Format(GetResString(IDS_ERR_OUTOFSPACE), this->GetFileName());
				theApp.emuledlg->ShowNotifier(msg, TBN_IMPORTANTEVENT, false);
			}
		}
		else
		{
			char buffer[150];
			error->GetErrorMessage(buffer,150);
			theApp.emuledlg->AddLogLine(true, GetResString(IDS_ERR_WRITEERROR), GetFileName(), buffer);
			status = PS_ERROR;
		}
		paused = true;
		datarate = 0;
		transferingsrc = 0;
	
		//theApp.emuledlg->transferwnd.downloadlistctrl.UpdateItem(this);
		UpdateDisplayedInfo();
		error->Delete();
	}
#endif
	catch(...)
	{
		theApp.emuledlg->AddLogLine(true, GetResString(IDS_ERR_WRITEERROR), GetFileName(), GetResString(IDS_UNKNOWN).GetData());
		status = PS_ERROR;
		paused = true;
		datarate = 0;
		transferingsrc = 0;
		//theApp.emuledlg->transferwnd.downloadlistctrl.UpdateItem(this);
		UpdateDisplayedInfo();
	}
	delete[] changedPart;

}
// Barry - This will invert the gap list, up to caller to delete gaps when done
// 'Gaps' returned are really the filled areas, and guaranteed to be in order
void CPartFile::GetFilledList(CTypedPtrList<CPtrList, Gap_Struct*> *filled)
{
	Gap_Struct *gap;
	Gap_Struct *best;
	POSITION pos;
	uint32 start = 0;
	uint32 bestEnd = 0;

	// Loop until done
	bool finished = false;
	while (!finished)
	{
		finished = true;
		// Find first gap after current start pos
		bestEnd = m_nFileSize;
		pos = gaplist.GetHeadPosition();
		while (pos != NULL)
		{
			gap = gaplist.GetNext(pos);
			if ((gap->start > start) && (gap->end < bestEnd))
			{
				best = gap;
				bestEnd = best->end;
				finished = false;
			}
		}

		if (!finished)
		{
			// Invert this gap
			gap = new Gap_Struct;
			gap->start = start;
			gap->end = best->start - 1;
			start = best->end + 1;
			filled->AddTail(gap);
		}
		else if (best->end < m_nFileSize)
		{
			gap = new Gap_Struct;
			gap->start = best->end + 1;
			gap->end = m_nFileSize;
			filled->AddTail(gap);
		}
	}
}

void CPartFile::UpdateFileRatingCommentAvail() {
	if (!this) return;

	bool prev=(hasComment || hasRating);

	hasComment=false;
	hasRating=false;

	POSITION pos1,pos2;
	for (int sl=0;sl<SOURCESSLOTS;sl++) if (!srclists[sl].IsEmpty())
	for (pos1 = srclists[sl].GetHeadPosition();( pos2 = pos1 ) != NULL;){
		srclists[sl].GetNext(pos1);
		CUpDownClient* cur_src = srclists[sl].GetAt(pos2);
		if (cur_src->GetFileComment().GetLength()>0) hasComment=true;
		if (cur_src->GetFileRate()>0) hasRating=true;
		if (hasComment && hasRating) break;
	}
	if (prev!=(hasComment || hasRating)) UpdateDisplayedInfo();//theApp.emuledlg->transferwnd.downloadlistctrl.UpdateItem(this);
}

uint16 CPartFile::GetSourceCount() {
	uint16 count=0;
	for (int i=0;i<SOURCESSLOTS;i++) count+=srclists[i].GetCount();
	return count;
}

bool CPartFile::HasBadRating(){
	if (!hasRating) return false;

	POSITION pos1,pos2;
	for (int sl=0;sl<SOURCESSLOTS;sl++) if (!srclists[sl].IsEmpty())
	for (pos1 = srclists[sl].GetHeadPosition();( pos2 = pos1 ) != NULL;){
		srclists[sl].GetNext(pos1);
		CUpDownClient* cur_src = srclists[sl].GetAt(pos2);
		if (cur_src->GetFileRate()==1) return true;
	}
	return false;
}

void CPartFile::UpdateDisplayedInfo(bool force) {
    DWORD curTick = ::GetTickCount();

    if(force || curTick-m_lastRefreshedDLDisplay > MINWAIT_BEFORE_DLDISPLAY_WINDOWUPDATE+(uint32)(rand()/(RAND_MAX/1000))) {
	    theApp.emuledlg->transferwnd->downloadlistctrl->UpdateItem(this);
        m_lastRefreshedDLDisplay = curTick;
    }
}

time_t CPartFile::GetLastChangeDatetime(bool forcecheck){
	if ((::GetTickCount()-m_lastdatetimecheck)<60000 && !forcecheck) return m_lastdatecheckvalue;
	m_lastdatetimecheck=::GetTickCount();
	if (!::wxFileExists(m_hpartfile.GetFilePath())) m_lastdatecheckvalue=-1; else {
	  //CFileStatus filestatus;
	  struct stat filestatus;
	  fstat(m_hpartfile.fd(),&filestatus);
	  //m_hpartfile.GetStatus(filestatus); // this; "...returns m_attribute without high-order flags" indicates a known MFC bug, wonder how many unknown there are... :)
	  m_lastdatecheckvalue=filestatus.st_mtime;
	}
	return m_lastdatecheckvalue;
}

uint8 CPartFile::GetCategory() {
	if(m_category>theApp.glob_prefs->GetCatCount()-1) m_category=0;
	return m_category;
}

CString CPartFile::GetDownloadFileInfo()
{
        if(this == NULL) return "";
                                                                                
        CString sRet;
        CString strHash=EncodeBase16(GetFileHash(), 16);;
                                                                                
        char lsc[50]; char complx[50]; char lastprogr[50];
                                                                                
        sprintf(complx,"%s/%s",CastItoXBytes(GetCompletedSize()).GetData(),CastItoXBytes(GetFileSize()).GetData());
                                                                                
        if (lastseencomplete==0) sprintf(lsc,GetResString(IDS_UNKNOWN).MakeLower() ); else
	  //sprintf(lsc,lastseencomplete.Format( theApp.glob_prefs->GetDateTimeFormat()));
	  strftime(lsc,sizeof(lsc),theApp.glob_prefs->GetDateTimeFormat(),localtime((time_t*)&lastseencomplete));
                                                                                
        if (GetFileDate()==0) sprintf(lastprogr,GetResString(IDS_UNKNOWN)); else
	  //sprintf(lastprogr,GetCFileDate().Format(theApp.glob_prefs->GetDateTimeFormat()));
	  strftime(lastprogr,sizeof(lsc),theApp.glob_prefs->GetDateTimeFormat(),localtime(GetCFileDate()));
                                                                                
        float availability = 0;
        if(GetPartCount() != 0) {
                availability = GetAvailablePartCount() * 100 / GetPartCount();
        }
        sRet.Format(GetResString(IDS_DL_FILENAME)+": %s (%s %s)\n\n%s\n\n"
                +GetResString(IDS_FD_HASH) +" %s\n"
                +GetResString(IDS_PARTINFOS)+
                GetResString(IDS_PARTINFOS2)+"\n%s\n%s",
                GetFileName(), CastItoXBytes(GetFileSize()).GetData(),GetResString(IDS_BYTES).GetData(),
                                (GetResString(IDS_STATUS)+": "+getPartfileStatus()).GetData(),
                                strHash.GetData(),
                                GetPartMetFileName(), GetPartCount(),GetResString(IDS_AVAIL).GetData(),
		    GetAvailablePartCount(),availability,
                                (int)GetPercentCompleted(), complx, GetTransferingSrcCount(),
                                (GetResString(IDS_LASTSEENCOMPL)+" "+wxString(lsc)).GetData(),
		    (GetResString(IDS_FD_LASTCHANGE) +" "+wxString(lastprogr)).GetData());
                                                                                
        return sRet;
}

wxString CPartFile::GetProgressString(uint16 size){
  char crProgress = '0';//green
  char crHave = '1';      // black
  char crPending='2';     // yellow
  char crWaiting='3';     // blue
  char crMissing='4';  // red
  
  wxString my_ChunkBar="";
  for (uint16 i=0;i<=size+1;i++) my_ChunkBar+=crHave; //.AppendChar(crHave);
  // one more for safety
  
  uint32 m_nFileSize=GetFileSize();
  float unit= (float)size/(float)m_nFileSize;
  uint32 allgaps = 0;
  
  if(GetStatus() == PS_COMPLETE || GetStatus() == PS_COMPLETING) {                        CharFillRange(&my_ChunkBar,0,(float)m_nFileSize*unit, crProgress);
  } else
    
    // red gaps
    for (POSITION pos = gaplist.GetHeadPosition();pos !=  0;gaplist.GetNext(pos)){
      Gap_Struct* cur_gap = gaplist.GetAt(pos);
      allgaps += cur_gap->end - cur_gap->start;
      bool gapdone = false;
      uint32 gapstart = cur_gap->start;
      uint32 gapend = cur_gap->end;
      for (uint32 i = 0; i < GetPartCount(); i++){
	if (gapstart >= i*PARTSIZE && gapstart <=  (i+1)*PARTSIZE){ // is in this part?
	  if (gapend <= (i+1)*PARTSIZE)
	    gapdone = true;
	  else{
	    gapend = (i+1)*PARTSIZE; // and next part
	      }
	  // paint
	  uint8 color;
	  if (m_SrcpartFrequency.GetCount() >= (int)i && m_SrcpartFrequency[i])  // frequency?
	    color = crWaiting;
	  else
	    color = crMissing;
	  
	  CharFillRange(&my_ChunkBar,(float)gapstart*unit, (float)gapend*unit + 1,  color);
	  
	  if (gapdone) // finished?
	    break;
	  else{
	    gapstart = gapend;
	    gapend = cur_gap->end;
	  }
	}
      }
    }
  
  // yellow pending parts
  for (POSITION pos = requestedblocks_list.GetHeadPosition();pos !=  0;requestedblocks_list.GetNext(pos))
    {
      Requested_Block_Struct* block =  requestedblocks_list.GetAt(pos);
      CharFillRange(&my_ChunkBar, (float)(block->StartOffset + block->transferred)*unit, (float)block->EndOffset*unit,  crPending);
    }
  return my_ChunkBar;
}
                                                                                
void CPartFile::CharFillRange(wxString* buffer,float start, float end, char color) {
  for (uint32 i=start;i<=end;i++)
    buffer->SetChar(i,color);
}
 
