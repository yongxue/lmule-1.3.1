
//#include "stdafx.h" 
#include "wintypes.h"
#include "emule.h"
#include <wx/dialog.h>
#include "CommentDialogLst.h"
#include "muuli_wdr.h"

#include "wx/xrc/xmlres.h"
#include "wx/xrc/xh_all.h"

#define GetDlgItem(x,clas) XRCCTRL(*this,#x,clas)
#define IsDlgButtonChecked(x) XRCCTRL(*this,#x,wxCheckBox)->GetValue()
#define CheckDlgButton(x,y) XRCCTRL(*this,#x,wxCheckBox)->SetValue(y)

//IMPLEMENT_DYNAMIC(CCommentDialogLst, CDialog) 
CCommentDialogLst::CCommentDialogLst(wxWindow*parent,CPartFile* file) 
  //: CDialog(CCommentDialogLst::IDD, 0) 
  : wxDialog(parent,CCommentDialogLst::IDD,_("File Comments"),
	   wxDefaultPosition,wxDefaultSize,wxDEFAULT_DIALOG_STYLE|wxSYSTEM_MENU)
{ 
   m_file = file; 
   //wxSizer* content=commentLstDlg(this,TRUE);
   //content->Show(this,TRUE);

   wxPanel* content=wxXmlResource::Get()->LoadPanel(this,"DLG_COMMENTLST");
   wxSize koko=content->GetSize();
   SetSize(content->GetSize());

   content->SetBackgroundColour(GetColour(wxSYS_COLOUR_WINDOW));
   Centre();

   pmyListCtrl=XRCCTRL(*this,"IDC_LST",wxListCtrl);
   OnInitDialog();
} 

CCommentDialogLst::~CCommentDialogLst() 
{ 
} 

BEGIN_EVENT_TABLE(CCommentDialogLst,wxDialog)
  EVT_BUTTON(XRCID("IDCOK"),CCommentDialogLst::OnBnClickedApply)
  EVT_BUTTON(XRCID("IDCREF"),CCommentDialogLst::OnBnClickedRefresh)
END_EVENT_TABLE()

#if 0
void CCommentDialogLst::DoDataExchange(CDataExchange* pDX) 
{ 
   CDialog::DoDataExchange(pDX); 
} 

BEGIN_MESSAGE_MAP(CCommentDialogLst, CDialog) 
   ON_BN_CLICKED(IDCOK, OnBnClickedApply) 
   ON_BN_CLICKED(IDCREF, OnBnClickedRefresh) 
END_MESSAGE_MAP() 

#endif

void CCommentDialogLst::OnBnClickedApply(wxEvent& evt) 
{ 
  //CDialog::OnOK(); 
  EndModal(0);
} 

void CCommentDialogLst::OnBnClickedRefresh(wxEvent& evt) 
{ 
   CompleteList(); 
} 


#define LVCFMT_LEFT wxLIST_FORMAT_LEFT

BOOL CCommentDialogLst::OnInitDialog(){ 
  //CDialog::OnInitDialog(); 

   pmyListCtrl->InsertColumn(0, GetResString(IDS_CD_UNAME), LVCFMT_LEFT, 130); 
   pmyListCtrl->InsertColumn(1, GetResString(IDS_DL_FILENAME), LVCFMT_LEFT, 130); 
   pmyListCtrl->InsertColumn(2, GetResString(IDS_QL_RATING), LVCFMT_LEFT, 80); 
   pmyListCtrl->InsertColumn(3, GetResString(IDS_COMMENT), LVCFMT_LEFT, 340); 

   Localize(); 
   CompleteList();

   return TRUE; 
} 

void CCommentDialogLst::Localize(void){ 
   GetDlgItem(IDCOK,wxControl)->SetLabel(GetResString(IDS_CW_CLOSE)); 
   GetDlgItem(IDCREF,wxControl)->SetLabel(GetResString(IDS_CMT_REFRESH)); 
   SetTitle(GetResString(IDS_CMT_READALL)+" ("+ m_file->GetFileName() +")"); 
} 

void CCommentDialogLst::CompleteList () { 
   POSITION pos1,pos2; 
   CUpDownClient* cur_src; 
   int count=0; 

   pmyListCtrl->DeleteAllItems();
   
   for (int sl=0;sl<SOURCESSLOTS;sl++) if (!m_file->srclists[sl].IsEmpty())
   for (pos1 = m_file->srclists[sl].GetHeadPosition(); (pos2 = pos1) != NULL;) { 
      m_file->srclists[sl].GetNext(pos1); 
      cur_src = m_file->srclists[sl].GetAt(pos2); 

	  if (cur_src->GetFileComment().Length()>0 || cur_src->GetFileRate()>0) {
			pmyListCtrl->InsertItem(count, cur_src->GetUserName()); 
			pmyListCtrl->SetItem(count, 1, cur_src->GetClientFilename()); 
			pmyListCtrl->SetItem(count, 2, GetRateString(cur_src->GetFileRate())); 
			pmyListCtrl->SetItem(count, 3, cur_src->GetFileComment()); 
			count++;
	  } 
   } 
   wxString info="";
   if (count==0) info="("+GetResString(IDS_CMT_NONE)+")";
   GetDlgItem(IDC_CMSTATUS,wxControl)->SetLabel(info);
   m_file->UpdateFileRatingCommentAvail();
}
