#ifndef _TABCTRL_H
#define _TABCTRL_H

#pragma interface "tabctrl.h" 

#include <wx/wx.h>
#include <gtk/gtk.h>

class wxTabItem;
WX_DECLARE_LIST(wxTabItem,TabList);

const int wxEVT_TABCTRL_FIRST = 10000+6341;

BEGIN_DECLARE_EVENT_TYPES()
     DECLARE_EVENT_TYPE(wxEVT_TAB_CHANGED,wxEVT_TABCTRL_FIRST+1)
END_DECLARE_EVENT_TYPES()

class wxTabCtrl : public wxControl
{
  DECLARE_DYNAMIC_CLASS(wxTabCtrl)
public:
  wxTabCtrl() {};
  wxTabCtrl(wxWindow* parent, const wxWindowID id=-1,
	    const wxPoint& pos = wxDefaultPosition,
	    const wxSize& size=wxDefaultSize,
	    const long int style=0)
    { Create(parent,id,pos,size,style); }

  bool Create(wxWindow* parent,const wxWindowID id,const wxPoint& pos,
	      const wxSize& size,const long int style);

  long InsertItem(int nMask,int nItem,wxString title,int nImage,long lpar);

  void OnPaint(GtkWidget* widget,GdkEventExpose* evt);
 private:
  void OnLeftDown(wxMouseEvent& evt);
  void OnMotion(wxMouseEvent& evt);

  int calcTabWidth();
  int tabsVisible();
  DECLARE_EVENT_TABLE()

  TabList m_tabs;
  int currentTab;
  int firstTab;
  bool prelightRight;
  bool prelightLeft;
};

#endif
