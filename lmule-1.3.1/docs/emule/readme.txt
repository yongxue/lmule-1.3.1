eMule Copyright (C)2002 Merkur (merkur-@users.sourceforge.net)


This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either
version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


-----------------------------------------------------------------------------------


Welcome to eMule, a filesharing client based on the eDonkey2000(C) network.


Visit us at
 http://www.emule-project.net
and
 http://sourceforge.net/projects/emule
or
 the IRC chatroom, #emule on irc.emule-project.net


Please remember that this a very early version, not all features are implemented
yet and not everything might work as intended.

Visit our forum for bugreports, feature requests, development or general dicussion.
(PLEASE do not report bugs that are already posted by someone else, and keep the
"Bug Reports", "Feature Requests" and "Development" sections clean and shiny)

If you have questions or serious problems, plz read the FAQ first :)
Then *search* the fourm for a topic related to your problem, DO NOT open a new
topic at once, most likely someone else had the same problem before.


Would you like to donate to the eMule project?
A PayPal link can be found on the portal page (www.emule-project.net)
Thanks ;)




INSTALLATION:
-------------

-Unzip eMule to a directory of your choice.
 If you don't know how to do that, plz stay away from this Alpha release ;)

-You can move your "Temp" and "Incoming" folders from eDonkey (or a previous version
 of eMule) to the new directory now, in order to continue your partial downloads.



START:
------

-double-click on emule.exe :)

-you can fetch a server-list now to start. Just go the "Servers" tab, enter
 the URL and press "Update". The working serverlist URLs change rapidly,
 so please look for one on the community pages mentionened below.




CONFIGURATION:
--------------

-Go to the "Preferences" tab

-Enter a nice nickname ;)

-Enter the "Download Capacity" and "Upload Capacity" according to your internet connection.
  All values in eMule are kiloBytes (KB), your Internet Service Provider's (ISP) numbers
  are most likely kiloBits (kB). 8 kiloBits make up 1 kiloByte, so when your Internet Connection
  is 768kB Downstrean and 128kB Upstrean (like German Telekom DSL), your correct values are:

 Downstrean: 768kB / 8 = 96KB, you enter 96 as "Download Capacity"
 Upstream: 128kB / 8 = 16 KB, you enter 16 as "Upload Capacity"

 These values are used to calculate the current bandwidth usage for display purposes only!
 Nevertheless, you need to know them to determine the following down/upload limits:


-Enter "Download Limit" and "Upload Limit"  (IMPORTANT!)
 
 Download Limit: leave this at 0 (should eMule become too fast and you are unable to surf
   the Internet or whatever, reduce it to 80-90% of "Download Capacity")
 
 Upload Limit: set this to ~80% of your "Upload Capacity" (so when your Upload Capacity is 16,
   set Upload Limit to 13)

   Setting Upload Limit to a value < 10 will automatically reduce your Download Limit, so
   upload as fast as you can.


NOTE: 56k Modem users: eMule only accepts integral values for these settings at the moment, you
      can't enter 1.6 or whatever your sweet-spot setting is, yet. Sorry :) Maybe later..



-"Maximum Connections": depends on your operating system. As a general rule...

  -Windows 95/98 and Windows ME users enter 50 here
  -Windows NT / 2000 / XP users leave this at 500

-"Maximum Sources per File": decide for yourself, how many you want :)

-Choose the directories you want to share with other users.
  DO NOT SHARE YOUR COMPLETE HARDDISK!
  Put the stuff you want to share in a seperate Folder.
  If you share more than ~200 files, you should reconsider that...

-The other options are pretty self-explaining. If you dunno what it does, don't touch it.

-Don't forget to connect to a server, or you won't probably download too much ;)




FAQ:
----

--"How do I know whether my ID is high or low?"

 Look at the arrow in the bottom right corner, next to the server name you are connected to.
 When it's green, your ID is high. When it's orange, your ID is low.
 


--"What does high and low ID mean anyway?"

 When your ID is high (green arrow), everything is fine :)
 When it's low (orange arrow), you are behind a firewall or router, and other clients can't
 connect to you directly (which is a bad thing). Plz read the FAQ or search the forums on 
 how to configure your firewall/router for eMule.

NOTE: you can also get a low ID when the server you connected to is too busy to answer
 properly, or simply badly configured. When you are sure your settings are ok and you SHOULD
 have a high ID, connect to antoher server.



--"I'd like to search for specific file types, what filter stands for which files?"

 File Type	Extensions found
 --------------------------------------------------------------------
 Audio		.mp3 .mp2 .mpc .wav .ogg .aac .mp4 .ape .au .wma
 Video		.avi .mpg .mpeg .ram .rm .vob .divx .mov .ogg .vivo
 Program	.exe .com
 Archive	.zip .rar .ace
 CDImage	.bin .cue .iso .nrg .ccd .sub .img
 Picture	.jpg .jpeg .bmp .gif .tif .png




COMPILING THE SOURCECODE:
-------------------------

The sourcecode of eMule is availible as seperate download at:
 http://sourceforge.net/projects/emule
You need Microsoft(C) Visual C++ .NET (7.0) to compile eMule.

-Unzip the sources (with subdirs) into a new folder
-Open the emule.sln Visual Studio Solution
-Build the solution. That's it :)
-If the compile was successful, the emule.exe is either in the
 \Debug or \Release directory
-You need additional .dll files to run emule.
 (zlib.dll for chunk compression, dbghelp.dll for debug builds)
 These are included in the binary release of eMule, but not
 in the sourcecode download.

-IMPORTANT:
 If you make modifications to eMule and you distribute the compiled version of your
 build, be sure to obey the GPL license. You have to include the sourcecode, or make
 it availible for download together with the binary version.




WWW LINKS:
----------

Official
http://sourceforge.net/projects/emule  (download your latest version of eMule here)
http://www.emule-project.net  (FAQ, forum, news and more)

Related (we are not responsible for their contents)
http://www.esel-community.de  (German eMule forum and news)
http://edonkey.bei.t-online.de  (German forum, great hints and FAQ section)
http://www.thedonkeynetwork.com  (news, statistics, serverlists and more)




MISC. STUFF:
------------

The eMule staff would like to thank the following (no particular order)

-Jed "swamp" McCaleb and MetaMachine for bringing the eDonkey network to life
-DrSiRiUs for the awesome artwork (splashscreen and icons)
-OCBmaurice & Stilmann for the serverlists and more (www.thedonkeynetwork.com)

-the people who sent us bug reports and useful suggestions :)




STAFF:
------

Who are the guys that sacrifice so much time to bring you this wonderful program?

-Merkur (merkur-@users.sourceforge.net) - Master of the Mule
-John aka. Unknown1 - Developer
-Ornis (ornis@web.de) - Developer
-Tecxx (tecxx@users.sourceforge.net) - Developer
-Monk and Sony666 (sony666@users.sourceforge.net) - Testers




LEGAL:
------

eMule Copyright (C)2002 Merkur (merkur-@users.sourceforge.net)

eDonkey2000 (C)Jed McCaleb, MetaMachine (www.eDonkey2000.com)

Windows(TM), Windows 95(TM), Windows 98(TM), Windows ME(TM),
Windows NT(TM), Windows 2000(TM) and Windows XP(TM)
are Copyright (C)Microsoft Corporation. All rights reserved.




Goodbye and happy sharing ;)


...oops, anything not ok with this file?
write me (sony666@users.sourceforge.net) or Merkur about it

\EOF