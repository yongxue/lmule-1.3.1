// OScopeCtrl.h : header file
//

#ifndef __OScopeCtrl_H__
#define __OScopeCtrl_H__

#include "mfc.h"
#include <wx/control.h>
#include <wx/dcmemory.h>
#include <wx/bitmap.h>
#include <wx/timer.h>
#define RGB(a,b,c) ((a&0xff)<<16|(b&0xff)<<8|(c&0xff))

/////////////////////////////////////////////////////////////////////////////
// COScopeCtrl window

#define TIMER_OSCOPE 7641

class COScopeCtrl : public wxControl
{
// Construction
public:
	COScopeCtrl( int NTrends=1, wxWindow* parent=NULL );

// Attributes
public:
	void AppendPoints(double dNewPoint[], bool bInvalidate = true, bool bAdd2List = true);
	void AppendEmptyPoints(double dNewPoint[], bool bInvalidate = true, bool bAdd2List = true);
	void SetRange(double dLower, double dUpper, int iTrend = 0);
	void SetRanges(double dLower, double dUpper);
	void SetXUnits(CString string, CString XMin = "", CString XMax = "");
	void SetYUnits(CString string, CString YMin = "", CString YMax = "");
	void SetGridColor(COLORREF color);
	void SetPlotColor(COLORREF color, int iTrend = 0);
	COLORREF GetPlotColor(int iTrend = 0);
	void SetBackgroundColor(COLORREF color);
	void InvalidateCtrl(bool deleteGraph = true);
	void DrawPoint();
	void Reset();

	// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(COScopeCtrl)
	public:
	//virtual BOOL Create(DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID=NULL);
	//}}AFX_VIRTUAL

 private:
	int oldwidth,oldheight;

// Implementation
public:
	bool drawBars;
	bool autofitYscale;
	int m_nXGrids;
	int m_nYGrids;
	int m_nShiftPixels;         // amount to shift with each new point 
	int m_nTrendPoints;			// when you set this to > 0, then plot will
								// contain that much points (regardless of the
								// Trend/control) width drawn on screen !!!

								// Otherwise, if this is -1 (which is default),
								// m_nShiftPixels will be in use
	int m_nMaxPointCnt;
	int m_nYDecimals;

	typedef struct m_str_struct {	
		CString XUnits, XMin, XMax;
		CString YUnits, YMin, YMax;
	} m_str_t;
	m_str_t m_str;

	COLORREF m_crBackColor;        // background color
	COLORREF m_crGridColor;        // grid color

	typedef struct PlotDataStruct {
		COLORREF crPlotColor;       // data plot color  
		wxPen   penPlot;
		double dCurrentPosition;    // current position
		double dPreviousPosition;   // previous position
		int nPrevY;
		double dLowerLimit;         // lower bounds
		double dUpperLimit;         // upper bounds
		double dRange;				// = UpperLimit - LowerLimit
		double dVerticalFactor;
	  //CList<double,double> lstPoints;
	  CList<float,float> lstPoints;
	} PlotData_t ;

	virtual ~COScopeCtrl();

	// Generated message map functions
protected:
	int m_NTrends;

#if 0
	//{{AFX_MSG(COScopeCtrl)
	afx_msg void OnPaint();
	afx_msg void OnSize(UINT nType, int cx, int cy); 
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
#endif
	  void OnPaint(wxPaintEvent& evt);
	void OnSize(wxSizeEvent& evt);

	  DECLARE_EVENT_TABLE()

	struct CustShiftStruct {		// when m_nTrendPoints > 0, this structure will contain needed vars
		int m_nRmndr;				// reminder after dividing m_nWidthToDo/m_nPointsToDo
		int m_nWidthToDo;
		int m_nPointsToDo;
	} CustShift;

	PlotData_t *m_PlotData; // !!! !!!

	int m_nClientHeight;
	int m_nClientWidth;
	int m_nPlotHeight;
	int m_nPlotWidth;

	RECT  m_rectClient;
	RECT  m_rectPlot;
	wxBrush m_brushBack;

	wxMemoryDC*     m_dcGrid;
	wxMemoryDC*     m_dcPlot;
	wxBitmap *m_pbitmapOldGrid;
	wxBitmap *m_pbitmapOldPlot;
	wxBitmap *m_bitmapGrid;
	wxBitmap *m_bitmapPlot;

	bool m_bDoUpdate;
	//UINT m_nRedrawTimer;
	wxTimer m_nRedrawTimer;
public:
	int ReCreateGraph(void);
	//afx_msg void OnTimer(UINT nIDEvent);
	void OnTimer(wxTimerEvent& evt);

	void GetPlotRect(RECT& rPlotRect)
	{
		rPlotRect = m_rectPlot;
	}
};

/////////////////////////////////////////////////////////////////////////////
#endif
