//this file is part of eMule
//Copyright (C)2002 Merkur ( merkur-@users.sourceforge.net / http://www.emule-project.net )
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


// emuleDlg.h : Headerdatei
//
#ifndef EMULEDLG_H
#define EMULEDLG_H

#include "sockets.h"
//#include "afxcmn.h"
//#include "afxwin.h"
#include "ServerListCtrl.h"
#include "KnownFileList.h"
#include "TransferWnd.h"
#include "ServerWnd.h"
#include "PreferencesDlg.h"
#include "SharedFilesWnd.h"
#include "SearchDlg.h"
#include "ChatWnd.h"
//#include "TrayDialog.h"
//#include "BtnST.h"
#include "StatisticsDlg.h"
//#include "MeterIcon.h"
//#include "IrcWnd.h"
//#include "TaskbarNotifier.h"
#include "SysTray.h"
#include <wx/toolbar.h>

enum APPState {APP_STATE_RUNNING=0,
	       APP_STATE_SHUTINGDOWN,
	       APP_STATE_DONE,
	       APP_STATE_STARTING
};

#define MP_RESTORE		4001
#define MP_CONNECT		4002
#define MP_DISCONNECT	4003
#define MP_EXIT			4004

#include "wx/dialog.h"
#include "wx/statusbr.h"
#include "wx/button.h"
#include "wx/frame.h"

class CKnownFileList; 
// CemuleDlg Dialogfeld
class CemuleDlg :public wxFrame //CTrayDialog
{
// Konstruktion
public:
	CemuleDlg(wxWindow* pParent = NULL);	// Standardkonstruktor
	~CemuleDlg();
	enum { IDD = IDD_EMULE_DIALOG };

//	void			AddLogLine(bool addtostatusbar,char* line,...);
	void			AddLogLine(bool addtostatusbar,const wxChar* line,...);
	void			AddDebugLogLine(bool addtostatusbar,const wxChar* line,...);
	void			AddServerMessageLine(char* line,...);
	void			ShowConnectionState(bool connected);
	void			ShowConnectionState(bool connected,wxString server,bool iconOnly=false);
	void			ShowNotifier(wxString Text, int MsgType, bool ForceSoundOFF = false); 
	void			ShowUserCount(uint32 toshow,uint32 filetoshow);
	void			ShowMessageState(uint8 iconnr);
	void			SetActiveDialog(wxWindow* dlg);
	void			ShowTransferRate(bool forceAll=true);
	void			ShowStatistics();
	void			Localize();
	void			ResetLog();
	void			ResetDebugLog();
	void			StopTimer();
	// Barry - To find out if app is running or shutting/shut down
	bool			IsRunning();
	void			DoVersioncheck(bool manual);
	// has to be done in own method
	void createSystray();
	void changeDesktopMode();

	CTransferWnd*	transferwnd;
	CServerWnd*		serverwnd;
	CPreferencesDlg* preferenceswnd;
	//CPreferencesDlg	preferenceswnd;
	CSharedFilesWnd*	sharedfileswnd;
	CSearchDlg*		searchwnd;
	CChatWnd*		chatwnd;
	wxStatusBar  statusbar;
	//wxDialog*		activewnd;
	wxWindow* activewnd;
	CStatisticsDlg*  statisticswnd;
	//CIrcWnd			ircwnd;
	//CTaskbarNotifier m_wndTaskbarNotifier;
	CSysTray *m_wndTaskbarNotifier;
	volatile APPState		m_app_state;	// added volatile to get some more security when accessing this as a shared object...
	uint8			status;
	uint16                  lastbutton;

	DECLARE_EVENT_TABLE()

protected:
	void socketHandler(wxSocketEvent& event);
	void OnUQTimer(wxTimerEvent& evt);
	void OnUDPTimer(wxTimerEvent& evt);
	void OnSocketTimer(wxTimerEvent& evt);
	void OnQLTimer(wxTimerEvent& evt);

	void btnServers(wxEvent& ev);
	void btnSearch(wxEvent& ev);
	void btnTransfer(wxEvent& ev);
	void btnPreferences(wxEvent& ev);
	void OnBnShared(wxEvent& ev);
	void OnBnStats(wxEvent& ev);
	void OnBnMessages(wxEvent& ev);

 public:
	void OnFinishedHashing(wxCommandEvent& evt);
	void OnDnsDone(wxCommandEvent& evt);

	void InitDialog();
	//HICON m_hIcon;
	/*
	virtual void DoDataExchange(CDataExchange* pDX);
	virtual BOOL OnInitDialog();
	virtual void OnTrayRButtonDown(CPoint pt);

	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedButton2();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);

	afx_msg void OnBnClicked2();
	afx_msg void OnBnClicked3();
	afx_msg void OnBnClicked4();
	afx_msg void OnBnClicked5();
	afx_msg void OnBnClicked6();
	afx_msg void OnBnClicked7();
	afx_msg void OnBnClicked8();
	afx_msg void OnBnClicked9();

	afx_msg LRESULT OnTaskbarNotifierClicked(WPARAM wParam,LPARAM lParam);
	afx_msg LRESULT OnWMData(WPARAM wParam,LPARAM lParam);
	afx_msg LRESULT OnFileHashed(WPARAM wParam,LPARAM lParam);
	virtual void OnSize(UINT nType,int cx,int cy);
	void		 OnOK()			{}
	void		 OnCancel();
	void		 OnClose();
	DECLARE_MESSAGE_MAP()
	*/
	void OnBnConnect(wxEvent& evt);
	void OnClose(wxCloseEvent& evt);
private:
	wxString			logtext;
	bool			ready;	
	wxBitmap transicons[4];
	/*
	HICON			connicons[3];
	HICON			transicons[4];
	HICON			imicons[3];
	HICON			mytrayIcon;
	HICON			usericon;

	CMeterIcon trayIcon;
	HICON sourceTrayIcon;
	HICON sourceTrayIconGrey;
	*/
	uint32			lastuprate;
	uint32			lastdownrate;
	wxImageList		imagelist;
	wxMenu		trayPopup;

	wxButton	m_btnConnect;
	wxButton	m_btnDownloads;
	wxButton	m_btnServers;
	wxButton	m_btnSearch;
	wxButton	m_btnFiles;
	wxButton	m_btnPreferences;
	wxButton	m_btnMessages;
	wxButton	m_btnStatistics;
	wxButton	m_btnIrc;
	wxToolBar*      m_wndToolbar;

	void StartConnection();
	void CloseConnection();
	void RestoreWindow();
	void UpdateTrayIcon(int procent);
};


#endif
