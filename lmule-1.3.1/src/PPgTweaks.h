#ifndef PPG_TWEAKS_H
#define PPG_TWEAKS_H

#include "Preferences.h"
#include "Wizard.h"

#include <wx/panel.h>
// CPPgTweaks dialog

class CPPgTweaks : public wxPanel
{
  DECLARE_DYNAMIC_CLASS(CPPgTweaks)
	//DECLARE_DYNAMIC(CPPgTweaks)

    CPPgTweaks() {};
public:
	CPPgTweaks(wxWindow* parent);
	virtual ~CPPgTweaks();

	void SetPrefs(CPreferences* in_prefs) {	app_prefs = in_prefs;}

// Dialog Data
	enum { IDD = IDD_PPG_TWEAKS };
protected:
	CPreferences *app_prefs;
	uint8 m_iFileBufferSize;
	uint8 m_iQueueSize;
	wxPanel* _panel;

#if 0
	//{{AFX_MSG(CSpinToolBar)
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	//}}AFX_MSG

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
#endif
	  DECLARE_EVENT_TABLE()

	  void OnHScroll(wxScrollEvent& evt);
public:
	virtual BOOL OnInitDialog();
	void LoadSettings(void);
	virtual BOOL OnApply();
	//afx_msg void OnSettingsChange()					{ SetModified(); }

	void Localize(void);

};

#endif
