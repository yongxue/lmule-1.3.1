//this file is part of eMule
//Copyright (C)2002 Merkur ( merkur-@users.sourceforge.net / http://www.emule-project.net )
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


// SharedFilesWnd.cpp : implementation file
//

//#include "stdafx.h"
#include "wintypes.h"
#include "emule.h"
#include "SharedFilesWnd.h"
#include "otherfunctions.h"
#include "muuli_wdr.h"


// CSharedFilesWnd dialog

BEGIN_EVENT_TABLE(CSharedFilesWnd,wxPanel)
  EVT_LIST_ITEM_SELECTED(ID_SHFILELIST,CSharedFilesWnd::OnLvnItemActivateSflist)
  EVT_BUTTON(IDC_RELOADSHAREDFILES, CSharedFilesWnd::OnBnClickedReloadsharedfiles)
END_EVENT_TABLE()

//IMPLEMENT_DYNAMIC(CSharedFilesWnd, CDialog)
CSharedFilesWnd::CSharedFilesWnd(wxWindow* pParent /*=NULL*/)
	: wxPanel(pParent,CSharedFilesWnd::IDD)
{
	//shownFileHash[0]=0;
  SetBackgroundColour(GetColour(wxSYS_COLOUR_WINDOW));
  wxSizer* content=sharedfilesDlg(this,TRUE);
  content->Show(this,TRUE);

  pop_bar=(wxGauge*)FindWindowByName("popbar");
  pop_baraccept=(wxGauge*)FindWindowByName("popbarAccept");
  pop_bartrans=(wxGauge*)FindWindowByName("popbarTrans");

  // can't do here. 
  //theApp.sharedfiles->SetOutputCtrl((CSharedFilesCtrl*)FindWindowByName("sharedFilesCt"));
  sharedfilesctrl=(CSharedFilesCtrl*)(FindWindowByName("sharedFilesCt"));
}

CSharedFilesWnd::~CSharedFilesWnd()
{
}

#if 0
void CSharedFilesWnd::DoDataExchange(CDataExchange* pDX){
	CResizableDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_SFLIST, sharedfilesctrl);
	DDX_Control(pDX, IDC_POPBAR, pop_bar);
	DDX_Control(pDX, IDC_POPBAR2, pop_baraccept);
	DDX_Control(pDX, IDC_POPBAR3, pop_bartrans);
	DDX_Control(pDX, IDC_STATISTICS, m_ctrlStatisticsFrm);
}
#endif

#if 0
BOOL CSharedFilesWnd::OnInitDialog(){
	CResizableDialog::OnInitDialog();
	Localize();
	sharedfilesctrl.Init();
	theApp.sharedfiles->SetOutputCtrl(&sharedfilesctrl);
	
	pop_bar.SetGradientColors(RGB(255,255,240),RGB(255,255,0));
	pop_bar.SetTextColor(RGB(20,70,255));
	pop_baraccept.SetGradientColors(RGB(255,255,240),RGB(255,255,0));
	pop_baraccept.SetTextColor(RGB(20,70,255));
	pop_bartrans.SetGradientColors(RGB(255,255,240),RGB(255,255,0));
	pop_bartrans.SetTextColor(RGB(20,70,255));
	
	((CStatic*)GetDlgItem(IDC_FILES_ICO))->SetIcon((HICON)::LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_SHAREDFILES), IMAGE_ICON, 16, 16, 0));
	
	LOGFONT lf;
	GetFont()->GetLogFont(&lf);
	lf.lfWeight = FW_BOLD;
	bold.CreateFontIndirect(&lf);
	m_ctrlStatisticsFrm.SetFont(&bold);
	m_ctrlStatisticsFrm.Init(IDI_SMALLSTATISTICS);
	GetDlgItem(IDC_CURSESSION_LBL)->SetFont(&bold);
	GetDlgItem(IDC_TOTAL_LBL)->SetFont(&bold);
	
	AddAnchor(IDC_FILES_ICO, TOP_LEFT);
	AddAnchor(IDC_TRAFFIC_TEXT, TOP_LEFT);
	AddAnchor(IDC_SFLIST,TOP_LEFT,BOTTOM_RIGHT);
	AddAnchor(IDC_RELOADSHAREDFILES, BOTTOM_RIGHT);
	AddAnchor(IDC_STATISTICS,BOTTOM_LEFT);
	AddAnchor(IDC_CURSESSION_LBL, BOTTOM_LEFT);
	AddAnchor(IDC_TOTAL_LBL, BOTTOM_LEFT);
	AddAnchor(IDC_FSTATIC4, BOTTOM_LEFT);
	AddAnchor(IDC_SREQUESTED,BOTTOM_LEFT);
	AddAnchor(IDC_POPBAR,BOTTOM_LEFT);
	AddAnchor(IDC_FSTATIC7,BOTTOM_LEFT);
	AddAnchor(IDC_SREQUESTED2,BOTTOM_LEFT);
	AddAnchor(IDC_FSTATIC5,BOTTOM_LEFT);
	AddAnchor(IDC_SACCEPTED,BOTTOM_LEFT);
	AddAnchor(IDC_POPBAR2,BOTTOM_LEFT);
	AddAnchor(IDC_FSTATIC8,BOTTOM_LEFT);
	AddAnchor(IDC_SACCEPTED2,BOTTOM_LEFT);
	AddAnchor(IDC_FSTATIC6,BOTTOM_LEFT);
	AddAnchor(IDC_STRANSFERED,BOTTOM_LEFT);
	AddAnchor(IDC_POPBAR3,BOTTOM_LEFT);
	AddAnchor(IDC_FSTATIC9,BOTTOM_LEFT);
	AddAnchor(IDC_STRANSFERED2,BOTTOM_LEFT);
	
	return true;
}

BEGIN_MESSAGE_MAP(CSharedFilesWnd, CResizableDialog)
	ON_BN_CLICKED(IDC_RELOADSHAREDFILES, OnBnClickedReloadsharedfiles)
	ON_NOTIFY(LVN_ITEMACTIVATE, IDC_SFLIST, OnLvnItemActivateSflist)
	ON_NOTIFY(NM_CLICK, IDC_SFLIST, OnNMClickSflist)
END_MESSAGE_MAP()
#endif

// CSharedFilesWnd message handlers

void CSharedFilesWnd::OnBnClickedReloadsharedfiles()
{
	printf("debug: Reload Button Clicked\n");
	theApp.sharedfiles->Reload(true, true);
}

#if 0
void CSharedFilesWnd::Check4StatUpdate(CKnownFile* file){
	if (!memcmp(file->GetFileHash(),shownFileHash ,16)) ShowDetails(file);
}
#endif

void CSharedFilesWnd::Check4StatUpdate(CKnownFile* file){
	if (!memcmp(file->GetFileHash(),shownFileHash ,16)) ShowDetails(file);
}

void CSharedFilesWnd::OnLvnItemActivateSflist(wxListEvent& evt)
{
  long item=-1;
  wxODListCtrl* sflist=wxStaticCast(FindWindowById(ID_SHFILELIST),wxODListCtrl);
  item=sflist->GetNextItem(item,wxLIST_NEXT_ALL,wxLIST_STATE_SELECTED);

	if (item != (-1) ) {
		CKnownFile* cur_file = (CKnownFile*)sflist->GetItemData(item);

		ShowDetails(cur_file);
	}
}


#define GetDlgItem(X) (wxStaticCast(FindWindowById((X)),wxStaticText))
void CSharedFilesWnd::ShowDetails(CKnownFile* cur_file) {
	char buffer[100];

	//pop_bartrans.SetRange32(0,theApp.knownfiles->transfered/1024);
	pop_bartrans->SetRange(theApp.knownfiles->transfered/1024);
	//pop_bartrans.SetPos(cur_file->statistic.GetTransfered()/1024);
	pop_bartrans->SetValue(cur_file->statistic.GetTransfered()/1024);
	//pop_bartrans.SetShowPercent();				
	GetDlgItem(IDC_STRANSFERED)->SetLabel(CastItoXBytes(cur_file->statistic.GetTransfered()));

	pop_bar->SetRange(theApp.knownfiles->requested);
	pop_bar->SetValue(cur_file->statistic.GetRequests());
	//pop_bar.SetShowPercent();			
	sprintf(buffer,"%u",cur_file->statistic.GetRequests());
	GetDlgItem(IDC_SREQUESTED)->SetLabel(buffer);

	sprintf(buffer,"%u",cur_file->statistic.GetAccepts());
	pop_baraccept->SetRange(theApp.knownfiles->accepted);
	pop_baraccept->SetValue(cur_file->statistic.GetAccepts());
	//pop_baraccept.SetShowPercent();
	GetDlgItem(IDC_SACCEPTED)->SetLabel(buffer);
	
	GetDlgItem(IDC_STRANSFERED2)->SetLabel(CastItoXBytes(cur_file->statistic.GetAllTimeTransfered()));

	sprintf(buffer,"%u",cur_file->statistic.GetAllTimeRequests());
	GetDlgItem(IDC_SREQUESTED2)->SetLabel(buffer);

	sprintf(buffer,"%u",cur_file->statistic.GetAllTimeAccepts());
	GetDlgItem(IDC_SACCEPTED2)->SetLabel(buffer);

	memcpy(shownFileHash,cur_file->GetFileHash(),16);

	//CString title=GetResString(IDS_SF_STATISTICS)+" ("+ cur_file->GetFileName() +")";
	//GetDlgItem(IDC_FSTATIC1)->SetWindowText( title );
}

#if 0
void CSharedFilesWnd::OnNMClickSflist(NMHDR *pNMHDR, LRESULT *pResult){
	OnLvnItemActivateSflist(pNMHDR,pResult);
	*pResult = 0;
}

BOOL CSharedFilesWnd::PreTranslateMessage(MSG* pMsg) 
{
   if((pMsg->message == WM_KEYUP)){
	   if (pMsg->hwnd == GetDlgItem(IDC_SFLIST)->m_hWnd)
			OnLvnItemActivateSflist(0,0);
   }
   return CResizableDialog::PreTranslateMessage(pMsg);
}
#endif

void CSharedFilesWnd::Localize(){
#if 0
	sharedfilesctrl.Localize();
	GetDlgItem(IDC_TRAFFIC_TEXT)->SetWindowText(GetResString(IDS_SF_FILES));
	GetDlgItem(IDC_RELOADSHAREDFILES)->SetWindowText(GetResString(IDS_SF_RELOAD));
	m_ctrlStatisticsFrm.SetText(GetResString(IDS_SF_STATISTICS));
	GetDlgItem(IDC_CURSESSION_LBL)->SetWindowText(GetResString(IDS_SF_CURRENT));
	GetDlgItem(IDC_TOTAL_LBL)->SetWindowText(GetResString(IDS_SF_TOTAL));
	GetDlgItem(IDC_FSTATIC6)->SetWindowText(GetResString(IDS_SF_TRANS));
	GetDlgItem(IDC_FSTATIC5)->SetWindowText(GetResString(IDS_SF_ACCEPTED));
	GetDlgItem(IDC_FSTATIC4)->SetWindowText(GetResString(IDS_SF_REQUESTED));
	GetDlgItem(IDC_FSTATIC9)->SetWindowText(GetResString(IDS_SF_TRANS));
	GetDlgItem(IDC_FSTATIC8)->SetWindowText(GetResString(IDS_SF_ACCEPTED));
	GetDlgItem(IDC_FSTATIC7)->SetWindowText(GetResString(IDS_SF_REQUESTED));
#endif
}
