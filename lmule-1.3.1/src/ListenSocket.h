//this file is part of eMule
//Copyright (C)2002 Merkur ( merkur-@users.sourceforge.net / http://www.emule-project.net )
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

// handling incoming connections (up or downloadrequests)

#ifndef LISTENSOCKET_H
#define LISTENSOCKET_H

#include "Preferences.h"
#include "packets.h"
#include "EMSocket.h"

#include <wx/socket.h>

class CUpDownClient;
class CPacket;
class CTimerWnd;

//class CClientReqSocket;

class CClientReqSocket : public CEMSocket
{
  friend class CClientSocket;

  DECLARE_DYNAMIC_CLASS(CClientReqSocket)
    
    CClientReqSocket(); /*{};*/
 public:

  CClientReqSocket(CPreferences* in_prefs, CUpDownClient* in_client = 0);	
  ~CClientReqSocket();
  void	Disconnect();
  
 void	ResetTimeOutTimer();
  bool	CheckTimeOut();
  void	Safe_Delete();
  
  long	deletethis;

  bool	Create();
  void		 OnClose(int nErrorCode);
  void		 OnSend(int nErrorCode);
  void		 OnReceive(int nErrorCode);
  void		 OnError(int nErrorCode);
  virtual	void OnInit();
  virtual bool Close(); /*	{return wxSocketBase::Close();}*/

  protected:
  void		 PacketReceived(Packet* packet);
  private:
  void	Delete_Timed();
  bool	ProcessPacket(char* packet, uint32 size,uint8 opcode);
  bool	ProcessExtPacket(char* packet, uint32 size,uint8 opcode);

 public:
  uint32	timeout_timer;
  uint32	deltimer;  
  CUpDownClient*	client;
  CPreferences* app_prefs;
};

#include <wx/list.h>
//WX_DECLARE_LIST(CClientReqSocket,SocketListL);

// CListenSocket command target
class CListenSocket : public wxSocketServer{
  DECLARE_DYNAMIC_CLASS(CListenSocket)

public:
	CListenSocket() : wxSocketServer(happyCompiler) {};
	CListenSocket(CPreferences* in_prefs,wxSockAddress& addr);
	~CListenSocket();
	bool	StartListening();
	void	StopListening();
	virtual void OnAccept(int nErrorCode);
	void	Process();
	void	RemoveSocket(CClientReqSocket* todel);
	void	AddSocket(CClientReqSocket* toadd);
	uint16	GetOpenSockets()		{return socket_list.GetCount();}
	void	KillAllSockets();
	bool	TooManySockets(bool bIgnoreInterval = false);
	uint32	GetMaxConnectionReached()	{return maxconnectionreached;}
	bool    IsValidSocket(CClientReqSocket* totest);
	void	AddConnection();
	void	RecalculateStats();
	void	ReStartListening();

	void	Debug_ClientDeleted(CUpDownClient* deleted);
private:
	bool bListening;
	CPreferences* app_prefs;
	CTypedPtrList<CPtrList, CClientReqSocket*> socket_list;
	//SocketListL socket_list;
	uint16 opensockets;
	uint16 m_OpenSocketsInterval;
	uint32 maxconnectionreached;
	wxIPV4address happyCompiler;
	uint16	m_ConnectionStates[3];
	uint16	m_nPeningConnections;
};


#endif
