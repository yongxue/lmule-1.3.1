#ifndef PPGFILES_H
#define PPGFILES_H

// CPPgFiles dialog
#include <wx/panel.h>

#include "Preferences.h"

class CPPgFiles : public wxPanel //CPropertyPage
{
  //DECLARE_DYNAMIC(CPPgFiles)
  DECLARE_DYNAMIC_CLASS(CPPgFiles)
    CPPgFiles() {};

public:
	CPPgFiles(wxWindow* parent);
	virtual ~CPPgFiles();

	void SetPrefs(CPreferences* in_prefs) {	app_prefs = in_prefs; }

// Dialog Data
	enum { IDD = IDD_PPG_FILES };
protected:
	CPreferences *app_prefs;
protected:
	//virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	//DECLARE_MESSAGE_MAP()
public:
	//virtual BOOL OnInitDialog();
 public:
	void LoadSettings(void);
public:
#if 0
	virtual BOOL OnApply();
	afx_msg void OnBnClickedSeeshare1()	{ SetModified(); }
	afx_msg void OnBnClickedSeeshare2()	{ SetModified(); }
	afx_msg void OnBnClickedSeeshare3()	{ SetModified(); }
	afx_msg void OnBnClickedIch()		{ SetModified(); }
#endif
	virtual void OnApply();

	void Localize(void);
};

#endif
