
// ColorFrameCtrl.h : header file
//

#ifndef __ColorFrameCtrl_H__
#define __ColorFrameCtrl_H__

#include <wx/brush.h>
#ifndef RGB
#define RGB(a,b,c) ((a&0xff)<<16|(b&0xff)<<8|(c&0xff))
#endif

/////////////////////////////////////////////////////////////////////////////
// CColorFrameCtrl window

#include <wx/control.h>

class CColorFrameCtrl : public wxControl
{
// Construction
public:
	CColorFrameCtrl( wxWindow* parent, int id,int wid,int hei );

// Attributes
public:
	void SetFrameColor(COLORREF color);
	void SetBackgroundColor(COLORREF color);

	// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CColorFrameCtrl)
	public:
	//virtual BOOL Create(DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID=NULL);
	//}}AFX_VIRTUAL

// Implementation
public:
	COLORREF m_crBackColor;        // background color
	COLORREF m_crFrameColor;       // frame color

	virtual ~CColorFrameCtrl();

	// Generated message map functions
protected:
	//{{AFX_MSG(CColorFrameCtrl)
	//afx_msg void OnPaint();
	void OnPaint(wxPaintEvent& evt);
	void OnSize(wxSizeEvent& evt);
	//ax_msg void OnSize(UINT nType, int cx, int cy); 
	//}}AFX_MSG
	//DECLARE_MESSAGE_MAP()
	DECLARE_EVENT_TABLE()

	  //CRect  m_rectClient;
	  //CBrush m_brushBack;
	  //CBrush m_brushFrame;
	  RECT m_rectClient;
	wxBrush m_brushBack,m_brushFrame;
};

/////////////////////////////////////////////////////////////////////////////
#endif
