//this file is part of eMule
//Copyright (C)2002 Merkur ( merkur-@users.sourceforge.net / http://www.emule-project.net )
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

// ChatWnd.cpp : implementation file
//
//#include "stdafx.h"
#include "wintypes.h"
#include "emule.h"
#include "ChatWnd.h"
//#include "HyperTextCtrl.h"
#include "muuli_wdr.h"


// CChatWnd dialog

//IMPLEMENT_DYNAMIC(CChatWnd, CDialog)
CChatWnd::CChatWnd(wxWindow* pParent /*=NULL*/)
  //: CResizableDialog(CChatWnd::IDD, pParent)
  : wxPanel(pParent,CChatWnd::IDD)
{
  SetBackgroundColour(GetColour(wxSYS_COLOUR_WINDOW));
  wxSizer* content=messagePage(this,TRUE);
  content->Show(this,TRUE);
}

CChatWnd::~CChatWnd()
{
}

#if 0
void CChatWnd::DoDataExchange(CDataExchange* pDX)
{
	CResizableDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CHATSEL, chatselector);
}


BEGIN_MESSAGE_MAP(CChatWnd, CResizableDialog)
	ON_BN_CLICKED(IDC_CSEND, OnBnClickedCsend)
	ON_WM_KEYDOWN()
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_CCLOSE, OnBnClickedCclose)
END_MESSAGE_MAP()


// CChatWnd message handlers

BOOL CChatWnd::OnInitDialog(){
	CResizableDialog::OnInitDialog();
	Localize();
	chatselector.Init();
	((CStatic*)GetDlgItem(IDC_MESSAGEICON))->SetIcon((HICON)::LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_MESSAGE), IMAGE_ICON, 16, 16, 0));
	((CStatic*)GetDlgItem(IDC_FRIENDSICON))->SetIcon((HICON)::LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_FRIEND), IMAGE_ICON, 16, 16, 0));

	AddAnchor(IDC_CHATSEL,TOP_LEFT,BOTTOM_RIGHT);
	AddAnchor(chatselector.chatout,TOP_LEFT,BOTTOM_RIGHT);
	AddAnchor(IDC_CMESSAGE,BOTTOM_LEFT,BOTTOM_RIGHT);
	AddAnchor(IDC_CSEND,BOTTOM_RIGHT);
	AddAnchor(IDC_CCLOSE,BOTTOM_RIGHT);
	AddAnchor(IDC_LIST2,TOP_RIGHT,BOTTOM_RIGHT);
	AddAnchor(IDC_FRIENDS_LBL,TOP_RIGHT);
	AddAnchor(IDC_FRIENDSICON,TOP_RIGHT);

	return true;
}
#endif

void CChatWnd::StartSession(CUpDownClient* client){
#if 0
	if (!client->GetUserName())
		return;
	theApp.emuledlg->SetActiveDialog(this);
	chatselector.StartSession(client,true);
#endif
}

#if 0
void CChatWnd::OnBnClickedCsend(){
	uint16 len = GetDlgItem(IDC_CMESSAGE)->GetWindowTextLength()+2;
	char* messagetosend = new char[len+1];
	GetDlgItem(IDC_CMESSAGE)->GetWindowText(messagetosend,len);
	if (chatselector.SendMessage(messagetosend))
		GetDlgItem(IDC_CMESSAGE)->SetWindowText("");
	delete[] messagetosend;
}

void CChatWnd::OnShowWindow(BOOL bShow,UINT nStatus){
	if (bShow)
		chatselector.ShowChat();
}

void CChatWnd::OnBnClickedCclose(){
	chatselector.EndSession();
}

BOOL CChatWnd::PreTranslateMessage(MSG* pMsg) 
{
   if((pMsg->message == WM_KEYDOWN) && (pMsg->wParam == 13)){
	   if (pMsg->hwnd == GetDlgItem(IDC_CMESSAGE)->m_hWnd)
			OnBnClickedCsend();
   }

   return CResizableDialog::PreTranslateMessage(pMsg);
}
#endif

void CChatWnd::Localize(){
#if 0
	GetDlgItem(IDC_MESSAGES_LBL)->SetWindowText(GetResString(IDS_CW_MESSAGES));
	GetDlgItem(IDC_FRIENDS_LBL)->SetWindowText(GetResString(IDS_CW_FRIENDS));
	GetDlgItem(IDC_CSEND)->SetWindowText(GetResString(IDS_CW_SEND));
	GetDlgItem(IDC_CCLOSE)->SetWindowText(GetResString(IDS_CW_CLOSE));
#endif
}
