// Ini.h: Schnittstelle f�r die Klasse CIni.

// Autor: Michael Schikora
// Mail:  schiko@schikos.de
//
// If you found this code useful,
// please let me know
//
// How to use:
//
//
//void CMyClass::UpdateFromIni(BOOL bFromIni)
//{
//   CIni ini(m_strFileName,m_strSection);
//   ini.SER_GET(bFromIni,m_nValueXY); 
//   ini.SER_GET(bFromIni,m_strValue);
//   ini.SER_ARR(bFromIni,m_arValue,MAX_AR); 
//   ini.SER_ARR(bFromIni,m_ar3D,3);
//   //ore with default values 
//   ini.SER_GETD(bFromIni,m_nValueXY,5); 
//   ini.SER_GETD(bFromIni,m_strValue,"Hello");
//   ini.SER_ARRD(bFromIni,m_arValue,MAX_AR,10); 
//   ini.SER_ARRD(bFromIni,m_ar3D,3,5); 
//}


#if !defined(AFX_INI_H__EEBAF800_182A_11D3_B51F_00104B4A13B4__INCLUDED_)
#define AFX_INI_H__EEBAF800_182A_11D3_B51F_00104B4A13B4__INCLUDED_

#include <wx/string.h>
#include <wx/window.h>

#define RGB(a,b,c) ((a&0xff)<<16|(b&0xff)<<8|(c&0xff))

#define SER_GET(bGet,value) SerGet(bGet,value,#value)
#define SER_ARR(bGet,value,n) SerGet(bGet,value,n,#value)
#define SER_GETD(bGet,value,default) SerGet(bGet,value,#value,NULL,default)
#define SER_ARRD(bGet,value,n,default) SerGet(bGet,value,n,#value,default)


class CIni  
{
public:

#ifdef __NEVER_DEFINED__
   // MAKRO: SerGet(bGet,value,#value)
   int SER_GET(BOOL bGet,int value);
   // MAKRO: SerGet(bGet,value,n,#value)
   int SER_ARR(bGet,int* value,int n);
#endif
   // If the IniFilename contains no path,
   // the module-directory will be add to the FileName,
   // to avoid storing in the windows-directory
   // bModulPath=TRUE: ModulDir, bModulPath=FALSE: CurrentDir
   static void AddModulPath(wxString& strFileName,BOOL bModulPath = TRUE);
   static wxString GetDefaultSection();
   static wxString GetDefaultIniFile(BOOL bModulPath = TRUE);

	CIni( BOOL bModulPath = TRUE);
	CIni(CIni const& Ini, BOOL bModulPath = TRUE);
	CIni(wxString const& strFileName, BOOL bModulPath = TRUE);
	CIni(wxString const& strFileName, wxString const& strSection, BOOL bModulPath = TRUE);
	virtual ~CIni();

	void SetFileName(wxString const& strFileName);
	void SetSection(wxString const& strSection);
	wxString const& GetFileName() const;
	wxString const& GetSection() const;
private:
	void Init(LPCSTR strIniFile, LPCSTR strSection = NULL);
public:
	wxString		GetString(wxString strEntry,	LPCSTR strDefault=NULL,					LPCSTR strSection = NULL);

	double		GetDouble(wxString strEntry,	double fDefault = 0.0,					LPCSTR strSection = NULL);
	float		GetFloat(wxString strEntry,	float fDefault = 0.0,					LPCSTR strSection = NULL);
	int			GetInt(wxString strEntry,	int nDefault = 0,						LPCSTR strSection = NULL);
	WORD		GetWORD(wxString strEntry,	WORD nDefault = 0,						LPCSTR strSection = NULL);
	BOOL		GetBool(wxString strEntry,	BOOL bDefault = FALSE,					LPCSTR strSection = NULL);
	wxPoint		GetPoint(wxString strEntry,	wxPoint ptDefault = wxPoint(0,0),			LPCSTR strSection = NULL);
	wxRect		GetRect(wxString strEntry,	wxRect rectDefault = wxRect(0,0,0,0),		LPCSTR strSection = NULL);
	COLORREF	GetColRef(wxString strEntry,	COLORREF crDefault = 0x808080,	LPCSTR strSection = NULL);

	void		WriteString(wxString strEntry,wxString	str,		LPCSTR strSection = NULL);
	void		WriteDouble(wxString strEntry,double		f,			LPCSTR strSection = NULL);
	void		WriteFloat(wxString strEntry,float		f,			LPCSTR strSection = NULL);
	void		WriteInt(wxString strEntry,int			n,			LPCSTR strSection = NULL);
	void		WriteWORD(wxString strEntry,WORD		n,			LPCSTR strSection = NULL);
	void		WriteBool(wxString strEntry,BOOL			b,			LPCSTR strSection = NULL);
	void		WritePoint(wxString strEntry,wxPoint		pt,			LPCSTR strSection = NULL);
	void		WriteRect(wxString strEntry,wxRect		rect,		LPCSTR strSection = NULL);
	void		WriteColRef(wxString strEntry,COLORREF	cr,			LPCSTR strSection = NULL);

	void		SerGetString(	BOOL bGet,wxString	& str,	wxString strEntry,	LPCSTR strSection = NULL,	LPCSTR strDefault=NULL);
	void		SerGetDouble(	BOOL bGet,double	& f,	wxString strEntry,	LPCSTR strSection = NULL,	double fDefault = 0.0);
	void		SerGetFloat(	BOOL bGet,float		& f,	wxString strEntry,	LPCSTR strSection = NULL,	float fDefault = 0.0);
	void		SerGetInt(		BOOL bGet,int		& n,	wxString strEntry,	LPCSTR strSection = NULL,	int nDefault = 0);
	void		SerGetDWORD(	BOOL bGet,DWORD		& n,	wxString strEntry,	LPCSTR strSection = NULL,	DWORD nDefault = 0);
	void		SerGetBool(		BOOL bGet,BOOL		& b,	wxString strEntry,	LPCSTR strSection = NULL,	BOOL bDefault = FALSE);
	void		SerGetPoint(	BOOL bGet,wxPoint	& pt,	wxString strEntry,	LPCSTR strSection = NULL,	wxPoint ptDefault = wxPoint(0,0));
	void		SerGetRect(		BOOL bGet,wxRect		& rect,	wxString strEntry,	LPCSTR strSection = NULL,	wxRect rectDefault = wxRect(0,0,0,0));
	void		SerGetColRef(	BOOL bGet,COLORREF	& cr,	wxString strEntry,	LPCSTR strSection = NULL,	COLORREF crDefault = RGB(128,128,128));

	void		SerGet(	BOOL bGet,wxString	& str,	wxString strEntry,	LPCSTR strSection = NULL,	LPCSTR strDefault=NULL);
	void		SerGet(	BOOL bGet,double	& f,	wxString strEntry,	LPCSTR strSection = NULL,	double fDefault = 0.0);
	void		SerGet(	BOOL bGet,float		& f,	wxString strEntry,	LPCSTR strSection = NULL,	float fDefault = 0.0);
	void		SerGet(	BOOL bGet,int		& n,	wxString strEntry,	LPCSTR strSection = NULL,	int nDefault = 0);
	void		SerGet(	BOOL bGet,short		& n,	wxString strEntry,	LPCSTR strSection = NULL,	int nDefault = 0);
	void		SerGet(	BOOL bGet,DWORD		& n,	wxString strEntry,	LPCSTR strSection = NULL,	DWORD nDefault = 0);
	void		SerGet(	BOOL bGet,WORD		& n,	wxString strEntry,	LPCSTR strSection = NULL,	DWORD nDefault = 0);
//	void		SerGet(	BOOL bGet,BOOL		& b,	wxString strEntry,	LPCSTR strSection = NULL,	BOOL bDefault = FALSE);
	void		SerGet(	BOOL bGet,wxPoint	& pt,	wxString strEntry,	LPCSTR strSection = NULL,	wxPoint ptDefault = wxPoint(0,0));
	void		SerGet(	BOOL bGet,wxRect		& rect,	wxString strEntry,	LPCSTR strSection = NULL,	wxRect rectDefault = wxRect(0,0,0,0));
//	void		SerGet(	BOOL bGet,COLORREF	& cr,	wxString strEntry,	LPCSTR strSection = NULL,	COLORREF crDefault = RGB(128,128,128));
   
//ARRAYs
	void		SerGet(	BOOL bGet,wxString	* str,	int nCount, wxString strEntry, LPCSTR strSection = NULL, LPCSTR strDefault=NULL);
	void		SerGet(	BOOL bGet,double	* f,	int nCount, wxString strEntry, LPCSTR strSection = NULL, double fDefault = 0.0);
	void		SerGet(	BOOL bGet,float		* f,	int nCount, wxString strEntry, LPCSTR strSection = NULL, float fDefault = 0.0);
	void		SerGet(	BOOL bGet,unsigned char	* n,int nCount, wxString strEntry, LPCSTR strSection = NULL, unsigned char nDefault = 0);
	void		SerGet(	BOOL bGet,int		* n,	int nCount, wxString strEntry, LPCSTR strSection = NULL, int nDefault = 0);
	void		SerGet(	BOOL bGet,short		* n,	int nCount, wxString strEntry, LPCSTR strSection = NULL, int nDefault = 0);
	void		SerGet(	BOOL bGet,DWORD		* n,	int nCount, wxString strEntry, LPCSTR strSection = NULL, DWORD nDefault = 0);
	void		SerGet(	BOOL bGet,WORD		* n,	int nCount, wxString strEntry, LPCSTR strSection = NULL, DWORD nDefault = 0);
	void		SerGet(	BOOL bGet,wxPoint	* pt,	int nCount, wxString strEntry, LPCSTR strSection = NULL, wxPoint ptDefault = wxPoint(0,0));
	void		SerGet(	BOOL bGet,wxRect		* rect,	int nCount, wxString strEntry, LPCSTR strSection = NULL, wxRect rectDefault = wxRect(0,0,0,0));

	int			Parse(wxString &strIn, int nOffset, wxString &strOut);
   //MAKRO :
   //SERGET(bGet,value) SerGet(bGet,value,#value)

private:
	wxChar* GetLPCSTR(wxString strEntry,const wxChar* strSection,const wxChar* strDefault);
   BOOL  m_bModulPath;  //TRUE: Filenames without path take the Modulepath
                        //FALSE: Filenames without path take the CurrentDirectory

#define MAX_INI_BUFFER 256
	char	m_chBuffer[MAX_INI_BUFFER];
	wxString m_strFileName;
	wxString m_strSection;
//////////////////////////////////////////////////////////////////////
// statische Methoden
//////////////////////////////////////////////////////////////////////
public:
	static wxString	Read( wxString const& strFileName, wxString const& strSection, wxString const& strEntry, wxString const& strDefault);
	static void		Write(wxString const& strFileName, wxString const& strSection, wxString const& strEntry, wxString const& strValue);
};

#endif // !defined(AFX_INI_H__EEBAF800_182A_11D3_B51F_00104B4A13B4__INCLUDED_)
