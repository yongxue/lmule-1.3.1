//this file is part of eMule
//Copyright (C)2002 Merkur ( merkur-@users.sourceforge.net / http://www.emule-project.net )
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#ifndef CHATWND_H
#define CHATWND_H 1

//#include "afxdlgs.h"
#include "wintypes.h"
//#include "HyperTextCtrl.h"
//#include "ChatSelector.h"
//#include "ResizableLib/ResizableDialog.h"
// CChatWnd dialog

class CChatWnd : public wxPanel //CResizableDialog
{
  //DECLARE_DYNAMIC(CChatWnd)
public:
	CChatWnd(wxWindow* pParent = NULL);   // standard constructor
	virtual ~CChatWnd();

// Dialog Data
	enum { IDD = IDD_CHAT };
	void StartSession(CUpDownClient* client);
	void Localize();
	//CChatSelector chatselector;
protected:
#if 0
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support	
	DECLARE_MESSAGE_MAP()
	virtual BOOL OnInitDialog(); 
	afx_msg void OnBnClickedCsend();
	void	OnShowWindow(BOOL bShow,UINT nStatus);
	virtual BOOL	PreTranslateMessage(MSG* pMsg);
public:
	afx_msg void OnBnClickedCclose();
#endif
};
#endif
