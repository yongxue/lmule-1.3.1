# Process this file with autoconf to produce a configure script.
AC_INIT([lmule],[1.3.1],[furax@r00tworld.com])
AM_INIT_AUTOMAKE
AC_CONFIG_SRCDIR([src/SharedFileList.h])
AM_CONFIG_HEADER([config.h])

# Checks for programs.
# hopefully this prevents -O2
CXXFLAGS="-g"
CFLAGS="-g"
AC_PROG_CXX
AC_PROG_CC
AC_PROG_CPP

# Checks for libraries.
AC_CHECK_LIB([pthread], [pthread_attr_init])
AC_CHECK_LIB([expat], [XML_ParserCreate], , AC_MSG_ERROR([libexpat not found], 1))

AM_GNU_GETTEXT

AM_OPTIONS_WXCONFIG

AM_PATH_WXCONFIG(2.4.0, WXFOUND=1)

if test "$WXFOUND" != 1; then
    AC_MSG_ERROR([
        Please check that wx-config is in path, the directory
        where wxWindows libraries are installed (returned by
        'wx-config --libs' command) is in LD_LIBRARY_PATH or
        equivalent variable and wxWindows is version 2.4.0 or above.
	Or this might also be a bug in our configure. Please try again
	with --with-wx-config=/usr/bin/wx-config
	(replace /usr/bin/wx-config with a valid path to your wx-config)
    ])
fi

CHECK_WX_BUILT_WITH_GTK2

if test x$GTK_USEDVERSION == x2 ; then
  AM_PATH_GTK_2_0(2.0.3, havegtk2=yes, havegtk2=no)
  ## compiles fine against gtk2 here
  #AC_MSG_WARN([
  #       GTK2 is not currently supported. You will run into problems.
  #	 Press enter to continue anyway or ctrl+c to stop.
  #	 Please, switch to GTK1, it will work better.
  #])
  #read
  CFLAGS="$CFLAGS -D__GTK2__"
  CXXFLAGS="$CXXFLAGS -D__GTK2__"
else
  AM_PATH_GTK(1.2.0, havegtk=yes, havegtk=no)
fi

CHECK_ZLIB


# Add args to configure
AC_ARG_ENABLE(debug,   [  --enable-debug          Enable debugging information],
              USE_DEBUG="$enableval", USE_DEBUG="no")

AC_ARG_ENABLE(optimise,[  --enable-optimise       Enable code optimising],
		USE_OPTIMISE="$enableval", USE_OPTIMISE="no")

AC_ARG_ENABLE(profile, [  --enable-profile        Enable code profiling],
		USE_PROFILE="$enableval", USE_PROFILE="no")

AC_ARG_ENABLE(disablegtk, [ --disable-gtk           Disable pure GTK code in 	
		LMule], DISABLE_GTK="$enableval", DISABLE_GTK="no")

# debugging only
AC_MSG_CHECKING([whether progress bar drawing should be disabled.])
AC_ARG_ENABLE(memdbg, [  --enable-memdbg      Disable progress bar drawing],
		DISABLE_PROGRESS="$enableval", DISABLE_PROGRESS="no")
AC_MSG_RESULT($DISABLE_PROGRESS)
AC_SUBST(DISABLE_PROGRESS)


# Checks for header files.
AC_HEADER_STDC
AC_CHECK_HEADERS([arpa/inet.h fcntl.h locale.h netdb.h netinet/in.h stdlib.h sys/socket.h sys/time.h sys/timeb.h unistd.h])

# Checks for typedefs, structures, and compiler characteristics.
AC_C_CONST
AC_C_INLINE
AC_TYPE_OFF_T
AC_TYPE_SIZE_T
AC_HEADER_TIME
AC_STRUCT_TM

# Checks for library functions.
AC_FUNC_MALLOC
AC_FUNC_MEMCMP
AC_FUNC_MKTIME
AC_TYPE_SIGNAL
AC_FUNC_STAT
AC_FUNC_STRFTIME
AC_CHECK_FUNCS([floor ftruncate gettimeofday inet_ntoa memchr memmove memset mkdir putenv select setlocale sqrt strcasecmp strchr strdup strrchr strstr strtoul])



# Other tests

case "$USE_DEBUG" in
yes)	DEBUG_FLAGS="-g -Wall -D__DEBUG__"
    ;;    
*)	DEBUG_FLAGS=""
    ;;
esac

case "$USE_OPTIMISE" in
yes)	OPTIMISE_FLAGS="-O"
	;;
*)	OPTIMISE_FLAGS=""
	;;
esac

case "$USE_PROFILE" in
yes)	PROFILE_FLAGS="-pg"
	;;
*)	PROFILE_FLAGS=""
	;;
esac

case "$DISABLE_PROGRESS" in
yes)     AC_DEFINE_UNQUOTED(DISABLE_PROGRESS, 1,
      [Define if progress bar drawing should be disabled.])
  ;;
esac

case "$DISABLE_GTK" in
*)	GTK_DEFS="-D__NOGTK__"
	;;
no) GTK_DEFS=""
	;;
esac

AC_DEFINE_UNQUOTED(LOCALEDIR, "`eval echo $datadir/locale`", [Define where the locales are.])
# we have it anyway by now
EXPATLIBS="-lexpat"
LIBS="$LIBS $WX_LIBS $ZLIB_LIBS $EXPATLIBS"

XRCFLAGS="-Isrc"
CXXFLAGS="$GTK_DEFS $CXXFLAGS $DEBUG_FLAGS $OPTIMISE_FLAGS $PROFILE_FLAGS $WX_CXXFLAGS $GTK_CFLAGS $XRCFLAGS $ZLIB_CFLAGSx"
CFLAGS="$CFLAGS $CXXFLAGS $OPTIMISE_FLAGS $PROFILE_FLAGS"
LDFLAGS="$PROFILE_FLAGS $LDFLAGS"

AC_CONFIG_FILES([src/Makefile intl/Makefile po/Makefile.in m4/Makefile src/wx/Makefile src/wx/xrc/Makefile Makefile Compilation.flags])
AC_OUTPUT
`cp config.h src`
