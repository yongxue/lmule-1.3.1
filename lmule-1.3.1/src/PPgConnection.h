#ifndef PPGCON_H
#define PPGCON_H

#include "Preferences.h"

#include <wx/panel.h>
// CPPgConnection dialog

class CPPgConnection : public wxPanel //CPropertyPage
{
  //DECLARE_DYNAMIC(CPPgConnection)
  DECLARE_DYNAMIC_CLASS(CPPgConnection)
    CPPgConnection() {};

public:
	CPPgConnection(wxWindow* parent);
	virtual ~CPPgConnection();

	void SetPrefs(CPreferences* in_prefs) {	app_prefs = in_prefs; }

// Dialog Data
	enum { IDD = IDD_PPG_CONNECTION };
protected:
	CPreferences *app_prefs;
protected:
	//virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	//DECLARE_MESSAGE_MAP()
	DECLARE_EVENT_TABLE()
public:
	//virtual BOOL OnInitDialog();
public:
	void LoadSettings(void);
	void Localize(void);
public:
	void OnApply();
#if 0
	virtual BOOL OnApply();
	afx_msg void OnEnChangeDownloadCap()			{ SetModified(); }
	afx_msg void OnEnChangeUploadCap()				{ SetModified(); }
	afx_msg void OnEnChangeMaxdown()				{ SetModified(); }
	afx_msg void OnEnChangeMaxup()					{ SetModified(); }
	afx_msg void OnEnChangePort()					{ SetModified(); }
	afx_msg void OnEnChangeMaxcon()					{ SetModified(); }
	afx_msg void OnEnChangeMaxsourceperfile()		{ SetModified(); }
	afx_msg void OnEnChangeMaxsourceperfilesoft()	{ SetModified(); }
	afx_msg void OnEnChangeMaxsourceperfileudp()	{ SetModified(); }
	afx_msg void OnBnClickedAutoconnect()			{ SetModified(); }
	afx_msg void OnBnClickedReconn()				{ SetModified(); }
#endif
	void OnBnClickedWizard(wxEvent& evt);
};

#endif
