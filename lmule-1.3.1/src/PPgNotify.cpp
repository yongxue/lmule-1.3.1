//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//#include "stdafx.h"
#include "wintypes.h"
#include "emule.h"
#include "PPgNotify.h"
#include "wx/xrc/xmlres.h"
#include "wx/xrc/xh_all.h"

#include <wx/notebook.h>
#include <wx/checkbox.h>
#include <wx/filedlg.h>

#define GetDlgItem(x,clas) XRCCTRL(*this,#x,clas)
#define IsDlgButtonChecked(x) XRCCTRL(*this,#x,wxCheckBox)->GetValue()


//IMPLEMENT_DYNAMIC(CPPgNotify, CPropertyPage)
IMPLEMENT_DYNAMIC_CLASS(CPPgNotify,wxPanel)

CPPgNotify::CPPgNotify(wxWindow* parent)
  : wxPanel(parent,CPPgNotify::IDD)
{
  wxNotebook* book=(wxNotebook*)parent;

  wxPanel* page1=wxXmlResource::Get()->LoadPanel(this,"DLG_PPG_NOTIFY");
  book->AddPage(this,_("Notify"));

  // looks stupid? it is :)
  SetSize(page1->GetSize().GetWidth(),page1->GetSize().GetHeight()+48);
  page1->SetSize(GetSize());

  LoadSettings();
  Localize();
}

CPPgNotify::~CPPgNotify()
{
}

#if 0
void CPPgNotify::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
}
#endif

void CPPgNotify::LoadSettings(void)
{
#if 0
	CEdit* editPtr;
	CButton* btnPTR;

	if(m_hWnd)
	{                   
		if (app_prefs->prefs->useDownloadNotifier) 
				CheckDlgButton(IDC_CB_TBN_ONDOWNLOAD, BST_CHECKED);
		if (app_prefs->prefs->useChatNotifier)  
				CheckDlgButton(IDC_CB_TBN_ONCHAT, BST_CHECKED);
		if (app_prefs->prefs->useSoundInNotifier)
				CheckDlgButton(IDC_CB_TBN_USESOUND, BST_CHECKED);
		if (app_prefs->prefs->useLogNotifier)
				CheckDlgButton(IDC_CB_TBN_ONLOG, BST_CHECKED);
		if (app_prefs->prefs->notifierPopsEveryChatMsg)
				CheckDlgButton(IDC_CB_TBN_POP_ALWAYS, BST_CHECKED);

		btnPTR = (CButton*) GetDlgItem(IDC_CB_TBN_POP_ALWAYS);
		btnPTR->EnableWindow(IsDlgButtonChecked(IDC_CB_TBN_ONCHAT));
		editPtr = (CEdit*) GetDlgItem(IDC_EDIT_TBN_WAVFILE);
		editPtr->SetWindowText(LPCTSTR(app_prefs->prefs->notifierSoundFilePath));		
	}
#endif
}

wxString CPPgNotify::DialogBrowseFile(wxString Filters, wxString DefaultFileName) {
#if 0
        CFileDialog myFileDialog(true,NULL,LPCTSTR(DefaultFileName),
                OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, LPCTSTR(Filters));
        myFileDialog.DoModal();
        return myFileDialog.GetPathName();
#endif
	return "";
}

void CPPgNotify::Localize(void)
{
	if(1){
	  //SetWindowText(GetResString(IDS_PW_EKDEV_OPTIONS));
	  GetDlgItem(IDC_CB_TBN_USESOUND,wxControl)->SetLabel(GetResString(IDS_PW_TBN_USESOUND));
	  GetDlgItem(IDC_BTN_BROWSE_WAV,wxControl)->SetLabel(GetResString(IDS_PW_BROWSE));
	  GetDlgItem(IDC_CB_TBN_ONLOG,wxControl)->SetLabel(GetResString(IDS_PW_TBN_ONLOG));
	  GetDlgItem(IDC_CB_TBN_ONCHAT,wxControl)->SetLabel(GetResString(IDS_PW_TBN_ONCHAT));
	  GetDlgItem(IDC_CB_TBN_POP_ALWAYS,wxControl)->SetLabel(GetResString(IDS_PW_TBN_POP_ALWAYS));
	  GetDlgItem(IDC_CB_TBN_ONNEWVERSION,wxControl)->SetLabel(GetResString(IDS_CB_TBN_ONNEWVERSION));
	  GetDlgItem(IDC_CB_TBN_ONDOWNLOAD,wxControl)->SetLabel(GetResString(IDS_PW_TBN_ONDOWNLOAD));
	  GetDlgItem(IDC_TASKBARNOTIFIER,wxControl)->SetLabel(GetResString(IDS_PW_TASKBARNOTIFIER));
	  GetDlgItem(IDC_TBN_OPTIONS,wxControl)->SetLabel(GetResString(IDS_PW_TBN_OPTIONS));
	}
}

BEGIN_EVENT_TABLE(CPPgNotify,wxPanel)
  EVT_BUTTON(XRCID("IDC_BTN_BROWSE_WAV"),CPPgNotify::OnBnClickedBtnBrowseWav)
END_EVENT_TABLE()

#if 0
BEGIN_MESSAGE_MAP(CPPgNotify, CPropertyPage)
	ON_BN_CLICKED(IDC_CB_TBN_USESOUND, OnBnClickedCbTbnUsesound)
	ON_BN_CLICKED(IDC_CB_TBN_ONLOG, OnBnClickedCbTbnOnlog)
	ON_BN_CLICKED(IDC_CB_TBN_ONCHAT, OnBnClickedCbTbnOnchat)
	ON_BN_CLICKED(IDC_CB_TBN_POP_ALWAYS, OnBnClickedCbTbnPopAlways)
	ON_BN_CLICKED(IDC_CB_TBN_ONDOWNLOAD, OnBnClickedCbTbnOndownload)
	ON_BN_CLICKED(IDC_BTN_BROWSE_WAV, OnBnClickedBtnBrowseWav)
END_MESSAGE_MAP()


// gestori di messaggi CPPgNotify

BOOL CPPgNotify::OnInitDialog()
{
	CPropertyPage::OnInitDialog();

	LoadSettings();
	Localize();

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}
#endif

BOOL CPPgNotify::OnApply() {
    wxTextCtrl* editPTR;
    wxString buffer;
    app_prefs->prefs->useDownloadNotifier = IsDlgButtonChecked(IDC_CB_TBN_ONDOWNLOAD);
    app_prefs->prefs->useChatNotifier = IsDlgButtonChecked(IDC_CB_TBN_ONCHAT);
    app_prefs->prefs->useLogNotifier = IsDlgButtonChecked(IDC_CB_TBN_ONLOG);        
    app_prefs->prefs->useSoundInNotifier = IsDlgButtonChecked(IDC_CB_TBN_USESOUND);
    app_prefs->prefs->notifierPopsEveryChatMsg = IsDlgButtonChecked(IDC_CB_TBN_POP_ALWAYS);
    editPTR = GetDlgItem(IDC_EDIT_TBN_WAVFILE,wxTextCtrl);
    buffer=editPTR->GetValue();
    sprintf(app_prefs->prefs->notifierSoundFilePath,"%s",buffer.GetData());
    
	//SaveConfiguration();
	//SetModified(FALSE);
	//return CPropertyPage::OnApply();
    return TRUE;
}

#if 0
void CPPgNotify::OnBnClickedCbTbnOnchat()
{
    CButton* btnPTR;
    btnPTR = (CButton*) GetDlgItem(IDC_CB_TBN_POP_ALWAYS);
    btnPTR->EnableWindow(IsDlgButtonChecked(IDC_CB_TBN_ONCHAT));	
	SetModified();
}
#endif

void CPPgNotify::OnBnClickedBtnBrowseWav(wxEvent& evt)
{
    wxTextCtrl* editPTR;
    wxString buffer;
    buffer = //DialogBrowseFile("File wav (*.wav)|*.wav||");
      wxFileSelector(_("Browse wav"),"","","*.wav",_("File wav (*.wav)|*.wav||"));
    editPTR = (wxTextCtrl*) GetDlgItem(IDC_EDIT_TBN_WAVFILE,wxTextCtrl);
    editPTR->SetValue((buffer));
    sprintf(app_prefs->prefs->notifierSoundFilePath,"%s",buffer.GetData()); // Added by enkeyDEV
    //SetModified();
}

