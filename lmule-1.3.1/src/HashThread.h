//This file is part of eMule
//Copyright (C)2003 eMule Project ( http://www.emule-project.net )
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#ifndef HASHTHREAD_H
#define HASHTHREAD_H

#include <time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "emule.h"
#include "opcodes.h"
#include "types.h"
#include "SharedFileList.h"
#include <pthread.h>

#define SHAREDFILESTHREAD 2
#define PARTFILEHASHINGTHREAD 1
#define SHAREDANDPARTFILETHREADS 3 // not used at this time...


struct UnknownFileStruct;
class CFilePtrList;

typedef void* LPSECURITY_ATTRIBUTES;

class CFileHashThread
{
public:
	CFileHashThread(CFilePtrList* pList);
	CFileHashThread(CPartFile* pOwner, char* pszDir, char* pszName);
	virtual ~CFileHashThread();

	bool BeginThread(int nPriority = 0, UINT nStackSize = 0,LPSECURITY_ATTRIBUTES lpSecurityAttrs = NULL);
	
	DWORD					m_dwThreadId;
	DWORD					m_hThreadHandle;
	static volatile pthread_t	s_hSharedFilesThread;	
	static pthread_mutex_t	s_lockPartFileThread;

private:
	static void	HashThreadProcStub(LPVOID pObj);
	void					HashThreadProc(LPVOID pParam);
	CKnownFile*				CreateNewKnownFile(UnknownFileStruct* pFileToHash);

	CFilePtrList*			m_pFilesToHashList;
	CPartFile*				m_pPartFileOwner;
};

#endif
