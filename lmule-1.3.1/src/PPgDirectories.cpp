// PPgDirectories.cpp : implementation file
//

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif // HAVE_CONFIG_H

// Test if we have _GNU_SOURCE before the next step will mess up 
// setting __USE_GNU 
// (only needed for gcc-2.95 compatibility, gcc 3.2 always defines it)
#include "wx/setup.h"

// Mario Sergio Fujikawa Ferreira <lioux@FreeBSD.org>
// to detect if this is a *BSD system
#if defined(HAVE_SYS_PARAM_H)
#include <sys/param.h>
#endif

//#include "stdafx.h"
#include "wintypes.h"
#include "emule.h"
#include "PPgDirectories.h"

#include "DirectoryTreeCtrl.h"
#include <wx/checkbox.h>
#include <wx/notebook.h>
#include <wx/slider.h>
#include "wx/xrc/xmlres.h"
#include "wx/xrc/xh_all.h"

// *BSD compatibility
#if (defined(BSD) && (BSD >= 199103))
#define MAX_PATH MAXPATHLEN
#else
#define MAX_PATH 2048
#endif

#define GetDlgItem(x,clas) XRCCTRL(*this,#x,clas)
#define IsDlgButtonChecked(x) XRCCTRL(*this,#x,wxCheckBox)->GetValue()
#define CheckDlgButton(x,y) XRCCTRL(*this,#x,wxCheckBox)->SetValue(y)

// CPPgDirectories dialog

IMPLEMENT_DYNAMIC_CLASS(CPPgDirectories,wxPanel)

//IMPLEMENT_DYNAMIC(CPPgDirectories, CPropertyPage)
CPPgDirectories::CPPgDirectories(wxWindow* parent)
	: wxPanel(parent,CPPgDirectories::IDD)
{
  wxNotebook* book=(wxNotebook*)parent;

  wxPanel*page1=wxXmlResource::Get()->LoadPanel(this,"DLG_PPG_DIRECTORIES");
  book->AddPage(this,_("Directories"));

  SetSize(page1->GetSize().GetWidth(),page1->GetSize().GetHeight()+40);
	
  // then replace the existing tree control with our own
  wxTreeCtrl* oldtree=GetDlgItem(IDC_SHARESELECTOR,wxTreeCtrl);
  wxPoint pos=oldtree->GetPosition();
  wxSize siz=oldtree->GetSize();

  // then replace it with our own
  oldtree->Destroy();

  CDirectoryTreeCtrl* newtree=new CDirectoryTreeCtrl((wxWindow*)page1,XRCID("IDC_SHARESELECTOR"),
						     pos,siz,wxTR_CHECKBOX|wxSUNKEN_BORDER|wxTR_DEFAULT_STYLE);

  m_ShareSelector=newtree;
  Localize();
}

CPPgDirectories::~CPPgDirectories()
{
}

BEGIN_EVENT_TABLE(CPPgDirectories,wxPanel)
  EVT_BUTTON(XRCID("IDC_SELTEMPDIR"),CPPgDirectories::OnBnClickedSeltempdir)
  EVT_BUTTON(XRCID("IDC_SELINCDIR"),CPPgDirectories::OnBnClickedSelincdir)
  EVT_BUTTON(XRCID("IDC_BROWSEV"),CPPgDirectories::BrowseVideoplayer)
END_EVENT_TABLE()

#if 0
void CPPgDirectories::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_SHARESELECTOR, m_ShareSelector);
}


BEGIN_MESSAGE_MAP(CPPgDirectories, CPropertyPage)
	ON_BN_CLICKED(IDC_SELTEMPDIR, OnBnClickedSeltempdir)
	ON_BN_CLICKED(IDC_SELINCDIR, OnBnClickedSelincdir)
	ON_EN_CHANGE(IDC_INCFILES, OnSettingsChange)
	ON_EN_CHANGE(IDC_TEMPFILES, OnSettingsChange)
	ON_BN_CLICKED(IDC_CHECK1, OnSettingsChange)
	ON_EN_CHANGE(IDC_VIDEOPLAYER, OnSettingsChange)
	ON_BN_CLICKED(IDC_VIDEOBACKUP, OnSettingsChange)
	ON_BN_CLICKED(IDC_BROWSEV, BrowseVideoplayer)
END_MESSAGE_MAP()


// CPPgDirectories message handlers

BOOL CPPgDirectories::OnInitDialog()
{
	CPropertyPage::OnInitDialog();
	m_ShareSelector.Init();	

	((CEdit*)GetDlgItem(IDC_INCFILES))->SetLimitText(509);
	((CEdit*)GetDlgItem(IDC_TEMPFILES))->SetLimitText(509);
	
	LoadSettings();
	Localize();

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}
#endif

void CPPgDirectories::LoadSettings(void)
{
	GetDlgItem(IDC_INCFILES,wxTextCtrl)->SetValue(app_prefs->prefs->incomingdir);
	GetDlgItem(IDC_TEMPFILES,wxTextCtrl)->SetValue(app_prefs->prefs->tempdir);

	m_ShareSelector->SetSharedDirectories(&app_prefs->shareddir_list);

	GetDlgItem(IDC_VIDEOPLAYER,wxTextCtrl)->SetValue(app_prefs->prefs->VideoPlayer);
	if(app_prefs->prefs->moviePreviewBackup)
		CheckDlgButton(IDC_VIDEOBACKUP,1);
	else
		CheckDlgButton(IDC_VIDEOBACKUP,0);
}

#include <wx/dirdlg.h>
#include <wx/filedlg.h>

bool CPPgDirectories::SelectDir(char* outdir, char* titletext)
{
  wxString str=wxDirSelector(titletext,"");
  if(str.IsEmpty()) {
    return FALSE;
  }

  strcpy(outdir,str.GetData());
  return TRUE;
#if 0
	CoInitialize(0);
	bool done;
	char* buffer = new char[MAX_PATH];
	BROWSEINFO bi = { GetSafeHwnd(), 0, buffer, titletext, BIF_VALIDATE | BIF_NEWDIALOGSTYLE | BIF_RETURNONLYFSDIRS, 0, 0, 0};
	LPITEMIDLIST pidl = SHBrowseForFolder(&bi);
	delete[] buffer;
	done = SHGetPathFromIDList(pidl,outdir);
	LPMALLOC ppMalloc;
	if(SHGetMalloc(&ppMalloc) == NOERROR)
		ppMalloc->Free(pidl);
	CoUninitialize();
	return done;
#endif
}

void CPPgDirectories::OnBnClickedSelincdir(wxEvent& e)
{
	char buffer[MAX_PATH];
	if(SelectDir(buffer,(char*)(GetResString(IDS_SELECT_INCOMINGDIR).GetData())))
		GetDlgItem(IDC_INCFILES,wxTextCtrl)->SetValue(buffer);
}

void CPPgDirectories::OnBnClickedSeltempdir(wxEvent& e)
{
	char buffer[MAX_PATH];
	if(SelectDir(buffer,(char*)(GetResString(IDS_SELECT_TEMPDIR).GetData())))
		GetDlgItem(IDC_TEMPFILES,wxTextCtrl)->SetValue(buffer);
}

void CPPgDirectories::BrowseVideoplayer(wxEvent& e){
#if 0
	CFileDialog dlgFile(TRUE, "*.exe", NULL,OFN_FILEMUSTEXIST | OFN_NOREADONLYRETURN ,"Executable (*.exe)|*.exe||");
	if (dlgFile.DoModal()==IDOK) GetDlgItem(IDC_VIDEOPLAYER)->SetWindowText(dlgFile.GetPathName());
#endif
	wxString str=wxFileSelector(_("Browse for videoplayer"),"","","",_("Executable (*)|*||"));
	if(!str.IsEmpty()) {
	  GetDlgItem(IDC_VIDEOPLAYER,wxTextCtrl)->SetValue(str);
	}
}

BOOL CPPgDirectories::OnApply()
{
  wxString buffer;
	
	if(GetDlgItem(IDC_INCFILES,wxTextCtrl)->GetValue().Length())
	{
		buffer=GetDlgItem(IDC_INCFILES,wxTextCtrl)->GetValue();
		strcpy(app_prefs->prefs->incomingdir,buffer.GetData());
	}
	
	if(GetDlgItem(IDC_TEMPFILES,wxTextCtrl)->GetValue().Length())
	{
		buffer=GetDlgItem(IDC_TEMPFILES,wxTextCtrl)->GetValue();
		strcpy(app_prefs->prefs->tempdir,buffer.GetData());
	}

	buffer=GetDlgItem(IDC_VIDEOPLAYER,wxTextCtrl)->GetValue();
	strcpy(app_prefs->prefs->VideoPlayer ,buffer.GetData());

	app_prefs->prefs->moviePreviewBackup = IsDlgButtonChecked(IDC_VIDEOBACKUP);
	app_prefs->shareddir_list.Clear();
	m_ShareSelector->GetSharedDirectories(&app_prefs->shareddir_list);
	theApp.sharedfiles->Reload();
	
	//return CPropertyPage::OnApply();
	return TRUE;
}

#if 0
BOOL CPPgDirectories::OnCommand(WPARAM wParam, LPARAM lParam)
{
//	if(wParam == USRMSG_ITEMSTATECHANGED)
//		SetModified();	
	return CPropertyPage::OnCommand(wParam, lParam);
}
#endif

void CPPgDirectories::Localize(void)
{
	if(1)
	{
	  //SetWindowText(GetResString(IDS_PW_DIR));
	  
	  GetDlgItem(IDC_INCOMING_FRM,wxControl)->SetLabel(GetResString(IDS_PW_INCOMING));
	  GetDlgItem(IDC_TEMP_FRM,wxControl)->SetLabel(GetResString(IDS_PW_TEMP));
	  GetDlgItem(IDC_SELINCDIR,wxControl)->SetLabel(GetResString(IDS_PW_BROWSE));
	  GetDlgItem(IDC_SELTEMPDIR,wxControl)->SetLabel(GetResString(IDS_PW_BROWSE));
	  GetDlgItem(IDC_BROWSEV,wxControl)->SetLabel(GetResString(IDS_PW_BROWSE));
	  GetDlgItem(IDC_SHARED_FRM,wxControl)->SetLabel(GetResString(IDS_PW_SHARED));
	  
	  GetDlgItem(IDC_STATICVIDEOPLAYER,wxControl)->SetLabel(GetResString(IDS_PW_VIDEOPLAYER));
	  GetDlgItem(IDC_VIDEOBACKUP,wxControl)->SetLabel(GetResString(IDS_VIDEOBACKUP));		
	  GetDlgItem(IDC_STATIC_EMPTY,wxControl)->SetLabel(GetResString(IDS_STATIC_EMPTY));
	  //GetDlgItem(IDC_INCLUDESUBDIRS)->SetWindowText(GetResString(IDS_PW_DIR_INCSUBDIR));
	}
}
