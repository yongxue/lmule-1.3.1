//this file is part of eMule
//Copyright (C)2002 Merkur ( merkur-@users.sourceforge.net / http://www.emule-project.net )
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


#ifndef SEARCHDLG_H
#define SEARCHDLG_H

#include "SearchListCtrl.h"
//#include "afxcmn.h"
#include "packets.h"
#include "types.h"
//#include "afxwin.h"
//#include "IconStatic.h"

#include "wx/gauge.h"
#include "wx/tabctrl.h"
#include "wx/combobox.h"
#include "wx/panel.h"
#include "wx/timer.h"
#include "CMuleNotebookBase.h"

// CSearchDlg dialog
class CSearchDlg : public wxPanel //CResizableDialog
{
  //	DECLARE_DYNAMIC(CSearchDlg)
  DECLARE_DYNAMIC_CLASS(CSearchDlg)
    CSearchDlg() {};

public:
	CSearchDlg(wxWindow* pParent);   // standard constructor
	virtual ~CSearchDlg();
	
	void OnBnClickedStarts(wxEvent& evt);
	void OnTimer(wxTimerEvent &evt);
	void OnBnClickedCancels(wxEvent& evt);
	void OnRclick(wxListEvent& ev);
	void OnBnClickedSdownload(wxCommandEvent& ev);
	void OnBnClickedSearchReset(wxEvent& ev);
	void OnBnClickedClearall();
	/*
	afx_msg void OnBnClickedStarts();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnBnClickedCancels();
	afx_msg void OnBnClickedSdownload();
	afx_msg void OnBnClickedClearall();
	*/
	enum { IDD = IDD_SEARCH };

	void	Localize();
	void	DownloadSelected();
	void	LocalSearchEnd(uint16 count);
	void	AddUDPResult(uint16 count);
	void	DeleteSearch(uint16 nSearchID);
	void	DeleteAllSearchs();
	void CreateNewTab(wxString searchString,uint32 nSearchID);
	CSearchListCtrl* searchlistctrl;

protected:
	void StartNewSearch();
	wxString	CreateWebQuery();

	void OnSearchClosed(CMuleNotebookEvent& evt);
	void OnColumnClick(wxListEvent& evt);

	//virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//virtual BOOL OnInitDialog();
	//virtual BOOL PreTranslateMessage(MSG* pMsg);

	//afx_msg void OnNMDblclkSearchlist(NMHDR *pNMHDR, LRESULT *pResult);
	
	//DECLARE_MESSAGE_MAP()
	DECLARE_EVENT_TABLE()

private:
	Packet*		searchpacket;		
	UINT_PTR	global_search_timer;
	wxGauge* searchprogress;
	bool		canceld;
	uint16		servercount;
	bool		globsearch;
	uint16		m_nSearchID;
	wxComboBox	typebox;
	wxComboBox	Stypebox;
	//wxTabCtrl	searchselect;
	wxImageList	m_ImageList;
	wxImageList     m_StateImageList;
	wxTimer m_timer;
public:
	//afx_msg void OnTcnSelchangeTab1(NMHDR *pNMHDR, LRESULT *pResult);
private:
	wxStaticBitmap m_ctrlSearchFrm;
	wxStaticBitmap m_ctrlWebSearchFrm;
	wxStaticBitmap m_ctrlDirectDlFrm;
};

#endif
