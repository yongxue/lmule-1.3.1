#ifndef SERVERLISTCTRL_H
#define SERVERLISTCTRL_H

#include "ServerList.h"
#include "server.h"
// ei t�t
//#include "TitleMenu.h"
#include "MuleListCtrl.h"

class serverListItem : public wxListItem {
 public:
  serverListItem() {};
  ~serverListItem() {};

  CServer* data;
};

// CServerListCtrl
class CServerList; 
class CServerListCtrl : public CMuleListCtrl 
{
  //	DECLARE_DYNAMIC(CServerListCtrl)
public:
	CServerListCtrl();
	CServerListCtrl(wxWindow*& parent,int id,const wxPoint& pos,wxSize siz,int flags);

//	void	ShowServers();
	virtual ~CServerListCtrl();
	bool	Init(CServerList* in_list);
	void InitSort();
	bool	AddServer(CServer* toadd,bool bAddToList = true);
	void	RemoveServer(CServer* todel,bool bDelToList = true);
	bool	AddServermetToList(wxString strFile);
	void	RefreshServer(CServer* server);
	void	RemoveDeadServer();
	//void	Hide() {ShowWindow(SW_HIDE);}
	//void	Visable() {ShowWindow(SW_SHOW);}
	void	Localize();
	void	ShowFilesCount();

	void OnRclickServlist(wxListEvent& event);
	void OnLDclick(wxMouseEvent& event);
	void OnConnectTo(wxCommandEvent& event);
	void OnCopyLink(wxCommandEvent& event);

	DECLARE_EVENT_TABLE()

	  void OnRemove(wxEvent& evt);
	void OnLvnColumnclickServlist(wxListEvent& evt);
	static int SortProc(long item1,long item2,long sortData);
#if 0
	afx_msg void OnNMRclickServlist(NMHDR *pNMHDR, LRESULT *pResult);
protected:
	afx_msg	void	OnLvnColumnclickServlist(NMHDR *pNMHDR, LRESULT *pResult);
	static	int		CALLBACK SortProc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort);
#endif
	bool	asc_sort[9]; 
	bool ProcessEvent(wxEvent& evt);

	// Barry - New methods
	bool StaticServerFileAppend(CServer *server);
	bool StaticServerFileRemove(CServer *server);

	//virtual BOOL OnCommand(WPARAM wParam,LPARAM lParam );
	//DECLARE_MESSAGE_MAP()
private:
	CServerList*	server_list;
	wxImageList		imagelist;
	wxMenu* m_ServerPrioMenu;
	wxMenu* m_ServerMenu;
	//afx_msg void OnNMLdblclk (NMHDR *pNMHDR, LRESULT *pResult); //<-- mod bb 27.09.02
};

#endif
