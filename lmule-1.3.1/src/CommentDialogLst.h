#ifndef COMMENTDLGLIST_H

#include <wx/dialog.h>

// CCommentDialogLst dialog 

class CCommentDialogLst : public wxDialog
{ 
  //DECLARE_DYNAMIC(CCommentDialogLst) 

public: 
   CCommentDialogLst(wxWindow* pParent,CPartFile* file); 
   virtual ~CCommentDialogLst(); 
   void Localize(); 
   virtual BOOL OnInitDialog(); 
   //CHyperTextCtrl lstbox; 
   wxListCtrl* pmyListCtrl;

// Dialog Data 
   enum { IDD = IDD_COMMENTLST }; 
protected: 
   //virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support 
   //DECLARE_MESSAGE_MAP() 
   DECLARE_EVENT_TABLE()
public: 
   void OnBnClickedApply(wxEvent& evt); 
   void OnBnClickedRefresh(wxEvent& evt); 
private: 
   void CompleteList(); 
   CPartFile* m_file; 
};

#endif
